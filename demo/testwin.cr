require "../crystal/engine.cr"
require "./resources.cr"

def anim_frame(count)
  (Time.monotonic.total_milliseconds / 500) % count
end

include Engine

Engine[Params::Antialias] = 4
Engine[Params::VSync] = 1

rng_base = Random.new

LibEngine.init "resources"

s = "Test message русский"
log s
fs = 1
quits = false
tex3 = nil
tex4 = RES::Images::Units::Man.clone
loop do
  RES::Images::Reset.background(v2(2, 1), v2(0, 20))

  panel(0, 0, 250, 200, HAlign::Center, VAlign::Bottom) do
    case button(RES::Ui::Menu::Normalbutton, 0, 10, 200, 50, "Очень длинный текст", HAlign::Center, VAlign::Flow, RES::Consola)
    when .clicked?
      fs = 1
      s = "Test message русский"
    when .pressed?
      s = s + "?"
    end
    pix = RES::Images::Reset.get_pixel(Mouse.pos)
    button(RES::Ui::Menu::Circlebutton, 0, 10, 200, 50, pix.to_s(16), HAlign::Left, VAlign::Flow, RES::Consola)
    quits = true if button(RES::Ui::Menu::Cancel, 0, 50, 200, 50, halign: HAlign::Right, valign: VAlign::Flow).clicked?
  end

  Engine[Params::Autoscale] = 1 if Keys[Key::F1].pressed?
  Engine[Params::Autoscale] = 0 if Keys[Key::F2].pressed?

  RES::Sounds::Doorclose.play if Keys[Key::Tab].pressed?
  RES::Sounds::Knifeslice.play(nil) if Keys[Key::Q].pressed?
  RES::Sounds::Chop.music if Keys[Key::Z].pressed?
  NO_MUSIC.music if Keys[Key::X].pressed?

  Engine[Params::Fullscreen] = 1 if Keys[Key::W].pressed?
  Engine[Params::Fullscreen] = 0 if Keys[Key::E].pressed?

  break if Keys[Key::Escape].pressed?

  RES::Consola.draw_text(s, Mouse.pos + v2(10, 10))

  RES::Fonts::Main_blue.draw_text(s, Mouse.pos + v2(10, 50))

  if Mouse.scroll != 0
    p Engine[Params::Volume]
    Engine[Params::Volume] += Mouse.scroll.to_i
    s = Engine[Params::Volume].to_s
  end

  s = s + "!" if Mouse.left.clicked?
  Engine.layer = 2
  RES::Shaders::Simple.activate
  RES::Images::Earth.tex_triangle v2(100, 100), v2(0, 0), v2(200, 200), v2(200, 200), v2(100, 200), v2(0, 200)
  RES::Shaders::Mono.mono_color = vec4(1, 0, 0, 1)
  RES::Shaders::Mono.activate
  RES::Images::Earth.tex_triangle v2(300, 100), v2(0, 0), v2(200, 200), v2(200, 200), v2(100, 200), v2(0, 200)
  RES::Images::Earth.tex_triangle v2(900, 100), v2(0, 0), v2(200, 200), v2(200, 200), v2(100, 200), v2(0, 200)
  DEFAULT_SHADER.activate

  Engine.layer = 1

  camera(v2(100, 100), v2(1, 1.5), 15) if Mouse.right.down?

  rect(aabb(v2(1024 / 2, 768 / 2), v2(1024 - 100, 768 - 100)), false, color(0, 0, 0), color(0, 0, 255), color(0, 255, 0), color(255, 0, 0), 15)
  ellipse(v2(500, 500), v2(50, 100), true, color(0, 0, 0), color(255, 255, 255), 45)
  Engine.layer = 5

  RES::Images::Rooms_tiled.draw_frame(4, v2(700, 500))
  RES::Images::Units::Monster.draw_frame(anim_frame(5), v2(700, 500))
  Engine.layer = 1

  Engine.layer = 102
  RES::Images::Units::Man.draw v2(500, 650)
  tex4.draw v2(540, 650)
  Engine.layer = 1

  ellipse(v2(500, 500), v2(55, 105), false, color(0, 0, 0), color(255, 255, 255), -45)

  if Mouse.right.clicked?
    fs = fs * 1.1
    # RES::Sansation.config(char_size: 24, color: Color::WHITE, styles: Font::UNDERLINED, kx: fs, ky: 1.0 / fs)
  elsif Mouse.left.down?
    rng = Random.new(1)
    100.times { RES::Images::Coin.draw v2(rng.rand(1024), rng.rand(768)), angle: rng.rand(360) }
    3.times do
      triangle(v2(rng.rand(1000), rng.rand(768)), color(255, 0, 0),
        v2(rng.rand(1000), rng.rand(768)), color(0, 255, 0),
        v2(rng.rand(1000), rng.rand(768)), color(0, 0, 255))
    end
    100.times { line v2(rng.rand(1000), rng.rand(1000)), v2(rng.rand(768), rng.rand(768)), 0, color(255, 255, 255) }
    10000.times { point v2(rand(1000), rand(768)), color(255, 255, 0) }
  end

  if Keys[Key::F11].pressed?
    tex = Texture.from_screen(100, 100, 400, 400)
    tex2 = tex.clone
    tex3 = tex2.clone
    tex.free
    tex2.free
    tex3.save("test.png")
  end

  if tex3
    tex3.draw v2(600, 500)
  end

  process
  break if quits || !Keys[Key::Quit].up?
end
