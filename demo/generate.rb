def fine_name(s)
  return s if s == "RES"
  s = s.chars.select { |c| c =~ /[[:alnum:]_]/ }.join
  s = "id_" + s if s == "" || s[0] =~ /[[:digit:]]/
  s.capitalize
end

$used_names = {}
$out_file = nil
$res_ids = Hash.new(0)
$indent = 0
$need_space = false

def unique_name(s)
  if $used_names[s]
    n = 1
    while $used_names[s + "_" + n.to_s]
      n += 1
    end
    s = s + "_" + n.to_s
  end
  $used_names[s] = true
  s
end

GRAPH_EXTS = ["bmp", "dds", "jpg", "png", "tga", "psd"]
SOUND_EXTS = ["wav", "ogg", "flac"]
FONT_EXTS = ["ttf", "otf", "fnt"]
PARTICLE_EXT     = "particle"
EMITTER_EXT      = "emitter"
DEFLECTOR_EXT    = "deflector"
SPACE_EXT        = "space"

def print_res(res, item)
  item = unique_name(fine_name(item))
  print "#{item} = #{res}.new(#{$res_ids[res]})"
  $res_ids[res] = $res_ids[res] + 1
end

def detect_dir(adir, andprint)
  if m = /(.*)_font$/.match(adir)
    print_res "Font", m[1] if andprint
    return true
  end
  if m = /(.*)_button$/.match(adir)
    print_res "Button", m[1] if andprint
    return true
  end
  false
end

def detect_file(afile)
  m = /(.*)\.([^\.]*)$/.match(afile)
  name, ext = m[1], m[2]
  ext.downcase!
  if GRAPH_EXTS.include? ext
    if m = /(.*)_tiled_([[:digit:]]*)x([[:digit:]]*)$/.match(name)
      print_res "TileMap", m[1]
      return
    end

    print_res "Sprite", name
    return
  end
  if SOUND_EXTS.include? ext
    print_res "Sound", name
    return
  end
  if FONT_EXTS.include? ext
    print_res "Font", name
    return
  end
  if [PARTICLE_EXT, EMITTER_EXT, DEFLECTOR_EXT, SPACE_EXT].include? ext
    print_res ext.capitalize, name
    return true
  end
  print_res "RawResource", name
end

def print(s)
  if $need_space
    $out_file.puts ""
    $need_space = false
  end
  $out_file.puts "#{" " * $indent}#{s}"
end

def process_dir(short, adir)
  print "module #{fine_name(short)}"
  $indent += 2
  Dir.foreach(adir) do |item|
    next if item == "." || item == ".."
    process_dir item, adir + "/" + item if Dir.exist?(adir + "/" + item) && !detect_dir(item, false)
  end
  Dir.foreach(adir) do |item|
    next if item == "." || item == ".."
    if Dir.exist?(adir + "/" + item)
      detect_dir(item, true)
    else
      detect_file(item)
    end
  end
  $indent -= 2
  $need_space = false
  print "end"
  $need_space = true
end

path = ARGV[0] || "./resources"
$out_file = File.open("resources.rb", "wt")

print 'require_relative "../ruby/nonoengine"
include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)

'

process_dir "RES", path
$out_file.close
