include: common.hs
vertex: simple.vs
fragment: simple.fs
uniform: screen_size: vec2
uniform: tex: sampler2D
attribute: color: vec4
attribute: pos: vec3
attribute: texpos: vec3
