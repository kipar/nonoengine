in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
  if(texcoord.z < 0)
	result = pixel_color;
  else
	result = texture(tex, texcoord.xy)*pixel_color;
}
