uniform vec4 mono_color;
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
	result = mono_color;
}
