require_relative "../ruby/nonoengine"
require_relative "./resources"

def anim_frame(count)
  (Time.now.to_f * 2).to_i % count
end

include Engine

Engine[Params::Antialias] = 4
Engine[Params::VSync] = 1

rng_base = Random.new

init "resources"

s = "Test message русский"
fs = 1
loop do
  RES::Images::Reset.background(2, 1, 0, 20)

  panel(1, 0, 0, 0, 250, 200, Align::CENTER, Align::BOTTOM)

  case RES::Ui::Menu::Normalbutton.show(1, 0, 10, 200, 50, Align::CENTER, Align::FLOW, "Очень длинный текст", RES::Consola)
  when :clicked
    fs = 1
    s = "Test message русский"
  when :pressed
    s = s + "?"
  end
  #TODO getpixel broken
  #RES::Ui::Menu::Circlebutton.show(1, 0, 10, 200, 50, Align::LEFT, Align::FLOW, pixel_at(Mouse[Mouse::X], Mouse[Mouse::Y]), RES::Consola)
  RES::Ui::Menu::Circlebutton.show(1, 0, 10, 200, 50, Align::LEFT, Align::FLOW, -1.to_s(16), RES::Consola)
  break if RES::Ui::Menu::Cancel.show(1, 0, 50, 200, 50, Align::RIGHT, Align::FLOW) == :clicked

  Engine[Params::Autoscale] = 1 if Key[KEY_F1] == :pressed
  Engine[Params::Autoscale] = 0 if Key[KEY_F2] == :pressed

  RES::Sounds::Knifeslice.play if Key[KEY_Q] == :pressed
  RES::Sounds::Chop.music if Key[KEY_Z] == :pressed
  NO_MUSIC.music if Key[KEY_X] == :pressed

  Engine[Params::Fullscreen] = 1 if Key[KEY_W] == :pressed
  Engine[Params::Fullscreen] = 0 if Key[KEY_E] == :pressed

  break if Key[KEY_ESC] == :pressed

  RES::Consola.draw_text(s, Mouse[Mouse::X] + 10, Mouse[Mouse::Y] + 10)

  RES::Fonts::Main_blue.draw_text(s, Mouse[Mouse::X] + 10, Mouse[Mouse::Y] + 50)

  if Mouse[Mouse::SCROLL] != 0
    p Engine[Params::Volume]
    Engine[Params::Volume] += Mouse[Mouse::SCROLL].to_i
    s = Engine[Params::Volume].to_s
  end

  s = s + "!" if Mouse[Mouse::LEFT_BUTTON] == :clicked
  Engine.layer = 2

  RES::Images::Earth.tex_triangle 100, 100, 0, 0, 200, 200, 200, 200, 100, 200, 0, 200

  Engine.layer = 1

  camera(100, 100, 1, 1.5, 15) if Mouse[Mouse::RIGHT_BUTTON] == :down

  rect(1024 / 2, 768 / 2, 1024 - 100, 768 - 100, false, color(0, 0, 0), color(0, 0, 255), color(0, 255, 0), color(255, 0, 0), 15)
  ellipse(500, 500, 50, 100, true, color(0, 0, 0), color(255, 255, 255), 45)
  Engine.layer = 5

  RES::Images::Rooms_tiled.draw_frame(4, 700, 500)
  RES::Images::Units::Monster.draw_frame(anim_frame(5), 700, 500)
  Engine.layer = 1

  ellipse(500, 500, 55, 105, false, color(0, 0, 0), color(255, 255, 255), -45)

  if Mouse[Mouse::RIGHT_BUTTON] == :clicked
    fs = fs * 1.1
    RES::Sansation::config(font_size: 24, color: Colors::WHITE, styles: Font::UNDERLINED, kx: fs, ky: 1.0 / fs)
  elsif Mouse[Mouse::LEFT_BUTTON] == :down
    rng = rng_base.clone
    100.times { RES::Images::Coin.draw rng.rand(1024), rng.rand(768), 1, 1, rng.rand(360) }
    3.times do
      triangle(rng.rand(1000), rng.rand(768), color(255, 0, 0),
               rng.rand(1000), rng.rand(768), color(0, 255, 0),
               rng.rand(1000), rng.rand(768), color(0, 0, 255))
    end
    100.times { line rng.rand(1000), rng.rand(1000), rng.rand(768), rng.rand(768), 0, color(255, 255, 255) }
    10000.times { point rand(1000), rand(768), color(255, 255, 0) }
  end
  process
  break if Key[QUIT] != :up
end
