program demo;

uses
  SysUtils,
  uEngine,
  Resources;

var
  i, v1, v2: integer;
  s: string;
  fs: single;
  tex, tex2, tex3, tex4: TTexture;
  MonoColor: array[1..4] of Single;

  function AnimationFrame(Count: integer): integer;
  begin
    Result := (GetTickCount div 500) mod Count;
  end;

begin
  EngineSet(Antialias, 4);
  EngineSet(VSync, 1);
  EngineInit('./resources');
  s := 'Test message русский';
  fs := 1;
  tex3 := TTexture(0);
  tex4 := TextureClone(RES.Images.Units.Man);
  v1 := 123;
  v2 := 5;
  repeat
    Background(RES.Images.Reset, 2, 1, 0, 20);

    Panel(1, 0, 0, 0, 250, 200, haCenter, vaBottom);
    case Button(RES.Ui.Menu.Normalbutton, 1, 0, 10, 200, 50, haCenter,
        vaFlow, 'Очень длинный текст', ord(RES.Consola)) of
      bsClicked:
      begin
        fs := 1;
        s := 'Test message русский';
      end;
      bsPressed:
      begin
        s := s + '?';
      end;
    end;
    Button(RES.Ui.Menu.Circlebutton, 1, 0, 10, 200, 50, haLeft, vaFlow,
      PChar(IntToHex(GetPixel(MouseGet(CursorX), MouseGet(CursorY),
      RES.Images.Reset), 8)),
      ord(RES.Consola));

    if Button(RES.Ui.Menu.Cancel, 1, 0, 50, 200, 50, haRight, vaFlow) = bsClicked then
      Break;

    if KeyState(KeyF1) = ksPressed then
      EngineSet(Autoscale, 1);
    if KeyState(KeyF2) = ksPressed then
      EngineSet(Autoscale, 0);
    if KeyState(KeyQ) = ksPressed then
      Play(RES.Sounds.Knifeslice, 100);
    if KeyState(KeyTab) = ksPressed then
      Play(RES.Sounds.Doorclose, 100, pointer(-2));
    if KeyState(KeyZ) = ksPressed then
      Music(RES.Sounds.Chop);
    if KeyState(KeyX) = ksPressed then
      Music(TSound(-1));
    if KeyState(KeyW) = ksPressed then
      EngineSet(Fullscreen, 1);
    if KeyState(KeyE) = ksPressed then
      EngineSet(Fullscreen, 0);
    if KeyState(KeyEscape) = ksPressed then
      break;
    DrawText(RES.Consola, PChar(s), MouseGet(CursorX) + 10, MouseGet(CursorY) + 10);

    DrawText(RES.Fonts.Main_blue, PChar(s), MouseGet(CursorX) + 10,
      MouseGet(CursorY) + 50);

    if MouseGet(ScrollPos) <> 0 then
    begin
      EngineSet(Volume, EngineGet(Volume) + Trunc(MouseGet(ScrollPos)));
      s := IntToStr(EngineGet(Volume));
    end;
    if MouseState(LeftButton) = mbsClicked then
    begin
      s := s + '!';
      EmitterAdd(RES.Particles.Torch, MouseGet(CursorX), MouseGet(CursorY), Random*2*Pi);
    end;

    SetLayer(2);
    ShaderActivate(RES.Shaders.Simple);
    TexturedTriangle(RES.Images.Earth, 100, 100, 0, 0, 200, 200, 200,
      200, 100, 200, 0, 200);

    MonoColor[1] := 1;
    MonoColor[4] := 1;
    UniformSetPtr(RES.Shaders.Mono, uni_mono_color, @MonoColor);
    ShaderActivate(RES.Shaders.Mono);
    TexturedTriangle(RES.Images.Earth, 300, 100, 0, 0, 200, 200, 200,
      200, 100, 200, 0, 200);
    TexturedTriangle(RES.Images.Earth, 900, 100, 0, 0, 200, 200, 200,
      200, 100, 200, 0, 200);
    ShaderActivate(DEFAULT_SHADER);
    SetLayer(1);

    if MouseState(RightButton) = mbsDown then
      Camera(100, 100, 1, 1.5, 15);

    Rect(1024 / 2, 768 / 2, 1024 - 100, 768 - 100, True, $000000FF, $0000FFFF,
      $00FF00FF, $FF0000FF, 15);
    Ellipse(500, 500, 50, 100, True, $000000FF, $FFFFFFFF, 45);

    SetLayer(5);
    DrawTiled(RES.Images.Rooms_tiled, 4, 700, 500);
    DrawTiled(RES.Images.Units.Monster, AnimationFrame(5), 700, 500);
    SetLayer(1);

    SetLayer(102);
    Sprite(RES.Images.Units.Man, 500, 650);
    Sprite(tex4, 540, 650);
    SetLayer(1);



    Ellipse(500, 500, 55, 105, False, $000000FF, $FFFFFFFF, -45);
    if MouseState(RightButton) = mbsClicked then
    begin
      fs := fs * 1.1;
      FontConfig(RES.Sansation, 24, $FFFFFFFF, [Underlined], fs, 1 / fs);
    end
    else if MouseState(LeftButton) = mbsDown then
    begin
      RandSeed := 0;
      for i := 1 to 100 do
        Sprite(RES.Images.Coin, Random * 1024, Random * 768, 1, 1, Random * 360);
      for i := 1 to 3 do
        Triangle(Random * 1000, Random * 768, $FF0000FF, Random * 1000, Random * 768,
          $00FF00FF, Random * 1000, Random * 768, $0000FF00);
      for i := 1 to 100 do
        Line(Random * 1000, Random * 1000, Random * 768, Random * 768, 0, $FFFFFFFF);
      Randomize;
      for i := 1 to 10000 do
        Point(Random * 1000, Random * 768, $FFFF00FF);
    end;

    SetLayer(120);
    DrawText(RES.Consola, PChar(''), MouseGet(CursorX) + 10, MouseGet(CursorY) + 10);

    if KeyState(keyF11) = ksPressed then
    begin
      tex := TextureCreate(400, 400);
      CaptureScreen(100, 100, 400, 400, tex);
      tex2 := TextureClone(tex);
      tex3 := TextureClone(tex2);
      TextureDelete(tex);
      TextureDelete(tex2);
      TextureSave(tex3, './test.png');
    end;
    if Ord(tex3) > 0 then
    begin
      Sprite(tex3, 600, 500);
    end;

    v1 := InputInt(v1, -1000, 10000,  1, 300, 0, 150, 50, haNone, vaNone, Ord(RES.Consola));
    v2 := InputInt(v2, 0, 100,        1, 300, 100, 150, 50, haNone, vaNone, Ord(RES.Consola));

    EngineProcess;
  until KeyState(Quit) <> ksUp;
end.
