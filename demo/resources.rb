require_relative "../ruby/nonoengine"
include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)

module RES
  module Fonts
    Main_blue = Font.new(0)
    Mini_default = Font.new(1)
  end

  module Images
    module Final
      Game_over = Sprite.new(0)
      Win = Sprite.new(1)
    end

    module Units
      Man = Sprite.new(2)
      Monster_2 = TileMap.new(0)
      Monster_3 = TileMap.new(1)
      Monster = TileMap.new(2)
    end

    Coin = Sprite.new(3)
    Earth = Sprite.new(4)
    Food = Sprite.new(5)
    Reset = Sprite.new(6)
    Rooms_tiled = TileMap.new(3)
  end

  module Shaders
    Common = RawResource.new(0)
    Mono = RawResource.new(1)
    Mono_1 = RawResource.new(2)
    Simple = RawResource.new(3)
    Simple_1 = RawResource.new(4)
    Simple_2 = RawResource.new(5)
  end

  module Sounds
    Chop = Sound.new(0)
    Doorclose = Sound.new(1)
    Knifeslice = Sound.new(2)
  end

  module Ui
    module Final
      Cross = Sprite.new(7)
    end

    module Menu
      Cancel = Button.new(0)
      Circlebutton = Button.new(1)
      Frame = Sprite.new(8)
      Normalbutton = Button.new(2)
    end
  end

  Admirationpains = Font.new(2)
  Arial = Font.new(3)
  Consola = Font.new(4)
  Sansation = Font.new(5)
end
