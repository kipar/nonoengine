unit Resources;

interface
{$J-}
type
TRawResource = (TRawResource_NOT_USED);
TSprite = (THE_SCREEN = -1, res_Game_over, res_Win, res_Man, res_Coin, res_Earth, res_Food, res_Reset, res_Cross, res_Frame);
TSound = (NO_MUSIC = -1, res_Chop, res_Doorclose, res_Knifeslice);
TButton = (res_Cancel, res_Circlebutton, res_Normalbutton);
TTileMap = (res_Monster_2, res_Monster_3, res_Monster, res_Rooms_tiled);
TFont = (res_Main_blue, res_Mini_default, res_Admirationpains, res_Arial, res_Consola, res_Sansation);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, res_Mono, res_Simple);
TParticle = (res_Testpar);
TEmitter = (res_Torch);
TDeflector = (TDeflector_NOT_USED);
TParticleSpace = (TParticleSpace_NOT_USED);

TShaderAttribute = (attr_color, attr_pos, attr_texpos);
TShaderUniform = (uni_screen_size, uni_tex, uni_mono_color);


TFonts = record
  Main_blue: TFont;
  Mini_default: TFont;
end;

TFinal = record
  Game_over: TSprite;
  Win: TSprite;
end;

TUnits = record
  Man: TSprite;
  Monster_2: TTileMap;
  Monster_3: TTileMap;
  Monster: TTileMap;
end;

TImages = record
  Final: TFinal;
  Units: TUnits;
  Coin: TSprite;
  Earth: TSprite;
  Food: TSprite;
  Reset: TSprite;
  Rooms_tiled: TTileMap;
end;

TParticles = record
  Testpar: TParticle;
  Torch: TEmitter;
end;

TShaders = record
  Mono: TShader;
  Simple: TShader;
end;

TSounds = record
  Chop: TSound;
  Doorclose: TSound;
  Knifeslice: TSound;
end;

TFinal_1 = record
  Cross: TSprite;
end;

TMenu = record
  Cancel: TButton;
  Circlebutton: TButton;
  Frame: TSprite;
  Normalbutton: TButton;
end;

TUi = record
  Final_1: TFinal_1;
  Menu: TMenu;
end;

TRES = record
  Fonts: TFonts;
  Images: TImages;
  Particles: TParticles;
  Shaders: TShaders;
  Sounds: TSounds;
  Ui: TUi;
  Admirationpains: TFont;
  Arial: TFont;
  Consola: TFont;
  Sansation: TFont;
end;

const RES: TRES = (
  Fonts: (
    Main_blue: res_Main_blue;
    Mini_default: res_Mini_default;
  );
  Images: (
    Final: (
      Game_over: res_Game_over;
      Win: res_Win;
    );
    Units: (
      Man: res_Man;
      Monster_2: res_Monster_2;
      Monster_3: res_Monster_3;
      Monster: res_Monster;
    );
    Coin: res_Coin;
    Earth: res_Earth;
    Food: res_Food;
    Reset: res_Reset;
    Rooms_tiled: res_Rooms_tiled;
  );
  Particles: (
    Testpar: res_Testpar;
    Torch: res_Torch;
  );
  Shaders: (
    Mono: res_Mono;
    Simple: res_Simple;
  );
  Sounds: (
    Chop: res_Chop;
    Doorclose: res_Doorclose;
    Knifeslice: res_Knifeslice;
  );
  Ui: (
    Final_1: (
      Cross: res_Cross;
    );
    Menu: (
      Cancel: res_Cancel;
      Circlebutton: res_Circlebutton;
      Frame: res_Frame;
      Normalbutton: res_Normalbutton;
    );
  );
  Admirationpains: res_Admirationpains;
  Arial: res_Arial;
  Consola: res_Consola;
  Sansation: res_Sansation;
);

implementation
end.
