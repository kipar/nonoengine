include Engine

THE_SCREEN     = Sprite.new(-1)
NO_MUSIC       = Sound.new(-1)
DEFAULT_SHADER = DefaultShader.new(0)
ALL_SHADERS    = ShaderAllShaders.new(-1)

module RES
  module Fonts
    Main_blue    = FontResource.new(0)
    Mini_default = FontResource.new(1)
  end

  module Images
    module Final
      Game_over = Sprite.new(0)
      Win       = Sprite.new(1)
    end

    module Units
      Man       = Sprite.new(2)
      Monster_2 = TileMap.new(0)
      Monster_3 = TileMap.new(1)
      Monster   = TileMap.new(2)
    end

    Coin        = Sprite.new(3)
    Earth       = Sprite.new(4)
    Food        = Sprite.new(5)
    Reset       = Sprite.new(6)
    Rooms_tiled = TileMap.new(3)
  end

  module Shaders
    Mono   = ShaderMono.new(1)
    Simple = ShaderSimple.new(2)
  end

  module Sounds
    Chop       = Sound.new(0)
    Doorclose  = Sound.new(1)
    Knifeslice = Sound.new(2)
  end

  module Ui
    module Final
      Cross = Sprite.new(7)
    end

    module Menu
      Cancel       = ButtonResource.new(0)
      Circlebutton = ButtonResource.new(1)
      Frame        = Sprite.new(8)
      Normalbutton = ButtonResource.new(2)
    end
  end

  Admirationpains = FontResource.new(2)
  Arial           = FontResource.new(3)
  Consola         = FontResource.new(4)
  Sansation       = FontResource.new(5)
end

class ShaderMono < Shader
  uniform mono_color, vec4, 2
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
end

class ShaderSimple < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end

class ShaderAllShaders < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  uniform mono_color, vec4, 2
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end
