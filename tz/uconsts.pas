unit uConsts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Types;

const
  MAP_SIZEX = 16;
  MAP_SIZEY = 16;
  CELL_SIZE = 32;
  MAP_OFFSETX = 150;
  MAP_OFFSETY = 0;
  MAP_BORDER = 0.3;
  TOTAL_SCALE = 1.25;

  BASE_X = MAP_SIZEX div 2;
  BASE_Y = 7;
  BASE_HP = 10;

  WALK_ANIM = 200;
  HIT_ANIM = 250;

  SHOT_DELAY = 1000 / 5;
  SHOT_SPEED = 4;
  PLAYER_SPEED = 1.5;

  SPAWN_INCREASE = 30000;

  BULLET_RADIUS = 1 / 32;
  UNIT_RADIUSX = 6 / 32;
  UNIT_RADIUSY = 4 / 32;
  UNIT_DY = 4 / 32;

  TOWER_DELAY = 400;

var
  OBSTRACLES: array[1..MAP_SIZEY, 1..MAP_SIZEX] of boolean;

type
  TEnemyType = (Red, Green);
  TEnemyStat = (Speed, HP, BaseSpawn, DeltaSpawn);
  TWay = array[1..3] of TPointF;
  TActualWay = array of TPointF;

const
  ENEMY_STATS: array[TEnemyType, TEnemyStat] of single = (
    (1, 3, 5000, 1000),
    (1.5, 2, 7500, 1500)
    );

function PointF(x, y: single): TPointF;
operator / (const apt1: TPointF; afactor: single): TPointF;
function Arctan2(p: TPointF): single; overload;

implementation

uses Math;

function PointF(x, y: single): TPointF;
begin
  Result.x := x;
  Result.y := y;
end;

operator / (const apt1: TPointF; afactor: single): TPointF;
begin
  Result.x := apt1.x / afactor;
  Result.y := apt1.y / afactor;
end;

function Arctan2(p: TPointF): single; overload;
begin
  Result := Arctan2(p.y, p.x);
end;

end.
