program demo;

uses
  SysUtils,
  Math,
  fgl,
  uEngine,
  Resources,
  uConsts,
  Types;

type
  TGameState = (Initial, Playing, Paused, Gameover);

type

  { TGameActor }

  TGameActor = class
    Pos: TPointF;
    function Move(Delta: TPointF; Speed: single): boolean; virtual;
    function CanStep(Point: TPointF): boolean; virtual; abstract;
    procedure Render; virtual; abstract;
    procedure Process; virtual; abstract;
    function WalkFrame: integer;
  end;

  { TPlayer }

  TPlayer = class(TGameActor)
    recharge: single;
    walking: boolean;
    function Move(Delta: TPointF; Speed: single): boolean; override;
    function CanStep(Point: TPointF): boolean; override;
    procedure Render; override;
    procedure Process; override;
    constructor Create;
  end;

  { TSpawnPoint }

  TSpawnPoint = class
    Pos: TPointF;
    Color: TEnemyType;
    Timer, Delay: single;
    procedure Process;
    constructor Create;
    procedure Render;
  end;

  { TEnemy }

  TEnemy = class(TGameActor)
    Color: TEnemyType;
    Way: TActualWay;
    CurPoint: integer;
    dead: boolean;
    HitTimer: integer;
    hp: single;
    walk_offset: integer;
    function CanStep(Point: TPointF): boolean; override;
    procedure Render; override;
    procedure Process; override;
    procedure TakeHit;
    constructor Create(spawn: TSpawnPoint);
  end;

  { TBullet }

  TBullet = class(TGameActor)
    Initial, Delta: TPointF;
    dead: boolean;
    function CanStep(Point: TPointF): boolean; override;
    procedure Render; override;
    procedure Process; override;
    constructor Create(aPos, Target: TPointF);
  end;

  { TTower }

  TTower = class
    typ: integer;
    Pos: TPointF;
    ShotDelay: integer;
    Angle: single;
    Target: TEnemy;
    procedure Render;
    procedure Process;
    procedure FindTarget;
    constructor Create(apos: TPointF);
  end;

  { TMap }

  TMap = class
    Tiles: array[1..MAP_SIZEX, 1..MAP_SIZEY] of TSprite;
    constructor Create;
    procedure Render;
  end;

  TBulletsList = specialize TFPGObjectList<TBullet>;
  TEnemiesList = specialize TFPGObjectList<TEnemy>;
  TSpawnList = specialize TFPGObjectList<TSpawnPoint>;
  TTowersList = specialize TFPGObjectList<TTower>;

var
  GameState: TGameState = Initial;
  Quitting: boolean = False;
  Map: TMap;
  Player: TPlayer;
  Bullets: TBulletsList;
  Enemies: TEnemiesList;
  Spawns: TSpawnList;
  IncTimer: integer;
  FullScores, Scores, PlayerHP: integer;
  TowerCost: integer;
  GameTime: int64;
  Towers: TTowersList;
  CellUsed: array[1..MAP_SIZEX, 1..MAP_SIZEY] of boolean;

  procedure FreeGame;
  begin
    Player.Free;
    Bullets.Clear;
    Enemies.Clear;
    Spawns.Clear;
    Towers.Clear;
  end;


  procedure NewGame;
  var
    i: integer;
  begin
    FreeGame;
    GameState := Playing;
    Player := TPlayer.Create;
    IncTimer := SPAWN_INCREASE;
    Scores := 0;
    TowerCost := 10;
    FullScores := 0;
    GameTime := 0;
    PlayerHP := BASE_HP;
    Spawns.Clear;
    Map := TMap.Create;
    Fillchar(CellUsed, sizeof(CellUsed), 0);
    for i := 1 to 3 do
      Spawns.Add(TSpawnPoint.Create);
  end;

  procedure DoMenu;
  var
    s: string;
  begin
    FontConfig(res_Font, 36, BLACK, [Bold]);
    Panel(1, 0, 0, 0, 96 * 2, 120 * 2, haCenter, vaCenter);
    Button(RES.Gui.Panel, 1, 0, 0, 96 * 2, 120 * 2, haCenter, vaCenter);

    if (GameState = Paused) and (Button(RES.Gui.Stdbutton, 1, 0, 16,
      80 * 2, 32 * 2, haCenter, vaFlow, 'Продолжить') = bsClicked) then
      GameState := Playing;
    if GameState = Initial then
      s := 'Новая игра'
    else
      s := 'Заново';
    if Button(RES.Gui.Stdbutton, 1, 0, ifthen(GameState = Paused, 8, 16),
      80 * 2, 32 * 2, haCenter, vaFlow, PChar(s)) = bsClicked then
      NewGame;
    if Button(RES.Gui.Stdbutton, 1, 0, 8, 80 * 2, 32 * 2, haCenter, vaFlow, 'Выход') =
      bsClicked then
      Quitting := True;
  end;

  procedure DoGameoverMenu;
  begin
    FontConfig(res_Font, 36, WHITE, [bold]);
    Panel(1, 0, 0, 0, 96 * 2, 120 * 2, haCenter, vaCenter);
    Button(RES.Gui.Panel, 1, 0, 0, 96 * 2, 120 * 2, haCenter, vaCenter);
    Button(RES.Gui.Emptybutton, 1, 0, 20, 96 * 2, 30, haCenter, vaFlow,
      PChar('Очков: ' + IntToStr(FullScores)));

    if Button(RES.Gui.Restart, 1, 20, 20, 32 * 2, 32 * 2, haLeft, vaBottom) = bsClicked then
      NewGame;
    if Button(RES.Gui.Exit, 1, 20, 20, 32 * 2, 32 * 2, haRight, vaBottom) = bsClicked then
      Quitting := True;
  end;


  procedure DrawGame;
  var
    b: TBullet;
    e: TEnemy;
    tw: TTower;
    sp: TSpawnPoint;
  begin
    Camera(-MAP_OFFSETX, -MAP_OFFSETY, TOTAL_SCALE, TOTAL_SCALE);
    Map.Render;
    if GameState = Initial then
      exit;
    Player.Render;
    for sp in Spawns do
      sp.Render;
    for b in Bullets do
      b.Render;
    for e in Enemies do
      e.Render;
    SetLayer(97);
    for tw in Towers do
      tw.Render;
  end;

  procedure DrawStats;
  begin
    SetLayer(101);
    FontConfig(res_Font, 36, $A1CAD4FF, [bold]);
    DrawTextBoxed(RES.Font,
      PChar(Format('Scores: %d/%d'#13#10'HP: %d/%d',
      [Scores, TowerCost, PlayerHP, BASE_HP])), -20, -10, EngineGet(Width),
      EngineGet(Height), haRight, vaTop);
  end;

  procedure ProcessGame;
  var
    i: integer;
    pt: TSpawnPoint;
    e: TEnemy;
    tw: TTower;
    b: TBullet;
  begin
    GameTime := GameTime + EngineGet(DeltaTime);
    Player.Process;
    for b in Bullets do
      b.Process;
    for e in Enemies do
      e.Process;
    for tw in Towers do
      tw.Process;
    for i := Bullets.Count - 1 downto 0 do
      if Bullets[i].dead then
        Bullets.Delete(i);
    for i := Enemies.Count - 1 downto 0 do
      if Enemies[i].dead then
        Enemies.Delete(i);
    Dec(IncTimer, EngineGet(DeltaTime));
    if IncTimer < 0 then
    begin
      for pt in Spawns do
        if pt.Delay > ENEMY_STATS[pt.Color, DeltaSpawn] then
          pt.Delay := pt.Delay - ENEMY_STATS[pt.Color, DeltaSpawn];
      IncTimer := SPAWN_INCREASE;
      Spawns.Add(TSpawnPoint.Create);
    end;
    for pt in Spawns do
      pt.Process;
    if PlayerHP <= 0 then
    begin
      Play(RES.Sounds.Gameover);
      GameState := Gameover;
    end;
  end;

  { TTower }

  procedure TTower.Render;
  begin
    DrawTiled(RES.Towers, typ, pos.x * CELL_SIZE, Pos.y * CELL_SIZE, 0.25, 0.25);
    DrawTiled(RES.Towers, typ + 8, pos.x * CELL_SIZE, Pos.y * CELL_SIZE, 0.25,
      0.25, -Angle * 180 / pi);
  end;

  procedure TTower.Process;
  begin
    if (Target <> nil) and (target.dead) then
      Target := nil;
    if Target = nil then
      FindTarget;
    if Target <> nil then
    begin
      Angle := arctan2(Target.Pos - Pos);
      if ShotDelay <= 0 then
      begin
        TBullet.Create(Pos, Target.Pos);
        Play(RES.Sounds.Shot, 50);
        ShotDelay := TOWER_DELAY + random(10);
      end
      else
        Dec(ShotDelay, EngineGet(DeltaTime));
    end;
  end;

  procedure TTower.FindTarget;
  var
    e: TEnemy;
    dist: single;
  begin
    dist := MAP_SIZEX + MAP_SIZEY;
    for e in Enemies do
      if not e.dead and (e.Pos.Distance(Pos) < dist) then
      begin
        dist := e.Pos.Distance(Pos);
        Target := e;
      end;
  end;

  constructor TTower.Create(apos: TPointF);
  begin
    Pos := apos + Pointf(0, -0.5);
    typ := 2 + random(3);
    Towers.Add(self);
  end;

  { TSpawnPoint }

  procedure TSpawnPoint.Process;
  begin
    Timer := Timer - EngineGet(DeltaTime);
    if Timer < 0 then
    begin
      Timer := Delay;
      TEnemy.Create(self);
    end;
  end;

  constructor TSpawnPoint.Create;
  var
    tries: integer;
  begin
    tries := 0;
    repeat
      Inc(tries);
      case Random(4) of
        0: Pos := PointF(1, Random(MAP_SIZEY) + 1);
        1: Pos := PointF(MAP_SIZEX, Random(MAP_SIZEY) + 1);
        2: Pos := PointF(Random(MAP_SIZEX) + 1, 1);
        3: Pos := PointF(Random(MAP_SIZEY) + 1, MAP_SIZEY);
      end;
    until not CellUsed[Round(pos.x), Round(pos.y)] or (tries > 10000);
    CellUsed[Round(pos.x), Round(pos.y)] := True;
    if random < 0.33 then
      Color := Green
    else
      Color := Red;
    Delay := ENEMY_STATS[Color, BaseSpawn];
    Timer := 0;
  end;

  procedure TSpawnPoint.Render;
  var
    pic: TSprite;
  begin
    SetLayer(Trunc(pos.y) * 3 + 2);
    if Color = green then
      pic := RES.Level.Floor_greenspawn
    else
      pic := RES.Level.Floor_redspawn;
    Sprite(pic, (pos.x + 0.5) * CELL_SIZE, (pos.y + 0.5) * CELL_SIZE);
  end;

  { TEnemy }

  function TEnemy.CanStep(Point: TPointF): boolean;
  begin
    Result := not (InRange(Point.x, BASE_X, BASE_X + 2) and
      InRange(Point.y, BASE_Y, BASE_Y + 2));
  end;

  procedure TEnemy.Render;
  var
    frame: integer;
  begin
    SetLayer(Trunc(pos.y) * 3 + 2);
    frame := 4 * (Ord(Color) + 1);
    if HitTimer > 0 then
      Inc(frame, 3)
    else
      Inc(frame, (WalkFrame + walk_offset) mod 3);
    DrawTiled(RES.Units, frame, pos.x * CELL_SIZE, pos.y * CELL_SIZE);
         {
  SetLayer(80);
  //LineSettings(5);
  for frame := 0 to Length(Way)-2 do
    Line(Way[frame].x*CELL_SIZE+CELL_SIZE/2, Way[frame].y*CELL_SIZE+CELL_SIZE/2, Way[frame+1].x*CELL_SIZE+CELL_SIZE/2, Way[frame+1].y*CELL_SIZE+CELL_SIZE/2, BLUE, BLUE);
          }
  end;

  procedure TEnemy.Process;
  var
    delta: TPointF;
  begin
    if dead then
      exit;
    Dec(HitTimer, EngineGet(DeltaTime));
    delta := Way[CurPoint] + PointF(0.5, 0.5) - Pos;
    if not Move(delta, ENEMY_STATS[Color, Speed]) then
    begin
      dead := True;
      Dec(PlayerHP);
    end
    else if delta.Length < 0.1 then
      Inc(CurPoint);
  end;

  procedure TEnemy.TakeHit;
  begin
    hp := hp - 1;
    HitTimer := HIT_ANIM;
    if hp <= 0 then
    begin
      dead := True;
      Inc(Scores);
      Inc(FullScores);
      if Color = Red then
        play(RES.Sounds.Red_death)
      else
        play(RES.Sounds.Green_death);
    end
    else
      play(RES.Sounds.Hit);
  end;


  function TheCallback(fromx, fromy, tox, toy: integer; opaque: pointer): single;
  begin
    if (not InRange(tox, 1, MAP_SIZEX)) or (not InRange(toy, 1, MAP_SIZEY)) or
      OBSTRACLES[toy, tox] then
      Result := 0
    else
      Result := 10 + random(150);
  end;

  constructor TEnemy.Create(spawn: TSpawnPoint);
  var
    ax, ay: integer;
  begin
    walk_offset := random(3);
    Color := spawn.Color;
    hp := ENEMY_STATS[Color, TEnemyStat.HP];
    Pos := spawn.Pos + PointF(0.5, 0.5);
    SetLength(Way, 0);
    ax := Trunc(spawn.Pos.x);
    ay := Trunc(spawn.Pos.y);
    repeat
      SetLength(Way, Length(Way) + 1);
      Way[Length(Way) - 1] := PointF(ax, ay);
    until not Pathfind(MAP_SIZEX + 1, MAP_SIZEY + 1, AStarReuse, -1, ax, ay,
        BASE_X, BASE_Y, ax, ay, @TheCallback, self);
    CurPoint := 1;
    Enemies.Add(Self);
  end;

  { TBullet }

  function TBullet.CanStep(Point: TPointF): boolean;
  begin
    Result :=
      InRange(Point.x, 1 + MAP_BORDER, MAP_SIZEX + 1 - MAP_BORDER) and
      InRange(Point.y, 1 + MAP_BORDER, MAP_SIZEY + 1 - MAP_BORDER) and
      ((Pos.Distance(Initial) < 1) or not OBSTRACLES[Trunc(Point.y), Trunc(Point.x)]);
  end;

  procedure TBullet.Render;
  begin
    SetLayer(Trunc(Pos.y) * 3 + 2);
    Sprite(RES.Projectile, Pos.x * CELL_SIZE, Pos.y * CELL_SIZE);
  end;

  procedure TBullet.Process;
  var
    e: TEnemy;
  begin
    if not Move(Delta, SHOT_SPEED) then
    begin
      dead := True;
    end;
    for e in Enemies do
    begin
      if (Pos - e.Pos).Length < BULLET_RADIUS + UNIT_RADIUSX then
      begin
        e.TakeHit;
        dead := True;
        break;
      end;
    end;
  end;

  constructor TBullet.Create(aPos, Target: TPointF);
  begin
    Pos := aPos;
    Initial := aPos;
    Delta := Target - Pos;
    Bullets.Add(Self);
  end;

  { TPlayer }

  function TPlayer.Move(Delta: TPointF; Speed: single): boolean;
  begin
    Result := inherited Move(Delta, Speed);
    if not Result then
    begin //try sliding
      if (Delta.x <> 0) then
        Result := inherited Move(PointF(0, Delta.y), Speed);
      if not Result and (Delta.y <> 0) then
        Result := inherited Move(PointF(Delta.x, 0), Speed);
    end;
  end;

  function TPlayer.CanStep(Point: TPointF): boolean;
  var
    corners: array[1..4] of TPointF;
    i: integer;
  begin
    Result := InRange(Point.x, 1 + MAP_BORDER, MAP_SIZEX + 1 - MAP_BORDER) and
      InRange(Point.y, 1 + MAP_BORDER, MAP_SIZEY + 1 - MAP_BORDER);
    if not Result then
      exit;
    //now check obstracles with corners
    corners[1] := PointF(-UNIT_RADIUSX, -UNIT_RADIUSY);
    corners[2] := PointF(-UNIT_RADIUSX, UNIT_RADIUSY);
    corners[3] := PointF(UNIT_RADIUSX, -UNIT_RADIUSY);
    corners[4] := PointF(UNIT_RADIUSX, UNIT_RADIUSY);
    Result := False;
    for i := 1 to 4 do
    begin
      corners[i] := corners[i] + Point + PointF(0, UNIT_DY);
      if OBSTRACLES[Trunc(corners[i].y), Trunc(corners[i].x)] then
        exit;
    end;
    Result := True;
  end;

  procedure TPlayer.Render;
  begin
    SetLayer(Trunc(Pos.y) * 3 + 2);
    DrawTiled(RES.Units, ifthen(walking, WalkFrame, 0), Pos.x * CELL_SIZE, Pos.y * CELL_SIZE);
  end;

  function GetMouse: TPointF;
  begin
    Result := Pointf(MouseGet(CursorX), MouseGet(CursorY)) / TOTAL_SCALE -
      PointF(MAP_OFFSETX, MAP_OFFSETY);
  end;

  procedure TPlayer.Process;
  var
    delta: TPointF;
    loc: TPointF;
  begin
    recharge := recharge - EngineGet(DeltaTime);
    delta := PointF(0, 0);
    if (KeyState(KeyUp) <> ksUp) or (KeyState(KeyW) <> ksUp) then
      delta.y := delta.y - 1;
    if (KeyState(KeyDown) <> ksUp) or (KeyState(KeyS) <> ksUp) then
      delta.y := delta.y + 1;
    if (KeyState(KeyLeft) <> ksUp) or (KeyState(KeyA) <> ksUp) then
      delta.x := delta.x - 1;
    if (KeyState(KeyRight) <> ksUp) or (KeyState(KeyD) <> ksUp) then
      delta.x := delta.x + 1;
    walking := not delta.IsZero;
    Move(delta, PLAYER_SPEED);
    if MouseState(LeftButton) <> mbsUp then
    begin
      if recharge > 0 then
        exit;
      Play(RES.Sounds.Shot);
      recharge := SHOT_DELAY;
      TBullet.Create(Pos, GetMouse / CELL_SIZE);
    end;

    if MouseState(RightButton) = mbsClicked then
    begin
      if Scores < TowerCost then
        exit;
      loc := GetMouse / CELL_SIZE + PointF(0, 0.5);
      if OBSTRACLES[Trunc(loc.y), Trunc(loc.x)] and not
        CellUsed[Trunc(loc.x), Trunc(loc.y)] then
      begin
        CellUsed[Trunc(loc.x), Trunc(loc.y)] := True;
        Dec(Scores, TowerCost);
        Inc(TowerCost, 5);
        TTower.Create(PointF(Trunc(loc.x) + 0.5, Trunc(loc.y) + 0.5));
      end;
    end;
  end;

  constructor TPlayer.Create;
  begin
    Pos := PointF(BASE_X + 1, BASE_Y + 1);
  end;

  { TGameActor }

  function TGameActor.Move(Delta: TPointF; Speed: single): boolean;
  var
    apos: TPointF;
  begin
    Result := False;
    if Delta.IsZero then
      exit;
    apos := pos + Delta / Delta.Length * Speed * EngineGet(DeltaTime) / 1000;
    Result := CanStep(apos);
    if Result then
      pos := apos;
  end;

  function TGameActor.WalkFrame: integer;
  begin
    Result := (GameTime div WALK_ANIM) mod 3;
  end;

  { TMap }

  constructor TMap.Create;
  var
    x, y: integer;
  begin
    for x := 2 to MAP_SIZEX - 1 {div 2} do
      for y := 2 to MAP_SIZEY - 1 do
      begin
        if random < 0.3 then
          OBSTRACLES[y, x] := True
        else
          OBSTRACLES[y, x] := False;
        //      OBSTRACLES[ay, MAP_SIZEX-ax+1] := OBSTRACLES[ay, ax];
      end;
    OBSTRACLES[BASE_Y, BASE_X] := False;
    OBSTRACLES[BASE_Y + 1, BASE_X] := False;
    OBSTRACLES[BASE_Y, BASE_X + 1] := False;
    OBSTRACLES[BASE_Y + 1, BASE_X + 1] := False;

    for x := 2 to MAP_SIZEX - 1 do
      for y := 2 to MAP_SIZEY - 1 do
        if OBSTRACLES[y, x] then
          Tiles[x, y] := RES.Level.Box
        else
          Tiles[x, y] := RES.Level.Floor;
    for y := 2 to MAP_SIZEY - 1 do
    begin
      Tiles[1, y] := RES.Level.Wall_l;
      Tiles[MAP_SIZEX, y] := RES.Level.Wall_r;
    end;
    for x := 2 to MAP_SIZEX - 1 do
    begin
      Tiles[x, 1] := RES.Level.Wall_u;
      Tiles[x, MAP_SIZEY] := RES.Level.Wall_b;
    end;
    Tiles[1, 1] := RES.Level.Wall_lu;
    Tiles[MAP_SIZEX, 1] := RES.Level.Wall_ru;
    Tiles[MAP_SIZEX, MAP_SIZEY] := RES.Level.Wall_rb;
    Tiles[1, MAP_SIZEY] := RES.Level.Wall_lb;

    Tiles[BASE_X, BASE_Y] := RES.Level.Wall_lu;
    Tiles[BASE_X + 1, BASE_Y] := RES.Level.Wall_ru;
    Tiles[BASE_X + 1, BASE_Y + 1] := RES.Level.Wall_rb;
    Tiles[BASE_X, BASE_Y + 1] := RES.Level.Wall_lb;

  end;

  procedure TMap.Render;
  var
    x, y: integer;
  begin
    for x := 1 to MAP_SIZEX do
      for y := 1 to MAP_SIZEY do
      begin
        if Tiles[x, y] = RES.Level.Box then
        begin
          SetLayer(y * 3 + 3);
          Sprite(Tiles[x, y], (x - 1) * CELL_SIZE + 3 * CELL_SIZE / 2, (y - 1) * CELL_SIZE +
            3 * CELL_SIZE / 2 - CELL_SIZE * 0.25);
        end
        else
        begin
          SetLayer(0);
          Sprite(Tiles[x, y], (x - 1) * CELL_SIZE + 3 * CELL_SIZE / 2, (y - 1) * CELL_SIZE +
            3 * CELL_SIZE / 2);
        end;
      end;
  end;


begin
  EngineSet(Width, Trunc(TOTAL_SCALE * (CELL_SIZE * MAP_SIZEX + 2 * MAP_OFFSETX + 2 * CELL_SIZE)));
  EngineSet(Height, Trunc(TOTAL_SCALE * (CELL_SIZE * MAP_SIZEY + MAP_OFFSETY + 2 * CELL_SIZE)));
  EngineSet(ClearColor, $4A4F9BFF);
  Randomize;
  EngineInit('./resources');
  Music(RES.Sounds.Music);
  Map := TMap.Create;
  Bullets := TBulletsList.Create;
  Enemies := TEnemiesList.Create;
  Spawns := TSpawnList.Create;
  Towers := TTowersList.Create;
  repeat
    DrawGame;
    DrawStats;
    if GameState = Playing then
    begin
      ProcessGame;
      if KeyState(KeyEscape) <> ksUp then
        GameState := Paused;
    end
    else if GameState = Gameover then
      DoGameoverMenu
    else
      DoMenu;
    EngineProcess;
  until (KeyState(Quit) <> ksUp) or Quitting;
end.
