unit Resources;

interface
{$J-}
type
TRawResource = (TRawResource_NOT_USED);
TSprite = (THE_SCREEN = -1, res_Box, res_Floor, res_Floor_greenspawn, res_Floor_redspawn, res_Wall_b, res_Wall_l, res_Wall_lb, res_Wall_lu, res_Wall_r, res_Wall_rb, res_Wall_ru, res_Wall_u, res_Projectile);
TSound = (NO_MUSIC = -1, res_Gameover, res_Green_death, res_Hit, res_Music, res_Red_death, res_Shot);
TButton = (res_Emptybutton, res_Exit, res_Panel, res_Restart, res_Stdbutton);
TTileMap = (res_Towers, res_Units);
TFont = (res_Font);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, TShader_NOT_USED);

TShaderAttribute = (attr_color, attr_pos, attr_texpos);
TShaderUniform = (uni_screen_size, uni_tex);


TGui = record
  Emptybutton: TButton;
  Exit: TButton;
  Panel: TButton;
  Restart: TButton;
  Stdbutton: TButton;
end;

TLevel = record
  Box: TSprite;
  Floor: TSprite;
  Floor_greenspawn: TSprite;
  Floor_redspawn: TSprite;
  Wall_b: TSprite;
  Wall_l: TSprite;
  Wall_lb: TSprite;
  Wall_lu: TSprite;
  Wall_r: TSprite;
  Wall_rb: TSprite;
  Wall_ru: TSprite;
  Wall_u: TSprite;
end;

TSounds = record
  Gameover: TSound;
  Green_death: TSound;
  Hit: TSound;
  Music: TSound;
  Red_death: TSound;
  Shot: TSound;
end;

TRES = record
  Gui: TGui;
  Level: TLevel;
  Sounds: TSounds;
  Font: TFont;
  Projectile: TSprite;
  Towers: TTileMap;
  Units: TTileMap;
end;

const RES: TRES = (
  Gui: (
    Emptybutton: res_Emptybutton;
    Exit: res_Exit;
    Panel: res_Panel;
    Restart: res_Restart;
    Stdbutton: res_Stdbutton;
  );
  Level: (
    Box: res_Box;
    Floor: res_Floor;
    Floor_greenspawn: res_Floor_greenspawn;
    Floor_redspawn: res_Floor_redspawn;
    Wall_b: res_Wall_b;
    Wall_l: res_Wall_l;
    Wall_lb: res_Wall_lb;
    Wall_lu: res_Wall_lu;
    Wall_r: res_Wall_r;
    Wall_rb: res_Wall_rb;
    Wall_ru: res_Wall_ru;
    Wall_u: res_Wall_u;
  );
  Sounds: (
    Gameover: res_Gameover;
    Green_death: res_Green_death;
    Hit: res_Hit;
    Music: res_Music;
    Red_death: res_Red_death;
    Shot: res_Shot;
  );
  Font: res_Font;
  Projectile: res_Projectile;
  Towers: res_Towers;
  Units: res_Units;
);

implementation
end.
