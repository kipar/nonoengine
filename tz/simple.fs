#version 130
uniform sampler2D tex;
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
  result = texture(tex, texcoord.xy)*pixel_color;
}
