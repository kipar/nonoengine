unit uRenderFailed;

{$mode objfpc}{$H+}

interface

uses
  uTestFramework, Classes, SysUtils, uEngine;

type

  { TRenderFailed }

  TRenderFailed = class
    CurTest: integer;
    constructor Create;
    procedure Render;
    procedure ShowSummary;
  end;

implementation

uses Resources;

{ TRenderFailed }

constructor TRenderFailed.Create;
begin
  EngineSet(Autoscale, 0);
  EngineSet(Width, 1200);
  //EngineSet(Height, 600);
end;

procedure TRenderFailed.Render;
begin
  if CurTest > 0 then
  begin
    Sprite(ReferenceScreens[CurTest-1], 150,300, 0.5,1);
    Sprite(FailedScreens[CurTest-1], 300+150,300, 0.5,1);
    SetLayer(101);
    DrawText(RES.Consola, PChar(FailedTests[CurTest-1]), 10, 10);
  end
  else
    ShowSummary;

  if MouseState(LeftButton) = mbsClicked then
    CurTest := (CurTest + 1) mod (FailedClasses.Count+1);
end;

procedure TRenderFailed.ShowSummary;
var
  i: integer;
begin
  for i := 0 to FailedTests.Count - 1 do
    DrawText(RES.Consola, PChar(FailedTests[i]), 10, i * 50 + 10);
end;

end.

