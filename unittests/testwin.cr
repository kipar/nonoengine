require "../crystal/engine.cr"
require "./resources.cr"

include Engine

# class VertexTest < VertexList
#   vertex(color, padding(name, String), pos)
# end

Engine[Params::Antialias] = 4
Engine[Params::VSync] = 1

LibEngine.init "resources"

loop do
  RES::Images::Landscape_day.background

  RES::Shaders::Mono_int.mono_int = 100
  RES::Shaders::Mono_int.activate
  rect(aabb(v2(10, 10), v2(100, 100)), true, Color::WHITE)
  RES::Shaders::Mono_int.mono_int = 200
  rect(aabb(v2(310, 10), v2(100, 100)), true, Color::WHITE)

  RES::Shaders::Mono_float.mono_float = (100/255).to_f32
  RES::Shaders::Mono_float.activate
  rect(aabb(v2(10, 210), v2(100, 100)), true, Color::WHITE)
  RES::Shaders::Mono_float.mono_float = (200/255).to_f32
  rect(aabb(v2(310, 210), v2(100, 100)), true, Color::WHITE)

  RES::Shaders::Mono_array.mono_array[0] = 0.65
  RES::Shaders::Mono_array.mono_array[1] = 0.79
  RES::Shaders::Mono_array.mono_array[2] = 0.94
  RES::Shaders::Mono_array.mono_array[3] = 1
  RES::Shaders::Mono_array.activate
  rect(aabb(v2(10, 510), v2(100, 100)), true, Color::WHITE)
  RES::Shaders::Mono_array.mono_array.unsafe_set(0, 1)
  RES::Shaders::Mono_array.mono_array.unsafe_set(1, 0.98)
  RES::Shaders::Mono_array.mono_array.unsafe_set(2, 0.94)
  RES::Shaders::Mono_array.mono_array.unsafe_set(3, 1)
  RES::Shaders::Mono_array.mono_array.apply

  rect(aabb(v2(310, 510), v2(100, 100)), true, Color::WHITE)

  RES::Shaders::Mono_sampler.mono_tex = RES::Images::Coin
  RES::Shaders::Mono_sampler.activate
  RES::Images::Landscape_day.draw v2(200, 410)
  RES::Shaders::Mono_sampler.mono_tex = RES::Images::Landscape_day
  RES::Images::Coin.draw v2(600, 410)

  process
  break if !Keys[Key::Quit].up?
  break if !Keys[Key::Escape].up?
end
