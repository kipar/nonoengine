include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)
DEFAULT_SHADER = DefaultShader.new(0)
ALL_SHADERS = ShaderAllShaders.new(-1)
    
module RES
  module Fonts
    Main_blue = FontResource.new(0)
    Mini_default = FontResource.new(1)
  end

  module Images
    module Empty_folder
    end

    module Units
      Monster = TileMap.new(0)
    end

    Coin = Sprite.new(0)
    Landscape_day = Sprite.new(1)
    Rooms_tiled = TileMap.new(1)
  end

  module Screenshots
    _00000_tdummytest = Sprite.new(2)
    _00001_ttestsprite = Sprite.new(3)
    _00002_ttesttilemap = Sprite.new(4)
    _00003_ttestgeometry = Sprite.new(5)
    _00004_ttestadditionalgeometry = Sprite.new(6)
    _00005_ttestuniforms = Sprite.new(7)
    _00006_ttestvertexlists = Sprite.new(8)
  end

  module Shaders
    Mono = ShaderMono.new(1)
    Mono_array = ShaderMono_array.new(2)
    Mono_float = ShaderMono_float.new(3)
    Mono_int = ShaderMono_int.new(4)
    Mono_sampler = ShaderMono_sampler.new(5)
    Simple = ShaderSimple.new(6)
    Special = ShaderSpecial.new(7)
  end

  module Some_folder
    module Some_folder
      module Some_folder
      end
    end
  end

  module Sounds
    Chop = Sound.new(0)
    Doorclose = Sound.new(1)
    Knifeslice = Sound.new(2)
  end

  Admirationpains = FontResource.new(2)
  Arial = FontResource.new(3)
  Consola = FontResource.new(4)
  Sansation = FontResource.new(5)
end


class ShaderMono < Shader
  uniform mono_color, vec4, 2
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
end

class ShaderMono_array < Shader
  uniform_array mono_array, 4, float, 3
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
end

class ShaderMono_float < Shader
  uniform mono_float, float, 4
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
end

class ShaderMono_int < Shader
  uniform mono_int, int, 5
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
end

class ShaderMono_sampler < Shader
  uniform mono_tex, sampler2D, 6
  uniform screen_size, vec2, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end

class ShaderSimple < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end

class ShaderSpecial < Shader
  attribute bright, int, 3
  attribute my_pos, ivec2, 4
end

class ShaderAllShaders < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  uniform mono_color, vec4, 2
  uniform_array mono_array, 4, float, 3
  uniform mono_float, float, 4
  uniform mono_int, int, 5
  uniform mono_tex, sampler2D, 6
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
  attribute bright, int, 3
  attribute my_pos, ivec2, 4
end

ALL_ATTRIBUTES = {
  {"color", vec4},
  {"texpos", vec3},
  {"bright", int},
  {"my_pos", ivec2},
}