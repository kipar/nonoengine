unit uTestFramework;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uEngine, Resources, fgl ;

type

  { TUnitTest }

  TUnitTest = class
    constructor Create; virtual;
    procedure DoWork; virtual; abstract;
    procedure Run;
    procedure Skip;
    procedure GenerateScreenshot;
    destructor Destroy;override;

    procedure Failed(message: string);
    procedure Passed;
  end;

  { TDummyTest }

  TDummyTest = class(TUnitTest)
    procedure DoWork; override;
  end;

  TUnitTestType = class of TUnitTest;
  TUnitTypeList = specialize TFPGList<TUnitTestType>;
  TTexList = specialize TFPGList<TTexture>;

var
  ScreenTex: TTexture;
  GlobalCounter: Integer;
  FailedTests: TStringList;
  FailedClasses: TUnitTypeList;
  FailedScreens: TTexList;
  ReferenceScreens: TTexList;
  WasRendered: TUnitTestType;

implementation

{ TDummyTest }

procedure TDummyTest.DoWork;
begin
  //do nothing
end;

{ TUnitTest }

constructor TUnitTest.Create;
begin

end;

type
  TSpriteArray = array[0..10000] of TSprite;
  PSpriteArray = ^TSpriteArray;

procedure TUnitTest.Run;
var
  cmp: TSprite;
  ptr1, ptr2: PIntegerArray;
  w1, h1, w2, h2: Single;
  i: integer;
begin
  try
    EngineProcess;
    DoWork;
    CaptureScreen(0, 0, EngineGet(Width), EngineGet(Height), ScreenTex);
    cmp := PSpriteArray(@RES.Screenshots)^[GlobalCounter];
    Inc(GlobalCounter);
    ptr1 := TextureGetPixels(cmp, @w1, @h1, AsByte);
    ptr2 := TextureGetPixels(ScreenTex, @w2, @h2, AsByte);
    if (w1 <> w2) or (w2 <> h2) then
    begin
      Failed(Format('Screenshot size do not match: old %d x %d, new %d x %d', [Trunc(w1),Trunc(h1),Trunc(w2),Trunc(h2)]));
      exit;
    end
    else
    begin
      for i := 0 to Trunc(w1)*Trunc(h1)-1 do
        if ptr1^[i]<>ptr2^[i] then
        begin
          Failed(Format('Pixel %d do not match: old %d , new %d', [i, ptr1^[i], ptr2^[i]]));
          exit;
        end;
      Passed;
    end;
  finally
    Free;
  end;
end;

procedure TUnitTest.Skip;
begin
  Inc(GlobalCounter);
  Free;
end;

procedure TUnitTest.GenerateScreenshot;
var
  s: string;
begin
  WasRendered := TUnitTestType(ClassType);
  EngineProcess;
  DoWork;
  CaptureScreen(0, 0, EngineGet(Width), EngineGet(Height), ScreenTex);
  s := IntToStr(GlobalCounter);
  if length(s) < 5 then
    s := StringOfChar('0',5-Length(s))+s;
  Inc(GlobalCounter);
  TextureSave(ScreenTex, PChar('.\resources\screenshots\_'+s+'_'+ClassName+'.png'));
  Free;
end;

destructor TUnitTest.Destroy;
begin
  inherited;
end;

procedure TUnitTest.Failed(message: string);
begin
  log(ClassName+' FAILED: '+message);
  FailedTests.Add(ClassName+': '+message);
  FailedClasses.Add(TUnitTestType(ClassType));
  FailedScreens.Add(TextureClone(ScreenTex));
  ReferenceScreens.Add(PSpriteArray(@RES.Screenshots)^[GlobalCounter-1]);
end;

procedure TUnitTest.Passed;
begin
  log(ClassName+' OK');
end;

begin
  FailedTests := TStringList.Create;
  FailedClasses := TUnitTypeList.Create;
  FailedScreens := TTexList.Create;
  ReferenceScreens := TTexList.Create;
end.

