program demo;

uses
  SysUtils,
  uEngine,
  Resources,
  uTestFramework,
  uPrimitives,
  uShaders,
  uvertexlists, uRenderFailed;


var
  i: integer;
  t: TUnitTest;
  s: string;
  failed: TRenderFailed;
begin
  EngineSet(Antialias, 4);
  EngineSet(VSync, 1);
  EngineSet(Width, 600);
  EngineSet(Height, 600);
  EngineInit('./resources');
  ScreenTex := TextureCreate(EngineGet(Width), EngineGet(Height));

  TDummyTest.Create.Run;
  TTestSprite.Create.Run;
  TTestTileMap.Create.Run;
  TTestGeometry.Create.Run;
  TTestAdditionalGeometry.Create.Run;
  TTestUniforms.Create.Run;
  TTestVertexLists.Create.Run;

  if Assigned(WasRendered) then
  begin
    t := WasRendered.Create;
    s := t.ClassName;
    repeat
      t.DoWork;
      SetLayer(101);
      DrawText(RES.Consola, PChar(s), 10, 10);
      EngineProcess;
    until KeyState(AnyKey) <> ksUp;
  end
  else if FailedTests.Count = 0 then
  begin
    repeat
      DrawText(RES.Consola, PChar('ALL TESTS PASSED'), 10, 10);
      EngineProcess;
    until KeyState(AnyKey) <> ksUp;
  end
  else
  begin
    failed := TRenderFailed.Create;
    repeat
      failed.Render;
      EngineProcess;
    until KeyState(AnyKey) <> ksUp;
  end;
end.
