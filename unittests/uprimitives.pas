unit uPrimitives;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uEngine, uTestFramework, Resources;

  type

    { TTestSprite }

    TTestSprite = class(TUnitTest)
      procedure DoWork; override;
    end;

    { TTestTileMap }

    TTestTileMap = class(TUnitTest)
      procedure DoWork; override;
    end;

    { TTestGeometry }

    TTestGeometry = class(TUnitTest)
      constructor Create; override;
      procedure DoWork; override;
      destructor Destroy; override;
    end;


    { TTestAdditionalGeometry }

    TTestAdditionalGeometry = class(TUnitTest)
      constructor Create; override;
      procedure DoWork; override;
      destructor Destroy; override;
    end;

implementation

{ TTestAdditionalGeometry }

constructor TTestAdditionalGeometry.Create;
begin
  inherited Create;
  EngineSet(Antialias, 0);
end;

procedure TTestAdditionalGeometry.DoWork;
begin
  Camera(-100,-100,0.5,0.5,45);
  Background(RES.Images.Landscape_day);
  SetLayer(2);
  DrawTiled(RES.Images.Rooms_tiled, 4, 100, 100,2,2);
  Rect(100, 100, 200,100,false,WHITE);
  SetLayer(-1);
  DrawTiled(RES.Images.Rooms_tiled, 4, 150, 150,2,2,0,RED);
  Rect(150, 150, 200,100,false,RED);
  SetLayer(3);
  DrawTiled(RES.Images.Rooms_tiled, 4, 50, 50,2,2,0,GREEN);
  Rect(50, 50, 200,100,false,GREEN);

  SetLayer(101);
  Rect(350, 150, 200,100,false,YELLOW);
  DrawTiled(RES.Images.Rooms_tiled, 4, 350, 150,2,2,0,YELLOW);

  //DrawTiled(RES.Images.Rooms_tiled, 4, 100, 100,2,2);
  //Sprite(RES.Images.Coin, 100,100);

end;

destructor TTestAdditionalGeometry.Destroy;
begin
  EngineSet(Antialias, 4);
  inherited Destroy;
end;

{ TTestGeometry }

constructor TTestGeometry.Create;
begin
  inherited Create;
  EngineSet(ClearColor, BLACK);
  EngineProcess;
end;

procedure TTestGeometry.DoWork;
var
  i, j: integer;
begin
  TexturedTriangle(RES.Images.Landscape_day, 100, 100, 0, 0, 200, 200, 200,
      200, 100, 200, 0, 200);
  Triangle(200, 100, 300, 200, 300, 100, GREEN);

  Rect(410, 110, 80, 80, True, $000000FF, $0000FFFF,
       $00FF00FF, $FF0000FF, 15);
  Rect(400, 100, 100, 100, False, $000000FF, $0000FFFF,
       $00FF00FF, $FF0000FF, 15);


  LineSettings(1);
  for i := 1 to 20 do
    Line(50, 300, 150, 300+i*10, RED, WHITE);
  //LineSettings(5); TODO make it work
  for i := 1 to 20 do
    Line(150, 300, 50, 300+i*10, RED, WHITE);

  for i := 1 to 10 do
  begin
    LineSettings(i);
    for j := 1 to 10 do
    begin
      Point(200 + i*10, 300+j*10, Color(150+i*10, 150+j*10,255,255));
    end;
  end;
  Ellipse(450, 400, 50, 100, True, $000000FF, $FFFFFFFF, 45);
  Ellipse(450, 400, 50, 100, False, $000000FF, $FFFFFFFF, -45);
end;

destructor TTestGeometry.Destroy;
begin
  EngineSet(ClearColor, BLUE);
  inherited Destroy;
end;

{ TTestTileMap }

procedure TTestTileMap.DoWork;
begin
   DrawTiled(RES.Images.Rooms_tiled, 4, 100, 100);
   DrawTiled(RES.Images.Rooms_tiled, 23, 100, 100);
   DrawTiled(RES.Images.Units.Monster, 0, 50, 200);
   DrawTiled(RES.Images.Units.Monster, 1, 200, 200, 0.5, 0.5, 15, GREEN);
   DrawTiled(RES.Images.Units.Monster, 2, 300, 200, 0.5, 0.5, -15, MAROON);
end;

{ TTestSprite }

procedure TTestSprite.DoWork;
begin
   //background
   Background(RES.Images.Landscape_day, 2, 1, 0, 20);
   //usual sprites
   Sprite(RES.Images.Coin, 100,100, 0.5, 0.5);
   Sprite(RES.Images.Coin, 100,250, 1, 0.5);
   Sprite(RES.Images.Coin, 100,400, 0.5, 1);

   Sprite(RES.Images.Coin, 300,100, 0.5, 0.5, 45);
   Sprite(RES.Images.Coin, 300,250, 0.5, 1, -55);
   Sprite(RES.Images.Coin, 300,400, 0.5, 1, -720);

   Sprite(RES.Images.Coin, 500,100, 0.5, 0.5, 45, RED);
   Sprite(RES.Images.Coin, 500,250, 0.5, 0.5, -45, GREEN);
   Sprite(RES.Images.Coin, 500,400, 0.5, 0.5, 0, BLACK);
end;

end.

