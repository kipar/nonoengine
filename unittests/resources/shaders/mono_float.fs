uniform float mono_float;
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
	result = vec4(mono_float,mono_float,mono_float,1.0f);
}
