include: common.hs
vertex: simple.vs
fragment: mono_float.fs
uniform: mono_float: float
uniform: screen_size: vec2
attribute: pos: vec3
