uniform sampler2D mono_tex;
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
	result = texture(mono_tex, texcoord.xy);
}
