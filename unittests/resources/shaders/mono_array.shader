include: common.hs
vertex: simple.vs
fragment: mono_array.fs
uniform: mono_array[4]: float
uniform: screen_size: vec2
attribute: pos: vec3
