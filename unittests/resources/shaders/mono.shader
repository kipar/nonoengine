include: common.hs
vertex: simple.vs
fragment: mono.fs
uniform: mono_color: vec4
uniform: screen_size: vec2
attribute: pos: vec3
