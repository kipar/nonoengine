#version 130

in float is_bright;
out vec4 result;

void main(void)
{
  if(is_bright > 0.5f)
	result = vec4(1.0f,1.0f,1.0f,1.0f);
  else
	result = vec4(0.0f,0.0f,0.0f,1.0f);
}
