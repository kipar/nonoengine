uniform int mono_int;
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
	float value = 1.0f*mono_int/255.0f;
	result = vec4(value, value, value, 1.0f);
}
