#version 130

in ivec2 my_pos;
in int bright;

out float is_bright;

void main(void)
{
  gl_Position = vec4(my_pos / 100.0f, 0.0f, 1.0f);
  is_bright = bright > 0 ? 1.0f : 0.0f;
}
