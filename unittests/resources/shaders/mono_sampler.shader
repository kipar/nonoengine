include: common.hs
vertex: simple.vs
fragment: mono_sampler.fs
uniform: mono_tex: sampler2D
uniform: screen_size: vec2
attribute: pos: vec3
attribute: texpos: vec3
