include: common.hs
vertex: simple.vs
fragment: mono_int.fs
uniform: mono_int: int
uniform: screen_size: vec2
attribute: pos: vec3
