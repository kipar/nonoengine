uniform float mono_array[4];
in vec3 texcoord;
in vec4 pixel_color;
out vec4 result;

void main(void)
{
	result = vec4(mono_array[0], mono_array[1], mono_array[2], mono_array[3]);
}
