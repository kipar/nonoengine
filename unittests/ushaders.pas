unit uShaders;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uTestFramework, uEngine, Resources;

type

  { TTestUniforms }

  TTestUniforms = class(TUnitTest)
    procedure DoWork; override;
  end;

implementation

{ TTestUniforms }

procedure TTestUniforms.DoWork;
var
  MonoColor: array[1..4] of Single;
  v: Single;
  i: integer;
begin
  MonoColor[1] := 0;
  MonoColor[2] := 1;
  MonoColor[3] := 0;
  MonoColor[4] := 1;
  UniformSetPtr(RES.Shaders.Mono, uni_mono_color, @MonoColor);
  ShaderActivate(RES.Shaders.Mono);
  Background(RES.Images.Coin);

  MonoColor[1] := 0;
  MonoColor[2] := 0;
  MonoColor[3] := 1;
  MonoColor[4] := 1;
  Rect(10,10,90,100,true, WHITE);

  MonoColor[1] := 1;
  MonoColor[2] := 0;
  MonoColor[3] := 1;
  MonoColor[4] := 1;
  UniformSetPtr(RES.Shaders.Mono, uni_mono_color, @MonoColor);
  Rect(110,10,90,100,true, WHITE);

  ShaderActivate(RES.Shaders.Simple);
  Rect(210,10,90,100,true, WHITE);

  v := 0.0;
  UniformSetPtr(RES.Shaders.Mono_float,uni_mono_float, @v);
  ShaderActivate(RES.Shaders.Mono_float);
  Rect(310,10,90,100,true, WHITE);
  UniformSetFloat(RES.Shaders.Mono_float,uni_mono_float, 0.6);
  Rect(410,10,90,100,true, WHITE);

  UniformSetInt(RES.Shaders.Mono_int,uni_mono_int, 150);
  ShaderActivate(RES.Shaders.Mono_int);
  Rect(10,120,90,100,true, WHITE);
  i := 200;
  UniformSetPtr(RES.Shaders.Mono_int,uni_mono_int, @i);
  Rect(110,120,90,100,true, WHITE);

  //SKYBLUE = $A6CAF0FF;
  MonoColor[1] := 0.65;
  MonoColor[2] := 0.79;
  MonoColor[3] := 0.94;
  MonoColor[4] := 1;
  UniformSetPtr(RES.Shaders.Mono_array,uni_mono_array, @MonoColor);
  ShaderActivate(RES.Shaders.Mono_array);
  Rect(210,120,90,100,true, WHITE);
  //CREAM = $FFFBF0FF;
  MonoColor[1] := 1;
  MonoColor[2] := 0.98;
  MonoColor[3] := 0.94;
  UniformSetPtr(RES.Shaders.Mono_array,uni_mono_array, @MonoColor);
  Rect(310,120,90,100,true, WHITE);

  UniformSetTexture(RES.Shaders.Mono_sampler,uni_mono_tex, RES.Images.Coin);
  ShaderActivate(RES.Shaders.Mono_sampler);
  Sprite(RES.Images.Landscape_day, 150, 350);
  UniformSetTexture(RES.Shaders.Mono_sampler,uni_mono_tex, RES.Images.Landscape_day);
  Sprite(RES.Images.Coin, 450, 350);

  ShaderActivate(DEFAULT_SHADER);
end;

end.

