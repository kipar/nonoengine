unit uvertexlists;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uTestFramework, uEngine, Resources;

type

  { TTestVertexLists }

  TVertex1 = packed record
    x, y, z: single;
  end;

  TVertex3 = packed record
    my_x, my_y: integer;
    padding: array[1..5] of byte;
    Bright: integer;
  end;

  TTestVertexLists = class(TUnitTest)
    vl1, vl2, vl3: TVertexList;
    items1: array[1..6] of TVertex1;
    items2: array[1..4] of TVertex1;
    items3: array[1..3] of TVertex3;
    procedure DoWork; override;
    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

{ TTestVertexLists }

procedure TTestVertexLists.DoWork;
begin
  UniformSetInt(RES.Shaders.Mono_int, uni_mono_int, 150);
  ShaderActivate(RES.Shaders.Mono_int);
  VertexListDraw(vl1, -1, False);
  UniformSetInt(RES.Shaders.Mono_int, uni_mono_int, 100);
  VertexListDraw(vl1, 3, False);
  items1[4].x := 800;
  UniformSetInt(RES.Shaders.Mono_int, uni_mono_int, 255);
  VertexListDraw(vl2, -1, True);
  items2[4].x := 425;

  ShaderActivate(RES.Shaders.Special);
  VertexListDraw(vl3, -1, False);

  ShaderActivate(DEFAULT_SHADER);
end;

constructor TTestVertexLists.Create;
begin
  inherited Create;
  vl1 := VertexListCreate(@items1, vlTriangles, sizeof(TVertex1), 6);
  VertexListAddField(vl1, attr_pos);
  items1[1].x := 50;
  items1[1].y := 50;
  items1[1].z := 0;
  items1[2].x := 150;
  items1[2].y := 50;
  items1[2].z := 0;
  items1[3].x := 50;
  items1[3].y := 150;
  items1[3].z := 0;

  items1[4].x := 250;
  items1[4].y := 50;
  items1[4].z := 0;
  items1[5].x := 350;
  items1[5].y := 50;
  items1[5].z := 0;
  items1[6].x := 250;
  items1[6].y := 150;
  items1[6].z := 0;

  vl2 := VertexListCopy(vl1);
  items2[1].x := 400;
  items2[1].y := 50;
  items2[1].z := 0;
  items2[2].x := 400;
  items2[2].y := 150;
  items2[2].z := 0;
  items2[3].x := 550;
  items2[3].y := 50;
  items2[3].z := 0;
  items2[4].x := 400;
  items2[4].y := 50;
  items2[4].z := 0;
  VertexListChange(vl2, @items2, vlLines, 4);



  vl3 := VertexListCreate(@items3, vlTriangles, sizeof(TVertex3), 3);
  VertexListAddField(vl3, attr_my_pos);
  VertexListAddPadding(vl3, 5);
  VertexListAddField(vl3, attr_bright);
  items3[1].my_x := 0;
  items3[1].my_y := 0;
  items3[1].Bright := 1;
  items3[2].my_x := -40;
  items3[2].my_y := -60;
  items3[2].Bright := 0;
  items3[3].my_x := -60;
  items3[3].my_y := 40;
  items3[3].Bright := 0;
end;

destructor TTestVertexLists.Destroy;
begin
  VertexListDelete(vl1);
  VertexListDelete(vl2);
  VertexListDelete(vl3);
  inherited Destroy;
end;

end.
