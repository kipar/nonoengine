unit Resources;

interface
{$J-}
type
TRawResource = (TRawResource_NOT_USED);
TSprite = (THE_SCREEN = -1, res_Coin, res_Landscape_day, res__00000_tdummytest, res__00001_ttestsprite, res__00002_ttesttilemap, res__00003_ttestgeometry, res__00004_ttestadditionalgeometry, res__00005_ttestuniforms, res__00006_ttestvertexlists);
TSound = (NO_MUSIC = -1, res_Chop, res_Doorclose, res_Knifeslice);
TButton = (TButton_NOT_USED);
TTileMap = (res_Monster, res_Rooms_tiled);
TFont = (res_Main_blue, res_Mini_default, res_Admirationpains, res_Arial, res_Consola, res_Sansation);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, res_Mono, res_Mono_array, res_Mono_float, res_Mono_int, res_Mono_sampler, res_Simple, res_Special);

TShaderAttribute = (attr_color, attr_pos, attr_texpos, attr_bright, attr_my_pos);
TShaderUniform = (uni_screen_size, uni_tex, uni_mono_color, uni_mono_array, uni_mono_float, uni_mono_int, uni_mono_tex);


TFonts = record
  Main_blue: TFont;
  Mini_default: TFont;
end;

TEmpty_folder = record
end;

TUnits = record
  Monster: TTileMap;
end;

TImages = record
  Empty_folder: TEmpty_folder;
  Units: TUnits;
  Coin: TSprite;
  Landscape_day: TSprite;
  Rooms_tiled: TTileMap;
end;

TScreenshots = record
  _00000_tdummytest: TSprite;
  _00001_ttestsprite: TSprite;
  _00002_ttesttilemap: TSprite;
  _00003_ttestgeometry: TSprite;
  _00004_ttestadditionalgeometry: TSprite;
  _00005_ttestuniforms: TSprite;
  _00006_ttestvertexlists: TSprite;
end;

TShaders = record
  Mono: TShader;
  Mono_array: TShader;
  Mono_float: TShader;
  Mono_int: TShader;
  Mono_sampler: TShader;
  Simple: TShader;
  Special: TShader;
end;

TSome_folder_2 = record
end;

TSome_folder_1 = record
  Some_folder_2: TSome_folder_2;
end;

TSome_folder = record
  Some_folder_1: TSome_folder_1;
end;

TSounds = record
  Chop: TSound;
  Doorclose: TSound;
  Knifeslice: TSound;
end;

TRES = record
  Fonts: TFonts;
  Images: TImages;
  Screenshots: TScreenshots;
  Shaders: TShaders;
  Some_folder: TSome_folder;
  Sounds: TSounds;
  Admirationpains: TFont;
  Arial: TFont;
  Consola: TFont;
  Sansation: TFont;
end;

const RES: TRES = (
  Fonts: (
    Main_blue: res_Main_blue;
    Mini_default: res_Mini_default;
  );
  Images: (
    Empty_folder: (
    );
    Units: (
      Monster: res_Monster;
    );
    Coin: res_Coin;
    Landscape_day: res_Landscape_day;
    Rooms_tiled: res_Rooms_tiled;
  );
  Screenshots: (
    _00000_tdummytest: res__00000_tdummytest;
    _00001_ttestsprite: res__00001_ttestsprite;
    _00002_ttesttilemap: res__00002_ttesttilemap;
    _00003_ttestgeometry: res__00003_ttestgeometry;
    _00004_ttestadditionalgeometry: res__00004_ttestadditionalgeometry;
    _00005_ttestuniforms: res__00005_ttestuniforms;
    _00006_ttestvertexlists: res__00006_ttestvertexlists;
  );
  Shaders: (
    Mono: res_Mono;
    Mono_array: res_Mono_array;
    Mono_float: res_Mono_float;
    Mono_int: res_Mono_int;
    Mono_sampler: res_Mono_sampler;
    Simple: res_Simple;
    Special: res_Special;
  );
  Some_folder: (
    Some_folder_1: (
      Some_folder_2: (
      );
    );
  );
  Sounds: (
    Chop: res_Chop;
    Doorclose: res_Doorclose;
    Knifeslice: res_Knifeslice;
  );
  Admirationpains: res_Admirationpains;
  Arial: res_Arial;
  Consola: res_Consola;
  Sansation: res_Sansation;
);

implementation
end.
