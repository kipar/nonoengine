unit uLog;

{$mode objfpc}{$H+}

interface

uses
  SysUtils;

var
  indent: integer = 0;
  LogEnabled: boolean = True;

procedure Log(s: string; First: boolean = False);
procedure LogEnter(s: string);
procedure LogExit(s: string);
procedure Logf(s: string; const Args: array of const);
procedure LogException(Cause: string; E: Exception);

procedure EngineLog(s: PChar); cdecl;

procedure FreeLog;//TODO - flush instead of close

implementation


procedure LogException(Cause: string; E: Exception);
var
  i: integer;
  s: string;
begin
  s := E.ClassName + ': ' + E.Message;
  i := pos(#13#10, s);
  while i > 0 do
  begin
    Delete(s, i, 2);
    insert(' ', s, i);
    i := pos(#13#10, s);
  end;
  log('Exception in ' + Cause + ' - ' + s);
end;

procedure EngineLog(s: PChar); cdecl;
begin
  log('User: ' + s);
end;

procedure FreeLog;
begin

end;

procedure Log(s: string; First: boolean = False);
var
  ff: TextFile;
  s1: string;
begin
  if not LogEnabled then
    exit;
  DateTimeToString(s1, 'dd/mm/yyyy hh:nn:ss.zzz ', Now);
  Assignfile(ff, './nonoengine.log');
  if (not First) and FileExists('./nonoengine.log') then
    Append(ff)
  else
    Rewrite(ff);
  WriteLn(ff, s1, StringOfChar(' ', indent), s);
  CloseFile(ff);
end;

procedure LogEnter(s: string);
begin
  if s <> '' then
    Log(s);
  Inc(indent, 4);
end;

procedure LogExit(s: string);
begin
  Dec(indent, 4);
  if s <> '' then
    Log(s);
end;

procedure Logf(s: string; const Args: array of const);
begin
  if not LogEnabled then
    exit;
  Log(Format(s, args));
end;

end.
