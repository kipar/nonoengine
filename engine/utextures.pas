unit uTextures;

interface

uses
  GL, glext, SDL2, sdl2_ttf, sdl2_image, uEngineTypes;

type

  { TGenericTexture }

  TGenericTexture = class
    texid: GLuint;
    Width, Height: integer;
    DataOnCPUFloat: array of tVec4Data;
    DataOnCPUByte: array of TColor;
    procedure ActivateReading(aunit: integer);
    function FromGPU(format: TPixelFormat): Pointer; virtual;
    procedure ToGPU(format: TPixelFormat);
    destructor Destroy; override;
  end;

  { TTexture }

  TTexture = class(TGenericTexture)
    fboid: GLuint;
    constructor Create(aWidth, aHeight: integer);
    procedure ActivateWriting;
    destructor Destroy; override;
  end;

  { TSimpleTexture }

  TSimpleTexture = class(TGenericTexture)
    pixelized: boolean;
    function FromGPU(format: TPixelFormat): Pointer; override;
    constructor Create(filename: string; apixelized: boolean = False);
    constructor CreateFromImage(tex: PSDL_Surface; apixelized: boolean = False);
    procedure UpdateFromImage(tex: PSDL_Surface);
    constructor CreateFromMessage(fnt: PTTF_Font; Text: string);
    procedure UpdateFromMessage(fnt: PTTF_Font; Text: string);
    procedure SetEmpty;
    constructor CreateEmpty;
  end;

const
  sdlWHITE: TSDL_Color = (r: 255; g: 255; b: 255; a: 255);

implementation

uses ulog;

{ TSimpleTexture }

function TSimpleTexture.FromGPU(format: TPixelFormat): Pointer;
begin
  if format = AsByte then
  begin
    if Length(DataOnCPUByte) > 0 then
    begin
      Result := @DataOnCPUByte[0];
      exit;
    end;
  end
  else
  begin
    if Length(DataOnCPUFloat) > 0 then
    begin
      Result := @DataOnCPUFloat[0];
      exit;
    end;
  end;
  Result := inherited FromGPU(format);
end;

constructor TSimpleTexture.Create(filename: string; apixelized: boolean);
var
  tex: PSDL_Surface;
begin
  if copy(filename, length(filename) - 3, 4) <> '.png' then
    filename := filename + '.png';
  tex := IMG_Load(PChar(filename));
  if tex = nil then
  begin
    log('Failed to load: ' + filename);
    exit;
  end;
  CreateFromImage(tex, apixelized);
  SDL_FreeSurface(tex);
end;

constructor TSimpleTexture.CreateFromImage(tex: PSDL_Surface; apixelized: boolean);
begin
  pixelized := apixelized;
  glGenTextures(1, @texid);
  UpdateFromImage(tex);
end;

procedure TSimpleTexture.UpdateFromImage(tex: PSDL_Surface);
var
  flt, format, iformat: GLenum;
  tex2: PSDL_Surface;
begin
  // сделаем текстуру активной
  glBindTexture(GL_TEXTURE_2D, texid);
  // установим параметры фильтрации текстуры - линейная
  if pixelized then
    flt := GL_NEAREST
  else
    flt := GL_LINEAR;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, flt);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, flt);
  // установим параметры "оборачивания" текстуры - отсутствие оборачивания
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  Width := tex^.w;
  Height := tex^.h;
  case tex^.format^.BitsPerPixel of
    24:
    begin
      iformat := GL_RGB8;
      format := GL_RGB;
    end;
    32:
    begin
      iformat := GL_RGBA8;
      format := GL_RGBA;
    end;
    else
    begin
      //log('Unsupported format: '+SDL_GetPixelFormatName(tex^.format^.format));
      tex2 := SDL_ConvertSurfaceFormat(tex, SDL_PIXELFORMAT_RGBA32, 0);
      iformat := GL_RGBA8;
      format := GL_RGBA;
      glTexImage2D(GL_TEXTURE_2D, 0, iformat, tex2^.w, tex2^.h, 0, format,
        GL_UNSIGNED_BYTE, tex2^.pixels);
      SDL_FreeSurface(tex2);
      exit;
    end;
  end;
  glTexImage2D(GL_TEXTURE_2D, 0, iformat, tex^.w, tex^.h, 0, format,
    GL_UNSIGNED_BYTE, tex^.pixels);
end;

function RenderText(fnt: PTTF_Font; s: string): PSDL_Surface;
begin
  Result := TTF_RenderUTF8_Blended(fnt, pointer(s), sdlWHITE);
end;


constructor TSimpleTexture.CreateFromMessage(fnt: PTTF_Font; Text: string);
var
  tex: PSDL_Surface;
begin
  if Text = '' then
    SetEmpty
  else
  begin
    tex := RenderText(fnt, Text);
    CreateFromImage(tex);
    SDL_FreeSurface(tex);
  end;
end;

procedure TSimpleTexture.UpdateFromMessage(fnt: PTTF_Font; Text: string);
var
  tex: PSDL_Surface;
begin
  if Text = '' then
    SetEmpty
  else
  begin
    tex := RenderText(fnt, Text);
    UpdateFromImage(tex);
    SDL_FreeSurface(tex);
  end;
end;

procedure TSimpleTexture.SetEmpty;
var
  flt, format, iformat: GLenum;
begin
  pixelized := True;
  glGenTextures(1, @texid);
  // сделаем текстуру активной
  glBindTexture(GL_TEXTURE_2D, texid);
  // установим параметры фильтрации текстуры - линейная
  if pixelized then
    flt := GL_NEAREST
  else
    flt := GL_LINEAR;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, flt);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, flt);
  // установим параметры "оборачивания" текстуры - отсутствие оборачивания
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  Width := 0;
  Height := 0;
  iformat := GL_RGBA8;
  format := GL_RGBA;
  glTexImage2D(GL_TEXTURE_2D, 0, iformat, 0, 0, 0, format,
    GL_UNSIGNED_BYTE, nil);
end;

constructor TSimpleTexture.CreateEmpty;
begin
  SetEmpty;
end;

{ TTexture }

constructor TTexture.Create(aWidth, aHeight: integer);
var
  depthbuffer: GLuint;
begin
  Width := aWidth;
  Height := aHeight;
  glGenTextures(1, @texid);
  // сделаем текстуру активной
  glBindTexture(GL_TEXTURE_2D, texid);
  // установим параметры фильтрации текстуры - нет её
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  // установим параметры "оборачивания" текстуры - отсутствие оборачивания
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  //fill with zeros
  SetLength(DataOnCPUFloat, Width * Height);
  ToGPU(AsFloat);
  SetLength(DataOnCPUFloat, 0);

  //create fbo
  glGenFramebuffers(1, @fboid);
  glBindFramebuffer(GL_FRAMEBUFFER, fboid);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texid, 0);
  assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) = GL_FRAMEBUFFER_COMPLETE);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  //depth buffer
  //glGenRenderbuffers(1, @depthbuffer);
  //glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);
  //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, Width, Height);
  //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthbuffer);
end;

procedure TTexture.ActivateWriting;
begin
  glViewport(0, 0, Width, Height);
  glBindFramebuffer(GL_FRAMEBUFFER, fboid);
end;

destructor TTexture.Destroy;
begin
  glDeleteFramebuffers(1, @fboid);
  inherited Destroy;
end;

{ TGenericTexture }

procedure TGenericTexture.ActivateReading(aunit: integer);
begin
  glActiveTexture(GL_TEXTURE0 + aunit);
  glBindTexture(GL_TEXTURE_2D, texid);
end;

function TGenericTexture.FromGPU(format: TPixelFormat): Pointer;
begin
  glBindTexture(GL_TEXTURE_2D, texid);
  case format of
    AsFloat:
    begin
      if length(DataOnCPUFloat) = 0 then
        SetLength(DataOnCPUFloat, Width * Height);
      glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, @DataOnCPUFloat[0]);
      Result := @DataOnCPUFloat[0];
    end;
    AsByte:
    begin
      if length(DataOnCPUByte) = 0 then
        SetLength(DataOnCPUByte, Width * Height);
      glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, @DataOnCPUByte[0]);
      Result := @DataOnCPUByte[0];
    end;
  end;
end;

procedure TGenericTexture.ToGPU(format: TPixelFormat);
begin
  glBindTexture(GL_TEXTURE_2D, texid);
  case format of
    AsFloat: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, Width, Height,
        0, GL_RGBA, GL_FLOAT, @DataOnCPUFloat[0]);
    AsByte: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height,
        0, GL_RGBA, GL_UNSIGNED_BYTE, @DataOnCPUByte[0]);
  end;
end;

destructor TGenericTexture.Destroy;
begin
  glDeleteTextures(1, @texid);
  inherited Destroy;
end;


end.
