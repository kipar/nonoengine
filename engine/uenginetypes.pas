unit uEngineTypes;

{$mode objfpc}{$H+}

interface


type
  TRawResource = integer;
  TButton = integer;
  TSprite = integer;
  TSound = integer;
  TTileMap = integer;
  TFont = integer;
  TFontInstance = integer;
  TPanel = integer;
  TMaterial = integer;
  TPoly = integer;
  TBody = integer;
  TPhysicsCoord = Double;
  PPhysicsCoord = ^TPhysicsCoord;

  TShaderID = integer;
  TShaderUniform = integer;
  TShaderAttribute = integer;

  TLayer = integer;
  TTextureID = integer;

  TEmitterID = integer;
  TEmitterInstance = integer;

type
  TKey = (
    AnyKey = -2,
    Quit = -1,
    KeyA, KeyB, KeyC, KeyD, KeyE, KeyF, KeyG, KeyH,
    KeyI, KeyJ, KeyK, KeyL, KeyM, KeyN, KeyO, KeyP,
    KeyQ, KeyR, KeyS, KeyT, KeyU, KeyV, KeyW, KeyX,
    KeyY, KeyZ,
    KeyNum0, KeyNum1, KeyNum2, KeyNum3,
    KeyNum4, KeyNum5, KeyNum6, KeyNum7,
    KeyNum8, KeyNum9,
    KeyEscape,
    KeyLControl, KeyLShift, KeyLAlt, KeyLSystem,
    KeyRControl, KeyRShift, KeyRAlt, KeyRSystem,
    KeyMenu,         ///< The Menu key
    KeyLBracket,     ///< The [ key
    KeyRBracket,     ///< The ] key
    KeySemiColon,    ///< The ; key
    KeyComma,        ///< The , key
    KeyPeriod,       ///< The . key
    KeyQuote,        ///< The ' key
    KeySlash,        ///< The / key
    KeyBackSlash,    ///< The \ key
    KeyTilde,        ///< The ~ key
    KeyEqual,        ///< The = key
    KeyDash,         ///< The - key
    KeySpace,        ///< The Space key
    KeyReturn,       ///< The Return key
    KeyBack,         ///< The Backspace key
    KeyTab,          ///< The Tabulation key
    KeyPageUp,       ///< The Page up key
    KeyPageDown,     ///< The Page down key
    KeyEnd,          ///< The End key
    KeyHome,         ///< The Home key
    KeyInsert,       ///< The Insert key
    KeyDelete,       ///< The Delete key
    KeyAdd,          ///< +
    KeySubtract,     ///< -
    KeyMultiply,     ///< *
    KeyDivide,       ///< /
    KeyLeft,         ///< Left arrow
    KeyRight,        ///< Right arrow
    KeyUp,           ///< Up arrow
    KeyDown,         ///< Down arrow
    KeyNumpad0, KeyNumpad1, KeyNumpad2, KeyNumpad3,
    KeyNumpad4, KeyNumpad5, KeyNumpad6, KeyNumpad7,
    KeyNumpad8, KeyNumpad9,
    KeyF1, KeyF2, KeyF3, KeyF4, KeyF5, KeyF6, KeyF7, KeyF8,
    KeyF9, KeyF10, KeyF11, KeyF12, KeyF13, KeyF14, KeyF15,
    KeyPause        ///< The Pause key
    );


  TCoord = single;
  PCoord = ^TCoord;
  TColor = cardinal;
  TMouseButton = (LeftButton, RightButton, MiddleButton);
  TMouseAxis = (CursorX, CursorY, ScrollPos, ScaledX, ScaledY);
  TKeyState = (ksUp, ksDown, ksPressed);
  TMouseButtonState = (mbsUp, mbsDown, mbsClicked);

  TVAlign = (vaNone, vaTop, vaCenter, vaBottom, vaFlow);
  THAlign = (haNone, haLeft, haCenter, haRight, haFlow);

  TEngineValue = (
    Fullscreen, Width, Height, VSync, Antialias, Log, Autoscale,
    Volume, ClearColor, PhysicsSpeed, GravityX, GravityY, Damping, ProgressWidth, ProgressHeight, ProgressX, ProgressY,
    CameraKX, CameraKY, CameraKXY, CameraKYX, CameraDX, CameraDY,
    RealWidth = 100, RealHeight = 101, FPS = 102, DeltaTime = 103);
  TEngineConfig = Fullscreen..CameraDY;

  TFontStyle = (Bold, Italic, Underlined);
  TFontStyles = set of TFontStyle;

  TButtonState = (bsNormal, bsHover, bsPressed, bsClicked);
  TInputType = (etString, etInteger);
  TGUICoord = (gcX, gcY, gcWidth, gcHeight, gcMouseX, gcMouseY);

  TPathfindCallback = function(fromx, fromy, tox, toy: integer; opaque: pointer): single;
  TPathfindAlgorithm = (AStarNew, AStarReuse, DijkstraNew, DijkstraReuse);

  tVec2Data = array[0 .. 1] of single;
  tVec3Data = array[0 .. 2] of single;
  tVec4Data = array[0 .. 3] of single;

  TPixelFormat = (AsByte, AsFloat);

  TVertexList = Integer;
  TVertexListPrimitive = (vlPoints, vlLines, vlTriangles);


const
  LAYER_GUI = 100;

  WHITE = $FFFFFFFF;

  ALL_SHADERS = -1;
  THE_SCREEN = -1;

  ATTR_COLOR = 0;
  ATTR_POS = 1;
  ATTR_TEXPOS = 2;

  UNI_SCREEN_SIZE = 0;
  UNI_TEX = 1;

  USER_TEXTURES_START = 10000;

  ANY_MATERIAL = -1;

implementation

end.
