unit uVertexWrap;

{$mode objfpc}{$H+}

interface

uses
  uVertexList, uEngineTypes, GL, uPools;

function VertexListCreate(buffer: Pointer; typ: TVertexListPrimitive;
  VertexSize: integer; NVertices: integer): TVertexList; cdecl;
procedure VertexListAddField(list: TVertexList; field: TShaderAttribute); cdecl;
procedure VertexListAddPadding(list: TVertexList; NBytes: integer); cdecl;
procedure VertexListDraw(list: TVertexList; size: integer; WasUpdated: boolean); cdecl;
function VertexListCopy(list: TVertexList): TVertexList; cdecl;
procedure VertexListDelete(list: TVertexList); cdecl;
procedure VertexListChange(list: TVertexList; buffer: Pointer;
  typ: TVertexListPrimitive; NVertices: integer); cdecl;

var
  CustomVertexLists: specialize TResourceArray<uVertexList.TVertexList>;

procedure FreeCustomVertexLists;

implementation

function VertexListCreate(buffer: Pointer; typ: TVertexListPrimitive;
  VertexSize: integer; NVertices: integer): TVertexList; cdecl;
var
  it: uVertexList.TVertexList;
begin
  it := uVertexList.TVertexList.Create(buffer, NVertices, VertexSize);
  case typ of
    vlPoints: it.thetyp := GL_POINTS;
    vlLines: it.thetyp := GL_LINES;
    vlTriangles: it.thetyp := GL_TRIANGLES;
  end;
  Result := CustomVertexLists.AddItem(it);
end;

procedure VertexListAddField(list: TVertexList; field: TShaderAttribute); cdecl;
var
  it: uVertexList.TVertexList;
begin
  it := CustomVertexLists[list];
  it.AddAttribute(field);
end;

procedure VertexListAddPadding(list: TVertexList; NBytes: integer); cdecl;
var
  it: uVertexList.TVertexList;
begin
  it := CustomVertexLists[list];
  it.AddPadding(NBytes);
end;

procedure VertexListDraw(list: TVertexList; size: integer; WasUpdated: boolean); cdecl;
var
  it: uVertexList.TVertexList;
begin
  if size = 0 then
    exit;
  it := CustomVertexLists[list];
  if WasUpdated then
    it.BufferChanged := True;
  it.Draw(it.thetyp, size);
end;

function VertexListCopy(list: TVertexList): TVertexList; cdecl;
begin
  Result := CustomVertexLists.AddItem(CustomVertexLists[list].Clone);
end;

procedure VertexListDelete(list: TVertexList); cdecl;
begin
  CustomVertexLists.Delete(list);
end;

procedure VertexListChange(list: TVertexList; buffer: Pointer;
  typ: TVertexListPrimitive; NVertices: integer); cdecl;
var
  it: uVertexList.TVertexList;
begin
  it := CustomVertexLists[list];
  if it.Buffer <> buffer then
  begin
    it.BufferChanged := True;
    it.Buffer := buffer;
  end;
  case typ of
    vlPoints: it.thetyp := GL_POINTS;
    vlLines: it.thetyp := GL_LINES;
    vlTriangles: it.thetyp := GL_TRIANGLES;
  end;
  if it.NPoints <> NVertices then
  begin
    if it.NPoints < NVertices then
      it.BufferChanged := True;
    it.NPoints := NVertices;
  end;
end;

procedure FreeCustomVertexLists;
begin
  CustomVertexLists.Free;
end;

begin
  CustomVertexLists := specialize TResourceArray<uVertexList.TVertexList>.Create;
end.
