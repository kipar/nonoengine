unit uTextureWrap;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, uTextures, uPools;

function GetPixel(x, y: TCoord; sprite: TTextureID): TColor; cdecl;
procedure RenderTo(sprite: TTextureID); cdecl;
function TextureCreate(Width, Height: single): TTextureID; cdecl;
function TextureClone(Texture: TTextureID): TTextureID; cdecl;
procedure TextureDelete(Texture: TTextureID); cdecl;
procedure TextureSave(Texture: TTextureID; filename: PChar); cdecl;
procedure CaptureScreen(x0, y0, Width, Height: TCoord; Dest: TTextureID); cdecl;
function TextureGetPixels(Texture: TTextureID; Width: PSingle;
  Height: PSingle; Format: TPixelFormat): Pointer; cdecl;
procedure TextureLoadPixels(Texture: TTextureID; v: Pointer;
  Format: TPixelFormat); cdecl;
procedure CrunchTextureGetInternals(Texture: TTextureID; texid: PCardinal;
  fboid: PCardinal); cdecl;

var
  UserTextures: specialize TResourceArray<TTexture>;
  CurWritingTexture: TTexture;

procedure SetRenderingTarget(tex: TTexture);
procedure FreeUserTextures;

implementation

uses uLoader, uRender, sdl2, sdl2_image, gl, glext, uMainEngine, uLog,
  ushaderprocess, uVertexList, Types;

procedure SetRenderingTarget(tex: TTexture);
begin
  if Assigned(tex) then
    tex.ActivateWriting
  else
  begin
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, WindowXSize, WindowYSize);
  end;
end;

function GetPixel(x, y: TCoord; sprite: TTextureID): TColor; cdecl;
var
  tex: TGenericTexture;
  ax, ay: integer;
begin
  tex := RESSprite(sprite);
  tex.FromGPU(AsByte);
  ax := Trunc(x);
  ay := Trunc(y);
  if (ax < 0) or (ax >= tex.Width) or (ay < 0) or (ay >= tex.Height) then
    Result := 0
  else
    Result := SwapEndian(tex.DataOnCPUByte[ax + ay * tex.Width]);
end;

var
  full_quad: array[boolean] of TSpriteList;

procedure DoCopyTexture(afrom: TGenericTexture; ato: TTexture; flipy: boolean);
var
  render_area: TPointF;
  v00, v01, v10, v11: TSpriteVertex;
begin
  //set params
  render_area.x := 1;
  render_area.y := 1;
  ato.ActivateWriting;

  //override shader params
  glUseProgram(DefaultShader.handle);
  afrom.ActivateReading(0);
  ApplyValue(DefaultShader.uniform_locations[DefaultShader.uniform_cache[UNI_TEX]],
    DefaultShader.uniforms[DefaultShader.uniform_cache[UNI_TEX]], SetIntValue(0));
  ApplyValue(DefaultShader.uniform_locations[DefaultShader.uniform_cache[
    UNI_SCREEN_SIZE]],
    DefaultShader.uniforms[DefaultShader.uniform_cache[UNI_SCREEN_SIZE]],
    SetPointerValue(@render_area));
  //draw
  if not assigned(full_quad[flipy]) then
  begin
    full_quad[flipy] := TSpriteList.Create(6);
    if flipy then
    begin
      v00 := Vertex(0, 0, 1, 0, 0, 0, WHITE);
      v01 := Vertex(0, 1, 1, 0, 1, 0, WHITE);
      v10 := Vertex(1, 0, 1, 1, 0, 0, WHITE);
      v11 := Vertex(1, 1, 1, 1, 1, 0, WHITE);
    end
    else
    begin
      v00 := Vertex(0, 0, 1, 0, 1, 0, WHITE);
      v01 := Vertex(0, 1, 1, 0, 0, 0, WHITE);
      v10 := Vertex(1, 0, 1, 1, 1, 0, WHITE);
      v11 := Vertex(1, 1, 1, 1, 0, 0, WHITE);
    end;
    full_quad[flipy].AddVertex(v00);
    full_quad[flipy].AddVertex(v10);
    full_quad[flipy].AddVertex(v01);
    full_quad[flipy].AddVertex(v10);
    full_quad[flipy].AddVertex(v01);
    full_quad[flipy].AddVertex(v11);
  end;
  full_quad[flipy].Draw(GL_TRIANGLES);
  //restore old state
  glUseProgram(CurShader.handle);
  if CurShader = DefaultShader then
  begin
    DefaultShader.ApplyUniform(UNI_TEX);
    DefaultShader.ApplyUniform(UNI_SCREEN_SIZE);
  end;
  SetRenderingTarget(CurWritingTexture);
end;



procedure RenderTo(sprite: TTextureID); cdecl;
var
  tex: TGenericTexture;
begin
  if sprite = THE_SCREEN then
  begin
    if CurWritingTexture = nil then
      exit;
    FlushRenderQueues;
    SetRenderingTarget(nil);
    CurWritingTexture := nil;
    exit;
  end;
  tex := RESSprite(sprite);
  if not (tex is TTexture) then
  begin
    Logf('Error: rendering to sprite not supported! (id: %d)', [sprite]);
    exit;
  end;
  if CurWritingTexture = tex then
    exit;
  FlushRenderQueues;
  TTexture(tex).ActivateWriting;
  CurWritingTexture := TTexture(tex);
end;

function TextureCreate(Width, Height: single): TTextureID; cdecl;
begin
  Result := UserTextures.AddItem(TTexture.Create(Trunc(Width), Trunc(Height)))+USER_TEXTURES_START;
end;

function TextureClone(Texture: TTextureID): TTextureID; cdecl;
var
  old: TGenericTexture;
  index: integer;
begin
  old := RESSprite(Texture);
  index := UserTextures.AddItem(TTexture.Create(old.Width, old.Height));
  DoCopyTexture(old, UserTextures[index], False);
  Result := index + USER_TEXTURES_START;
end;

procedure TextureDelete(Texture: TTextureID); cdecl;
begin
  if Texture < USER_TEXTURES_START then
    exit;
  UserTextures.Delete(Texture - USER_TEXTURES_START);
end;


procedure TextureSave(Texture: TTextureID; filename: PChar); cdecl;
var
  tex: TGenericTexture;
  temp_tex: TTexture;
  ptr: Pointer;
  surf: PSDL_Surface;
begin
  tex := RESSprite(Texture);
  temp_tex := TTexture.Create(tex.Width, tex.Height);
  DoCopyTexture(tex, temp_tex, False);
  temp_tex.FromGPU(AsByte);
  ptr := @temp_tex.DataOnCPUByte[0];
  surf := SDL_CreateRGBSurfaceFrom(ptr, tex.Width, tex.Height, 32,
    tex.Width * 4, $000000FF, $0000FF00, $00FF0000, 0);
  IMG_SavePNG(surf, filename);
  SDL_FreeSurface(surf);
  temp_tex.Free;
end;

procedure CaptureScreen(x0, y0, Width, Height: TCoord; Dest: TTextureID); cdecl;
var
  tex: TGenericTexture;
  temp_tex: TTexture;
begin
  tex := RESSprite(Dest);
  if not (tex is TTexture) then
    exit;
  FlushRenderQueues;
  if CurWritingTexture <> nil then
    SetRenderingTarget(nil);
  temp_tex := TTexture.Create(Trunc(Width), Trunc(Height));
  glBindTexture(GL_TEXTURE_2D, temp_tex.texid);
  //TODO should it check camera?
  glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, Trunc(x0),
    WindowYSize - Trunc(Height) - Trunc(y0), Trunc(Width), Trunc(Height));
  DoCopyTexture(temp_tex, TTexture(tex), True);
  temp_tex.Free;
  //glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Trunc(x0), WindowYSize-Trunc(height)-Trunc(y0), Trunc(width), Trunc(height),0);
  if CurWritingTexture <> nil then
    CurWritingTexture.ActivateWriting;
end;

function TextureGetPixels(Texture: TTextureID; Width: PSingle;
  Height: PSingle; Format: TPixelFormat): Pointer; cdecl;
var
  tex: TGenericTexture;
begin
  tex := RESSprite(Texture);
  Width^ := Tex.Width;
  Height^ := Tex.Height;
  Result := tex.FromGPU(Format);
end;

procedure TextureLoadPixels(Texture: TTextureID; v: Pointer;
  Format: TPixelFormat); cdecl;
var
  tex: TGenericTexture;
begin
  tex := RESSprite(Texture);
  case format of
    AsByte:
    begin
      if length(tex.DataOnCPUByte) = 0 then
        SetLength(tex.DataOnCPUByte, tex.Width * tex.Height);
      if v <> @tex.DataOnCPUByte[0] then
        Move(v^, tex.DataOnCPUByte[0], tex.Width * tex.Height * sizeof(TColor));
    end;
    AsFloat:
    begin
      if length(tex.DataOnCPUFloat) = 0 then
        SetLength(tex.DataOnCPUFloat, tex.Width * tex.Height);
      if v <> @tex.DataOnCPUFloat[0] then
        Move(v^, tex.DataOnCPUFloat[0], tex.Width * tex.Height * sizeof(tVec4Data));
    end;
  end;
  tex.ToGPU(format);
end;

procedure CrunchTextureGetInternals(Texture: TTextureID; texid: PCardinal;
  fboid: PCardinal); cdecl;
var
  tex: TGenericTexture;
begin
  tex := RESSprite(Texture);
  if tex is TTexture then
    fboid^ := TTexture(tex).fboid
  else
    fboid^ := 0;
  texid^ := tex.texid;
end;


procedure FreeUserTextures;
begin
  UserTextures.Free;
  full_quad[False].Free;
  full_quad[True].Free;
end;

begin
  UserTextures := specialize TResourceArray<TTexture>.Create;
end.
