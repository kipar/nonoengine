unit uPoly;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, uPools
  {$IFNDEF NO_PHYSICS}
  , chipmunk
  {$ENDIF}
  ;

function PolygonCreate(Capacity: Integer): TPoly; cdecl;
procedure PolygonFree(p: TPoly); cdecl;
procedure PolygonAddPoint(p: TPoly; x, y: TPhysicsCoord); cdecl;

type

  { TPolyData }

  {$IFDEF NO_PHYSICS}
  cpVect = record
    x: Double;
    y: Double;
  end;
  {$ENDIF}


  TPolyData = class
    NPoints: integer;
    points: array of cpVect;
    constructor Create(Capacity: Integer);
    procedure AddPoint(x, y: TPhysicsCoord);
    function IsComplete: Boolean;
  end;

var
  Polygons: specialize TResourceArray<TPolyData>;

procedure FreePolygons;
implementation

uses uLog;


{$IFDEF NO_PHYSICS}
function cpv(x,y: Double): cpVect;
begin
  Result.x := x;
  Result.y := y;
end;
{$ENDIF}


function PolygonCreate(Capacity: Integer): TPoly; cdecl;
begin
  if Capacity <= 0 then Capacity := 3;
  Result := Polygons.AddItem(TPolyData.Create(Capacity));
end;

procedure PolygonFree(p: TPoly); cdecl;
begin
  Polygons.Delete(p);
end;

procedure PolygonAddPoint(p: TPoly; x, y: TPhysicsCoord); cdecl;
begin
  Polygons[p].AddPoint(x, y);
end;

procedure FreePolygons;
begin
  Polygons.Free;
end;

{ TPolyData }

constructor TPolyData.Create(Capacity: Integer);
begin
  NPoints := 0;
  SetLength(points, Capacity);
end;

procedure TPolyData.AddPoint(x, y: TPhysicsCoord);
begin
  Inc(NPoints);
  if NPoints >= Length(points) then
    SetLength(points, Length(points)*2);
  points[NPoints-1] := cpv(x,y);
end;

function TPolyData.IsComplete: Boolean;
begin
  Result := NPoints > 2;
  if not result then logf('Polygon has only %d points, need at least 3', [NPoints]);
end;

begin
  Polygons := specialize TResourceArray<TPolyData>.Create;
end.

