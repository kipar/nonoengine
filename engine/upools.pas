unit uPools;

{$mode objfpc}{$H+}

interface

uses
  Classes;

type

  { TLinearPool }

  generic TLinearPool<T> = class
  type
    TCreator =
    function: T;
  var
    Items: TList;
    Used: Integer;
    Creator: TCreator;

    function LastOrNext(out reused: Boolean): T;
    function Next: T;
    procedure Reset;
    constructor Create(aCreator: TCreator);
    destructor Destroy; override;
    function Item(i: Integer): T;
  end;

  { TResourceEnumerator }

  { TUnorderedEnumerator }

  generic TUnorderedEnumerator<T> = class
    ptr: Pointer;
    remains: PInteger;
  private
    function GetCurrent: T;
  public
    function MoveNext: Boolean;
    property Current: T read GetCurrent;
  end;


  { TUnorderedPool }

  generic TUnorderedPool<T> = class
  type
    PT = ^T;
    TUnorderedEnumeratorSpec = specialize TUnorderedEnumerator<PT>;
  var
    Items: array of T;
    Used: Integer;
    function Add: PT;
    procedure Delete(item: PT);
    function GetEnumerator: TUnorderedEnumeratorSpec;
    constructor Create;
    destructor Destroy; override;
  end;

  { TResourceArray }

  { TResourceEnumerator }

  generic TResourceEnumerator<T> = class
    owner: Pointer;
    Index: Integer;
  private
    function GetCurrent: T;
  public
    function MoveNext: Boolean;
    property Current: T read GetCurrent;
  end;

  generic TResourceArray<T> = class
  protected
    items: TList;
    function Get(i: Integer): T;
  public
  type TResourceEnumeratorSpec = specialize TResourceEnumerator<T>;
    constructor Create;
    destructor Destroy; override;
    function AddItem(x: T): Integer;
    procedure Delete(index: Integer);
    property Data[Index: Integer]: T read Get; default;
    procedure Clear;
    function GetEnumerator: TResourceEnumeratorSpec;
  end;

  { TResourceStructEnumerator }

  generic TResourceStructEnumerator<T> = class
    owner: Pointer;
    Index: Integer;
  private
    function GetCurrent: T;
  public
    function MoveNext: Boolean;
    property Current: T read GetCurrent;
  end;


  generic TResourceStructPtrEnumerator<T> = class
  type
    Ptr = ^T;
  var
    owner: Pointer;
    Index: Integer;
  private
    function GetCurrent: Ptr;
  public
    function MoveNext: Boolean;
    property Current: Ptr read GetCurrent;
  end;

  { TPtrWrapper }

  generic TPtrWrapper<T> = object
  type
    TResourceEnumeratorPtrSpec =
    specialize TResourceStructPtrEnumerator<T>;
  var
    owner: pointer;

    function GetEnumerator: TResourceEnumeratorPtrSpec;
  end;
  { TResourceStructArray }

  generic TResourceStructArray<T> = class
  protected
    items: array of T;
    Used: Integer;
    function Get(i: Integer): T;
    function IsActive(x: T): Boolean;
  public
  type
    TResourceEnumeratorSpec = specialize TResourceStructEnumerator<T>;
    TResourceEnumeratorPtrSpec = specialize TResourceStructPtrEnumerator<T>;
    TPtrSpec = specialize TPtrWrapper<T>;
    constructor Create(size: Integer);
    destructor Destroy; override;
    function AddItem(x: T): Integer;
    procedure Delete(index: Integer);
    property Data[Index: Integer]: T read Get; default;
    procedure Clear;
    procedure Cleanup;
    function GetEnumerator: TResourceEnumeratorSpec;
    function GetEnumeratorPtr: TResourceEnumeratorPtrSpec;
    function AsPtr: TPtrSpec;
    function Count: Integer;
  end;


implementation

{ TPtrWrapper }

function TPtrWrapper.GetEnumerator: specialize
TResourceStructPtrEnumerator<T>;
type
  X = specialize TResourceStructArray<T>;
begin
  Result := X(Owner).GetEnumeratorPtr;
end;

{ TResourceStructPtrEnumerator }

function TResourceStructPtrEnumerator.GetCurrent: Ptr;
type
  X = specialize TResourceStructArray<T>;
begin
  Result := @X(owner).items[index];
end;

function TResourceStructPtrEnumerator.MoveNext: Boolean;
type
  X = specialize TResourceStructArray<T>;
var
  storage: X;
begin
  storage := X(owner);
  Result := False;

  repeat
    Inc(Index);
    if Index >= storage.Used then exit;
  until storage.IsActive(storage.items[Index]);
  Result := True;
end;

{ TResourceStructEnumerator }

function TResourceStructEnumerator.GetCurrent: T;
type
  X = specialize TResourceStructArray<T>;
begin
  Result := X(owner).items[index];
end;

function TResourceStructEnumerator.MoveNext: Boolean;
type
  X = specialize TResourceStructArray<T>;
var
  storage: X;
begin
  storage := X(owner);
  Result := False;

  repeat
    Inc(Index);
    if Index >= storage.Used then exit;
  until storage.IsActive(storage.items[Index]);
  Result := True;
end;

{ TResourceStructArray }

function TResourceStructArray.Get(i: Integer): T;
begin
  Result := items[i - 1];
end;

function TResourceStructArray.IsActive(x: T): Boolean;
begin
  Result := PInteger(@x)^ <> 0;
end;

constructor TResourceStructArray.Create(size: Integer);
begin
  Used := 0;
  SetLength(items, size);
end;

destructor TResourceStructArray.Destroy;
  //var
  //  i: Integer;
  //  x: T;
begin
  // TODO - safe deletion
  //for i := 0 to Used-1 do
  //begin
  //  x := items[i];
  //  if IsActive(x) then ;
  //end;
  inherited Destroy;
end;

function TResourceStructArray.AddItem(x: T): Integer;
var
  i: Integer;
begin
  for i := 0 to Used - 1 do
    if not IsActive(Items[i]) then
    begin
      Result := i + 1;
      items[i] := x;
      exit;
    end;
  if Used >= Length(items) then
    SetLength(items, Length(items) * 2);
  Inc(Used);
  Result := Used;
  items[Used - 1] := x;
end;

procedure TResourceStructArray.Delete(index: Integer);
begin
  PInteger(@(items[index - 1]))^ := 0;
  if index = Used then
    repeat
      Dec(Used);
    until (Used = 0) or IsActive(items[Used - 1]);
end;

procedure TResourceStructArray.Clear;
begin
  // TODO - safe deletion
  Used := 0;
end;

procedure TResourceStructArray.Cleanup;
var
  i: Integer;
begin
  for i := Used - 1 downto 0 do
    if not IsActive(Items[i]) then
    begin
      Items[i] := Items[Used - 1];
      Dec(Used);
    end;
end;

function TResourceStructArray.GetEnumerator: TResourceEnumeratorSpec;
begin
  Result := TResourceEnumeratorSpec.Create;
  Result.owner := Self;
  Result.Index := -1;
end;

function TResourceStructArray.GetEnumeratorPtr: TResourceEnumeratorPtrSpec;
begin
  Result := TResourceEnumeratorPtrSpec.Create;
  Result.owner := Self;
  Result.Index := -1;
end;

function TResourceStructArray.AsPtr: TPtrSpec;
begin
  Result.owner := Self;
end;

function TResourceStructArray.Count: Integer;
var
  x: T;
begin
  Result := 0;
  for x in self do
    Inc(Result);
end;

{ TUnorderedPool }

function TUnorderedPool.Add: PT;
begin

end;

procedure TUnorderedPool.Delete(item: PT);
begin

end;

function TUnorderedPool.GetEnumerator: TUnorderedEnumeratorSpec;
begin

end;

constructor TUnorderedPool.Create;
begin

end;

destructor TUnorderedPool.Destroy;
begin
  inherited Destroy;
end;

{ TUnorderedEnumerator }

function TUnorderedEnumerator.GetCurrent: T;
begin
  Result := T(ptr);
end;

function TUnorderedEnumerator.MoveNext: Boolean;
begin
  Dec(remains);
  Result := remains^ >= 0;
  ptr := Pointer(Integer(ptr) + sizeof(T^));
end;

{ TResourceEnumerator }


function TResourceEnumerator.GetCurrent: T;
type
  X = specialize TResourceArray<T>;
begin
  Result := T(X(owner).items[index]);
end;

function TResourceEnumerator.MoveNext: Boolean;
type
  X = specialize TResourceArray<T>;
begin
  Result := False;
  repeat
    Inc(Index);
    if Index >= X(owner).items.Count then exit;
  until Assigned(X(owner).items[Index]);
  Result := True;
end;

{ TResourceArray }

constructor TResourceArray.Create;
begin
  Items := TList.Create;
end;

destructor TResourceArray.Destroy;
var
  x: Pointer;
begin
  for x in items do
    T(x).Free;
  items.Free;
  inherited Destroy;
end;

function TResourceArray.AddItem(x: T): Integer;
var
  i: Integer;
begin
  for i := 0 to items.Count - 1 do
    if Items[i] = nil then
    begin
      Result := i + 1;
      items[i] := x;
      exit;
    end;
  items.Add(x);
  Result := items.Count;
end;

procedure TResourceArray.Delete(index: Integer);
begin
  T(Items[index - 1]).Free;
  Items[index - 1] := nil;
end;

function TResourceArray.Get(i: Integer): T;
begin
  Result := T(items[i - 1]);
end;

procedure TResourceArray.Clear;
var
  x: Pointer;
begin
  for x in items do
    T(x).Free;
  items.Clear;
end;

function TResourceArray.GetEnumerator: TResourceEnumeratorSpec;
begin
  Result := TResourceEnumeratorSpec.Create;
  Result.owner := Self;
  Result.Index := -1;
end;

{ TLinearPool }

function TLinearPool.LastOrNext(out reused: Boolean): T;
begin
  reused := (Used < items.Count) and (Used > 0);
  if reused then
    Result := T(Items[Used - 1])
  else
    Result := Next;
end;

function TLinearPool.Next: T;
begin
  if Used < Items.Count then
    Result := T(Items[Used])
  else
  begin
    Result := Creator();
    Items.Add(Result);
  end;
  Inc(Used);
end;

procedure TLinearPool.Reset;
begin
  Used := 0;
end;

constructor TLinearPool.Create(aCreator: TCreator);
begin
  Creator := aCreator;
  Items := TList.Create;
  Used := 0;
end;

destructor TLinearPool.Destroy;
var
  it: Pointer;
begin
  for it in Items do
    T(it).Free;
  Items.Free;
  inherited Destroy;
end;

function TLinearPool.Item(i: Integer): T;
begin
  Result := T(Items[i]);
end;

end.
