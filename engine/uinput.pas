unit uInput;

{$mode objfpc}{$H+}

interface

uses
   uEngineTypes, SDL2;

function KeyState(key: TKey): TKeyState; cdecl;
function MouseGet(axis: TMouseAxis): TCoord; cdecl;
function MouseState(btn: TMouseButton): TMouseButtonState; cdecl;

procedure UpdateInputStates;

var
  btnstates: array[TMouseButton] of TMouseButtonState;
  keypresses: array[0..512] of boolean;
  anykeypress: TKeyState;

  sdl_keys: PUInt8;
  scroll: single;
  firstquitting: boolean;
  quitting: boolean;

implementation

uses uMainEngine, uRender, ulog, uGUI;

const
  sdltable: array[TKey(0)..High(TKey)] of TSDL_ScanCode =
    (
    SDL_SCANCODE_A, SDL_SCANCODE_B, SDL_SCANCODE_C, SDL_SCANCODE_D,
    SDL_SCANCODE_E, SDL_SCANCODE_F, SDL_SCANCODE_G, SDL_SCANCODE_H,
    SDL_SCANCODE_I, SDL_SCANCODE_J, SDL_SCANCODE_K, SDL_SCANCODE_L,
    SDL_SCANCODE_M, SDL_SCANCODE_N, SDL_SCANCODE_O, SDL_SCANCODE_P,
    SDL_SCANCODE_Q, SDL_SCANCODE_R, SDL_SCANCODE_S, SDL_SCANCODE_T,
    SDL_SCANCODE_U, SDL_SCANCODE_V, SDL_SCANCODE_W, SDL_SCANCODE_X,
    SDL_SCANCODE_Y, SDL_SCANCODE_Z,
    SDL_SCANCODE_0, SDL_SCANCODE_1, SDL_SCANCODE_2, SDL_SCANCODE_3,
    SDL_SCANCODE_4, SDL_SCANCODE_5, SDL_SCANCODE_6, SDL_SCANCODE_7,
    SDL_SCANCODE_8, SDL_SCANCODE_9,
    SDL_SCANCODE_ESCAPE,
    SDL_SCANCODE_LCTRL, SDL_SCANCODE_LSHIFT, SDL_SCANCODE_LALT, SDL_SCANCODE_LGUI,
    SDL_SCANCODE_RCTRL, SDL_SCANCODE_RSHIFT, SDL_SCANCODE_RALT, SDL_SCANCODE_RGUI,
    SDL_SCANCODE_MENU,         ///< The Menu key
    SDL_SCANCODE_LEFTBRACKET,     ///< The [ key
    SDL_SCANCODE_RIGHTBRACKET,     ///< The ] key
    SDL_SCANCODE_SEMICOLON,    ///< The ; key
    SDL_SCANCODE_COMMA,        ///< The , key
    SDL_SCANCODE_PERIOD,       ///< The . key
    SDL_SCANCODE_APOSTROPHE,        ///< The ' key
    SDL_SCANCODE_SLASH,        ///< The / key
    SDL_SCANCODE_BACKSLASH,    ///< The \ key
    SDL_SCANCODE_GRAVE,        ///< The ~ key
    SDL_SCANCODE_EQUALS,        ///< The = key
    SDL_SCANCODE_MINUS,         ///< The - key
    SDL_SCANCODE_SPACE,        ///< The Space key
    SDL_SCANCODE_RETURN,       ///< The Return key
    SDL_SCANCODE_BACKSPACE,         ///< The Backspace key
    SDL_SCANCODE_TAB,          ///< The Tabulation key
    SDL_SCANCODE_PAGEUP,       ///< The Page up key
    SDL_SCANCODE_PAGEDOWN,     ///< The Page down key
    SDL_SCANCODE_END,          ///< The End key
    SDL_SCANCODE_HOME,         ///< The Home key
    SDL_SCANCODE_INSERT,       ///< The Insert key
    SDL_SCANCODE_DELETE,       ///< The Delete key
    SDL_SCANCODE_KP_PLUS,          ///< +
    SDL_SCANCODE_KP_MINUS,     ///< -
    SDL_SCANCODE_KP_MULTIPLY,     ///< *
    SDL_SCANCODE_KP_DIVIDE,       ///< /
    SDL_SCANCODE_LEFT,         ///< Left arrow
    SDL_SCANCODE_RIGHT,        ///< Right arrow
    SDL_SCANCODE_UP,           ///< Up arrow
    SDL_SCANCODE_DOWN,         ///< Down arrow
    SDL_SCANCODE_KP_0, SDL_SCANCODE_KP_1, SDL_SCANCODE_KP_2, SDL_SCANCODE_KP_3,
    SDL_SCANCODE_KP_4, SDL_SCANCODE_KP_5, SDL_SCANCODE_KP_6, SDL_SCANCODE_KP_7,
    SDL_SCANCODE_KP_8, SDL_SCANCODE_KP_9,
    SDL_SCANCODE_F1, SDL_SCANCODE_F2, SDL_SCANCODE_F3, SDL_SCANCODE_F4,
    SDL_SCANCODE_F5, SDL_SCANCODE_F6, SDL_SCANCODE_F7, SDL_SCANCODE_F8,
    SDL_SCANCODE_F9, SDL_SCANCODE_F10, SDL_SCANCODE_F11, SDL_SCANCODE_F12,
    SDL_SCANCODE_F13, SDL_SCANCODE_F14, SDL_SCANCODE_F15,
    SDL_SCANCODE_PAUSE        ///< The Pause key
    );

function KeyToSDLKey(key: TKey): TSDL_ScanCode;
begin
  Result := sdltable[key];
end;

function KeyState(key: TKey): TKeyState; cdecl;
begin
  if key = Quit then
  begin
    if firstquitting then
      Result := ksPressed
    else if quitting then
      Result := ksDown
    else
      Result := ksUp;
  end
  else if key = AnyKey then
  begin
    Result := anykeypress;
  end
  else if keypresses[KeyToSDLKey(key)] then
    Result := ksPressed
  else if sdl_keys[KeyToSDLKey(key)] <> 0 then
    Result := ksDown
  else
    Result := ksUp;
end;

function MouseX: TCoord;
var
  v: integer;
begin
  SDL_GetMouseState(@v, nil);
  Result := v;
end;

function MouseY: TCoord;
var
  v: integer;
begin
  SDL_GetMouseState(nil, @v);
  Result := v;
end;

function MouseBtnToSdl(btn: TMouseButton): integer;
begin
  case btn of
    LeftButton: Result := SDL_BUTTON_LEFT - 1;
    RightButton: Result := SDL_BUTTON_RIGHT - 1;
    MiddleButton: Result := SDL_BUTTON_MIDDLE - 1;
  end;
end;

procedure UpdateInputStates;
var
  btn: TMouseButton;
  key: TKey;
  event: TSDL_Event;
  mousestate: UInt32;
  any: boolean;
begin
  mousestate := SDL_GetMouseState(nil, nil);
  for btn in TMouseButton do
  begin
    if (mousestate and (1 shl MouseBtnToSdl(btn))) <> 0 then
    begin
      btnstates[btn] := mbsDown;
    end
    else
    begin
      if btnstates[btn] = mbsDown then
        btnstates[btn] := mbsClicked
      else
        btnstates[btn] := mbsUp;
    end;
  end;
  FillChar(keypresses, SizeOf(keypresses), 0);
  scroll := 0;
  if firstquitting then
  begin
    firstquitting := False;
    quitting := True;
  end;

  any := False;
  for key := TKey(0) to High(TKey) do
    if sdl_keys[Ord(key)] <> 0 then
    begin
      any := True;
      break;
    end;
  if any then
    anykeypress := ksDown
  else if anykeypress = ksDown then
    anykeypress := ksPressed
  else
    anykeypress := ksUp;

  while SDL_PollEvent(@event) > 0 do
    case event.type_ of
      SDL_QUITEV: FirstQuitting := True;
      SDL_KEYDOWN:
      begin
        keypresses[event.key.keysym.scancode] := True;
        ProcessEdits('', event.key.keysym.sym);
      end;
      SDL_TEXTINPUT: ProcessEdits(event.text.text, SDLK_UNKNOWN);
      SDL_MOUSEWHEEL: scroll := scroll + event.wheel.y;
    end;
end;

function MouseScroll: single;
begin
  Result := scroll;
end;

function MouseGet(axis: TMouseAxis): TCoord; cdecl;
var
  ax, ay: Single;
begin
  case axis of
    CursorX: Result := MouseX * LogicXSize / WindowXSize;
    CursorY: Result := MouseY * LogicYSize / WindowYSize;
    ScaledX:
      begin
        ax := MouseX * LogicXSize / WindowXSize;
        ay := MouseY * LogicYSize / WindowYSize;
        ReverseCam(ax,ay,ax,ay);
        Result := ax;
      end;
    ScaledY:
      begin
        ax := MouseX * LogicXSize / WindowXSize;
        ay := MouseY * LogicYSize / WindowYSize;
        ReverseCam(ax,ay,ax,ay);
        Result := ay;
      end;
    ScrollPos: Result := MouseScroll;
    else
      Result := 0
  end;
end;

function MouseState(btn: TMouseButton): TMouseButtonState; cdecl;
begin
  Result := btnstates[btn];
end;

end.
