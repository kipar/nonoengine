unit uPhysics;

{$mode objfpc}{$H+}
interface

{$IFNDEF NO_PHYSICS}

uses
  SysUtils, uEngineTypes,
  chipmunk,
  uPools, Math, uPoly, uRender;

type
  TPhysicCoordinatesMode = (Read, Write, ReadWrite, Increment);
  TBodyType = (bodyDynamic, bodyStatic, bodyKinematic, bodyNonRotating);
  TCollisionType = (Pass, Hit, PassDetect, HitDetect, Processable);
  TBodyID = Int64;
  PBodyID = ^TBodyID;
  PMaterial = ^TMaterial;
const
  NO_BODY_ID = Int64(-1);

procedure PhysicsReset; cdecl;
function BodyCreate(material: TMaterial; Data: TBodyID): TBody; cdecl;
procedure BodyFree(body: TBody); cdecl;
procedure BodyAddShapeCircle(body: TBody; dx, dy, r: TPhysicsCoord); cdecl;
procedure BodyAddShapeBox(body: TBody; x1, y1, x2, y2: TPhysicsCoord); cdecl;
procedure BodyAddShapeLine(body: TBody; x1, y1, x2, y2: TPhysicsCoord); cdecl;
procedure BodyAddShapePoly(body: TBody; poly: TPoly); cdecl;
procedure BodyApplyForce(body: TBody; fx, fy, dx, dy, torque: TPhysicsCoord); cdecl;
procedure BodyApplyControl(body: TBody; tx, ty, max_speed, max_force: TPhysicsCoord); cdecl;
procedure BodyCoords(body: TBody; Mode: TPhysicCoordinatesMode;
  x, y, vx, vy, a, omega: PPhysicsCoord); cdecl;
procedure Material(Material: TMaterial; Density, Friction, Elasticity: double;
  SpecialType: TBodyType; DefRadius: double); cdecl;
procedure MaterialCollisions(First, second: TMaterial;
  CollisionType: TCollisionType); cdecl;
function GetCollisions(Body: TBody; WithMat: TMaterial; IsFirst: PBoolean;
  x, y, nx, ny, energy, impulsex, impulsey: PPhysicsCoord): TBodyID; cdecl;
function GetMaterialCollisions(Mat: TMaterial; WithMat: TMaterial;
  Body1, Body2: PBodyID; IsFirst: PBoolean;
  x, y, nx, ny, energy, impulsex, impulsey: PPhysicsCoord): boolean; cdecl;
procedure SetCurrentCollisionResult(ShouldHit: boolean); cdecl;

procedure InitPhysics;
procedure ProcessPhysics;

procedure FreePhysics;

procedure DebugPhysicsRender;cdecl;

function RaycastLine(startx, starty, tox, toy: TPhysicsCoord; material: TMaterial; FirstOnly: Boolean; x, y : PPhysicsCoord; AMaterial: PMaterial; Body: PBodyID): Boolean; cdecl;

var
  Space: PcpSpace;
{$ENDIF}

implementation

{$IFNDEF NO_PHYSICS}

uses uLog, uMainEngine;

type

  TArbiterData = class
    typ: TCollisionType;
    WasFetched: int64;
    WasSolved, SolveResult: boolean;
    First: boolean;
  end;

  TBodyData = class;

  TCollisionInfo = record
    arbiterdata: TArbiterData;
    xy, n, impulse: cpVect;
    energy: TPhysicsCoord;
    Body2: TBodyData;
  end;

  { TMaterialData }

  TMaterialData = class
    id: integer;
    Density, Friction, Elasticity: double;
    SpecialType: TBodyType;
    DefRadius: double;
    Collisions: array of TCollisionType;
    ResolvedCategory: cpCollisionType;
    ResolvedFilter: cpShapeFilter;
    ResolvedSensor: cpBool;
    constructor Create(aid: integer);
    function AddCollision(with_mat: TMaterialData; typ: TCollisionType): boolean;
    function GetCollision(with_mat: TMaterialData): TCollisionType;
  end;


  { TBody }

  TShapes = array of PcpShape;

  { TBodyData }

  TBodyData = class
    material: TMaterialData;
    id: TBodyID;
    handle: PcpBody;
    shapes: TShapes;

    leash: PcpConstraint;
    target: PcpBody;
    MaxBias, MaxForce: TPhysicsCoord;

    oldx, oldy, oldvx, oldvy, olda, oldomega: TPhysicsCoord;
    IndicatedCollisions: array of TCollisionInfo;
    NIndicatedCollisions: integer;
    constructor Create(amat: TMaterialData; aid: TBodyID);
    procedure ReadOldData;
    procedure WriteIfChanged(ax, ay, avx, avy, aa, aomega: TPhysicsCoord);
    procedure ReapplyMaterial;
    procedure AddCircleShape(dx, dy, r: TPhysicsCoord);
    procedure AddAnyShape(shape: PcpShape);
    destructor Destroy; override;
    procedure AddIndicatedCollision(coll: TCollisionInfo);
    procedure DeleteIndicatedCollision(Data: TArbiterData);
    procedure ApplyControl(tx, ty, max_bias, max_force: TPhysicsCoord);
  end;

  TRaycastResult = record
    Body: TBodyData;
    X: TPhysicsCoord;
    Y: TPhysicsCoord;
  end;



var
  AllBodies: specialize TResourceArray<TBodyData>;
  Materials: array of TMaterialData;

procedure BuildCollisionsMap; forward;


function AngleToPhysics(a: TPhysicsCoord): TPhysicsCoord;
begin
  Result := - a * pi / 180
end;

function AngleToEngine(a: TPhysicsCoord): TPhysicsCoord;
begin
  Result := - a * 180 / pi
end;


procedure PhysicsReset; cdecl;
begin
  AllBodies.Clear;
end;

function BodyCreate(material: TMaterial; Data: TBodyID): TBody; cdecl;
begin
  if (material < 0) or (material >= Length(Materials)) or not assigned(Materials[material]) then
  begin
    logf('BodyCreate: Incorrect material index: %d', [Material]);
    Result := -1;
    exit
  end;
  Result := AllBodies.AddItem(TBodyData.Create(Materials[material], Data));
end;

procedure BodyFree(body: TBody); cdecl;
begin
  AllBodies.Delete(body);
end;

procedure BodyAddShapeCircle(body: TBody; dx, dy, r: TPhysicsCoord); cdecl;
var
  b: TBodyData;
begin
  if r <= 0 then
  begin
    logf('BodyAddShapeCircle: Radius is not positive: %f', [r]);
    exit
  end;
  AllBodies[body].AddCircleShape(dx, dy, r);
end;

procedure BodyAddShapeBox(body: TBody; x1, y1, x2, y2: TPhysicsCoord); cdecl;
var
  b: TBodyData;
  r: TPhysicsCoord;
  vertexes: array[1..4] of cpVect;
begin
  b := AllBodies[body];
  //must be CCW
  if x1 > x2 then
  begin
    r := x1;
    x1 := x2;
    x2 := r;
  end;
  if y1 > y2 then
  begin
    r := y1;
    y1 := y2;
    y2 := r;
  end;
  vertexes[1] := cpv(x1, y1);
  vertexes[2] := cpv(x1, y2);
  vertexes[3] := cpv(x2, y2);
  vertexes[4] := cpv(x2, y1);
  r := 50;//radius 5 is fine
  if x2 - x1 < r then
    r := x2 - x1;
  if y2 - y1 < r then
    r := y2 - y1;
  if r < 1 then
    r := 1;
  b.AddAnyShape(cpPolyShapeNewRaw(b.handle, 4, @vertexes, r / 10));
end;

procedure BodyAddShapeLine(body: TBody; x1, y1, x2, y2: TPhysicsCoord); cdecl;
var
  b: TBodyData;
  r: TPhysicsCoord;
begin
  b := AllBodies[body];
  if Abs(x1-x2)+abs(y1-y2) > 10 then
    r := 1
  else
    r := 5;
  b.AddAnyShape(cpSegmentShapeNew(b.handle, cpv(x1,y1), cpv(x2,y2), r));
end;

procedure BodyAddShapePoly(body: TBody; poly: TPoly); cdecl;
var
  b: TBodyData;
  p: TPolyData;
  r: TPhysicsCoord;
begin
  b := AllBodies[body];
  p := Polygons[poly];
  if not p.IsComplete then
    exit;
  p.NPoints := cpConvexHull(p.NPoints, Pointer(p.points), Pointer(p.points), nil, 1.0);
  SetLength(p.points, p.NPoints);
  r := 50;//todo - check max size
  b.AddAnyShape(cpPolyShapeNewRaw(b.handle, p.NPoints, Pointer(p.points), r / 10));
end;



procedure BodyApplyForce(body: TBody; fx, fy, dx, dy, torque: TPhysicsCoord);
  cdecl;
var
  b: TBodyData;
begin
  b := AllBodies[body];
  if (fx <> 0) or (fy <> 0) then
    cpBodyApplyForceAtLocalPoint(b.handle, cpv(fx, fy), cpv(dx, dy));
  if torque <> 0 then
    cpBodySetTorque(b.handle, cpBodyGetTorque(b.handle) + torque);
end;

function SafeValue(ptr: PPhysicsCoord; default: TPhysicsCoord): TPhysicsCoord;
begin
  if Assigned(ptr) then result := ptr^ else Result := default;
end;

procedure BodyCoords(body: TBody; Mode: TPhysicCoordinatesMode;
  x, y, vx, vy, a, omega: PPhysicsCoord); cdecl;
var
  b: TBodyData;
begin
  b := AllBodies[body];
  case mode of
    Read:
    begin
      b.ReadOldData;
      if assigned(x) then
        x^ := b.oldx;
      if assigned(y) then
        y^ := b.oldy;
      if assigned(vx) then
        vx^ := b.oldvx;
      if assigned(vy) then
        vy^ := b.oldvy;
      if assigned(a) then
        a^ := b.olda;
      if assigned(omega) then
        omega^ := b.oldomega;
    end;
    Increment:
    begin
      b.ReadOldData;
      b.WriteIfChanged(
        b.oldx + SafeValue(x, 0),
        b.oldy + SafeValue(y, 0),
        b.oldvx + SafeValue(vx, 0),
        b.oldvy + SafeValue(vy, 0),
        b.olda + SafeValue(a, 0),
        b.oldomega + SafeValue(omega, 0));
    end;
    ReadWrite:
    begin
      b.WriteIfChanged(
        SafeValue(x, b.oldx),
        SafeValue(y, b.oldy),
        SafeValue(vx, b.oldvx),
        SafeValue(vy, b.oldvy),
        SafeValue(a, b.olda),
        SafeValue(omega, b.oldomega));
      b.ReadOldData;
      if assigned(x) then
        x^ := b.oldx;
      if assigned(y) then
        y^ := b.oldy;
      if assigned(vx) then
        vx^ := b.oldvx;
      if assigned(vy) then
        vy^ := b.oldvy;
      if assigned(a) then
        a^ := b.olda;
      if assigned(omega) then
        omega^ := b.oldomega;
    end;
    Write:
    begin
      b.ReadOldData;
      b.WriteIfChanged(
        SafeValue(x, b.oldx),
        SafeValue(y, b.oldy),
        SafeValue(vx, b.oldvx),
        SafeValue(vy, b.oldvy),
        SafeValue(a, b.olda),
        SafeValue(omega, b.oldomega));
    end;
    else
      logf('BodyCoords: Incorrect mode: %d', [ord(Mode)]);
  end;
end;

procedure BodyApplyControl(body: TBody; tx, ty, max_speed,
  max_force: TPhysicsCoord); cdecl;
var
  b: TBodyData;
begin
  b := AllBodies[body];
  b.ApplyControl(tx,ty,max_speed, max_force);
end;

procedure Material(Material: TMaterial; Density, Friction, Elasticity: double;
  SpecialType: TBodyType; DefRadius: double); cdecl;
var
  body: TBodyData;
  mat: TMaterialData;
  need_change: boolean;
begin
  if Material < 0 then
  begin
    Log('Material: negative material index: ' + IntToStr(Material));
    exit;
  end;
  if not InRange(ord(SpecialType), 0, Ord(High(TBodyType))) then
  begin
    logf('Material: Incorrect SpecialType: %d', [ord(SpecialType)]);
    exit
  end;
  if Material >= Length(Materials) then
    SetLength(Materials, Material + 1);
  if Materials[Material] = nil then
  begin
    Materials[Material] := TMaterialData.Create(Material);
    need_change := False;
  end
  else
    need_change := True;
  mat := Materials[Material];

  need_change := need_change and ((Density <> mat.Density) or
    (Friction <> mat.Friction) or (Elasticity <> mat.Elasticity));
  mat.Density := Density;
  mat.Friction := Friction;
  mat.Elasticity := Elasticity;
  mat.SpecialType := SpecialType;
  mat.DefRadius := DefRadius;

  if need_change then
    for body in AllBodies do
      if body.material = mat then
        body.ReapplyMaterial;
end;

procedure MaterialCollisions(First, Second: TMaterial;
  CollisionType: TCollisionType); cdecl;
var
  changed: boolean;
begin
  if not InRange(First, 0, Length(Materials)-1) or not Assigned(Materials[First]) then
  begin
    Log('MaterialCollisions: wrong first material index: ' + IntToStr(First));
    exit;
  end;
  if not InRange(Second, 0, Length(Materials)-1) or not Assigned(Materials[Second]) then
  begin
    Log('MaterialCollisions: wrong second material index: ' + IntToStr(Second));
    exit;
  end;
  if not InRange(ord(CollisionType), 0, Ord(High(TCollisionType))) then
  begin
    logf('BodyCoords: Incorrect CollisionType: %d', [ord(CollisionType)]);
    exit
  end;
  //TODO - AnyMaterial
  if First >= Second then
    changed := Materials[First].AddCollision(Materials[Second], CollisionType)
  else
    changed := Materials[Second].AddCollision(Materials[First], CollisionType);
  //if Assigned(space) and changed then
  //  BuildCollisionsMap;
end;


var
  CollisionResults: array of TCollisionInfo;
  CheckedIndicatedCollisions, CheckedCollisions, NCollisions: integer;
  PrevBody: TBody;
  PrevMat: TMaterial;
  NeedXY, NeedN, NeedEnergy, NeedImpulse: boolean;

procedure IterateBodyArbiters(body: PcpBody; arbiter: PcpArbiter; Data: pointer); stdcall;
var
  our, other: PcpBody;
  mat: TMaterial;
  res: ^TCollisionInfo;
begin
  mat := TMaterial(IntPtr(Data));
  if not assigned(arbiter^.Data) then
    exit;
  if TArbiterData(arbiter^.Data).WasFetched = GlobalTick then
    exit;
  if mat <> ANY_MATERIAL then
  begin
    cpArbiterGetBodies(arbiter, @our, @other);
    if TBodyData(other^.userData).material.id <> mat then
      exit;
  end;
  Inc(NCollisions);
  if NCollisions > Length(CollisionResults) then
    SetLength(CollisionResults, Length(CollisionResults) * 2);
  res := @CollisionResults[NCollisions - 1];
  res^.arbiterdata := TArbiterData(arbiter^.Data);
  res^.Body2 := TBodyData(other^.userData);
  res^.arbiterdata.First := cpArbiterIsFirstContact(arbiter);
  if NeedXY then
    res^.xy := cpBodyLocalToWorld(body, cpArbiterGetPointA(arbiter, 0));
  if NeedN then
    res^.n := cpArbiterGetNormal(arbiter);
  if NeedEnergy then
    res^.energy := cpArbiterTotalKE(arbiter);
  if NeedImpulse then
    res^.impulse := cpArbiterTotalImpulse(arbiter);
end;

function GetCollisions(Body: TBody; WithMat: TMaterial; IsFirst: PBoolean;
  x, y, nx, ny, energy, impulsex, impulsey: PPhysicsCoord): TBodyID; cdecl;
var
  abody: TBodyData;
  coll: ^TCollisionInfo;
  ok: boolean;

  function ReturnCollision(const coll: TCollisionInfo): TBodyID;
  begin
    coll.arbiterdata.WasFetched := GlobalTick;
    if assigned(x) then
      x^ := coll.xy.x;
    if assigned(y) then
      y^ := coll.xy.y;
    if assigned(nx) then
      nx^ := coll.n.x;
    if assigned(ny) then
      ny^ := coll.n.y;
    if Assigned(energy) then
      energy^ := coll.energy;
    if assigned(impulsex) then
      impulsex^ := coll.impulse.x;
    if assigned(impulsey) then
      impulsey^ := coll.impulse.y;
    Result := coll.Body2.id;
    if Assigned(IsFirst) then
      IsFirst^ := coll.arbiterdata.First;
    coll.arbiterdata.First := False;
  end;

begin
  if (Body <> PrevBody) or (WithMat <> PrevMat) then
  begin
    if not InRange(WithMat, 0, Length(Materials)-1) or not assigned(Materials[WithMat]) then
    begin
      logf('GetCollisions: Incorrect material index: %d', [WithMat]);
      Result := NO_BODY_ID;
      exit
    end;

    NeedXY := assigned(x) or Assigned(y);
    NeedN := Assigned(nx) or Assigned(ny);
    NeedImpulse := Assigned(impulsex) or Assigned(impulsey);
    NeedEnergy := Assigned(energy);
    NCollisions := 0;
    CheckedCollisions := 0;
    CheckedIndicatedCollisions := 0;
    //TODO - replace cpBodyEachArbiter with pascal code
    cpBodyEachArbiter(AllBodies[body].handle, @IterateBodyArbiters,
      Pointer(Ord(WithMat)));
  end;
  PrevBody := Body;
  PrevMat := WithMat;
  repeat
    if CheckedCollisions >= NCollisions then
    begin
      //let's check indicated collisions
      abody := AllBodies[body];
      ok := False;
      repeat
        if CheckedIndicatedCollisions >= abody.NIndicatedCollisions then
          break;
        coll := @abody.IndicatedCollisions[CheckedIndicatedCollisions];
        Inc(CheckedIndicatedCollisions);
        ok := (WithMat = ANY_MATERIAL) or (coll^.Body2.material.id = WithMat);
        ok := ok or (coll^.arbiterdata.WasFetched < GlobalTick)
      until ok;
      if ok then
        Result := ReturnCollision(coll^)
      else
        Result := NO_BODY_ID;
      exit;
    end;
    coll := @CollisionResults[CheckedCollisions];
    Inc(CheckedCollisions);
  until coll^.arbiterdata.WasFetched < GlobalTick;
  Result := ReturnCollision(coll^);
end;

function GetMaterialCollisions(Mat: TMaterial; WithMat: TMaterial;
  Body1, Body2: PBodyID; IsFirst: PBoolean;
  x, y, nx, ny, energy, impulsex, impulsey: PPhysicsCoord): boolean; cdecl;
var
  i: integer;
  b1: TBodyData;
  b2: TBodyID;
begin
  Result := False;
  if not InRange(Mat, 0, Length(Materials)-1) or not assigned(Materials[Mat]) then
  begin
    logf('GetMaterialCollisions: Incorrect first material index: %d', [Mat]);
    exit
  end;
  if not InRange(WithMat, 0, Length(Materials)-1) or not assigned(Materials[WithMat]) then
  begin
    logf('GetMaterialCollisions: Incorrect second material index: %d', [WithMat]);
    exit
  end;

  i := 0;
  for b1 in AllBodies do
  begin
    inc(i);
    if b1.material.id <> mat then
      continue;
    b2 := GetCollisions(i-1, WithMat, IsFirst, x, y, nx, ny, energy, impulsex, impulsey);
    if b2 <> NO_BODY_ID then
    begin
      Result := True;
      if assigned(Body1) then
        Body1^ := b1.id;
      if assigned(Body2) then
        Body2^ := b2;
      exit;
    end;
  end;
end;

procedure SetCurrentCollisionResult(ShouldHit: boolean); cdecl;
var
  Data: TArbiterData;
  abody: TBodyData;
begin
  if PrevBody <= 0 then
    exit;
  abody := AllBodies[PrevBody];
  if CheckedIndicatedCollisions = 0 then
    exit;
  if CheckedIndicatedCollisions - 1 >= abody.NIndicatedCollisions then
    exit;
  Data := abody.IndicatedCollisions[CheckedIndicatedCollisions - 1].arbiterdata;
  if (Data.typ <> Processable) or Data.WasSolved then
    exit;
  Data.SolveResult := ShouldHit;
  Data.WasSolved := True;
end;

procedure InitPhysics;
begin
  Space := cpHastySpaceNew;
  cpSpaceSetGravity(space, cpv(PARAMS[GravityX] / 1000, PARAMS[GravityY] / 1000));
  cpSpaceSetDamping(space, PARAMS[Damping] / 1000);
  cpSpaceSetIterations(space, 4);
  cpHastySpaceSetThreads(space, 2);
  logf('Chipmunk physics uses %d threads', [cpHastySpaceGetThreads(space)]);
  SetLength(CollisionResults, 128);
  AllBodies := specialize TResourceArray<TBodyData>.Create;
  //BuildCollisionsMap;
end;

var
  passed: Double;
  map_built : Boolean = False;
procedure ProcessPhysics;
begin
  if not map_built then
  begin
    BuildCollisionsMap;
    map_built := True;
  end;

  PrevBody := -1;
  if PARAMS[PhysicsSpeed] <= 0 then exit;
  passed := passed + PARAMS[PhysicsSpeed];
  while passed > 1000 do
  begin
    cpHastySpaceStep(Space, 1 / 60);
    passed := passed - 1000;
  end;
end;

procedure FreePhysics;
var
  mat: TMaterialData;
begin
  AllBodies.Free;
  for mat in Materials do
    mat.Free;
  cpHastySpaceFree(Space);
  SetLength(Materials, 0);
end;

procedure DebugPhysicsRender; cdecl;
var
  body: TBodyData;
  shape: PcpShape;
  acircle: PcpCircleShape;
  asegment: PcpSegmentShape;
  apoly: PcpPolyShape;
  i: integer;
  v0 : cpVect;
begin
  for body in AllBodies do
  begin
    for shape in body.shapes do
    begin
      case shape^.klass^._type of
        CP_CIRCLE_SHAPE:
        begin
          acircle := PcpCircleShape(shape);
          Ellipse(acircle^.tc.x, acircle^.tc.y, acircle^.r, acircle^.r,false, $FF0000FF, $FF0000FF, 0);
        end;
        CP_SEGMENT_SHAPE:
        begin
          asegment := PcpSegmentShape(shape);
          Line(asegment^.ta.x, asegment^.ta.y, asegment^.tb.x, asegment^.b.y, $FF0000FF, $FF0000FF);
        end;
        CP_POLY_SHAPE:
        begin
          apoly := PcpPolyShape(shape);
          v0 :=apoly^.planes[apoly^.count-1].v0;
          for i := 0 to apoly^.count-1 do
          begin
            Line(v0.x, v0.y, apoly^.planes[i].v0.x, apoly^.planes[i].v0.y, $FF0000FF, $FF0000FF);
            v0 := apoly^.planes[i].v0;
          end;
        end;
      end;
    end;

  end;

end;


var
  old_startx,
  old_starty,
  old_tox,
  old_toy : TPhysicsCoord;
  results: array of TRaycastResult;
  results_count: Integer = 0;
  stop_after_first: Boolean;
  first_alpha : cpFloat = 0;
  results_iterated: Integer = 0;


procedure SaveRaycast(shape:PcpShape; point:cpVect; normal:cpVect; alpha:cpFloat; data:pointer);stdcall;
var
  needed_mat, other_mat: TMaterialData;
begin
  needed_mat := TMaterialData(data);
  other_mat := Materials[TBodyData(shape^.body^.userData).material.id];
  if needed_mat.GetCollision(other_mat) = Pass then exit;
  //log('raycast coll: '+IntToStr(needed_mat.id)+' and '+IntToStr(other_mat.id));
  if results_count >= Length(results) then
    SetLength(results, Max(16, Length(results)*2));

  //log(FloatToStr(alpha));
  alpha := Hypot(point.x-old_startx, point.y-old_starty);
  if stop_after_first and (results_count > 0) and (alpha > first_alpha) then exit;
  first_alpha := alpha;

  results[results_count].Body := TBodyData(shape^.body^.userData);
  results[results_count].X := point.x;
  results[results_count].Y := point.Y;
  if (results_count = 0) or not stop_after_first then inc(results_count);
end;

function RaycastLine(startx, starty, tox, toy : TPhysicsCoord;
  material : TMaterial; FirstOnly: Boolean; x, y : PPhysicsCoord; AMaterial : PMaterial;
  Body : PBodyID) : Boolean; cdecl;
var
  aout: cpSegmentQueryInfo;
  ashape: PcpShape;
  req: TRaycastResult;
  TheMaterial: TMaterialData;
begin
  Result := False;
  TheMaterial := Materials[Material];
  if FirstOnly or (old_startx <> startx) or (old_starty <> starty) or (old_tox <> tox) or (old_toy <> toy) then
  begin
    results_count := 0;
    results_iterated := 0;
    if FirstOnly then
    begin
      if Assigned(cpSpaceSegmentQueryFirst(Space, cpv(startx, starty), cpv(tox, toy), TheMaterial.DefRadius, TheMaterial.ResolvedFilter, @aout)) then
      begin
        results_count := 1;
        if results_count >= Length(results) then
          SetLength(results, Max(16, Length(results)*2));
        results[0].Body := TBodyData(aout.shape^.body^.userData);
        results[0].X := aout.point.x;
        results[0].Y := aout.point.Y;
      end;
    end
    else
    begin
      stop_after_first := FirstOnly;
      first_alpha := Infinity;
      old_startx := startx;
      old_starty := starty;
      old_tox := tox;
      old_toy := toy;
      cpSpaceSegmentQuery(Space, cpv(startx, starty), cpv(tox, toy), TheMaterial.DefRadius, TheMaterial.ResolvedFilter, @SaveRaycast, TheMaterial);
    end;
    //log('best: '+FloatToStr(first_alpha));
  end;
  if results_iterated >= results_count then
    exit;
  req := results[results_iterated];
  inc(results_iterated);
  x^ := req.X;
  y^ := req.Y;
  Body^ := req.Body.id;
  AMaterial^ := req.Body.material.id;
  Result := True;
end;


procedure IndicateCollision(arb: PcpArbiter);
var
  body1, body2: TBodyData;
  coll: TCollisionInfo;
begin
  body1 := TBodyData(arb^.body_a^.userData);
  body2 := TBodyData(arb^.body_b^.userData);
  coll.arbiterdata := TArbiterData(arb^.Data);
  coll.xy := cpArbiterGetPointA(arb, 0);
  coll.Body2 := body2;
  body1.AddIndicatedCollision(coll);
  coll.Body2 := body1;
  body2.AddIndicatedCollision(coll);
end;

procedure DeleteIndicatedCollision(arb: PcpArbiter);
var
  body1, body2: TBodyData;
  Data: TArbiterData;
begin
  body1 := TBodyData(arb^.body_a^.userData);
  body2 := TBodyData(arb^.body_b^.userData);
  Data := TArbiterData(arb^.Data);
  body1.DeleteIndicatedCollision(Data);
  body2.DeleteIndicatedCollision(Data);
end;


function JustTrue(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer): cpBool; stdcall;
begin
  Result := True;
end;

function CreateCollision(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer): cpBool; stdcall;
var
  c: TArbiterData;
begin
  Result := True;
  c := TArbiterData.Create;
  c.First := True;
  arb^.Data := c;
  c.typ := TCollisionType(IntPtr(userData));
end;

function CreateAndIndicateCollision(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer): cpBool; stdcall;
begin
  Result := CreateCollision(arb, space, userData);
  IndicateCollision(arb);
end;

procedure FreeCollision(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer); stdcall;
begin
  TArbiterData(arb^.Data).Free;
end;

function JustFalse(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer): cpBool; stdcall;
begin
  Result := False;
end;


procedure FreeAndDeleteIndicatedCollision(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer); stdcall;
begin
  DeleteIndicatedCollision(arb);
  TArbiterData(arb^.Data).Free;
end;


function ProcessAndDecide(arb: PcpArbiter; space: PcpSpace;
  userData: cpDataPointer): cpBool; stdcall;
begin
  if (Assigned(arb^.Data) and TArbiterData(arb^.Data).WasSolved) then
    Result := TArbiterData(arb^.Data).SolveResult
  else
    Result := False;
end;

procedure BuildCollisionsMap;
var
  x: PcpCollisionHandler;
  mat1, mat2: TMaterialData;
  typ: TCollisionType;
  mask : cpBitmask;
begin
  //TODO - groups, categories
  //Log('Building collisions map');
  //TODO - cpSpaceAddDefaultCollisionHandler
  //TODO - cpSpaceAddWildcardHandler
  for mat1 in Materials do
  begin
    mask := 0;
    for mat2 in Materials do
      if mat1.GetCollision(mat2) <> Pass then
        inc(mask, 1 shl mat2.ResolvedCategory);
    mat1.ResolvedFilter.mask := mask;
    mat1.ResolvedFilter.categories := 1 << mat1.id;
  end;
  for mat2 in Materials do
  begin
    if not Assigned(mat2) then
      continue;
    for mat1 in Materials do
    begin
      if not Assigned(mat1) then
        continue;
      if mat1.id > mat2.id then
        break;
      x := cpSpaceAddCollisionHandler(space, mat1.ResolvedCategory,
        mat2.ResolvedCategory);
      typ := mat2.GetCollision(mat1);
      //log(Format('coll %d with %d : %d', [mat1.id, mat2.id, ord(typ)]));
      case typ of
        //Pass, Hit, PassDetect, HitDetect, Processable
        Pass: x^.beginFunc := @JustFalse;
         Hit:
        begin
          x^.beginFunc := @JustTrue;
          x^.preSolveFunc := @JustTrue;
        end;
        PassDetect:
        begin
          x^.userData := Pointer(Ord(PassDetect));
          x^.beginFunc := @CreateAndIndicateCollision;
          x^.separateFunc := @FreeAndDeleteIndicatedCollision;
          x^.preSolveFunc := @JustFalse;
        end;
        HitDetect:
        begin
          x^.userData := Pointer(Ord(HitDetect));
          x^.beginFunc := @CreateCollision;
          x^.separateFunc := @FreeCollision;
          x^.preSolveFunc := @JustTrue;
        end;
        Processable:
        begin
          x^.userData := Pointer(Ord(Processable));
          x^.beginFunc := @CreateAndIndicateCollision;
          x^.separateFunc := @FreeAndDeleteIndicatedCollision;
          x^.preSolveFunc := @ProcessAndDecide;
        end;
      end;
    end;
  end;
end;

constructor TMaterialData.Create(aid: integer);
begin
  id := aid;
  ResolvedCategory := id;
  ResolvedFilter.categories := $FFFFFFFF;
  ResolvedFilter.group := 0;
  ResolvedFilter.mask := $FFFFFFFF;
  ResolvedSensor := False;
end;

function TMaterialData.AddCollision(with_mat: TMaterialData;
  typ: TCollisionType): boolean;
begin
  if Length(Collisions) <= with_mat.id then
    SetLength(Collisions, with_mat.id + 1);
  Result := Collisions[with_mat.id] <> typ;
  if Result then
    Collisions[with_mat.id] := typ;
end;

function TMaterialData.GetCollision(with_mat: TMaterialData): TCollisionType;
begin
  if with_mat.id > id then
    Result := with_mat.GetCollision(self)
  else if Length(Collisions) <= with_mat.id then
    Result := TCollisionType(0)
  else
    Result := Collisions[with_mat.id];
end;

constructor TBodyData.Create(amat: TMaterialData; aid: TBodyID);
begin
  //log('body with mat'+IntToStr(amat.id));
  id := aid;
  SetLength(IndicatedCollisions, 8);
  material := amat;
  case material.SpecialType of
    bodyStatic: handle := cpBodyNewStatic;
    bodyKinematic: handle := cpBodyNewKinematic;
    bodyDynamic, bodyNonRotating: handle := cpBodyNew(0, 0);
  end;
  cpBodySetUserData(handle, self);
  handle := cpSpaceAddBody(space, handle);
  if material.DefRadius > 0 then
    AddCircleShape(0, 0, material.DefRadius);
end;

procedure TBodyData.ReadOldData;
var
  v: cpVect;
begin
  v := cpBodyGetPosition(handle);
  oldx := v.x;
  oldy := v.y;
  v := cpBodyGetVelocity(handle);
  oldvx := v.x;
  oldvy := v.y;
  //cpBodyGetAngle();
  olda := AngleToEngine(handle^.a);
  //cpBodyGetAngularVelocity();
  oldomega := handle^.w;
end;

procedure TBodyData.WriteIfChanged(ax, ay, avx, avy, aa, aomega: TPhysicsCoord);
var
  v: cpVect;
begin
  if (ax <> oldx) or (ay <> oldy) then
  begin
    v.x := ax;
    v.y := ay;
    cpBodySetPosition(handle, v);
    cpSpaceReindexShapesForBody(space, handle);
  end;
  if (avx <> oldvx) or (avy <> oldvy) then
  begin
    v.x := avx;
    v.y := avy;
    cpBodySetVelocity(handle, v);
  end;
  if aa <> olda then
    cpBodySetAngle(handle, AngleToPhysics(aa));
  if aomega <> oldomega then
    cpBodySetAngularVelocity(handle, aomega);
end;

procedure TBodyData.ReapplyMaterial;
var
  shape: PcpShape;
begin
  case material.SpecialType of
    bodyStatic: cpBodySetType(handle, CP_BODY_TYPE_STATIC);
    bodyKinematic: cpBodySetType(handle, CP_BODY_TYPE_KINEMATIC);
    bodyDynamic, bodyNonRotating: cpBodySetType(handle, CP_BODY_TYPE_DYNAMIC);
  end;
  for shape in shapes do
  begin
    if material.SpecialType in [bodyDynamic, bodyNonRotating] then
      cpShapeSetDensity(shape, material.Density);
    cpShapeSetElasticity(shape, material.Elasticity);
    cpShapeSetFriction(shape, material.Friction);
  end;
  if material.SpecialType = bodyNonRotating then
    cpBodySetMoment(handle, 100000);
end;

procedure TBodyData.AddCircleShape(dx, dy, r: TPhysicsCoord);
begin
  AddAnyShape(cpCircleShapeNew(handle, r, cpv(dx, dy)));
end;

procedure TBodyData.AddAnyShape(shape: PcpShape);
begin
  if material.SpecialType in [bodyDynamic, bodyNonRotating] then
    cpShapeSetDensity(shape, material.Density);
  cpShapeSetElasticity(shape, material.Elasticity);
  cpShapeSetFriction(shape, material.Friction);
  cpShapeSetCollisionType(shape, material.ResolvedCategory);
  cpShapeSetFilter(shape, material.ResolvedFilter);
  cpShapeSetSensor(shape, material.ResolvedSensor);
  cpShapeSetUserData(shape, cpDataPointer(id));
  cpSpaceAddShape(Space, shape);
  Insert(shape, shapes, MAXINT);
  if material.SpecialType = bodyNonRotating then
    cpBodySetMoment(handle, Infinity);
end;

destructor TBodyData.Destroy;
var
  sh: PcpShape;
begin
  for sh in shapes do
  begin
    cpSpaceRemoveShape(Space, sh);
    cpShapeFree(sh);
  end;
  if Assigned(leash) then
  begin
    cpSpaceRemoveConstraint(Space, leash);
    cpSpaceRemoveBody(Space, target);
    cpConstraintFree(leash);
    cpBodyFree(target);
  end;
  cpSpaceRemoveBody(Space, handle);
  cpBodyFree(handle);
  inherited Destroy;
end;

procedure TBodyData.AddIndicatedCollision(coll: TCollisionInfo);
begin
  Inc(NIndicatedCollisions);
  if NIndicatedCollisions > Length(IndicatedCollisions) then
    SetLength(IndicatedCollisions, Length(IndicatedCollisions) * 2);
  IndicatedCollisions[NIndicatedCollisions - 1] := coll;
end;

procedure TBodyData.DeleteIndicatedCollision(Data: TArbiterData);
var
  i: integer;
begin
  for i := 0 to NIndicatedCollisions - 1 do
    if IndicatedCollisions[I].arbiterdata = Data then
    begin
      IndicatedCollisions[i] := IndicatedCollisions[NIndicatedCollisions - 1];
      Dec(NIndicatedCollisions);
      break;
    end;
end;

procedure TBodyData.ApplyControl(tx, ty, max_bias, max_force: TPhysicsCoord);
begin
  if not Assigned(leash) then
  begin
    target := cpBodyNewKinematic;
    cpBodySetPosition(target, cpBodyGetPosition(handle));
    cpSpaceAddBody(space, target);
    leash := cpPivotJointNew2(handle, target, cpv(0,0), cpv(0,0));
    cpConstraintSetMaxBias(leash, 0);
    cpConstraintSetMaxForce(leash, 0);
    cpSpaceAddConstraint(Space, leash);
  end;
  if max_bias <> MaxBias then
  begin
    MaxBias := max_bias;
    cpConstraintSetMaxBias(leash, max_bias);
  end;
  if max_force <> MaxForce then
  begin
    MaxForce := max_force;
    cpConstraintSetMaxForce(leash, max_force);
  end;
  cpBodySetPosition(target, cpv(tx,ty));
end;

{$ENDIF}

end.
