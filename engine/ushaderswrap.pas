unit uShadersWrap;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, ushaderprocess;

procedure ShaderActivate(ShaderID: TShaderID); cdecl;
function ShaderHandle(ShaderID: TShaderID): cardinal; cdecl;
procedure UniformSetInt(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: integer); cdecl;
procedure UniformSetFloat(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: single); cdecl;
procedure UniformSetTexture(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: TSprite); cdecl;
procedure UniformSetPtr(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: Pointer); cdecl;


implementation

uses uLoader, uRender, gl;

procedure ShaderActivate(ShaderID: TShaderID); cdecl;
var
  Shader: TShader;
begin
  Shader := RESShader(ShaderID);
  if Shader = CurShader then
    exit;
  FlushRenderQueues;
  Shader.Activate;
end;

function ShaderHandle(ShaderID: TShaderID): cardinal; cdecl;
var
  Shader: TShader;
begin
  Shader := RESShader(ShaderID);
  Result := Shader.handle;
end;


procedure UniformSetInt(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: integer); cdecl;
var
  Shader: TShader;
  n: integer;
begin
  if ShaderID = ALL_SHADERS then
  begin
    DefaultUniformValues[Uniform] := SetIntValue(Value);
    exit;
  end;
  Shader := RESShader(ShaderID);
  n := Shader.GetUniformIndex(Uniform, typInt);
  if n < 0 then
    exit;
  if Shader = CurShader then
    FlushRenderQueues;
  Shader.uniform_values[n] := SetIntValue(Value);
  if Shader = CurShader then
    Shader.ApplyUniform(Uniform);
end;

procedure UniformSetFloat(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: single); cdecl;
var
  Shader: TShader;
  n: integer;
begin
  if ShaderID = ALL_SHADERS then
  begin
    DefaultUniformValues[Uniform] := SetFloatValue(Value);
    exit;
  end;
  Shader := RESShader(ShaderID);
  n := Shader.GetUniformIndex(Uniform, typFloat);
  if n < 0 then
    exit;
  if Shader = CurShader then
    FlushRenderQueues;
  Shader.uniform_values[n] := SetFloatValue(Value);
  if Shader = CurShader then
    Shader.ApplyUniform(Uniform);
end;

procedure UniformSetTexture(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: TSprite); cdecl;
var
  Shader: TShader;
  n: integer;
begin
  if ShaderID = ALL_SHADERS then
  begin
    DefaultUniformValues[Uniform] := SetSamplerValue(RESSprite(Value));
    exit;
  end;
  Shader := RESShader(ShaderID);
  n := Shader.GetUniformIndex(Uniform, typSampler);
  if n < 0 then
    exit;
  if Shader = CurShader then
    FlushRenderQueues;
  Shader.uniform_values[n] := SetSamplerValue(RESSprite(Value));
  if Shader = CurShader then
    Shader.ApplyUniform(Uniform);
end;

procedure UniformSetPtr(ShaderID: TShaderID; Uniform: TShaderUniform;
  Value: Pointer); cdecl;
var
  Shader: TShader;
  n: integer;
begin
  if ShaderID = ALL_SHADERS then
  begin
    DefaultUniformValues[Uniform] := SetPointerValue(Value);
    exit;
  end;
  Shader := RESShader(ShaderID);
  n := Shader.GetUniformIndex(Uniform, typPointer);
  if n < 0 then
    exit;
  if Shader = CurShader then
    FlushRenderQueues;
  Shader.uniform_values[n] := SetPointerValue(Value);
  if Shader = CurShader then
    Shader.ApplyUniform(Uniform);
end;



end.
