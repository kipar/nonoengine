unit uPathfind;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, uLog;

function Pathfind(SizeX, SizeY: integer; algorithm: TPathfindAlgorithm;
  diagonal_cost: single; fromx, fromy, tox, toy: integer; out x: integer;
  out y: integer; callback: TPathfindCallback; opaque: pointer): boolean; cdecl;
function PathFindUnreachable(out x : Integer; out y : Integer): Boolean;cdecl;

procedure FreePathfind;

implementation

uses uDijkstra, uAStar, uPathfinder;

var
  AStar: TAStar;
  Dijkstra: TDijkstra;

function Pathfind(SizeX, SizeY: integer; algorithm: TPathfindAlgorithm;
  diagonal_cost: single; fromx, fromy, tox, toy: integer; out x: integer;
  out y: integer; callback: TPathfindCallback; opaque: pointer): boolean; cdecl;
var
  path: TPath;
  w: TDijkstraRecord;
begin
  Result := False;
  if (algorithm in [AStarNew, AStarReuse]) and ((AStar = nil) or
    (SizeX <> AStar.SizeX) or (SizeY <> AStar.SizeY) or
    (diagonal_cost <> AStar.Diagonal) or (callback <> AStar.Callback) or
    (opaque <> AStar.Opaque)) then
  begin
    AStar.Free;
    AStar := TAStar.Create(SizeX, SizeY, diagonal_cost);
    AStar.InitForCallback(opaque, callback);
  end;
  if (algorithm in [DijkstraNew, DijkstraReuse]) and
    ((Dijkstra = nil) or (SizeX <> Dijkstra.SizeX) or (SizeY <> Dijkstra.SizeY) or
    (diagonal_cost <> Dijkstra.Diagonal) or (callback <> Dijkstra.Callback) or
    (opaque <> Dijkstra.Opaque)) then
  begin
    Dijkstra.Free;
    Dijkstra := TDijkstra.Create(SizeX, SizeY, diagonal_cost);
    Dijkstra.InitForCallback(opaque, callback);
  end;
  case algorithm of
    AStarNew:
    begin
      path := AStar.GCPath(opaque);
      Result := AStar.Calculate(fromx, fromy, tox, toy, path);
      if Result then
        Result := path.Walk(x, y, False);
    end;
    AStarReuse:
    begin
      path := AStar.GCPath(opaque);
      if (not path.Empty) and (path.Data[Length(path.Data) - 1].x = fromx) and
        (path.Data[Length(path.Data) - 1].y = fromy) then
      begin
        Result := path.Walk(x, y, False);
        exit;
      end;
      Result := AStar.Calculate(fromx, fromy, tox, toy, path);
      if Result then
        Result := path.Walk(x, y, False);
    end;
    DijkstraNew, DijkstraReuse:
    begin
      //Logf('before calc %d, %d',[tox, toy]);
      if (algorithm = DijkstraNew) or (tox <> Dijkstra.DestX) or
        (toy <> Dijkstra.DestY) then
        Dijkstra.CalcMap(tox, toy);
      //Logf('before calc %d, %d',[fromx, fromy]);
      if Dijkstra.WaveData[fromx, fromy].length = 0 then
      begin
        Result := False;
        exit;
      end;
      Result := True;
      w := Dijkstra.WaveData[fromx, fromy];
      x := fromx + w.NextX;
      y := fromy + w.NextY;
    end;
  end;
end;

function PathFindUnreachable(out x : Integer; out y : Integer) : Boolean; cdecl;
begin
  if not assigned(Dijkstra) then
  begin
    x := -1;
    y := -1;
    Result := True;
  end;
  Result := Dijkstra.FindUnreachable(x, y);
end;

procedure FreePathfind;
begin
  AStar.Free;
  Dijkstra.Free;
end;

end.
