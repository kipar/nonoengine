unit uRender;

{$mode objfpc}{$H+}

interface

uses
   uEngineTypes, uPools, gl, glext, ushaderprocess,
  uVertexList, uTextures;

procedure Sprite(sprite: TSprite; x, y: TCoord; kx, ky, angle: single;
  Color: TColor); cdecl;
procedure SpriteSliced(sprite: TSprite; x, y: TCoord; w, h: single; Color: TColor); cdecl;
procedure SpriteSlicedInternal(sprite: TGenericTexture; x, y: TCoord; w, h: single; Color: TColor);
procedure DrawTiled(tiled: TTileMap; frame: integer; x, y: TCoord;
  kx, ky, angle: single; Color: TColor); cdecl;
procedure Background(sprite: TSprite; kx, ky, dx, dy: single; Color: TColor); cdecl;
procedure Line(x1, y1, x2, y2: TCoord; color1, color2: TColor); cdecl;
procedure LineSettings(Width: single; Stipple: cardinal; StippleScale: single); cdecl;
procedure Ellipse(x, y, rx, ry: TCoord; filled: boolean; color1, color2: TColor;
  angle: single); cdecl;
procedure Rect(x0, y0, w, h: TCoord; filled: boolean;
  Color1, Color2, Color3, Color4: TColor; angle: single); cdecl;
procedure Point(x, y: TCoord; color: TColor); cdecl;
procedure Triangle(x1, y1: TCoord; color1: TColor; x2, y2: TCoord;
  color2: TColor; x3, y3: TCoord; color3: TColor); cdecl;
procedure TexturedTriangle(sprite: TSprite;
  x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3: TCoord); cdecl;
procedure PolygonDraw(poly: TPoly; X, Y, Angle: TCoord; c: TColor; sprite: TSprite; dx: TCoord;
  dy: TCoord; kx: TCoord; ky: TCoord); cdecl;

procedure SetLayer(z: integer); cdecl;
procedure Camera(dx, dy: TCoord; kx, ky, angle: single); cdecl;


type
  TSpritesPool = specialize TLinearPool<TSpriteList>;

  { TTileMapResource }

  TTileMapResource = class
    tex: TSimpleTexture;
    h, w, nx: integer;
    constructor Create(atex: TSimpleTexture; anx, any: integer);
  end;


var
  DefaultShader: TShader;
  SplitSprites: TSpritesPool;

  CurLayer: integer;
  CurLineWidth: single = 1;
  LineWidthChanged: boolean;

const
  CIRCLE_COUNT = 32;

procedure FlushRenderQueues;
procedure PrepareRenderState;
procedure InitRender;


function NextOrSame(typ: GLEnum = GL_TRIANGLES; tex: TGenericTexture = nil): TSpriteList;
procedure ResetView;
function CamVertex(ax, ay, tx, ty: single; Color: cardinal): TSpriteVertex; overload;
function CamVertex(ax, ay: single; Color: cardinal): TSpriteVertex; overload;
function LayerToZ(layer: integer): single;
procedure SetDefaulUniforms;
procedure ReverseCam(ax, ay: single; out x : Single; out y : Single);

procedure FreeRender;

type
  TCamera = record
    kx, ky, kxy, kyx, dx, dy: TCoord;
  end;

var
  CurCamera: TCamera;


implementation

uses uLoader, uMainEngine, uLog, uText, uTextureWrap, uPoly;

function LayerToZ(layer: integer): single;
begin
  Result := 1 - (layer + 1) / 200;
  //z = 0   -> 1-e
  //z = 100 -> 0.5-e
  //z = 200 -> 0.0-e
end;

function CamVertex(ax, ay, tx, ty: single; Color: cardinal): TSpriteVertex; overload;
begin
  if CurLayer >= LAYER_GUI then
    Result := Vertex(ax, ay, LayerToZ(CurLayer), tx, ty, 0, Color)
  else
    Result := Vertex(ax * CurCamera.kx + ay * CurCamera.kyx + CurCamera.dx,
      ax * CurCamera.kxy + ay * CurCamera.ky + CurCamera.dy,
      LayerToZ(CurLayer), tx, ty, 0, Color);
end;

procedure ReverseCam(ax, ay: single; out x : Single; out y : Single);
begin
  // x * kx + y * kyx = ax - dx
  // x * kxy + y * ky = ay - dy
  //TODO - angle
  x := (ax - CurCamera.dx) / CurCamera.kx;
  y := (ay - CurCamera.dy) / CurCamera.ky;
end;


function CamVertex(ax, ay: single; Color: cardinal): TSpriteVertex; overload;
begin
  if CurLayer >= LAYER_GUI then
    Result := Vertex(ax, ay, LayerToZ(CurLayer), 0, 0, -1, Color)
  else
    Result := Vertex(ax * CurCamera.kx + ay * CurCamera.kyx + CurCamera.dx,
      ax * CurCamera.kxy + ay * CurCamera.ky + CurCamera.dy,
      LayerToZ(CurLayer), 0, 0, -1, Color);
end;

function CrunchCreator: TSpriteList;
begin
  Result := TSpriteList.Create(128);
  Result.thetyp := GL_TRIANGLES;
  Result.LineWidth := 0;
end;

function NextOrSame(typ: GLEnum; tex: TGenericTexture): TSpriteList;
var
  reused: boolean;
begin
  Result := SplitSprites.LastOrNext(reused);
  if reused and ((Result.thetyp <> typ) or (Result.thetex <> tex) or
    LineWidthChanged) then
  begin
    Result := SplitSprites.Next;
    reused := False;
  end;
  if reused then
    exit;
  if LineWidthChanged then
  begin
    Result.LineWidth := CurLineWidth;
    LineWidthChanged := False;
  end
  else
    Result.LineWidth := 0;
  Result.NPoints := 0;
  Result.thetyp := typ;
  Result.thetex := tex;
end;

procedure Sprite(sprite: TSprite; x, y: TCoord; kx, ky, angle: single;
  Color: TColor); cdecl;
var
  it: TSpriteList;
  v00, v01, v10, v11: TSpriteVertex;
  dx, dy, ca, sa: single;
begin
  it := NextOrSame(GL_TRIANGLES, RESSprite(sprite));

  dx := it.thetex.Width * kx / 2;
  dy := it.thetex.Height * ky / 2;

  ca := cos(-angle * pi / 180);
  sa := sin(-angle * pi / 180);
  v00 := CamVertex(x - dx * ca + dy * sa, y - dy * ca - dx * sa, 0, 0, color);
  v01 := CamVertex(x - dx * ca - dy * sa, y + dy * ca - dx * sa, 0, 1, color);
  v10 := CamVertex(x + dx * ca + dy * sa, y - dy * ca + dx * sa, 1, 0, color);
  v11 := CamVertex(x + dx * ca - dy * sa, y + dy * ca + dx * sa, 1, 1, color);

  it.AddVertex(v00);
  it.AddVertex(v10);
  it.AddVertex(v01);

  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v11);
end;

procedure SpriteSliced(sprite: TSprite; x, y: TCoord; w, h: single;
  Color: TColor); cdecl;
begin
  SpriteSlicedInternal(RESSprite(sprite), x,y,w,h,Color);
end;

procedure SpriteSlicedInternal(sprite: TGenericTexture; x, y: TCoord; w, h: single; Color: TColor);
var
  it: TSpriteList;
  dx, dy: single;

procedure addrect(x1,y1,x2,y2, tx1, ty1, tx2, ty2: Single);
var
  v00, v01, v10, v11: TSpriteVertex;
begin
  v00 := CamVertex(x1, y1, tx1, ty1, color);
  v01 := CamVertex(x1, y2, tx1, ty2, color);
  v10 := CamVertex(x2, y1, tx2, ty1, color);
  v11 := CamVertex(x2, y2, tx2, ty2, color);
  it.AddVertex(v00);
  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v11);
end;

begin
  it := NextOrSame(GL_TRIANGLES, sprite);
  dx := it.thetex.Width/2;
  dy := it.thetex.Height/2;

  if (w <= dx*2) and (h <= dy*2) then
    addrect(x,y,x+w,y+h,0,0,1,1)
  else if (w <= dx*2) then
  //
  // 00 - 01
  // |
  // e0   e1
  // f0   f1
  // |
  // 10   11
  //
  begin
    addrect(x,y, x+w,y+dy,0,0,1,0.5);
    addrect(x,y+dy, x+w,y+h-dy,0,0.5,1,0.5);
    addrect(x,y+h-dy, x+w,y+h,0,0.5,1,1);
  end
  else if (w <= dx*2) then
  //
  // 00 - 0e --- 0f - 01
  // |
  // 10   1e     1f   11
  //
  begin
    addrect(x,y, x+dx,y+h,0,0,0.5,1);
    addrect(x+dx,y, x+w-dx,y+h,0.5,0,0.5,1);
    addrect(x+w-dx,y, x+w,y+h,0.5,0,1,1);
  end
  else
  //
  // 00 - 0e --- 0f - 01
  // |
  // e0   ee     ef   e1
  // f0   fe     ff   f1
  // |
  // 10   1e     1f   11
  //
  begin
    addrect(x,y, x+dx,y+dy,0,0,0.5,0.5);
    addrect(x+dx,y, x+w-dx,y+dy,0.5,0,0.5,0.5);
    addrect(x+w-dx,y, x+w,y+dy,0.5,0,1,0.5);

    addrect(x,y+dy, x+dx,y+h-dy,0,0.5,0.5,0.5);
    addrect(x+dx,y+dy, x+w-dx,y+h-dy,0.5,0.5,0.5,0.5);
    addrect(x+w-dx,y+dy, x+w,y+h-dy,0.5,0.5,1,0.5);

    addrect(x,y+h-dy, x+dx,y+h,0,0.5,0.5,1);
    addrect(x+dx,y+h-dy, x+w-dx,y+h,0.5,0.5,0.5,1);
    addrect(x+w-dx,y+h-dy, x+w,y+h,0.5,0.5,1,1);
  end;
end;

procedure Background(sprite: TSprite; kx, ky, dx, dy: single; Color: TColor); cdecl;
var
  it: TSpriteList;
  v00, v01, v10, v11: TSpriteVertex;
begin
  it := NextOrSame(GL_TRIANGLES, RESSprite(sprite));

  kx := LogicXSize / kx / it.thetex.Width;
  ky := LogicYSize / ky / it.thetex.Height;
  dx := -dx / it.thetex.Width;
  dy := -dy / it.thetex.Height;

  v00 := Vertex(0, 0, 1, dx, dy, 0, color);
  v01 := Vertex(0, LogicYSize, 1, dx, ky + dy, 0, color);
  v10 := Vertex(LogicXSize, 0, 1, kx + dx, dy, 0, color);
  v11 := Vertex(LogicXSize, LogicYSize, 1, kx + dx, ky + dy, 0, color);

  it.AddVertex(v00);
  it.AddVertex(v10);
  it.AddVertex(v01);

  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v11);
end;

procedure Line(x1, y1, x2, y2: TCoord; color1, color2: TColor); cdecl;
var
  it: TSpriteList;
begin
  it := NextOrSame(GL_LINES, nil);
  it.AddLine(CamVertex(x1, y1, color1), CamVertex(x2, y2, color2));
end;

procedure LineSettings(Width: single; Stipple: cardinal; StippleScale: single); cdecl;
begin
  if CurLineWidth <> Width then
  begin
    CurLineWidth := Width;
    LineWidthChanged := True;
  end;

  //TODO - 1D texture

end;

procedure Ellipse(x, y, rx, ry: TCoord; filled: boolean; color1, color2: TColor;
  angle: single); cdecl;
var
  i: integer;
  a, sint, cost, sina, cosa: single;
  it: TSpriteList;
  v0, vr, vr1: TSpriteVertex;
begin
  if filled then
  begin
    it := NextOrSame(GL_TRIANGLES, nil);
    v0 := CamVertex(x, y, color1);
  end
  else
    it := NextOrSame(GL_LINES, nil);
  sint := sin(-angle * pi / 180);
  cost := cos(-angle * pi / 180);
  vr := CamVertex(x + rx * cost, y + rx * sint, color2);
  for i := 1 to CIRCLE_COUNT  do
  begin
    a := 2 * pi / CIRCLE_COUNT * i;
    sina := sin(a);
    cosa := cos(a);
    vr1 := CamVertex(x + rx * cosa * cost - ry * sina * sint, y +
      rx * cosa * sint + ry * sina * cost, color2);
    if filled then
    begin
      it.AddVertex(v0);
      it.AddVertex(vr);
      it.AddVertex(vr1);
    end
    else
      it.AddLine(vr, vr1);
    vr := vr1;
  end;
end;

procedure PolygonDraw(poly: TPoly; X, Y, Angle: TCoord; c: TColor; sprite: TSprite; dx: TCoord;
  dy: TCoord; kx: TCoord; ky: TCoord); cdecl;
var
  i: integer;
  it: TSpriteList;
  v: TSpriteVertex;
  p: TPolyData;
  tex: TGenericTexture;
  ca, sa, ax, ay: TCoord;
begin
  if sprite <= 0 then
    tex := nil
  else
    tex := RESSprite(sprite);
  p := Polygons[poly];
  if not p.IsComplete then exit;
  //TODO - tesselation?
  it := NextOrSame(GL_TRIANGLE_FAN, tex);

  ca := cos(-angle * pi / 180);
  sa := sin(-angle * pi / 180);
  if Assigned(tex) then
  begin
    kx := LogicXSize / kx / tex.Width;
    ky := LogicYSize / ky / tex.Height;
    dx := -dx / tex.Width;
    dy := -dy / tex.Height;
    dx := dx - p.points[0].x/kx;
    dy := dy - p.points[0].y/ky;
    for i := 0 to p.NPoints-1 do
    begin
      //x => x*ca-y*sa,
      //y => y*ca+x*sa
      ax := p.points[i].x;
      ay := p.points[i].y;
      it.AddVertex(CamVertex(X+ca*ax-sa*ay, Y+ca*ay+sa*ax, ax*kx+dx, ay*ky+dy, c));
    end;
  end
  else
    for i := 0 to p.NPoints-1 do
    begin
      ax := p.points[i].x;
      ay := p.points[i].y;
      it.AddVertex(CamVertex(X+ca*ax-sa*ay, Y+ca*ay+sa*ax, c));
    end;
end;



procedure Rect(x0, y0, w, h: TCoord; filled: boolean;
  Color1, Color2, Color3, Color4: TColor; angle: single); cdecl;
var
  it: TSpriteList;
  v00, v01, v10, v11: TSpriteVertex;
  x, y, dx, dy, ca, sa: single;
begin
  x := x0 + w / 2;
  y := y0 + h / 2;
  dx := w / 2;
  dy := h / 2;
  ca := cos(-angle * pi / 180);
  sa := sin(-angle * pi / 180);
  v00 := CamVertex(x - dx * ca + dy * sa, y - dy * ca - dx * sa, color1);
  v01 := CamVertex(x - dx * ca - dy * sa, y + dy * ca - dx * sa, color2);
  v10 := CamVertex(x + dx * ca + dy * sa, y - dy * ca + dx * sa, color4);
  v11 := CamVertex(x + dx * ca - dy * sa, y + dy * ca + dx * sa, color3);

  if filled then
  begin
    it := NextOrSame(GL_TRIANGLES, nil);
    it.AddVertex(v00);
    it.AddVertex(v10);
    it.AddVertex(v01);
    it.AddVertex(v10);
    it.AddVertex(v01);
    it.AddVertex(v11);
  end
  else
  begin
    it := NextOrSame(GL_LINES, nil);
    it.AddLine(v00, v01);
    it.AddLine(v01, v11);
    it.AddLine(v11, v10);
    it.AddLine(v10, v00);
  end;
end;

procedure Point(x, y: TCoord; color: TColor); cdecl;
var
  it: TSpriteList;
begin
  it := NextOrSame(GL_POINTS, nil);
  it.AddVertex(CamVertex(x, y, color));
end;

procedure Triangle(x1, y1: TCoord; color1: TColor; x2, y2: TCoord;
  color2: TColor; x3, y3: TCoord; color3: TColor); cdecl;
var
  it: TSpriteList;
begin
  it := NextOrSame(GL_TRIANGLES, nil);
  it.AddVertex(CamVertex(x1, y1, color1));
  it.AddVertex(CamVertex(x2, y2, color2));
  it.AddVertex(CamVertex(x3, y3, color3));
end;

procedure TexturedTriangle(sprite: TSprite;
  x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3: TCoord); cdecl;
var
  it: TSpriteList;
begin
  it := NextOrSame(GL_TRIANGLES, RESSprite(sprite));
  it.AddVertex(CamVertex(x1, y1, tx1 / it.thetex.Width, ty1 / it.thetex.Height, WHITE));
  it.AddVertex(CamVertex(x2, y2, tx2 / it.thetex.Width, ty2 / it.thetex.Height, WHITE));
  it.AddVertex(CamVertex(x3, y3, tx3 / it.thetex.Width, ty3 / it.thetex.Height, WHITE));
end;

procedure SetLayer(z: integer); cdecl;
begin
  CurLayer := z;
end;

procedure ResetView;
begin
  CurCamera.dx := 0;
  CurCamera.dy := 0;
  CurCamera.kx := 1;
  CurCamera.ky := 1;
  CurCamera.kxy := 0;
  CurCamera.kyx := 0;
end;

procedure Camera(dx, dy: TCoord; kx, ky, angle: single); cdecl;
var
  w2, h2, cosa, sina: Single;
begin
  angle := - angle * pi / 180;
  cosa := cos(angle);
  sina := sin(angle);

  w2 := LogicXSize/2;
  h2 := LogicYSize/2;

  //well, matrices.
  //1. translate coordinates so (w/2+dx, h/2+dy) become (0,0)
  //m1 = [1, 0, -dx-w/2]
  //     [0, 1, -dy-h/2]
  //     [0, 0, 1]
  //2. rotate
  //m2 = [cos(a), sin(a), 0]
  //     [-sin(a), cos(a), 0]
  //     [0, 0, 1]
  //3. scale
  //m3 = [kx, 0, 0]
  //     [0, ky, 0]
  //     [0, 0, 1]
  //4. translate so (0,0) become (w/2, h/2)
  //m3 = [1, 0, -dx]
  //     [0, 1, -dy]
  //     [0, 0, 1]

  CurCamera.kx := kx*cosa;
  CurCamera.ky := ky*cosa;
  CurCamera.kxy := -ky*sina;
  CurCamera.kyx := kx*sina;

  CurCamera.dx := kx*(cosa*(-dx-w2)+(-dy-h2)*sina) + w2;
  CurCamera.dy := ky*(cosa*(-dy-h2)-(-dx-w2)*sina) + h2;
end;

procedure DrawTiled(tiled: TTileMap; frame: integer; x, y: TCoord;
  kx, ky, angle: single; Color: TColor); cdecl;
var
  it: TSpriteList;
  v00, v01, v10, v11: TSpriteVertex;
  tx1, ty1, tx2, ty2: TCoord;
  res: TTileMapResource;
  dx, dy, ca, sa: single;
begin
  res := RESTileMap(tiled);
  it := NextOrSame(GL_TRIANGLES, res.tex);

  tx1 := (frame mod res.nx) * res.w + 1;
  ty1 := (frame div res.nx) * res.h + 1;
  tx2 := tx1 + res.w - 1;
  ty2 := ty1 + res.h - 1;

  tx1 := tx1 / res.tex.Width;
  tx2 := tx2 / res.tex.Width;
  ty1 := ty1 / res.tex.Height;
  ty2 := ty2 / res.tex.Height;

  dx := res.w * kx / 2;
  dy := res.h * ky / 2;
  ca := cos(-angle * pi / 180);
  sa := sin(-angle * pi / 180);
  v00 := CamVertex(x - dx * ca + dy * sa, y - dy * ca - dx * sa, tx1, ty1, color);
  v01 := CamVertex(x - dx * ca - dy * sa, y + dy * ca - dx * sa, tx1, ty2, color);
  v10 := CamVertex(x + dx * ca + dy * sa, y - dy * ca + dx * sa, tx2, ty1, color);
  v11 := CamVertex(x + dx * ca - dy * sa, y + dy * ca + dx * sa, tx2, ty2, color);

  it.AddVertex(v00);
  it.AddVertex(v10);
  it.AddVertex(v01);

  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v11);
end;

procedure FlushRenderQueues;
var
  i: integer;
  it: TSpriteList;
  w: single;
  atyp: GLEnum;
begin
  w := 1;
  for i := 0 to SplitSprites.Used - 1 do
  begin
    it := SplitSprites.Item(i);
    if it.thetex <> nil then
      it.thetex.ActivateReading(0);
    atyp := it.thetyp;
    if (it.LineWidth <> w) and (it.LineWidth > 0) then
    begin
      w := it.LineWidth;
      //glLineWidth(w);
      glPointSize(w);
      if w > 1.0 then
        atyp := GL_TRIANGLES;
    end;
    it.Draw(atyp);
  end;

  SplitSprites.Reset;
end;

var
  p: array[1..2] of single;

procedure SetDefaulUniforms;
begin
  //own_shader.SetIntParam('tex', 0);
  //p[0] :=
  //DefaultUniformValues[UNI_SCREEN_SIZE] := SetPointerValue();
  //UniformSetInt(0, ;
  //own_shader.SetVectorParam('screen_size', LogicXSize, LogicYSize);
  DefaultUniformValues[UNI_TEX] := SetIntValue(0);
  p[1] := LogicXSize;
  p[2] := LogicYSize;
  DefaultUniformValues[UNI_SCREEN_SIZE] := SetPointerValue(@p[1]);
end;

procedure FreeRender;
begin
  SplitSprites.Free;
end;

procedure PrepareRenderState;
begin
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  if Params[Antialias] > 0 then
    glEnable(GL_LINE_SMOOTH);
  //glDisable(GL_DEPTH_TEST);

  CurLineWidth := 1;
  LineWidthChanged := False;
  glLineWidth(1);
  glPointSize(1);
  SetLayer(1);
  ResetFontPools;
  ResetView;

  SetDefaulUniforms;
  DefaultShader.Activate;

  SetRenderingTarget(nil);
end;

procedure InitRender;
begin
  SplitSprites := TSpritesPool.Create(@CrunchCreator);
end;

//TODO - move it to uTextures
{ TTileMapResource }

constructor TTileMapResource.Create(atex: TSimpleTexture; anx, any: integer);
begin
  nx := anx;
  tex := atex;
  w := tex.Width div anx;
  if tex.Width mod anx <> 0 then
    Logf('TileMap has incorrect size: %d doesn''t divide %d', [anx, tex.Width]);
  h := tex.Height div any;
  if tex.Height mod any <> 0 then
    Logf('TileMap has incorrect size: %d doesn''t divide %d', [any, tex.Height]);
  logf('Tilemap loaded: %d x %d (elements %d x %d)', [anx, any, w, h]);
end;

end.
