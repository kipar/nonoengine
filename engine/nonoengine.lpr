library nonoengine;

{$mode objfpc}{$H+}

uses
  uMainEngine,
  uInput,
  uRender,
  uText,
  uEngineTypes,
  uLoader,
  uGUI,
  uSound,
  uPools,
  uLog,
  uPathfind,
  uPhysics,
  uShadersWrap,
  uTextureWrap,
  ushaderprocess,
  uVertexWrap,
  uMyGLEXT,
  uFreeAll, uPoly, uParticles;

exports
  EngineInit,
  EngineFree,
  EngineGet,
  EngineSet,
  EngineProcess,
  RawResource,
  RawTexture,
  EngineLog,

  KeyState,
  MouseGet,
  MouseState,

  uRender.Sprite Name 'Sprite',
  SpriteSliced,
  Background,
  Line,
  LineSettings,
  Ellipse,
  Rect,
  Point,
  Triangle,
  TexturedTriangle,
  FontConfig,
  FontCreate,
  DrawText,
  DrawTextBoxed,
  SetLayer,
  Camera,
  Button,
  Panel,
  InputInt,
  DrawTiled,
  MeasureText,

  Play,
  Music,
  SoundPlaying,
  GetGUICoord,

  Pathfind,
  PathFindUnreachable,

{$IFNDEF NO_PHYSICS}
  PhysicsReset,
  BodyCreate,
  BodyFree,
  BodyAddShapeCircle,
  BodyAddShapeBox,
  BodyAddShapeLine,
  BodyApplyForce,
  BodyApplyControl,
  BodyCoords,
  Material,
  MaterialCollisions,
  GetCollisions,
  GetMaterialCollisions,
  SetCurrentCollisionResult,
  BodyAddShapePoly,
  DebugPhysicsRender,
  RaycastLine,
{$ENDIF}
  PolygonCreate,
  PolygonFree,
  PolygonAddPoint,
  PolygonDraw,

  ShaderActivate,
  ShaderHandle,
  UniformSetInt,
  UniformSetFloat,
  UniformSetTexture,
  UniformSetPtr,

  GetPixel,
  RenderTo,
  TextureCreate,
  TextureClone,
  TextureDelete,
  TextureSave,
  CaptureScreen,
  TextureGetPixels,
  TextureLoadPixels,
  //CrunchTextureGetInternals,
  VertexListCreate,
  VertexListAddField,
  VertexListAddPadding,
  VertexListDraw,
  VertexListCopy,
  VertexListDelete,
  VertexListChange,

  EmitterAdd,
  EmitterDelete,
  EmitterMove
;


begin
  {$IFDEF DEBUG}
  SetHeapTraceOutput('heaptrc.log');
  {$ENDIF}
end.
