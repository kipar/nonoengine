unit ushaderprocess;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, gl, glext, fgl
  {$IFNDEF GENERATE}
  , uTextures
  {$ENDIF}
  ;

type

  {$IFDEF GENERATE}
  TGenericTexture = Integer;
  {$ENDIF}

  TGLSLType = (
    float,
    vec2,
    vec3,
    vec4,
    double,
    dvec2,
    dvec3,
    dvec4,
    int,
    ivec2,
    ivec3,
    ivec4,
    unsigned_int,
    uvec2,
    uvec3,
    uvec4,
    bool,
    bvec2,
    bvec3,
    bvec4,
    mat2,
    mat3,
    mat4,
    mat2x3,
    mat2x4,
    mat3x2,
    mat3x4,
    mat4x2,
    mat4x3,
    dmat2,
    dmat3,
    dmat4,
    dmat2x3,
    dmat2x4,
    dmat3x2,
    dmat3x4,
    dmat4x2,
    dmat4x3,
    sampler1D,
    sampler2D,
    sampler3D,
    samplerCube,
    sampler1DShadow,
    sampler2DShadow,
    sampler1DArray,
    sampler2DArray,
    sampler1DArrayShadow,
    sampler2DArrayShadow,
    sampler2DMS,
    sampler2DMSArray,
    samplerCubeShadow,
    samplerBuffer,
    sampler2DRect,
    sampler2DRectShadow,
    isampler1D,
    isampler2D,
    isampler3D,
    isamplerCube,
    isampler1DArray,
    isampler2DArray,
    isampler2DMS,
    isampler2DMSArray,
    isamplerBuffer,
    isampler2DRect,
    usampler1D,
    usampler2D,
    usampler3D,
    usamplerCube,
    usampler1DArray,
    usampler2DArray,
    usampler2DMS,
    usampler2DMSArray,
    usamplerBuffer,
    usampler2DRect
    );

  TGLSLTypeStrings = array[TGLSLType] of string;
  TGLSLTypeValues = array[TGLSLType] of GLenum;

const
  GLSL_TYPE_NAMES: TGLSLTypeStrings = (
    'float',
    'vec2',
    'vec3',
    'vec4',
    'double',
    'dvec2',
    'dvec3',
    'dvec4',
    'int',
    'ivec2',
    'ivec3',
    'ivec4',
    'unsigned int',
    'uvec2',
    'uvec3',
    'uvec4',
    'bool',
    'bvec2',
    'bvec3',
    'bvec4',
    'mat2',
    'mat3',
    'mat4',
    'mat2x3',
    'mat2x4',
    'mat3x2',
    'mat3x4',
    'mat4x2',
    'mat4x3',
    'dmat2',
    'dmat3',
    'dmat4',
    'dmat2x3',
    'dmat2x4',
    'dmat3x2',
    'dmat3x4',
    'dmat4x2',
    'dmat4x3',
    'sampler1D',
    'sampler2D',
    'sampler3D',
    'samplerCube',
    'sampler1DShadow',
    'sampler2DShadow',
    'sampler1DArray',
    'sampler2DArray',
    'sampler1DArrayShadow',
    'sampler2DArrayShadow',
    'sampler2DMS',
    'sampler2DMSArray',
    'samplerCubeShadow',
    'samplerBuffer',
    'sampler2DRect',
    'sampler2DRectShadow',
    'isampler1D',
    'isampler2D',
    'isampler3D',
    'isamplerCube',
    'isampler1DArray',
    'isampler2DArray',
    'isampler2DMS',
    'isampler2DMSArray',
    'isamplerBuffer',
    'isampler2DRect',
    'usampler1D',
    'usampler2D',
    'usampler3D',
    'usamplerCube',
    'usampler1DArray',
    'usampler2DArray',
    'usampler2DMS',
    'usampler2DMSArray',
    'usamplerBuffer',
    'usampler2DRect'
    );

  GLSL_TYPE_VALUES: TGLSLTypeValues = (
    GL_FLOAT,
    GL_FLOAT_VEC2,
    GL_FLOAT_VEC3,
    GL_FLOAT_VEC4,
    GL_DOUBLE,
    GL_DOUBLE_VEC2,
    GL_DOUBLE_VEC3,
    GL_DOUBLE_VEC4,
    GL_INT,
    GL_INT_VEC2,
    GL_INT_VEC3,
    GL_INT_VEC4,
    GL_UNSIGNED_INT,
    GL_UNSIGNED_INT_VEC2,
    GL_UNSIGNED_INT_VEC3,
    GL_UNSIGNED_INT_VEC4,
    GL_BOOL,
    GL_BOOL_VEC2,
    GL_BOOL_VEC3,
    GL_BOOL_VEC4,
    GL_FLOAT_MAT2,
    GL_FLOAT_MAT3,
    GL_FLOAT_MAT4,
    GL_FLOAT_MAT2x3,
    GL_FLOAT_MAT2x4,
    GL_FLOAT_MAT3x2,
    GL_FLOAT_MAT3x4,
    GL_FLOAT_MAT4x2,
    GL_FLOAT_MAT4x3,
    GL_DOUBLE_MAT2,
    GL_DOUBLE_MAT3,
    GL_DOUBLE_MAT4,
    GL_DOUBLE_MAT2x3,
    GL_DOUBLE_MAT2x4,
    GL_DOUBLE_MAT3x2,
    GL_DOUBLE_MAT3x4,
    GL_DOUBLE_MAT4x2,
    GL_DOUBLE_MAT4x3,
    GL_SAMPLER_1D,
    GL_SAMPLER_2D,
    GL_SAMPLER_3D,
    GL_SAMPLER_CUBE,
    GL_SAMPLER_1D_SHADOW,
    GL_SAMPLER_2D_SHADOW,
    GL_SAMPLER_1D_ARRAY,
    GL_SAMPLER_2D_ARRAY,
    GL_SAMPLER_1D_ARRAY_SHADOW,
    GL_SAMPLER_2D_ARRAY_SHADOW,
    GL_SAMPLER_2D_MULTISAMPLE,
    GL_SAMPLER_2D_MULTISAMPLE_ARRAY,
    GL_SAMPLER_CUBE_SHADOW,
    GL_SAMPLER_BUFFER,
    GL_SAMPLER_2D_RECT,
    GL_SAMPLER_2D_RECT_SHADOW,
    GL_INT_SAMPLER_1D,
    GL_INT_SAMPLER_2D,
    GL_INT_SAMPLER_3D,
    GL_INT_SAMPLER_CUBE,
    GL_INT_SAMPLER_1D_ARRAY,
    GL_INT_SAMPLER_2D_ARRAY,
    GL_INT_SAMPLER_2D_MULTISAMPLE,
    GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,
    GL_INT_SAMPLER_BUFFER,
    GL_INT_SAMPLER_2D_RECT,
    GL_UNSIGNED_INT_SAMPLER_1D,
    GL_UNSIGNED_INT_SAMPLER_2D,
    GL_UNSIGNED_INT_SAMPLER_3D,
    GL_UNSIGNED_INT_SAMPLER_CUBE,
    GL_UNSIGNED_INT_SAMPLER_1D_ARRAY,
    GL_UNSIGNED_INT_SAMPLER_2D_ARRAY,
    GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE,
    GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,
    GL_UNSIGNED_INT_SAMPLER_BUFFER,
    GL_UNSIGNED_INT_SAMPLER_2D_RECT
    );

type

  TShaderComponent = (
    vertex, fragment, geometry,
    tess_control, tess_evaluation
//  compute
  );
  TShaderComponentStrings = array[TShaderComponent] of string;
  TShaderComponentValues = array[TShaderComponent] of GLenum;
const
  SHADER_COMPONENT_TYPE_NAMES: TShaderComponentStrings = (
  'vertex', 'fragment', 'geometry',
  'tess_control', 'tess_evaluation'
  //  compute
  );
  SHADER_COMPONENT_TYPE_ENUMS: TShaderComponentValues = (
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
    GL_GEOMETRY_SHADER,
    GL_TESS_CONTROL_SHADER,
    GL_TESS_EVALUATION_SHADER
    //GL_COMPUTE_SHADER,
  );



type

  { TGLSLItem }

  TGLSLItem = class
    index: Integer;
    name: string;
    typ: TGLSLType;
    array_size: Integer;
    texture_unit: Integer;
    function ToString: string;override;
    constructor FromString(s: string);
    function IsSame(other: TGLSLItem): boolean;
  end;

  TGLSLItemsList = specialize TFPGList<TGLSLItem>;


  TGLSLItemValueType = (typInt, typFloat, typSampler, typPointer);
  TGLSLItemValue = record
    was_set: boolean;
    case typ: TGLSLItemValueType of
      typInt: (AsInt: Integer;);
      typFloat: (AsFloat: Single;);
      typSampler: (AsSampler: TGenericTexture);
      typPointer: (AsPointer: Pointer);
  end;
  function SetIntValue(x: integer): TGLSLItemValue;
  function SetFloatValue(x: single): TGLSLItemValue;
  function SetPointerValue(x: pointer): TGLSLItemValue;
  function SetSamplerValue(x: TGenericTexture): TGLSLItemValue;
  procedure ApplyValue(index: integer; item: TGLSLItem; value: TGLSLItemValue);

  type
  { TShader }

  TShader = class
    handle: GLhandleARB;
    include_names: array of string;
    include_string: string;
    base_dir: string;
    own_name: string;
    component_names: TShaderComponentStrings;
    components: TShaderComponentStrings;
    attributes, uniforms: TGLSLItemsList;
    attribute_cache, uniform_cache: array of Integer;
    attribute_logged, uniform_logged: array of Boolean;
    uniform_values: array of TGLSLItemValue;
    uniform_locations: array of integer;
    attribute_locations: array of integer;
    internal_index: Integer;
    constructor Create;
    procedure Load(filename: string);
    procedure Save(filename: string);
    procedure Compile;
    procedure Introspect(out something_changed: boolean);
    procedure Activate;
    procedure BuildCache(all_attributes, all_uniforms: array of TGLSLItem);
    function GetUniformIndex(uniform: Integer; typ: TGLSLItemValueType): Integer;
    function GetAttributeLocation(attribute: Integer; typ: TGLSLItemValueType): Integer;
    procedure ProcessGeneration(AttributesList, UniformsList: TStringList);
    procedure ApplyUniform(uniform: integer);
    destructor Destroy; override;
    private
      function AddShader(what: TShaderComponent): GLhandleARB;
  end;



  TGLSLTypesList = array of TGLSLItem;


  function TypeCompatible(ourtyp: TGLSLItemValueType; item: TGLSLItem): Boolean;

  procedure InitShadersCache(list: TList);

var
  DefaultUniformValues: array of TGLSLItemValue;
  CurShader: TShader;
  AllAttributes: array of TGLSLItem;

procedure FreeShaders;

implementation

{$IFDEF GENERATE}
procedure log(s: string);
begin
  writeln(s);
end;
{$ELSE}
uses uLog, uLoader;
{$ENDIF}

procedure logdump(Name, s: string);
begin
  writeln();
  writeln('------------' + Name + '------------');
  writeln(s);
  writeln('------------------------');
  writeln();
end;

function split(s: string; out Name: string; out Value: string): boolean;
var
  i: integer;
begin
  Result := False;
  i := pos(':', s);
  if i <= 0 then
    exit;
  Result := True;
  Name := Trim(Copy(s, 1, i - 1));
  Value := Trim(Copy(s, i + 1, MAXINT));
end;

function split_by(s: string; what: string; out Name: string; out Value: string): boolean;
var
  i: integer;
begin
  Result := False;
  i := pos(what, s);
  if i <= 0 then
    exit;
  Result := True;
  Name := Trim(Copy(s, 1, i - 1));
  Value := Trim(Copy(s, i + 1, MAXINT));
end;

function EFRead(Filename: string): string;
var
  ff: file;
  n: integer;
begin
  AssignFile(ff, Filename);
  Reset(ff, 1);
  n := FileSize(ff);
  Result := '';
  SetLength(Result, n);
  BlockRead(ff, pointer(Result)^, n);
  CloseFile(ff);
end;

procedure ShaderStatus(shader, param: integer);
var
  Buff: array[0..1023] of Ansichar;
  l, status: integer;
begin
  glGetShaderiv(shader, param, @status);
  if status <> GL_TRUE then
  begin
    glGetShaderInfoLog(shader, 1024, @l, Buff);
    log(' Shader error!');
    log(Buff);
  end;
end;

procedure ProgramStatus(aprogram, param: integer);
var
  Buff: array[0..1023] of Ansichar;
  l, status: integer;
begin
  glGetProgramiv(aprogram, param, @status);
  if status <> GL_TRUE then
  begin
    glGetProgramInfoLog(aprogram, 1024, @l, Buff);
    log(' Program error!');
    log(Buff);
  end;
end;

function TShader.AddShader(what: TShaderComponent): GLhandleARB;
var
  Text: string;
  sh: GLhandleARB;
  N: integer;
begin
  Result := 0;
  if component_names[what] = '' then exit;
  Text := include_string + components[what];
  sh := glCreateShader(SHADER_COMPONENT_TYPE_ENUMS[what]);
  log(SHADER_COMPONENT_TYPE_NAMES[what]+': '+component_names[what]);
  N := Length(Text);
  glShaderSource(sh, 1, @Text, @N);
  glCompileShader(sh);
  ShaderStatus(sh, GL_COMPILE_STATUS);
  glAttachShader(handle, sh);
  Result := sh;
end;

function DecodeType(typ: GLenum): TGLSLType;
var
   x: TGLSLType;
begin
  for x in TGLSLType do
      if GLSL_TYPE_VALUES[x] = typ then
      begin
        Result := x;
        exit;
      end;
  raise Exception.Create('Unknown GLSL type returned: '+IntToStr(typ));
end;

function DecodeType(typ: string): TGLSLType;
var
   x: TGLSLType;
begin
  for x in TGLSLType do
      if GLSL_TYPE_NAMES[x] = typ then
      begin
        Result := x;
        exit;
      end;
  raise Exception.Create('Unknown GLSL type: '+typ);
end;

{ TGLSLItem }

function TGLSLItem.ToString: string;
begin
  Result := name;
  if array_size > 1 then
  begin
    Result := Result + '[' + IntToStr(array_size) + ']';
  end;
  Result:= Result + ': ' + GLSL_TYPE_NAMES[typ];
end;

constructor TGLSLItem.FromString(s: string);
var
   anam, anum, nam, val: string;
begin
  if not split(s, nam, val) then
    raise Exception.Create('incorrect definition: '+s);
  typ := DecodeType(val);
  if pos('[', nam) <= 0 then
  begin
    array_size := 1;
    name := nam;
  end
  else
  begin
    if nam[length(nam)] <> ']' then
      raise Exception.Create('incorrect definition: '+s);
    split_by(nam,'[', anam, anum);
    name := anam;
    array_size := StrToInt(Copy(anum, 1, length(anum)-1));
  end;
end;

function TGLSLItem.IsSame(other: TGLSLItem): boolean;
begin
  Result :=
    (name = other.name) and
    (typ = other.typ) and
    (array_size = other.array_size);
  {IFDEF GENERATE}
  if not Result then
  begin
    log('***changes for '+name);
    if name <> other.name then log('name: '+name+' <> '+other.name);
    if typ <> other.typ then log('typ: '+GLSL_TYPE_NAMES[typ]+' <> '+GLSL_TYPE_NAMES[other.typ]);
    if array_size <> other.array_size then log('size: '+IntTostr(array_size)+' <> '+IntTostr(other.array_size));
    log('***');
  end;
  {ENDIF}
end;

{ TShader }

constructor TShader.Create;
begin
  attributes := TGLSLItemsList.Create;
  uniforms := TGLSLItemsList.Create;
end;

procedure TShader.Load(filename: string);
var
  s, aname, avalue: string;
  Text: TextFile;
  comp: TShaderComponent;
  parsed: Boolean;
begin
  if filename = '' then
    exit;
  if not FileExists(filename) then
    exit;
  //log('file: ' + filename);
  own_name := ExtractFileName(filename);
  base_dir := IncludeTrailingPathDelimiter(ExtractFileDir(filename));
  AssignFile(Text, filename);
  Reset(Text);
  include_string := '';
  while not EOF(Text) do
  begin
    readln(Text, s);
    if not split(s, aname, avalue) then
      raise Exception.Create('Incorrect string: ' + s);
    if aname = 'include' then
    begin
      if not FileExists(base_dir + avalue) then
        raise Exception.Create('Include not found: ' + avalue);
      insert(avalue, include_names, MaxInt);
      include_string := include_string + EFRead(base_dir + avalue) + #13#10;
    end
    else if aname = 'uniform' then
      uniforms.Add(TGLSLItem.FromString(avalue))
    else if aname = 'attribute' then
      attributes.Add(TGLSLItem.FromString(avalue))
    else
    begin
      parsed := false;
      for comp in TShaderComponent do
          if aname = SHADER_COMPONENT_TYPE_NAMES[comp] then
          begin
            if component_names[comp] <> '' then
              raise Exception.Create(' Duplicate component: ' + SHADER_COMPONENT_TYPE_NAMES[comp]);
            component_names[comp] := avalue;
            components[comp] := EFRead(base_dir+avalue);
            parsed := true;
            break;
          end;
      if not parsed then
        raise Exception.Create(' Unknown token: ' + aname);
    end;
  end;
  CloseFile(Text);
end;

procedure TShader.Save(filename: string);
var
  i: integer;
  comp: TShaderComponent;
  f: Text;
begin
  AssignFile(f, filename);
  Rewrite(f);
  for i := 0 to length(include_names)-1 do
    writeln(f, 'include: ',include_names[i]);
  for comp in TShaderComponent do
    if component_names[comp] <> '' then
      writeln(f, SHADER_COMPONENT_TYPE_NAMES[comp], ': ', component_names[comp]);
  for i := 0 to uniforms.Count-1 do
    writeln(f, 'uniform: ',uniforms[i].ToString);
  for i := 0 to attributes.Count-1 do
    writeln(f, 'attribute: ',attributes[i].ToString);
  CloseFile(f);
end;

procedure TShader.Compile;
var
  comp: TShaderComponent;
  list: array[TShaderComponent] of GLhandleARB;
begin
  handle := glCreateProgram();
  for comp in TShaderComponent do
    list[comp] := AddShader(comp);
  log('linking...');
  glLinkProgram(handle);
  ProgramStatus(handle, GL_LINK_STATUS);
  log('validating...');
  glValidateProgram(handle);
  ProgramStatus(handle, GL_VALIDATE_STATUS);
  for comp in TShaderComponent do
    if list[comp] > 0 then
    begin
      //glDetachShader(handle, list[comp]); - can be bad on some drivers
      glDeleteShader(list[comp]);
    end;
end;

function CompareItems(const Item1, Item2: TGLSLItem): Integer;
begin
  Result := CompareStr(Item1.name, Item2.name);
end;

procedure TShader.Introspect(out something_changed: boolean);
var
  i,n, max_len, nitems: GLint;
  outstr: string;
  oldattr, olduniforms: TGLSLItemsList;
  size: GLsizei;
  typ: GLenum;
  item: TGLSLItem;
  index: GLuint;
begin
  log('introspection...');
  something_changed := false;
  oldattr := attributes;
  olduniforms := uniforms;
  attributes := TGLSLItemsList.Create;
  uniforms := TGLSLItemsList.Create;
  //Activate; not possible because caches still wasn't built
  glUseProgram(handle);
  CurShader := nil;

  n := -2;
  glGetProgramiv(handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, @max_len);
  glGetProgramiv(handle, GL_ACTIVE_ATTRIBUTES, @n);
  if n <> oldattr.Count then something_changed := True;
  SetLength(attribute_locations, n);
  for i := 0 to n-1 do
  begin
    item := TGLSLItem.Create;
    outstr := '';
    SetLength(outstr, max_len + 1);
    glGetActiveAttrib(handle, i, max_len, @size, @nitems, @typ, pointer(outstr));
    SetLength(outstr, size);
    item.array_size := nitems;
    item.typ := DecodeType(typ);
    item.name := StringReplace(outstr, '[0]', '', []);
    attributes.Add(Item);
  end;
  attributes.Sort(@CompareItems);
  for i := 0 to n-1 do
  begin
    item := attributes[i];
    if not something_changed and not item.IsSame(oldattr[i]) then something_changed := True;
    attribute_locations[i] := glGetAttribLocation(handle, PChar(item.name));
  end;
  n := -2;
  glGetProgramiv(handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, @max_len);
  glGetProgramiv(handle, GL_ACTIVE_UNIFORMS, @n);
  SetLength(uniform_locations, n);
  if n <> olduniforms.Count then something_changed := True;
  for i := 0 to n-1 do
  begin
    item := TGLSLItem.Create;
    outstr := '';
    SetLength(outstr, max_len + 1);
    size := 0;
    glGetActiveUniformName(handle, i, max_len, @size, pointer(outstr));
    SetLength(outstr, size);
    index := i;
    glGetActiveUniformsiv(handle, 1, @index, GL_UNIFORM_SIZE, @nitems);
    glGetActiveUniformsiv(handle, 1, @index, GL_UNIFORM_TYPE, @typ);
    item.array_size := nitems;
    item.typ := DecodeType(typ);
    item.name := StringReplace(outstr, '[0]', '', []);
    uniforms.Add(item);
  end;
  uniforms.Sort(@CompareItems);
  for i := 0 to n-1 do
  begin
    item := uniforms[i];
    if not something_changed and not item.IsSame(olduniforms[i]) then something_changed := True;
    uniform_locations[i] := glGetUniformLocation(handle, PChar(item.name));
  end;
end;

procedure TShader.Activate;
var
  i: integer;
begin
  if CurShader = self then exit;
  glUseProgram(handle);
  for i := 0 to length(uniform_cache)-1 do
    ApplyUniform(i);
  CurShader := Self;
end;

procedure TShader.BuildCache(all_attributes, all_uniforms: array of TGLSLItem);
var
  i, j: integer;
  it: TGLSLItem;
begin
  SetLength(attribute_cache, length(all_attributes));
  SetLength(uniform_cache, length(all_uniforms));
  SetLength(attribute_logged, length(all_attributes));
  SetLength(uniform_logged, length(all_uniforms));
  SetLength(uniform_values, uniforms.Count);
  for i := 0 to length(all_attributes)-1 do
  begin
    it := all_attributes[i];
    attribute_cache[i] := -1;
    for j := 0 to attributes.Count-1 do
      if attributes[j].name = it.name then
      begin
        attribute_cache[i] := j;
        attributes[j].index := it.index;
        break;
      end;
  end;
  for i := 0 to length(all_uniforms)-1 do
  begin
    it := all_uniforms[i];
    uniform_cache[i] := -1;
    for j := 0 to uniforms.Count-1 do
      if uniforms[j].name = it.name then
      begin
        uniform_cache[i] := j;
        uniforms[j].index := it.index;
        uniforms[j].texture_unit := it.texture_unit;
        break;
      end;
  end;
end;

type
  TGLSLItemValueTypeNames = array[TGLSLItemValueType] of string;
const
  OUR_TYPE_NAMES: TGLSLItemValueTypeNames = ('SetInt', 'SetFloat', 'SetSampler', 'SetPointer');

  function SetIntValue(x: integer): TGLSLItemValue;
  begin
    Result.was_set := True;
    Result.typ := typInt;
    Result.AsInt := x;
  end;

  function SetFloatValue(x: single): TGLSLItemValue;
  begin
    Result.was_set := True;
    Result.typ := typFloat;
    Result.AsFloat := x;
  end;

  function SetPointerValue(x: pointer): TGLSLItemValue;
  begin
    Result.was_set := True;
    Result.typ := typPointer;
    Result.AsPointer := x;
  end;

  function SetSamplerValue(x: TGenericTexture): TGLSLItemValue;
  begin
    Result.was_set := True;
    Result.typ := typSampler;
    Result.AsSampler := x;
  end;

  procedure ApplyValue(index: integer; item: TGLSLItem;
    value: TGLSLItemValue);
  begin
    case item.typ of
      float:
      begin
        if (item.array_size > 1) or (value.typ = typPointer) then
          glUniform1fv(index,item.array_size, value.AsPointer)
        else
          glUniform1f(index,value.AsFloat);
      end;
      vec2:
        glUniform2fv(index,item.array_size, value.AsPointer);
      vec3:
        glUniform3fv(index,item.array_size, value.AsPointer);
      vec4:
        glUniform4fv(index,item.array_size, value.AsPointer);
      double:
      begin
        if (item.array_size > 1) or (value.typ = typPointer) then
          glUniform1dv(index,item.array_size, value.AsPointer)
        else
          glUniform1d(index,value.AsFloat);
      end;
      dvec2:
        glUniform2dv(index,item.array_size, value.AsPointer);
      dvec3:
        glUniform3dv(index,item.array_size, value.AsPointer);
      dvec4:
        glUniform4dv(index,item.array_size, value.AsPointer);
      int:
      begin
        if (item.array_size > 1) or (value.typ = typPointer) then
          glUniform1iv(index,item.array_size, value.AsPointer)
        else
          glUniform1i(index,value.AsInt);
      end;
      ivec2:
        glUniform2iv(index,item.array_size, value.AsPointer);
      ivec3:
        glUniform3iv(index,item.array_size, value.AsPointer);
      ivec4:
        glUniform4iv(index,item.array_size, value.AsPointer);
      unsigned_int:
      begin
        if (item.array_size > 1) or (value.typ = typPointer) then
          glUniform1uiv(index,item.array_size, value.AsPointer)
        else
          glUniform1ui(index,value.AsInt);
      end;
      uvec2:
        glUniform2uiv(index,item.array_size, value.AsPointer);
      uvec3:
        glUniform3uiv(index,item.array_size, value.AsPointer);
      uvec4:
        glUniform4uiv(index,item.array_size, value.AsPointer);
      bool:
      begin
        if (item.array_size > 1) or (value.typ = typPointer) then
          glUniform1iv(index,item.array_size, value.AsPointer)
        else
          glUniform1i(index,value.AsInt);
      end;
      bvec2:
        glUniform2iv(index,item.array_size, value.AsPointer);
      bvec3:
        glUniform3iv(index,item.array_size, value.AsPointer);
      bvec4:
        glUniform4iv(index,item.array_size, value.AsPointer);
      mat2:
        glUniformMatrix2fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat3:
        glUniformMatrix3fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat4:
        glUniformMatrix4fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat2x3:
        glUniformMatrix2x3fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat2x4:
        glUniformMatrix2x4fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat3x2:
        glUniformMatrix3x2fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat3x4:
        glUniformMatrix3x4fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat4x2:
        glUniformMatrix4x2fv(index, item.array_size, GL_FALSE, value.AsPointer);
      mat4x3:
        glUniformMatrix4x3fv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat2:
        glUniformMatrix2dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat3:
        glUniformMatrix3dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat4:
        glUniformMatrix4dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat2x3:
        glUniformMatrix2x3dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat2x4:
        glUniformMatrix2x4dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat3x2:
        glUniformMatrix3x2dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat3x4:
        glUniformMatrix3x4dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat4x2:
        glUniformMatrix4x2dv(index, item.array_size, GL_FALSE, value.AsPointer);
      dmat4x3:
        glUniformMatrix4x3dv(index, item.array_size, GL_FALSE, value.AsPointer);
      sampler2D:
      begin
        if value.typ = typInt then
          glUniform1i(index, value.AsInt)
        else
        begin
          {$IFNDEF GENERATE}
          value.AsSampler.ActivateReading(item.texture_unit);
          {$ENDIF}
          glUniform1i(index, item.texture_unit)
        end;
      end;
    end;

  end;

  function TypeCompatible(ourtyp: TGLSLItemValueType; item: TGLSLItem): Boolean;
  begin
    if (ourtyp <> typPointer) and (item.array_size > 1) then
      Result := False
    else
    case ourtyp of
      typInt: Result := item.typ in [int, unsigned_int];
      typFloat: Result := item.typ in [float, double];
      typSampler: Result := item.typ = sampler2D;
      typPointer: Result := item.typ in [
        float,
        vec2,
        vec3,
        vec4,
        double,
        dvec2,
        dvec3,
        dvec4,
        int,
        ivec2,
        ivec3,
        ivec4,
        unsigned_int,
        uvec2,
        uvec3,
        uvec4,
        bool,
        bvec2,
        bvec3,
        bvec4,
        mat2,
        mat3,
        mat4,
        mat2x3,
        mat2x4,
        mat3x2,
        mat3x4,
        mat4x2,
        mat4x3,
        dmat2,
        dmat3,
        dmat4,
        dmat2x3,
        dmat2x4,
        dmat3x2,
        dmat3x4,
        dmat4x2,
        dmat4x3
        ];
    end;
  end;

  var
    all_uniforms: array of TGLSLItem;


function TShader.GetUniformIndex(uniform: Integer; typ: TGLSLItemValueType): Integer;
begin
  Result := uniform_cache[uniform];
  if (Result < 0) then
  begin
    if not uniform_logged[uniform] then
    begin
      log(Format('Nonexistant uniform ''%s'' set for shader %s', [all_uniforms[uniform].name, self.own_name]));
      uniform_logged[uniform] := True;
    end;
    exit;
  end;
  if not TypeCompatible(typ, uniforms[Result]) then
  begin
    log(Format('Wrong uniform type: %s called for ''%s''', [OUR_TYPE_NAMES[typ], uniforms[Result].ToString]));
    Result := -1
  end;
end;

function TShader.GetAttributeLocation(attribute: Integer; typ: TGLSLItemValueType
  ): Integer;
begin
  Result := attribute_cache[attribute];
  if (Result < 0) then
  begin
    if not attribute_logged[attribute] then
    begin
      log(Format('Nonexistant attribute ''%s'' set for shader %s', [AllAttributes[attribute].name, self.own_name]));
      //attribute_logged[attribute] := True;
    end;
    exit;
  end;
  if not TypeCompatible(typ, attributes[Result]) then
  begin
    log(Format('Wrong attribute type: %s called for ''%s''', [OUR_TYPE_NAMES[typ], attributes[Result].ToString]));
    Result := -1
  end;
  Result := attribute_locations[Result];
end;

procedure TShader.ProcessGeneration(AttributesList, UniformsList: TStringList);
var
  it: TGLSLItem;
begin
  for it in Attributes do
    AttributesList.Add(it.name);
  for it in Uniforms do
    UniformsList.Add(it.name);
end;

procedure TShader.ApplyUniform(uniform: integer);
var
  index: Integer;
begin
  //TODO - don't load uniforms if not changed
  index := uniform_cache[uniform];
  if index < 0 then exit;
  if uniform_values[index].was_set then
    ApplyValue(uniform_locations[index], uniforms[index], uniform_values[index])
  else if DefaultUniformValues[uniform].was_set then
    ApplyValue(uniform_locations[index], uniforms[index], DefaultUniformValues[uniform]);
end;

destructor TShader.Destroy;
var
  it: TGLSLItem;
begin
  if handle > 0 then glDeleteProgram(handle);
  for it in attributes do
    it.Free;
  for it in uniforms do
    it.Free;
  attributes.Free;
  uniforms.Free;
  inherited Destroy;
end;

procedure TryAddItem(var list: TGLSLTypesList; item: TGLSLItem);
var
  it: TGLSLItem;
begin
  for it in list do
    if it.name = item.name then
    begin
      if not it.IsSame(item) then
        raise Exception.Create(Format('Names conflist in shader: ''%s'' and ''%s'' ', [it, item]));
      exit;
    end;
  Insert(item, list, MaxInt);
end;

procedure InitShadersCache(list: TList);
var
  it: TGLSLItem;
  sh: Pointer;
  n: integer;
begin
  SetLength(AllAttributes, 0);
  SetLength(all_uniforms, 0);
  n := 0;
  for sh in list do
  begin
    TShader(sh).internal_index := n;
    inc(n);
    for it in TShader(sh).attributes do
      TryAddItem(AllAttributes, it);
    for it in TShader(sh).uniforms do
      TryAddItem(all_uniforms, it);
  end;
  SetLength(DefaultUniformValues, Length(all_uniforms));

  n := 0;
  for it in all_uniforms do
    if it.typ = sampler2D then//TODO other types?
    begin
      it.texture_unit := n;
      //Log(Format('%s: %d', [it.name, it.texture_unit]));
      inc(n);
    end;
  n := 0;
  for it in AllAttributes do
  begin
    it.index := n;
    inc(n);
  end;
  n := 0;
  for it in all_uniforms do
  begin
    it.index := n;
    inc(n);
  end;
  for sh in list do
    TShader(sh).BuildCache(AllAttributes, all_uniforms);
end;

procedure FreeShaders;
begin
  SetLength(DefaultUniformValues, 0);
  SetLength(AllAttributes, 0);
  SetLength(all_uniforms, 0);
end;

end.
