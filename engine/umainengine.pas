unit uMainEngine;

{$mode objfpc}{$H+}

interface

uses
  SDL2,
  sdl2_image,
  sdl2_ttf,
  sdl2_mixer,
  gl, glext, Math,
   SysUtils, uEngineTypes, heaptrc
  {$IFNDEF NO_PHYSICS}
  , Chipmunk
  {$ENDIF}  ;

var
  Window: PSDL_Window;
  Context: TSDL_GLContext;
  WindowXSize, WindowYSize: integer;

  PARAMS: array[TEngineConfig] of integer;
  ResourcesLoaded: boolean = False;
  lastfps, curfps: integer;
  lasttime: TTime;
  DTime: QWord;
  prevclock, clock, freq: UInt64;
  GlobalTick: int64;

const
  SECOND = 1 / 24 / 3600;

procedure EngineInit(ResDir: PChar); cdecl;
procedure EngineSet(param: TEngineConfig; Value: integer); cdecl;
function EngineGet(param: TEngineValue): integer; cdecl;
procedure EngineProcess; cdecl;

function RawResource(res: TRawResource; out size: integer): Pointer; cdecl;
function RawTexture(res: TSprite): cardinal; cdecl;

function LogicXSize: integer;
function LogicYSize: integer;

procedure LogSDLError;

procedure FreeMain;

implementation

uses uLoader, uLog, uInput, uGUI, uSound, uRender, uMyGLEXT, uFreeAll, uPhysics,
  uParticles;

procedure InitWindow;
var
  R: TSDL_Rect;
  got_msaa: Int32;
  ok : Boolean;
begin
  if Window <> nil then
  begin
    SDL_DestroyWindow(Window);
    Window := nil;
  end;

  LogEnter('Creating window');

  Log('Setting attributes');
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);

  {$IFDEF WINDOWS}
  if PARAMS[Antialias] > 0 then
  begin
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
  end;
  {$ENDIF}

  if PARAMS[Fullscreen] <> 0 then
  begin
    SDL_GetDisplayBounds(0, @R);
    WindowXSize := R.w;
    WindowYSize := R.h;
  end
  else
  begin
    WindowXSize := PARAMS[Width];
    WindowYSize := PARAMS[Height];
  end;
  Log('SDL_CreateWindow');
  Window := SDL_CreateWindow('NoNoEngine', SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED, WindowXSize, WindowYSize, SDL_WINDOW_OPENGL +
    ifthen(PARAMS[Fullscreen] <> 0, SDL_WINDOW_FULLSCREEN, 0) + SDL_WINDOW_RESIZABLE);
  if not Assigned(Window) then log('window not created!');
  Log('Creating context');
  Context := SDL_GL_CreateContext(Window);
  Log('Loading 3.2 functions');
  ok := Load_GL_VERSION_3_2();
  if not ok then log('Not all functions loaded!');
  Log('Loading EXT functions');
  LoadMyGLEXT;
  log('OpenGL version: ' + glGetString(GL_VERSION));
  log('OpenGL renderer: ' + glGetString(GL_RENDERER));
  log('OpenGL vendor: ' + glGetString(GL_VENDOR));



  if PARAMS[VSync] <> 0 then
    SDL_GL_SetSwapInterval(1)
  else
    SDL_GL_SetSwapInterval(0);


  if Params[Antialias] <> 0 then
    glEnable(GL_MULTISAMPLE);

  LogSDLError;
  SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, @got_msaa);
  SDL_ClearError; // suppressing strange error
  log('MSAA: ' + IntToStr(got_msaa));

  InitRender;

  LogExit('Window created');
end;

procedure EngineInit(ResDir: PChar); cdecl;
var
  ver: TSDL_Version;
  aver: PSDL_Version;
  //major, minor, patch: Integer;
begin
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide,
    exOverflow, exUnderflow, exPrecision]);
  Log('*****NO NO ENGINE!*****', True);
  if SDL_Init(SDL_INIT_VIDEO or SDL_INIT_TIMER or SDL_INIT_AUDIO) < 0 then
  begin
    log('Failed to initialize SDL');
    exit;
  end;
  try
    log('SDL Platform: ' + SDL_GetPlatform);
    log(Format('System info: %d CPU cores, %d MB RAM',
      [SDL_GetCPUCount, SDL_GetSystemRAM]));
    SDL_GetVersion(@ver);
    log(Format('SDL version: %d.%d.%d', [ver.major, ver.minor, ver.patch]));
    aver := IMG_Linked_Version;
    log(Format('SDL_Image version: %d.%d.%d', [aver^.major, aver^.minor, aver^.patch]));
    aver := TTF_Linked_Version;
    log(Format('SDL_TTF version: %d.%d.%d', [aver^.major, aver^.minor, aver^.patch]));
    //major := -1;
    //minor := -1;
    //patch := -1;
    //TTF_GetFreeTypeVersion(major, minor, patch);
    //log(Format('FreeType version: %d.%d.%d', [major, minor, patch]));
    //TTF_GetHarfBuzzVersion(major, minor, patch);
    //log(Format('HarfBuzz version: %d.%d.%d', [major, minor, patch]));

    aver := Mix_Linked_Version;
    log(Format('SDL_Mixer version: %d.%d.%d', [aver^.major, aver^.minor, aver^.patch]));
    {$IFNDEF NO_PHYSICS}
    log('Chipmunk version: ' + cpVersionString);
    {$ENDIF}

    IMG_Init(IMG_INIT_JPG + IMG_INIT_PNG);
    TTF_Init;
    Mix_Init(MIX_INIT_OGG);
    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096);
    Mix_AllocateChannels(INIT_SOUND_CHANNELS);

    freq := SDL_GetPerformanceFrequency;
    log('Timer accuracy: ' + FloatToStr(trunc(1000000000.0 / freq)) + 'ns');
    sdl_keys := SDL_GetKeyboardState(nil);
    InitWindow;
    log('loading');
    if not ResourcesLoaded then
    begin
      LoadResources(ResDir);
      ResourcesLoaded := True;
    end;
    lasttime := Now;
    lastfps := 0;
    curfps := 0;
    GlobalTick := 0;
    UpdateVolume;
    {$IFNDEF NO_PHYSICS}
    InitPhysics;
    {$ENDIF}
    InitParticles;
    LogSDLError;
  except
    on E: Exception do
      LogException('EngineInit: ', E);
  end;
end;

var
  CONFIG_NAMES: array[TEngineConfig] of
  string = ('Fullscreen', 'Width', 'Height', 'VSync', 'Antialias',
    'Log', 'Autoscale', 'Volume', 'ClearColor', 'PhysicsSpeed', 'GravityX',
    'GravityY', 'Damping', 'ProgressWidth', 'ProgressHeight', 'ProgressX', 'ProgressY',
    'CameraKX', 'CameraKY', 'CameraKXY', 'CameraKYX', 'CameraDX', 'CameraDY');

procedure EngineSet(param: TEngineConfig; Value: integer); cdecl;
begin
  if param <= ProgressY then Logf('Configuring engine: %s=%d', [CONFIG_NAMES[param], Value]);
  PARAMS[param] := Value;
  if Window = nil then
    exit;
  case param of
    Antialias:
    begin
      if Params[Antialias] = 0 then
        glDisable(GL_MULTISAMPLE)
      else
        glEnable(GL_MULTISAMPLE);
    end;
    Fullscreen:
    begin
      if Params[Fullscreen] <> 0 then
      begin
        SDL_SetWindowBordered(window, SDL_FALSE);
        SDL_MaximizeWindow(window);
      end
      else
      begin
        SDL_SetWindowBordered(window, SDL_TRUE);
        SDL_RestoreWindow(window);
      end;
      //SDL_SetWindowFullscreen(window, IfThen(Params[Fullscreen] <> 0, SDL_WINDOW_FULLSCREEN, 0)); this has some issues
    end;
    Width, Height:
    begin
      if PARAMS[Autoscale] <> 0 then
        exit;
      SDL_SetWindowSize(window, PARAMS[Width], PARAMS[Height]);
    end;
    VSync:
    begin
      if PARAMS[VSync] <> 0 then
        SDL_GL_SetSwapInterval(1)
      else
        SDL_GL_SetSwapInterval(0);
    end;
    TEngineConfig.Log: LogEnabled := PARAMS[TEngineConfig.Log] <> 0;
    Autoscale: ;//NOTHING
    Volume: UpdateVolume;
    {$IFNDEF NO_PHYSICS}
    GravityX, GravityY: cpSpaceSetGravity(uPhysics.Space,
        cpv(PARAMS[GravityX] / 1000, PARAMS[GravityY] / 1000));
    Damping: cpSpaceSetDamping(uPhysics.space, PARAMS[Damping] / 1000);
    {$ENDIF}
    CameraKX: CurCamera.kx := Value/1000;
    CameraKY: CurCamera.ky := Value/1000;
    CameraKXY: CurCamera.kxy := Value/1000;
    CameraKYX: CurCamera.kyx := Value/1000;
    CameraDX: CurCamera.dx := Value/1000;
    CameraDY: CurCamera.dy := Value/1000;
  end;
end;

function EngineGet(param: TEngineValue): integer; cdecl;
begin
  case param of
    Fullscreen, VSync, Antialias, TEngineConfig.Log, Autoscale, Volume, ClearColor:
      Result := PARAMS[TEngineConfig(param)];
    Width: Result := LogicXSize;
    Height: Result := LogicYSize;
    RealWidth:
      Result := WindowXSize;
    RealHeight:
      Result := WindowYSize;
    FPS:
      Result := lastfps;
    DeltaTime:
      Result := DTime;
    CameraKX:
      Result := Round(CurCamera.kx*1000);
    CameraKY:
      Result := Round(CurCamera.ky*1000);
    CameraKXY:
      Result := Round(CurCamera.kxy*1000);
    CameraKYX:
      Result := Round(CurCamera.kyx*1000);
    CameraDX:
      Result := Round(CurCamera.dx*1000);
    CameraDY:
      Result := Round(CurCamera.dy*1000);
    else
      Result := -1;
  end;
end;

procedure EngineProcess; cdecl;
var
  atime: TDateTime;
  dt: integer;
begin
  try
    try
      Inc(GlobalTick);
      // Update the window
      FlushRenderQueues;
      PrepareRenderState;
      ProcessParticles;
      RenderParticles;
      DrawGUI;
      FlushRenderQueues;
      SDL_GL_SwapWindow(Window);
      dt := Trunc((clock - prevclock) / freq * 1000);
      if (dt >= 13) or (PARAMS[VSync] = 0) then
        sleep(0)
      else
        sleep(15-dt);
      SDL_GetWindowSize(Window, @WindowXSize, @WindowYSize);
      glClearColor((PARAMS[ClearColor] shr 24) / 255,
        ((PARAMS[ClearColor] shr 16) and 255) / 255,
        ((PARAMS[ClearColor] shr 8) and 255) / 255, 0);
      glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
      PrepareRenderState;

      ResetGUI;
      Inc(curfps);
      atime := Now;
      if atime - lasttime > SECOND then
      begin
        lastfps := curfps;
        curfps := 0;
        lasttime := atime;
        SDL_SetWindowTitle(Window, PChar(lowerCase(
    {$I %FPCTARGETCPU%}
          ) + '-' + lowerCase(
    {$I %FPCTARGETOS%}
          ) + ': ' + IntToStr(lastfps) + ' FPS, ' + IntToStr(DTime) + ' ms, particles: '+IntToStr(ParticlesCount)));
      end;
      UpdateInputStates;
      CleanupSounds;
      if MouseState(LeftButton) = mbsClicked then
        ProcessClick(MouseGet(CursorX), MouseGet(CursorY));
      {$IFNDEF NO_PHYSICS}
      ProcessPhysics;
      {$ENDIF}
      LogSDLError;
    except
      on E: Exception do
        LogException('EngineProcess: ', E);
    end;
  finally
    clock := SDL_GetPerformanceCounter;
    DTime := Trunc((clock - prevclock) / freq * 1000);
    prevclock := clock;
    LogSDLError;
  end;
end;

function RawResource(res: TRawResource; out size: integer): pointer; cdecl;
var
  x: TRawResourceData;
begin
  x := RESRaw(res);
  Result := x.Data;
  size := x.Size;
end;

function RawTexture(res: TSprite): cardinal; cdecl;
begin
  Result := RESSprite(res).texid;
end;

function LogicXSize: integer;
begin
  if Params[Autoscale] <> 0 then
    Result := PARAMS[Width]
  else
  begin
    Result := WindowXSize;
  end;
end;

function LogicYSize: integer;
begin
  if Params[Autoscale] <> 0 then
    Result := PARAMS[Height]
  else
  begin
    Result := WindowYSize;
  end;
end;

procedure LogSDLError;
var
  s: PChar;
begin
  s := SDL_GetError;
  if length(s) > 0 then
  begin
    Log('SDL: ' + s);
    SDL_ClearError;
  end;
end;

procedure FreeMain;
begin
  LogSDLError;
  if Context <> nil then
    SDL_GL_DeleteContext(Context);
  if Window <> nil then
    SDL_DestroyWindow(Window);
  IMG_Quit();
  TTF_Quit();
  Mix_Quit();
  LogSDLError;
end;

initialization
  PARAMS[Fullscreen] := 0;
  PARAMS[Width] := 1024;
  PARAMS[Height] := 768;
  PARAMS[VSync] := 1;
  PARAMS[Antialias] := 0;
  PARAMS[TEngineConfig.Log] := 1;
  PARAMS[Autoscale] := 1;
  PARAMS[Volume] := 100;
  PARAMS[ClearColor] := $0000FFFF;
  PARAMS[PhysicsSpeed] := 1000;
  PARAMS[ProgressWidth] := 700;
  PARAMS[ProgressHeight] := 100;
  PARAMS[ProgressX] := PARAMS[Width] - PARAMS[ProgressWidth] div 2;
  PARAMS[ProgressY] := PARAMS[Height] - PARAMS[ProgressHeight] div 2 -150;
  {$IFDEF DEBUG}
  SetHeapTraceOutput('heaptrace.log');
  {$ENDIF}
  useheaptrace := True;

finalization
  LogSDLError;
  EngineFree;
end.
