unit uParticles;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, IniFiles, uPools, Generics.Collections, SysUtils ;

function EmitterAdd(Resource: TEmitterID; X, Y: TCoord; Angle: TCoord): TEmitterInstance; cdecl;
procedure EmitterDelete(Instance: TEmitterInstance);  cdecl;
procedure EmitterMove(Instance: TEmitterInstance; X, Y: TCoord); cdecl;


type


{ TParamCurve }

TSingleParamCurve = record
  min, max: array of Single;
  fixed : Boolean;
  fixed_value: Single;
end;



TParamCurves = class
  Params : array of TSingleParamCurve;
  min, max: array of array of Single;
  time: array of integer;
  cycling: boolean;
  function GetValue(param: Integer; step: integer; stepage : Integer; randomkey : Single): Single;
  procedure Advance(var step: integer; stepage : Integer);
  constructor Create(inif: TCustomIniFile; sections: array of string);
end;

//TParticleDeflector = class
//  //params
//  damping: TParamCurve;
//  Friction: TParamCurve;
//end;
//
//TParticleSpace = class
//  //links
//  deflectors: array of TParticleDeflector;
//  //params
//  damping, gravityX, gravityY: TParamCurve;
//  //flags
//end;
//
//TAngleMode = (ToScreen, ToSpeed, ToEmitter, ToSpace);
//TParticleRender = (AsPoint, AsLine, AsSprite, AsAnimated);

TEmitter = class;

TParticleParam = ({ScaleX, ScaleY, TraceLength, Angle, AnimationFrame,} ColorR, ColorG, ColorB, ColorA);

TParticle = class
  id: string;
  //links
  //sprite: TTextureID;
  //animated: TTileMap;
  //CarriedEmitter, DeathEmitter: TEmitter;
  //params
  curves: TParamCurves;
  //flags
  //AngleMode: TAngleMode;
  //RenderMode: TParticleRender;

  //caching
  constructor Create(filename: string);
  procedure FillLinks;
end;

//TEmitterShape = (esPoint, esLine, esRing, esCircle, esSquare);

TEmitterParam = (Intensity, {ParticleSpeedTan, ParticleSpeedNor, ParticleAngle, AgeDelta,} MaxLife);

TEmitter = class
  //links
  ParticlesName: string;
  Particles: TParticle;
  //params
  Curves: TParamCurves;
  //flags
  //RelativeSpeed: Boolean;
  //Flashy: Boolean;
  //Shape: TEmitterShape;
  constructor Create(filename: string);
  procedure FillLinks;
end;


procedure InitParticles;
procedure FreeParticles;
procedure ProcessParticles;
procedure RenderParticles;

function ParticlesCount: Integer;
implementation

uses uLoader, uLog, uRender;

type

{ TParticleItem }

TParticleItem = object
  emitter: TEmitter;
  particle: TParticle;
  X, Y, vX, vY{, Angle}: Single;
  Step, AgeStep, Life: Integer;
  Randoms: array[TParticleParam] of Single;
  procedure Render;
  function Value(param: TParticleParam): Single;
  procedure Init;
  procedure Process;
  procedure Die;
end;

{ TEmitterItem }

TEmitterItem = object
  emitter: TEmitter;
  X, Y{, pX, pY, Angle, Age}, PartCount: Single;
  Spawned: Integer;
  Step, AgeStep: Integer;
  procedure Process;
  procedure Die;
  procedure MoveTo(ax, ay: Single);
end;

var
  AllParticles: specialize TResourceStructArray<TParticleItem>;
  AllEmitters: specialize TResourceStructArray<TEmitterItem>;

procedure InitParticles;
var
  r: Pointer;
begin
  AllEmitters := (specialize TResourceStructArray<TEmitterItem>).Create(128);
  AllParticles := (specialize TResourceStructArray<TParticleItem>).Create(1024);
  for r in RES[Emitter] do
    TEmitter(r).FillLinks;
  for r in RES[Particle] do
    TParticle(r).FillLinks;
end;

procedure FreeParticles;
begin
  AllEmitters.Free;
  AllParticles.Free;
end;

procedure ProcessParticles;
var
  emi: TEmitterItem;
  parti: ^TParticleItem;
begin
  for emi in AllEmitters do
    emi.Process;
  for parti in AllParticles.AsPtr do
    parti^.Process;
  AllParticles.Cleanup;
end;

procedure RenderParticles;
var
  parti: TParticleItem;
begin
  for parti in AllParticles do
    parti.Render;
end;

function ParticlesCount: Integer;
begin
  Result := AllParticles.Count;
end;

function FindParticleById(id: string): TParticle;
var
  x: Pointer;
begin
  for x in RES[Particle] do
    if TParticle(x).id = id then
    begin
      Result := TParticle(x);
      exit;
    end;
  log('Particle reference not found: '+id);
  Result := nil;
end;

{ TParamCurve }

function TParamCurves.GetValue(param: Integer; step: integer; stepage: Integer; randomkey : Single): Single;
var
  left_value, right_value : Single;
  data : ^TSingleParamCurve;
begin
  data := @Params[param];
  if data^.fixed then
  begin
    Result := data^.fixed_value;
    exit;
  end;
  left_value := data^.min[step]+(data^.max[step]-data^.min[step])*randomkey;
  if step = length(time)-1 then
  begin
    if cycling then
      right_value := data^.min[0]+(data^.max[0]-data^.min[0])*randomkey
    else
    begin
      Result := left_value;
      exit;
    end;
  end
  else
    right_value := data^.min[step+1]+(data^.max[step+1]-data^.min[step+1])*randomkey;
  Result := left_value + (right_value-left_value) * stepage / time[step];
end;

procedure TParamCurves.Advance(var step : integer; stepage : Integer);
begin
  if stepage >= time[step] then
  begin
    stepage := 0;
    inc(step);
    if step >= length(time) then
    begin
      if cycling then
        step := 0
      else
        step := -1;
    end;
  end;
end;

constructor TParamCurves.Create(inif: TCustomIniFile; sections: array of string);
var
  i, n: integer;
  section: String;
  data: ^TSingleParamCurve;
begin
  SetLength(Params, Length(sections));
  i := 0;
  cycling := inif.ReadBool('Time', 'cycling', false);
  n := inif.ReadInteger('Time', 'steps', 0);
  SetLength(time, n);
  for i := 1 to n do
    time[i-1] := inif.ReadInteger('Time1', 'Step'+IntToStr(i), 1);
  for section in sections do
  begin
    data := @Params[i];
    inc(i);
    data^.fixed := inif.ValueExists(section, 'value');
    if data^.fixed then
    begin
      data^.fixed_value := inif.ReadFloat(section, 'value', 0);
      continue;
    end;
    SetLength(min, n);
    SetLength(max, n);
    for i := 1 to n do
    begin
      data^.min[i-1] := inif.ReadFloat(section, 'min'+IntToStr(i), 0);
      data^.max[i-1] := inif.ReadFloat(section, 'max'+IntToStr(i), 0);
    end;
  end;

end;

{ TEmitter }

constructor TEmitter.Create(filename: string);
var
  inif: TCustomIniFile;
begin
  inherited Create;
  inif := TMemIniFile.Create(filename);
  ParticlesName := inif.ReadString('Links', 'Particles', '');
  Curves := TParamCurves.Create(inif, ['Intensity', 'MaxLife']);
  inif.Free
end;

procedure TEmitter.FillLinks;
begin
  Particles := FindParticleById(ParticlesName);
end;

{ TParticle }

constructor TParticle.Create(filename: string);
var
  inif: TCustomIniFile;
  param: TParticleParam;
begin
  inherited Create;
  inif := TMemIniFile.Create(filename);
  id := filename;
  Curves := TParamCurves.Create(inif, ['ColorR', 'ColorG', 'ColorB', 'ColorA']);
  inif.Free
end;

procedure TParticle.FillLinks;
begin

end;

{ TParticleItem }

function color(r, g, b, a: byte): TColor;
begin
  Result := (r shl 24) + (g shl 16) + (b shl 8) + a;
end;

procedure TParticleItem.Render;
begin
  Point(X, Y, color(Trunc(value(ColorR)*255), Trunc(value(ColorG)*255), Trunc(Value(ColorB)*255), Trunc(Value(ColorA)*255)));
end;

function TParticleItem.Value(param: TParticleParam): Single;
begin
  Result := particle.Curves.GetValue(ord(param), Step, AgeStep, Randoms[param]);
end;

procedure TParticleItem.Init;
var
  param: TParticleParam;
begin
  for param in TParticleParam do
    if not particle.Curves.params[ord(param)].fixed then
      Randoms[param] := Random;
  AgeStep := 0;
  Step := 0;
end;

procedure TParticleItem.Process;
begin
  X := X+vX;
  Y := Y+vY;
  AgeStep := AgeStep+1;
  particle.Curves.Advance(Step, AgeStep);
  if Step < 0 then
    Die;
  Dec(Life);
  if Life <= 0 then
    Die;
end;

procedure TParticleItem.Die;
begin
  emitter := TEmitter(0)
end;

{ TEmitterItem }

procedure TEmitterItem.Process;
var
  next: TParticleItem;
  par: TParticleParam;
  i: Integer;
begin
  PartCount := PartCount + emitter.Curves.GetValue(Ord(Intensity),Step, AgeStep, random);
  next.particle := emitter.Particles;
  next.emitter := emitter;
  next.X := X;
  next.Y := Y;
  for par in TParticleParam do
    next.Randoms[par] := random;
  next.vX := random-0.5;
  next.vY := random-0.5;
  next.Init;
  AllParticles.AddItem(next);
end;

procedure TEmitterItem.Die;
begin

end;

procedure TEmitterItem.MoveTo(ax, ay: Single);
begin
  X := ax;
  Y := ay;
end;


function EmitterAdd(Resource: TEmitterID; X, Y: TCoord; Angle: TCoord
  ): TEmitterInstance; cdecl;
var
  item: TEmitterItem;
begin
  item.emitter := RESEmitter(Resource);
  item.X := X;
  item.Y := Y;
  Result := AllEmitters.AddItem(item);
end;

procedure EmitterDelete(Instance: TEmitterInstance); cdecl;
begin
  AllEmitters.Delete(Instance);
end;

procedure EmitterMove(Instance: TEmitterInstance; X, Y: TCoord); cdecl;
begin
  AllEmitters[Instance].MoveTo(X, Y);
end;


end.

