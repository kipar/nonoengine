unit uGUI;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, fgl, Math, uTextures, SDL2;

type

  { TButtonResource }

  TButtonResource = class
    tex: array[TButtonState] of TSimpleTexture;
    destructor Destroy; override;
  end;

  TPanelItem = class
    id: TPanel;
    parent: TPanelItem;
    flowx, flowy: TCoord;
    x, y, w, h: TCoord;
  end;

  TButtonItem = class
    res: TButtonResource;
    Data: pointer;
    parent: TPanelItem;
    x, y, w, h: TCoord;
    State: TButtonState;
    Font: TFontInstance;
    Text: string;
  end;

  { TEditItem }

  TEditItem = class
    Data: pointer;
    parent: TPanelItem;
    x, y, w, h: TCoord;
    rawx, rawy, raww, rawh: TCoord;
    typ: TInputType;
    Font: TFontInstance;
    Text: string;
    ChangedByInput: boolean;
    Cursor: Integer;
    IntValue, NewValue, IntMin, IntMax: Integer;
    destructor Destroy; override;
  end;

  { TFocusTag }

  TFocusTag = class
    res: TButtonResource;
    Exists: boolean;
    Data: pointer;
    x, y, w, h: TCoord;
    function Match(bt: TButtonItem): boolean;
    procedure Assign(bt: TButtonItem);
  end;

  TPanelsMap = specialize TFPGMapObject<TPanel, TPanelItem>;
  TButtonsList = specialize TFPGObjectList<TButtonItem>;
  TEditList = specialize TFPGObjectList<TEditItem>;

procedure Panel(id: TPanel; Parent: TPanel; x, y, w, h: TCoord;
  HAlign: THAlign; VAlign: TVAlign); cdecl;
function Button(btn: TButton; Parent: TPanel; x, y, w, h: TCoord;
  HAlign: THAlign; VAlign: TVAlign; Text: PChar; Font: TFontInstance;
  Data: pointer): TButtonState; cdecl;

function InputInt(Value: integer; min: integer; max: integer;
  Parent: TPanel; x, y, w, h: TCoord; HAlign: THAlign; VAlign: TVAlign;
  Font: TFontInstance; active: PBoolean; Data: pointer): integer; cdecl;
//function InputString(Parent: TPanel; x, y,w,h: TCoord; Value: PChar; HAlign: THAlign; VAlign: TVAlign; Font: TFontInstance; active: PBoolean; was_changed: Boolean): Boolean; cdecl;

function GetGUICoord(Coord: TGUICoord): TCoord; cdecl;

procedure ResetGUI;
procedure DrawGUI;
function ProcessClick(x, y: TCoord): boolean;
function ProcessEdits(text: string; key: TSDL_KeyCode): boolean;

var
  panels: TPanelsMap;
  Buttons: TButtonsList;
  PrevButtons: TButtonsList;
  Root: TPanelItem;
  LastButton: TObject;
  Focused: TFocusTag;
  BlinkTime: Integer;

  PrevEdits, Edits: TEditList;
  FocusedEdit: TEditItem;
  sdl_input_started: boolean;

procedure StartTextInput;
procedure EndTextInput;

procedure FreeGUI;

implementation

uses uMainEngine, uInput, uLoader, uRender, uText, uVertexList, uLog, GL;

procedure ApplyAligns(parent: TPanelItem; var x, y: TCoord; w, h: TCoord;
  HAlign: THAlign; VAlign: TVAlign);
begin
  case HAlign of
    haNone: x := x;
    haLeft: x := x;
    haCenter: x := parent.w / 2 - w / 2 + x;
    haRight: x := parent.w - w - x;
    haFlow:
    begin
      parent.flowx := parent.flowx + x + w;
      x := parent.flowx - w;
    end;
  end;
  x := x + parent.x;
  case VAlign of
    vaNone: y := y;
    vaTop: y := y;
    vaCenter: y := parent.h / 2 - h / 2 + y;
    vaBottom: y := parent.h - h - y;
    vaFlow:
    begin
      parent.flowy := parent.flowy + y + h;
      y := parent.flowy - h;
    end;
  end;
  y := y + parent.y;
end;

procedure Panel(id: TPanel; Parent: TPanel; x, y, w, h: TCoord;
  HAlign: THAlign; VAlign: TVAlign); cdecl;
var
  pn: TPanelItem;
begin
  pn := TPanelItem.Create;
  if not panels.TryGetData(Parent, pn.parent) then
    pn.parent := Root;
  ApplyAligns(pn.parent, x, y, w, h, HAlign, VAlign);
  pn.id := id;
  pn.x := x;
  pn.y := y;
  pn.h := h;
  pn.w := w;
  LastButton := pn;
  panels.Add(id, pn);
end;

function Button(btn: TButton; Parent: TPanel; x, y, w, h: TCoord;
  HAlign: THAlign; VAlign: TVAlign; Text: PChar; Font: TFontInstance;
  Data: pointer): TButtonState; cdecl;
var
  i: integer;
  it, old: TButtonItem;
  fine: boolean;
begin
  it := TButtonItem.Create;
  if not panels.TryGetData(Parent, it.parent) then
    it.parent := Root;
  it.res := RESButton(Btn);
  it.Data := Data;
  ApplyAligns(it.parent, x, y, w, h, HAlign, VAlign);
  it.x := x;
  it.y := y;
  it.w := w;
  it.h := h;
  it.State := bsNormal;
  if (Font >= 0) and (Text <> nil) and (Text <> '') then
  begin
    it.Font := Font;
    it.Text := Text;
  end
  else
  begin
    it.Text := '';
  end;
  LastButton := it;
  Buttons.Add(it);

  Result := bsNormal;
  //find prev button
  for i := PrevButtons.Count - 1 downto 0 do
  begin
    old := PrevButtons[i];
    if it.Data <> nil then
      fine := old.Data = it.Data
    else
      fine := (old.res = it.res) and (old.x = it.x) and (old.y = it.y) and
        (old.w = it.w) and (old.h = it.h);
    if fine then
    begin
      Result := old.State;
      LastButton := old;
      exit;
    end;
  end;
end;

function InputInt(Value: integer; min: integer; max: integer;
  Parent: TPanel; x, y, w, h: TCoord; HAlign: THAlign; VAlign: TVAlign;
  Font: TFontInstance; active: PBoolean; Data: pointer): integer; cdecl;
var
  olded, ed: TEditItem;
  pan: TPanelItem;
  fine: boolean;
begin
  //find prev edit
  ed := nil;
  if not panels.TryGetData(Parent, pan) then pan := Root;
  for olded in PrevEdits do
  begin
    if Data <> nil then
      fine := olded.Data = Data
    else
    begin
      fine := (olded.parent.id = pan.id) and (olded.rawx = x) and (olded.rawy = y) and
        (olded.raww = w) and (olded.rawh = h);
    end;
    if fine then
    begin
      ed := olded;
      break;
    end;
  end;
  //create new
  if ed = nil then
  begin
    ed := TEditItem.Create;
    if not panels.TryGetData(Parent, ed.parent) then
      ed.parent := pan;
    ed.typ := etInteger;
    ed.Data := Data;
    Edits.Add(ed);
  end
  else
  begin
    //move prev
    PrevEdits.FreeObjects := False;
    PrevEdits.Remove(ed);
    PrevEdits.FreeObjects := True;
    Edits.Add(ed);
  end;
  ed.Font := Font;
  ed.rawx := x;
  ed.rawy := y;
  ed.raww := w;
  ed.rawh := h;
  ApplyAligns(pan, x, y, w, h, HAlign, VAlign);
  ed.x := x;
  ed.y := y;
  ed.w := w;
  ed.h := h;
  ed.IntMax := max;
  ed.IntMin := min;
  if Value <> ed.IntValue then
  begin
    logf('edit changed from program %d instead of %d(%d)', [value, ed.IntValue, ed.NewValue]);
    ed.IntValue := Value;
    Str(Value,ed.Text);
    ed.Cursor := Length(ed.Text);
  end
  else if ed.ChangedByInput then ed.IntValue := ed.NewValue;
  ed.ChangedByInput := False;
  if Assigned(active) and active^ then
  begin
    FocusedEdit := ed;
    StartTextInput;
  end;
  if Assigned(active) then active^ := ed = FocusedEdit;
  //logf('returning %d', [ed.IntValue]);
  Result := ed.IntValue;
end;

function GetGUICoord(Coord: TGUICoord): TCoord; cdecl;
var
  bx, by, w, h: TCoord;
begin
  if LastButton <> nil then
  begin
    if LastButton is TButtonItem then
    begin
      bx := TButtonItem(LastButton).x;
      by := TButtonItem(LastButton).y;
      w := TButtonItem(LastButton).w;
      h := TButtonItem(LastButton).h;
    end
    else
    begin
      bx := TPanelItem(LastButton).x;
      by := TPanelItem(LastButton).y;
      w := TPanelItem(LastButton).w;
      h := TPanelItem(LastButton).h;
    end;
  end
  else
  begin
    bx := 0;
    by := 0;
    w := 1;
    h := 1;
  end;
  case Coord of
    gcX: Result := bx;
    gcY: Result := by;
    gcWidth: Result := w;
    gcHeight: Result := h;
    gcMouseX: Result := (MouseGet(CursorX) - bx) / w;
    gcMouseY: Result := (MouseGet(CursorY) - by) / h;
    else
      Result := 0;
  end;
end;

procedure ResetGUI;
var
  tmp: TButtonsList;
  tmpe: TEditList;
begin
  panels.Clear;
  tmp := PrevButtons;
  PrevButtons := Buttons;
  Buttons := tmp;
  Buttons.Clear;
  LastButton := nil;

  tmpe := PrevEdits;
  PrevEdits := Edits;
  Edits := tmpe;
  Edits.Clear;
  //FocusedEdit := nil;

  root.w := LogicXSize;
  root.h := LogicYSize;
end;

procedure DrawGUI;
var
  bt: TButtonItem;
  ed: TEditItem;
  s: string;
  ax, ay, xs,ys: TCoord;
  v00, v01, v10, v11: TSpriteVertex;
begin
  if MouseState(LeftButton) = mbsUp then
    Focused.Exists := False;
  ResetView;
  for bt in buttons do
  begin
    if InRange(MouseGet(CursorX), bt.x, bt.x + bt.w) and
      InRange(MouseGet(CursorY), bt.y, bt.y + bt.h) then
    begin
      if (MouseState(LeftButton) <> mbsUp) and
        ((not Focused.Exists) or (Focused.Match(bt))) then
      begin
        bt.State := bsPressed;
        Focused.Assign(bt);
        FocusedEdit := nil;
        EndTextInput
      end
      else
        bt.State := bsHover;
    end
    else
      bt.state := bsNormal;
  end;
  SetLayer(LAYER_GUI);
  for bt in Buttons do
  begin
    SpriteSlicedInternal(bt.res.tex[bt.State], bt.x, bt.y, bt.w, bt.h, WHITE);

    if bt.Text <> '' then
    begin
      DrawTextBoxed(bt.Font, PChar(bt.Text), bt.x, bt.y, bt.w, bt.h, haCenter, vaCenter);
    end;
  end;
  for ed in Edits do
  begin
    DrawTextBoxed(ed.Font, PChar(ed.text), ed.x, ed.y, ed.w, ed.h, haLeft, vaCenter);
    if (ed = FocusedEdit) and odd((SDL_GetTicks-BlinkTime) div 500) then
    begin
      MeasureText(ed.Font, PChar(Copy(ed.Text, 1, ed.Cursor)), ax, ay);
      if ed.Cursor = 0 then
        ay := ed.h/2;
      DrawTextBoxed(ed.Font, '|', ed.x + ax - 50, ed.y, 100, ed.h, haCenter, vaCenter);
    end;
  end
end;

function ProcessClick(x, y: TCoord): boolean;
var
  it: TButtonItem;
  ed: TEditItem;
begin
  Result := False;
    for ed in PrevEdits do
      if InRange(x, ed.x, ed.x + ed.w) and
      InRange(y, ed.y, ed.y + ed.h) then
      begin
        FocusedEdit := ed;
      StartTextInput;
        //TODO calculate cursor pos
        //FocusedEdit.Cursor := 0;
    exit;
  end;
  if not Focused.Exists then exit;
  if not (InRange(x, Focused.x, Focused.x + Focused.w) and
    InRange(y, Focused.y, Focused.y + Focused.h)) then
    exit;
  for it in PrevButtons do
  begin
    if Focused.Match(it) then
    begin
      it.State := bsClicked;
      Focused.Exists := False;
      Result := True;
    end;
  end;
end;

function ProcessEdits(text: string; key: TSDL_KeyCode): boolean;
var
  v, c: Integer;
begin
  Result := false;
  if FocusedEdit = nil then exit;
  if text <> '' then
  begin
    Insert(text, FocusedEdit.Text, FocusedEdit.Cursor+1);
    Inc(FocusedEdit.Cursor, Length(text));
  end
  else case key of
    SDLK_BACKSPACE:
      if FocusedEdit.Cursor > 0 then
      begin
        Delete(FocusedEdit.Text, FocusedEdit.Cursor, 1);
        dec(FocusedEdit.Cursor);
      end;
    SDLK_HOME: FocusedEdit.Cursor := 0;
    SDLK_DELETE: if FocusedEdit.Cursor < Length(FocusedEdit.Text) then Delete(FocusedEdit.Text, FocusedEdit.Cursor+1, 1);
    SDLK_END: FocusedEdit.Cursor := Length(FocusedEdit.Text);
    SDLK_RIGHT: FocusedEdit.Cursor := min(Length(FocusedEdit.Text), FocusedEdit.Cursor+1);
    SDLK_LEFT: FocusedEdit.Cursor := max(0, FocusedEdit.Cursor-1);
    else exit;
  end;
  FocusedEdit.ChangedByInput := true;
  Val(FocusedEdit.Text, v, c);
  logf('edit changed from input: %d -> %d', [FocusedEdit.IntValue, FocusedEdit.NewValue]);
  if c = 0 then
  begin
    if v > FocusedEdit.IntMax then
    begin
      v := FocusedEdit.IntMax;
      Str(v, FocusedEdit.Text);
    end
    else if v < FocusedEdit.IntMin then
    begin
      v := FocusedEdit.IntMin;
      Str(v, FocusedEdit.Text);
    end;
    FocusedEdit.NewValue := v
  end;
end;

procedure StartTextInput;
begin
  if sdl_input_started then exit;
  SDL_StartTextInput;
  sdl_input_started := true;
end;

procedure EndTextInput;
begin
  if not sdl_input_started then exit;
  SDL_StopTextInput;
  sdl_input_started := false;
end;

procedure FreeGUI;
begin
  ResetGUI;
  panels.Free;
  Root.Free;
  Buttons.Free;
  PrevButtons.Free;
  Edits.Free;
  PrevEdits.Free;
  Focused.Free;
end;

{ TEditItem }

destructor TEditItem.Destroy;
begin
  if FocusedEdit = self then
  begin
    FocusedEdit := nil;
    EndTextInput;
  end;
  inherited Destroy;
end;

{ TButtonResource }

destructor TButtonResource.Destroy;
var
  atex: TSimpleTexture;
  st, st2: TButtonState;
begin
  for st in TButtonState do
    for st2 in TButtonState do
    begin
      if tex[st] = nil then
        break;
      if Ord(st2) >= Ord(st) then
        break;
      if tex[st2] = tex[st] then
      begin
        tex[st] := nil;
        break;
      end;
    end;
  for atex in tex do
    atex.Free;
  inherited Destroy;
end;

{ TFocusTag }

function TFocusTag.Match(bt: TButtonItem): boolean;
begin
  if Data <> nil then
    Result := bt.Data = Data
  else
    Result := (bt.Data = nil) and (bt.res = res) and (bt.x = x) and
      (bt.y = y) and (bt.w = w) and (bt.h = h);
end;

procedure TFocusTag.Assign(bt: TButtonItem);
begin
  Exists := True;
  Data := bt.Data;
  res := bt.res;
  x := bt.x;
  y := bt.y;
  w := bt.w;
  h := bt.h;
end;


begin
  panels := TPanelsMap.Create;
  Root := TPanelItem.Create;
  Buttons := TButtonsList.Create;
  PrevButtons := TButtonsList.Create;
  Edits := TEditList.Create;
  PrevEdits := TEditList.Create;
  Focused := TFocusTag.Create;
end.
