unit uMyGLEXT;

{$MACRO ON}
{$mode DELPHI}{$H+}
{$IFDEF Windows}
  {$DEFINE extdecl:=stdcall }
{$ELSE}
  {$DEFINE extdecl:=cdecl }
{$ENDIF}

interface

uses
  gl, glext;

type
  TglVertexAttribLPointer = procedure(index: GLuint; size: GLint; _type: GLenum; stride: GLsizei; const pointer: PGLvoid); extdecl;
var
  glVertexAttribLPointer : TglVertexAttribLPointer;

procedure LoadMyGLEXT;

implementation

uses ulog;

procedure LoadMyGLEXT;
begin
  glVertexAttribLPointer := wglGetProcAddress('glVertexAttribLPointer');
  if not Assigned(glVertexAttribLPointer) then log('no glVertexAttribLPointer');
end;

end.

