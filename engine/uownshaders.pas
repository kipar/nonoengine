unit uownshaders;

{$mode objfpc}{$H+}

interface


const
  SIMPLE_VS_SHADER = ''#13#10 + '    #version 130'#13#10 +
    '    uniform sampler2D tex;'#13#10 + '    uniform vec2 screen_size;'#13#10 +
    '    in vec3 pos;'#13#10 + '    in vec3 texpos;'#13#10 + '    in vec4 color;'#13#10 +
    ''#13#10 + '    out vec4 pixel_color;'#13#10 + '    out vec3 texcoord;'#13#10 +
    ''#13#10 + '    void main(void)'#13#10 + '    {'#13#10 +
    '      gl_Position = vec4(((pos.xy/screen_size)-0.5f)*vec2(2.0, -2.0), pos.z, 1.0f);'#13#10 + '      texcoord = texpos;'#13#10 + '      pixel_color = color;'#13#10 + '    }'#13#10 +
    ''#13#10;

  SIMPLE_FS_SHADER = ''#13#10 + '    #version 130             '#13#10 +
    '    uniform sampler2D tex;   '#13#10 + '    in vec3 texcoord;        '#13#10 +
    '    in vec4 pixel_color;     '#13#10 + '    out vec4 result;         '#13#10 +
    '                             '#13#10 + '    void main(void)          '#13#10 +
    '    {                        '#13#10 + '      if(texcoord.z < 0)     '#13#10 +
    '    	result = pixel_color;   '#13#10 + '      else                   '#13#10 +
    '    	result = texture(tex, texcoord.xy)*pixel_color; '#13#10 +
    '      if(result.a <= 0)discard;'#13#10 + '    }                        '#13#10 + ''#13#10;



implementation

end.
