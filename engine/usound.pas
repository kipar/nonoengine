unit uSound;

{$mode objfpc}{$H+}

interface

uses
  uEngineTypes, Math, sdl2_mixer;

procedure Play(sound: TSound; volume: single; Data: Pointer); cdecl;
procedure Music(music: TSound; volume: single); cdecl;
function SoundPlaying(sound: TSound; Data: Pointer): boolean; cdecl;

type

  TSoundItem = record
    active: boolean;
    sound: TSound;
    Data: Pointer;
    volume: single;
  end;

var
  PlayingSounds: array of TSoundItem;

procedure UpdateVolume;
procedure CleanupSounds;

const
  INIT_SOUND_CHANNELS = 256;


procedure FreeSounds;

implementation

uses uLoader, uMainEngine, uLog;

function FindFreeSound: integer;
var
  i: integer;
  it: TSoundItem;
begin
  Result := 0;
  for i := 1 to length(PlayingSounds) - 1 do
  begin
    it := PlayingSounds[i];
    if not it.active then
    begin
      Result := i;
      exit;
    end;
  end;
  Result := -1;
end;

function FindSound(sound: TSound; Data: Pointer): integer;
var
  i: integer;
  it: TSoundItem;
begin
  Result := -1;
  if Data = nil then
    exit;
  Result := 0;
  for i := 1 to length(PlayingSounds) - 1 do
  begin
    it := PlayingSounds[i];
    if it.active and (it.sound = sound) and (it.Data = Data) then
    begin
      Result := i;
      exit;
    end;
  end;
end;

function VolumeToSDL(avolume: single): integer;
begin
  Result := EnsureRange(Trunc((avolume / 100 * 255)), 0, 255);
end;

procedure Play(sound: TSound; volume: single; Data: Pointer); cdecl;
var
  v: integer;
begin
  v := FindSound(sound, Data);
  if v > 0 then
  begin
    PlayingSounds[v].volume := volume;
    Mix_SetPanning(v, VolumeToSDL(volume), VolumeToSDL(volume));
  end
  else
  begin
    v := FindFreeSound;
    if v < 0 then
    begin
      v := Length(PlayingSounds);
      SetLength(PlayingSounds, Length(PlayingSounds) * 2);
      Mix_AllocateChannels(Length(PlayingSounds));
      Logf('Allocating mixer channels: %d', [Length(PlayingSounds)]);
    end;
    PlayingSounds[v].sound := sound;
    PlayingSounds[v].volume := volume;
    PlayingSounds[v].Data := Data;
    PlayingSounds[v].active := True;
    Mix_PlayChannel(v, RESSound(sound), 0);
    Mix_SetPanning(v, VolumeToSDL(volume), VolumeToSDL(volume));
  end;
end;

procedure Music(music: TSound; volume: single); cdecl;
begin
  if music < 0 then
  begin
    Mix_HaltChannel(0);
    PlayingSounds[0].active := False;
    exit;
  end;
  if PlayingSounds[0].active and (PlayingSounds[0].sound = music) then
  begin
    Mix_SetPanning(0, VolumeToSDL(volume), VolumeToSDL(volume));
    PlayingSounds[0].volume := volume;
  end
  else
  begin
    PlayingSounds[0].sound := music;
    PlayingSounds[0].volume := volume;
    PlayingSounds[0].active := True;
    Mix_PlayChannel(0, RESSound(music), -1);
    Mix_SetPanning(0, VolumeToSDL(volume), VolumeToSDL(volume));
  end;
end;

function SoundPlaying(sound: TSound; Data: Pointer): boolean; cdecl;
var
  v: integer;
begin
  v := FindSound(sound, Data);
  Result := v >= 0;
end;

procedure CleanupSounds;
var
  i: integer;
  it: ^TSoundItem;
begin
  for i := 1 to length(PlayingSounds) - 1 do
  begin
    it := @PlayingSounds[i];
    if it^.active and (Mix_Playing(i) = 0) then
      it^.active := False;
  end;
end;

procedure FreeSounds;
var
  i: integer;
  it: ^TSoundItem;
begin
  for i := 1 to length(PlayingSounds) - 1 do
  begin
    it := @PlayingSounds[i];
    if it^.active and (Mix_Playing(i) <> 0) then
      Mix_HaltChannel(i);
  end;
  SetLength(PlayingSounds, 0);
end;


procedure UpdateVolume;
begin
  Mix_SetPanning(MIX_CHANNEL_POST, VolumeToSDL(PARAMS[Volume]),
    VolumeToSDL(PARAMS[Volume]));
end;

begin
  SetLength(PlayingSounds, INIT_SOUND_CHANNELS);
end.
