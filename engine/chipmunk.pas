
unit chipmunk;
interface

{
  Automatically converted by H2Pas 1.0.0 from c:\projects1\MMO\Chipmunk-7.0.1\testinc\test.h
  The following command line parameters were used:
    -d
    -s
    c:\projects1\MMO\Chipmunk-7.0.1\testinc\test.h
}
const
  {$IFDEF WINDOWS}
    LIBRARY_chipmunk = 'chipmunk.dll';
  {$ELSE}
    LIBRARY_chipmunk = 'libchipmunk.so';
  {$ENDIF}


  Type
  Pchar  = ^char;
  PcpArbiter  = ^cpArbiter;
  PcpBBTree  = ^cpBBTree;
  PcpBody  = ^cpBody;
  PPcpBody  = ^PcpBody;
  PcpCircleShape  = ^cpCircleShape;
  PcpConstraint  = ^cpConstraint;
  PcpContactPointSet  = ^cpContactPointSet;
  PcpDampedRotarySpring  = ^cpDampedRotarySpring;
  PcpDampedSpring  = ^cpDampedSpring;
  PcpGearJoint  = ^cpGearJoint;
  PcpGrooveJoint  = ^cpGrooveJoint;
  PcpPinJoint  = ^cpPinJoint;
  PcpPivotJoint  = ^cpPivotJoint;
  PcpPointQueryInfo  = ^cpPointQueryInfo;
  PcpPolyShape  = ^cpPolyShape;
  PcpRatchetJoint  = ^cpRatchetJoint;
  PcpRotaryLimitJoint  = ^cpRotaryLimitJoint;
  PcpSegmentQueryInfo  = ^cpSegmentQueryInfo;
  PcpSegmentShape  = ^cpSegmentShape;
  PcpShape  = ^cpShape;
  PPcpShape  = ^PcpShape;
  PcpSimpleMotor  = ^cpSimpleMotor;
  PcpSlideJoint  = ^cpSlideJoint;
  PcpSpace  = ^cpSpace;
  PcpSpaceDebugDrawOptions  = ^cpSpaceDebugDrawOptions;
  PcpSpaceHash  = ^cpSpaceHash;
  PcpSpatialIndex  = ^cpSpatialIndex;
  PcpSweep1D  = ^cpSweep1D;
  PcpVect  = ^cpVect;
  Plongint  = ^longint;

  PcpCollisionHandler = ^cpCollisionHandler;
{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}

uintptr_t = UIntPtr;
uint32_t = UInt32;


cpFloat = double;

    cpHashValue = uintptr_t;

    cpCollisionID = uint32_t;

    cpBool = ByteBool;

    cpDataPointer = pointer;

    cpCollisionType = uintptr_t;

    cpGroup = uintptr_t;

    cpBitmask = dword;

    cpTimestamp = dword;

    cpVect = record
        x : cpFloat;
        y : cpFloat;
      end;

    cpTransform = record
        a : cpFloat;
        b : cpFloat;
        c : cpFloat;
        d : cpFloat;
        tx : cpFloat;
        ty : cpFloat;
      end;

    cpMat2x2 = record
        a : cpFloat;
        b : cpFloat;
        c : cpFloat;
        d : cpFloat;
      end;

    //cpArray;
    cpArray = record
        num : longint;
        max : longint;
        arr : ^pointer;
      end;

    //cpHashSet;
    cpHashSet = record

    end;
    cpContactBufferHeader = record

    end;
    cpSpaceHash = record

    end;
    cpBBTree = record

    end;
    cpSweep1D = record

    end;

    //cpBody;

    cpBodyVelocityFunc = procedure (body:PcpBody; gravity:cpVect; damping:cpFloat; dt:cpFloat);stdcall;
    cpBodyPositionFunc = procedure (body:PcpBody; dt:cpFloat);stdcall;
    cpBody = record
        velocity_func : cpBodyVelocityFunc;
        position_func : cpBodyPositionFunc;
        m : cpFloat;
        m_inv : cpFloat;
        i : cpFloat;
        i_inv : cpFloat;
        cog : cpVect;
        p : cpVect;
        v : cpVect;
        f : cpVect;
        a : cpFloat;
        w : cpFloat;
        t : cpFloat;
        transform : cpTransform;
        userData : cpDataPointer;
        v_bias : cpVect;
        w_bias : cpFloat;
        space : ^cpSpace;
        shapeList : ^cpShape;
        arbiterList : ^cpArbiter;
        constraintList : ^cpConstraint;
        sleeping : record
            root : ^cpBody;
            next : ^cpBody;
            idleTime : cpFloat;
          end;
      end;
    //cpShape;
    //cpCircleShape;
    //cpSegmentShape;
    //cpPolyShape;
    cpShapeMassInfo = record
         m : cpFloat;
         i : cpFloat;
         cog : cpVect;
         area : cpFloat;
       end;


     cpShapeType = (CP_CIRCLE_SHAPE,CP_SEGMENT_SHAPE,CP_POLY_SHAPE,
       CP_NUM_SHAPES);

     cpBB = record
         l : cpFloat;
         b : cpFloat;
         r : cpFloat;
         t : cpFloat;
       end;

     cpSpatialIndexBBFunc = function (obj:pointer):cpBB;stdcall;

     cpSpatialIndex = record
         klass : ^cpSpatialIndexClass;
         bbfunc : cpSpatialIndexBBFunc;
         staticIndex : ^cpSpatialIndex;
         dynamicIndex : ^cpSpatialIndex;
       end;


         cpBBTreeVelocityFunc = function (obj:pointer):cpVect;stdcall;
     cpShapeCacheDataImpl = function (shape:PcpShape; transform:cpTransform):cpBB;stdcall;

     cpShapeDestroyImpl = procedure (shape:PcpShape);stdcall;
 (* Const before type ignored *)

     cpShapePointQueryImpl = procedure (shape:PcpShape; p:cpVect; info:PcpPointQueryInfo);stdcall;
 (* Const before type ignored *)

     cpShapeSegmentQueryImpl = procedure (shape:PcpShape; a:cpVect; b:cpVect; radius:cpFloat; info:PcpSegmentQueryInfo);stdcall;
     cpShapeClass = record
         _type : cpShapeType;
         cacheData : cpShapeCacheDataImpl;
         destroy : cpShapeDestroyImpl;
         pointQuery : cpShapePointQueryImpl;
         segmentQuery : cpShapeSegmentQueryImpl;
       end;

 (* Const before type ignored *)

 cpShapeFilter = record
     group : cpGroup;
     categories : cpBitmask;
     mask : cpBitmask;
   end;
     cpShape = record
         klass : ^cpShapeClass;
         space : ^cpSpace;
         body : ^cpBody;
         massInfo : cpShapeMassInfo;
         bb : cpBB;
         sensor : cpBool;
         e : cpFloat;
         u : cpFloat;
         surfaceV : cpVect;
         userData : cpDataPointer;
         collision_type : cpCollisionType;
         filter : cpShapeFilter;
         next : ^cpShape;
         prev : ^cpShape;
         hashid : cpHashValue;
       end;

     cpCircleShape = record
         shape : cpShape;
         c : cpVect;
         tc : cpVect;
         r : cpFloat;
       end;

     cpSegmentShape = record
         shape : cpShape;
         a : cpVect;
         b : cpVect;
         n : cpVect;
         ta : cpVect;
         tb : cpVect;
         tn : cpVect;
         r : cpFloat;
         a_tangent : cpVect;
         b_tangent : cpVect;
       end;

     cpSplittingPlane = record
         v0 : cpVect;
         n : cpVect;
       end;


     { The untransformed planes are appended at the end of the transformed planes. }
   { Allocate a small number of splitting planes internally for simple poly. }


   cpPolyShape = record
         shape : cpShape;
         r : cpFloat;
         count : longint;
         planes : ^cpSplittingPlane;
         _planes : array[0..(2*6)-1] of cpSplittingPlane;
       end;

    //cpConstraint;
    //cpPinJoint;
    //cpSlideJoint;
    //cpPivotJoint;
    //cpGrooveJoint;
    //cpDampedSpring;
    //cpDampedRotarySpring;
    //cpRotaryLimitJoint;
    //cpRatchetJoint;
    //cpGearJoint;
    //cpSimpleMotorJoint;
    cpConstraintPreStepImpl = procedure (constraint:PcpConstraint; dt:cpFloat);stdcall;

    cpConstraintApplyCachedImpulseImpl = procedure (constraint:PcpConstraint; dt_coef:cpFloat);stdcall;

    cpConstraintApplyImpulseImpl = procedure (constraint:PcpConstraint; dt:cpFloat);stdcall;

    cpConstraintGetImpulseImpl = function (constraint:PcpConstraint):cpFloat;stdcall;

    cpConstraintClass = record
        preStep : cpConstraintPreStepImpl;
        applyCachedImpulse : cpConstraintApplyCachedImpulseImpl;
        applyImpulse : cpConstraintApplyImpulseImpl;
        getImpulse : cpConstraintGetImpulseImpl;
      end;
(* Const before type ignored *)
cpConstraintPreSolveFunc = procedure (constraint:PcpConstraint; space:PcpSpace);stdcall;

cpConstraintPostSolveFunc = procedure (constraint:PcpConstraint; space:PcpSpace);stdcall;

    cpConstraint = record
        klass : ^cpConstraintClass;
        space : ^cpSpace;
        a : ^cpBody;
        b : ^cpBody;
        next_a : ^cpConstraint;
        next_b : ^cpConstraint;
        maxForce : cpFloat;
        errorBias : cpFloat;
        maxBias : cpFloat;
        collideBodies : cpBool;
        preSolve : cpConstraintPreSolveFunc;
        postSolve : cpConstraintPostSolveFunc;
        userData : cpDataPointer;
      end;

    cpPinJoint = record
        constraint : cpConstraint;
        anchorA : cpVect;
        anchorB : cpVect;
        dist : cpFloat;
        r1 : cpVect;
        r2 : cpVect;
        n : cpVect;
        nMass : cpFloat;
        jnAcc : cpFloat;
        bias : cpFloat;
      end;

    cpSlideJoint = record
        constraint : cpConstraint;
        anchorA : cpVect;
        anchorB : cpVect;
        min : cpFloat;
        max : cpFloat;
        r1 : cpVect;
        r2 : cpVect;
        n : cpVect;
        nMass : cpFloat;
        jnAcc : cpFloat;
        bias : cpFloat;
      end;

    cpPivotJoint = record
        constraint : cpConstraint;
        anchorA : cpVect;
        anchorB : cpVect;
        r1 : cpVect;
        r2 : cpVect;
        k : cpMat2x2;
        jAcc : cpVect;
        bias : cpVect;
      end;

    cpGrooveJoint = record
        constraint : cpConstraint;
        grv_n : cpVect;
        grv_a : cpVect;
        grv_b : cpVect;
        anchorB : cpVect;
        grv_tn : cpVect;
        clamp : cpFloat;
        r1 : cpVect;
        r2 : cpVect;
        k : cpMat2x2;
        jAcc : cpVect;
        bias : cpVect;
      end;

    cpDampedSpringForceFunc = function (spring:PcpConstraint; dist:cpFloat):cpFloat;stdcall;

    cpDampedSpring = record
        constraint : cpConstraint;
        anchorA : cpVect;
        anchorB : cpVect;
        restLength : cpFloat;
        stiffness : cpFloat;
        damping : cpFloat;
        springForceFunc : cpDampedSpringForceFunc;
        target_vrn : cpFloat;
        v_coef : cpFloat;
        r1 : cpVect;
        r2 : cpVect;
        nMass : cpFloat;
        n : cpVect;
        jAcc : cpFloat;
      end;

    cpDampedRotarySpringTorqueFunc = function (spring:PcpConstraint; relativeAngle:cpFloat):cpFloat;stdcall;

    cpDampedRotarySpring = record
        constraint : cpConstraint;
        restAngle : cpFloat;
        stiffness : cpFloat;
        damping : cpFloat;
        springTorqueFunc : cpDampedRotarySpringTorqueFunc;
        target_wrn : cpFloat;
        w_coef : cpFloat;
        iSum : cpFloat;
        jAcc : cpFloat;
      end;

    cpRotaryLimitJoint = record
        constraint : cpConstraint;
        min : cpFloat;
        max : cpFloat;
        iSum : cpFloat;
        bias : cpFloat;
        jAcc : cpFloat;
      end;

    cpRatchetJoint = record
        constraint : cpConstraint;
        angle : cpFloat;
        phase : cpFloat;
        ratchet : cpFloat;
        iSum : cpFloat;
        bias : cpFloat;
        jAcc : cpFloat;
      end;

    cpGearJoint = record
        constraint : cpConstraint;
        phase : cpFloat;
        ratio : cpFloat;
        ratio_inv : cpFloat;
        iSum : cpFloat;
        bias : cpFloat;
        jAcc : cpFloat;
      end;

    cpSimpleMotor = record
        constraint : cpConstraint;
        rate : cpFloat;
        iSum : cpFloat;
        jAcc : cpFloat;
      end;
    //cpCollisionHandler;
    cpCollisionBeginFunc = function (arb:PcpArbiter; space:PcpSpace; userData:cpDataPointer):cpBool;stdcall;

    cpCollisionPreSolveFunc = function (arb:PcpArbiter; space:PcpSpace; userData:cpDataPointer):cpBool;stdcall;

    cpCollisionPostSolveFunc = procedure (arb:PcpArbiter; space:PcpSpace; userData:cpDataPointer);stdcall;

    cpCollisionSeparateFunc = procedure (arb:PcpArbiter; space:PcpSpace; userData:cpDataPointer);stdcall;
    cpCollisionHandler = record
        typeA : cpCollisionType;
        typeB : cpCollisionType;
        beginFunc : cpCollisionBeginFunc;
        preSolveFunc : cpCollisionPreSolveFunc;
        postSolveFunc : cpCollisionPostSolveFunc;
        separateFunc : cpCollisionSeparateFunc;
        userData : cpDataPointer;
      end;

    //cpContactPointSet;
    //cpArbiter;
    cpArbiterState = (CP_ARBITER_STATE_FIRST_COLLISION,CP_ARBITER_STATE_NORMAL,
      CP_ARBITER_STATE_IGNORE,CP_ARBITER_STATE_CACHED,
      CP_ARBITER_STATE_INVALIDATED);

    cpArbiterThread = record
        next : ^cpArbiter;
        prev : ^cpArbiter;
      end;

  { TODO: look for an alternate bounce solution. }
    cpContact = record
        r1 : cpVect;
        r2 : cpVect;
        nMass : cpFloat;
        tMass : cpFloat;
        bounce : cpFloat;
        jnAcc : cpFloat;
        jtAcc : cpFloat;
        jBias : cpFloat;
        bias : cpFloat;
        hash : cpHashValue;
      end;

(* Const before type ignored *)
  { TODO Should this be a unique struct type? }
    cpCollisionInfo = record
        a : ^cpShape;
        b : ^cpShape;
        id : cpCollisionID;
        n : cpVect;
        count : longint;
        arr : ^cpContact;
      end;

(* Const before type ignored *)
  { Regular, wildcard A and wildcard B collision handlers. }
    cpArbiter = record
        e : cpFloat;
        u : cpFloat;
        surface_vr : cpVect;
        data : cpDataPointer;
        a : ^cpShape;
        b : ^cpShape;
        body_a : ^cpBody;
        body_b : ^cpBody;
        thread_a : cpArbiterThread;
        thread_b : cpArbiterThread;
        count : longint;
        contacts : ^cpContact;
        n : cpVect;
        handler : ^cpCollisionHandler;
        handlerA : ^cpCollisionHandler;
        handlerB : ^cpCollisionHandler;
        swapped : cpBool;
        stamp : cpTimestamp;
        state : cpArbiterState;
      end;


    (* Const before type ignored *)
    (* Const before type ignored *)
    //cpSpace;
    cpSpaceArbiterApplyImpulseFunc = procedure (arb:PcpArbiter);stdcall;
     cpSpace = record
         iterations : longint;
         gravity : cpVect;
         damping : cpFloat;
         idleSpeedThreshold : cpFloat;
         sleepTimeThreshold : cpFloat;
         collisionSlop : cpFloat;
         collisionBias : cpFloat;
         collisionPersistence : cpTimestamp;
         userData : cpDataPointer;
         stamp : cpTimestamp;
         curr_dt : cpFloat;
         dynamicBodies : ^cpArray;
         staticBodies : ^cpArray;
         rousedBodies : ^cpArray;
         sleepingComponents : ^cpArray;
         shapeIDCounter : cpHashValue;
         staticShapes : ^cpSpatialIndex;
         dynamicShapes : ^cpSpatialIndex;
         constraints : ^cpArray;
         arbiters : ^cpArray;
         contactBuffersHead : ^cpContactBufferHeader;
         cachedArbiters : ^cpHashSet;
         pooledArbiters : ^cpArray;
         allocatedBuffers : ^cpArray;
         locked : dword;
         usesWildcards : cpBool;
         collisionHandlers : ^cpHashSet;
         defaultHandler : cpCollisionHandler;
         skipPostStep : cpBool;
         postStepCallbacks : ^cpArray;
         staticBody : ^cpBody;
         _staticBody : cpBody;
       end;


       cpPostStepFunc = procedure (space:PcpSpace; key:pointer; data:pointer);stdcall;

         cpSpacePointQueryFunc = procedure (shape:PcpShape; point:cpVect; distance:cpFloat; gradient:cpVect; data:pointer);stdcall;

         cpSpaceSegmentQueryFunc = procedure (shape:PcpShape; point:cpVect; normal:cpVect; alpha:cpFloat; data:pointer);stdcall;

         cpSpaceBBQueryFunc = procedure (shape:PcpShape; data:pointer);stdcall;

         cpSpaceShapeQueryFunc = procedure (shape:PcpShape; points:PcpContactPointSet; data:pointer);stdcall;
       cpSpaceBodyIteratorFunc = procedure (body:PcpBody; data:pointer);stdcall;

         cpSpaceShapeIteratorFunc = procedure (shape:PcpShape; data:pointer);stdcall;
         cpSpaceConstraintIteratorFunc = procedure (constraint:PcpConstraint; data:pointer);stdcall;
     cpPostStepCallback = record
         func : cpPostStepFunc;
         key : pointer;
         data : pointer;
       end;







            cpSpatialIndexIteratorFunc = procedure (obj:pointer; data:pointer);stdcall;

            cpSpatialIndexQueryFunc = function (obj1:pointer; obj2:pointer; id:cpCollisionID; data:pointer):cpCollisionID;stdcall;

            cpSpatialIndexSegmentQueryFunc = function (obj1:pointer; obj2:pointer; data:pointer):cpFloat;stdcall;


            cpSpatialIndexDestroyImpl = procedure (index:PcpSpatialIndex);stdcall;

        cpSpatialIndexCountImpl = function (index:PcpSpatialIndex):longint;stdcall;

        cpSpatialIndexEachImpl = procedure (index:PcpSpatialIndex; func:cpSpatialIndexIteratorFunc; data:pointer);stdcall;

        cpSpatialIndexContainsImpl = function (index:PcpSpatialIndex; obj:pointer; hashid:cpHashValue):cpBool;stdcall;

        cpSpatialIndexInsertImpl = procedure (index:PcpSpatialIndex; obj:pointer; hashid:cpHashValue);stdcall;

        cpSpatialIndexRemoveImpl = procedure (index:PcpSpatialIndex; obj:pointer; hashid:cpHashValue);stdcall;

        cpSpatialIndexReindexImpl = procedure (index:PcpSpatialIndex);stdcall;

        cpSpatialIndexReindexObjectImpl = procedure (index:PcpSpatialIndex; obj:pointer; hashid:cpHashValue);stdcall;

        cpSpatialIndexReindexQueryImpl = procedure (index:PcpSpatialIndex; func:cpSpatialIndexQueryFunc; data:pointer);stdcall;

        cpSpatialIndexQueryImpl = procedure (index:PcpSpatialIndex; obj:pointer; bb:cpBB; func:cpSpatialIndexQueryFunc; data:pointer);stdcall;

        cpSpatialIndexSegmentQueryImpl = procedure (index:PcpSpatialIndex; obj:pointer; a:cpVect; b:cpVect; t_exit:cpFloat;
                      func:cpSpatialIndexSegmentQueryFunc; data:pointer);stdcall;
        cpSpatialIndexClass = record
            destroy : cpSpatialIndexDestroyImpl;
            count : cpSpatialIndexCountImpl;
            each : cpSpatialIndexEachImpl;
            contains : cpSpatialIndexContainsImpl;
            insert : cpSpatialIndexInsertImpl;
            remove : cpSpatialIndexRemoveImpl;
            reindex : cpSpatialIndexReindexImpl;
            reindexObject : cpSpatialIndexReindexObjectImpl;
            reindexQuery : cpSpatialIndexReindexQueryImpl;
            query : cpSpatialIndexQueryImpl;
            segmentQuery : cpSpatialIndexSegmentQueryImpl;
          end;


        cpContactPointSet = record
            count : longint;
            normal : cpVect;
            points : array[0..1] of record
                pointA : cpVect;
                pointB : cpVect;
                distance : cpFloat;
              end;
          end;


        cpBodyType = (CP_BODY_TYPE_DYNAMIC,CP_BODY_TYPE_KINEMATIC,
          CP_BODY_TYPE_STATIC);



        cpBodyShapeIteratorFunc = procedure (body:PcpBody; shape:PcpShape; data:pointer);stdcall;


      cpBodyConstraintIteratorFunc = procedure (body:PcpBody; constraint:PcpConstraint; data:pointer);stdcall;
          cpBodyArbiterIteratorFunc = procedure (body:PcpBody; arbiter:PcpArbiter; data:pointer);stdcall;

          (* Const before type ignored *)

              cpPointQueryInfo = record
                  shape : ^cpShape;
                  point : cpVect;
                  distance : cpFloat;
                  gradient : cpVect;
                end;
          (* Const before type ignored *)

              cpSegmentQueryInfo = record
                  shape : ^cpShape;
                  point : cpVect;
                  normal : cpVect;
                  alpha : cpFloat;
                end;


(* Const before type ignored *)
(* Const before type ignored *)
(* Const before type ignored *)
cpSpaceDebugColor = record
    r : single;
    g : single;
    b : single;
    a : single;
  end;

cpSpaceDebugDrawCircleImpl = procedure (pos:cpVect; angle:cpFloat; radius:cpFloat; outlineColor:cpSpaceDebugColor; fillColor:cpSpaceDebugColor;
              data:cpDataPointer);stdcall;

cpSpaceDebugDrawSegmentImpl = procedure (a:cpVect; b:cpVect; color:cpSpaceDebugColor; data:cpDataPointer);stdcall;

cpSpaceDebugDrawFatSegmentImpl = procedure (a:cpVect; b:cpVect; radius:cpFloat; outlineColor:cpSpaceDebugColor; fillColor:cpSpaceDebugColor;
              data:cpDataPointer);stdcall;
(* Const before type ignored *)

cpSpaceDebugDrawPolygonImpl = procedure (count:longint; verts:PcpVect; radius:cpFloat; outlineColor:cpSpaceDebugColor; fillColor:cpSpaceDebugColor;
              data:cpDataPointer);stdcall;

cpSpaceDebugDrawDotImpl = procedure (size:cpFloat; pos:cpVect; color:cpSpaceDebugColor; data:cpDataPointer);stdcall;

cpSpaceDebugDrawColorForShapeImpl = function (shape:PcpShape; data:cpDataPointer):cpSpaceDebugColor;stdcall;

cpSpaceDebugDrawFlags = (CP_SPACE_DEBUG_DRAW_SHAPES := 1 shl 0,CP_SPACE_DEBUG_DRAW_CONSTRAINTS := 1 shl 1,
  CP_SPACE_DEBUG_DRAW_COLLISION_POINTS := 1 shl 2
  );

cpSpaceDebugDrawOptions = record
    drawCircle : cpSpaceDebugDrawCircleImpl;
    drawSegment : cpSpaceDebugDrawSegmentImpl;
    drawFatSegment : cpSpaceDebugDrawFatSegmentImpl;
    drawPolygon : cpSpaceDebugDrawPolygonImpl;
    drawDot : cpSpaceDebugDrawDotImpl;
    flags : cpSpaceDebugDrawFlags;
    shapeOutlineColor : cpSpaceDebugColor;
    colorForShape : cpSpaceDebugDrawColorForShapeImpl;
    constraintColor : cpSpaceDebugColor;
    collisionPointColor : cpSpaceDebugColor;
    data : cpDataPointer;
  end;

  procedure cpMessage(condition:Pchar; afile:Pchar; line:longint; isError:longint; isHardError:longint; 
              message:Pchar; args:array of const);stdcall;external LIBRARY_chipmunk;

  procedure cpMessage(condition:Pchar; afile:Pchar; line:longint; isError:longint; isHardError:longint; 
              message:Pchar);stdcall;external LIBRARY_chipmunk;



  function cpSpaceHashAlloc:PcpSpaceHash;stdcall;external LIBRARY_chipmunk;

  function cpSpaceHashInit(hash:PcpSpaceHash; celldim:cpFloat; numcells:longint; bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;

  function cpSpaceHashNew(celldim:cpFloat; cells:longint; bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceHashResize(hash:PcpSpaceHash; celldim:cpFloat; numcells:longint);stdcall;external LIBRARY_chipmunk;


  function cpBBTreeAlloc:PcpBBTree;stdcall;external LIBRARY_chipmunk;

  function cpBBTreeInit(tree:PcpBBTree; bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;

  function cpBBTreeNew(bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;

  procedure cpBBTreeOptimize(index:PcpSpatialIndex);stdcall;external LIBRARY_chipmunk;

  procedure cpBBTreeSetVelocityFunc(index:PcpSpatialIndex; func:cpBBTreeVelocityFunc);stdcall;external LIBRARY_chipmunk;


  function cpSweep1DAlloc:PcpSweep1D;stdcall;external LIBRARY_chipmunk;

  function cpSweep1DInit(sweep:PcpSweep1D; bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;

  function cpSweep1DNew(bbfunc:cpSpatialIndexBBFunc; staticIndex:PcpSpatialIndex):PcpSpatialIndex;stdcall;external LIBRARY_chipmunk;


  procedure cpSpatialIndexFree(index:PcpSpatialIndex);stdcall;external LIBRARY_chipmunk;

  procedure cpSpatialIndexCollideStatic(dynamicIndex:PcpSpatialIndex; staticIndex:PcpSpatialIndex; func:cpSpatialIndexQueryFunc; data:pointer);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetRestitution(arb:PcpArbiter):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterSetRestitution(arb:PcpArbiter; restitution:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetFriction(arb:PcpArbiter):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterSetFriction(arb:PcpArbiter; friction:cpFloat);stdcall;external LIBRARY_chipmunk;

  function cpArbiterGetSurfaceVelocity(arb:PcpArbiter):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterSetSurfaceVelocity(arb:PcpArbiter; vr:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetUserData(arb:PcpArbiter):cpDataPointer;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterSetUserData(arb:PcpArbiter; userData:cpDataPointer);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterTotalImpulse(arb:PcpArbiter):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterTotalKE(arb:PcpArbiter):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpArbiterIgnore(arb:PcpArbiter):cpBool;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  procedure cpArbiterGetShapes(arb:PcpArbiter; a:PPcpShape; b:PPcpShape);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  procedure cpArbiterGetBodies(arb:PcpArbiter; a:PPcpBody; b:PPcpBody);stdcall;external LIBRARY_chipmunk;


(* Const before type ignored *)

  function cpArbiterGetContactPointSet(arb:PcpArbiter):cpContactPointSet;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterSetContactPointSet(arb:PcpArbiter; aset:PcpContactPointSet);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterIsFirstContact(arb:PcpArbiter):cpBool;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterIsRemoval(arb:PcpArbiter):cpBool;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetCount(arb:PcpArbiter):longint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetNormal(arb:PcpArbiter):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetPointA(arb:PcpArbiter; i:longint):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetPointB(arb:PcpArbiter; i:longint):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpArbiterGetDepth(arb:PcpArbiter; i:longint):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpArbiterCallWildcardBeginA(arb:PcpArbiter; space:PcpSpace):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpArbiterCallWildcardBeginB(arb:PcpArbiter; space:PcpSpace):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpArbiterCallWildcardPreSolveA(arb:PcpArbiter; space:PcpSpace):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpArbiterCallWildcardPreSolveB(arb:PcpArbiter; space:PcpSpace):cpBool;stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterCallWildcardPostSolveA(arb:PcpArbiter; space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterCallWildcardPostSolveB(arb:PcpArbiter; space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterCallWildcardSeparateA(arb:PcpArbiter; space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpArbiterCallWildcardSeparateB(arb:PcpArbiter; space:PcpSpace);stdcall;external LIBRARY_chipmunk;


  function cpBodyAlloc:PcpBody;stdcall;external LIBRARY_chipmunk;

  function cpBodyInit(body:PcpBody; mass:cpFloat; moment:cpFloat):PcpBody;stdcall;external LIBRARY_chipmunk;

  function cpBodyNew(mass:cpFloat; moment:cpFloat):PcpBody;stdcall;external LIBRARY_chipmunk;

  function cpBodyNewKinematic:PcpBody;stdcall;external LIBRARY_chipmunk;

  function cpBodyNewStatic:PcpBody;stdcall;external LIBRARY_chipmunk;

  procedure cpBodyDestroy(body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyFree(body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyActivate(body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyActivateStatic(body:PcpBody; filter:PcpShape);stdcall;external LIBRARY_chipmunk;

  procedure cpBodySleep(body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpBodySleepWithGroup(body:PcpBody; group:PcpBody);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyIsSleeping(body:PcpBody):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpBodyGetType(body:PcpBody):cpBodyType;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetType(body:PcpBody; _type:cpBodyType);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetSpace(body:PcpBody):PcpSpace;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetMass(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetMass(body:PcpBody; m:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetMoment(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetMoment(body:PcpBody; i:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetPosition(body:PcpBody):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetPosition(body:PcpBody; pos:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetCenterOfGravity(body:PcpBody):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetCenterOfGravity(body:PcpBody; cog:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetVelocity(body:PcpBody):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetVelocity(body:PcpBody; velocity:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetForce(body:PcpBody):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetForce(body:PcpBody; force:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetAngle(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetAngle(body:PcpBody; a:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetAngularVelocity(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetAngularVelocity(body:PcpBody; angularVelocity:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetTorque(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetTorque(body:PcpBody; torque:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetRotation(body:PcpBody):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetUserData(body:PcpBody):cpDataPointer;stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetUserData(body:PcpBody; userData:cpDataPointer);stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetVelocityUpdateFunc(body:PcpBody; velocityFunc:cpBodyVelocityFunc);stdcall;external LIBRARY_chipmunk;

  procedure cpBodySetPositionUpdateFunc(body:PcpBody; positionFunc:cpBodyPositionFunc);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyUpdateVelocity(body:PcpBody; gravity:cpVect; damping:cpFloat; dt:cpFloat);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyUpdatePosition(body:PcpBody; dt:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
(* Const before type ignored *)
  function cpBodyLocalToWorld(body:PcpBody; point:cpVect):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
(* Const before type ignored *)
  function cpBodyWorldToLocal(body:PcpBody; point:cpVect):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpBodyApplyForceAtWorldPoint(body:PcpBody; force:cpVect; point:cpVect);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyApplyForceAtLocalPoint(body:PcpBody; force:cpVect; point:cpVect);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyApplyImpulseAtWorldPoint(body:PcpBody; impulse:cpVect; point:cpVect);stdcall;external LIBRARY_chipmunk;

  procedure cpBodyApplyImpulseAtLocalPoint(body:PcpBody; impulse:cpVect; point:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetVelocityAtWorldPoint(body:PcpBody; point:cpVect):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyGetVelocityAtLocalPoint(body:PcpBody; point:cpVect):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpBodyKineticEnergy(body:PcpBody):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpBodyEachShape(body:PcpBody; func:cpBodyShapeIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  procedure cpBodyEachConstraint(body:PcpBody; func:cpBodyConstraintIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  procedure cpBodyEachArbiter(body:PcpBody; func:cpBodyArbiterIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  procedure cpShapeDestroy(shape:PcpShape);stdcall;external LIBRARY_chipmunk;

  procedure cpShapeFree(shape:PcpShape);stdcall;external LIBRARY_chipmunk;

  function cpShapeCacheBB(shape:PcpShape):cpBB;stdcall;external LIBRARY_chipmunk;

  function cpShapeUpdate(shape:PcpShape; transform:cpTransform):cpBB;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapePointQuery(shape:PcpShape; p:cpVect; aout:PcpPointQueryInfo):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeSegmentQuery(shape:PcpShape; a:cpVect; b:cpVect; radius:cpFloat; info:PcpSegmentQueryInfo):cpBool;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
(* Const before type ignored *)
  function cpShapesCollide(a:PcpShape; b:PcpShape):cpContactPointSet;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetSpace(shape:PcpShape):PcpSpace;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetBody(shape:PcpShape):PcpBody;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetBody(shape:PcpShape; body:PcpBody);stdcall;external LIBRARY_chipmunk;

  function cpShapeGetMass(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetMass(shape:PcpShape; mass:cpFloat);stdcall;external LIBRARY_chipmunk;

  function cpShapeGetDensity(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetDensity(shape:PcpShape; density:cpFloat);stdcall;external LIBRARY_chipmunk;

  function cpShapeGetMoment(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpShapeGetArea(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpShapeGetCenterOfGravity(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetBB(shape:PcpShape):cpBB;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetSensor(shape:PcpShape):cpBool;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetSensor(shape:PcpShape; sensor:cpBool);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetElasticity(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetElasticity(shape:PcpShape; elasticity:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetFriction(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetFriction(shape:PcpShape; friction:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetSurfaceVelocity(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetSurfaceVelocity(shape:PcpShape; surfaceVelocity:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetUserData(shape:PcpShape):cpDataPointer;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetUserData(shape:PcpShape; userData:cpDataPointer);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetCollisionType(shape:PcpShape):cpCollisionType;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetCollisionType(shape:PcpShape; collisionType:cpCollisionType);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpShapeGetFilter(shape:PcpShape):cpShapeFilter;stdcall;external LIBRARY_chipmunk;

  procedure cpShapeSetFilter(shape:PcpShape; filter:cpShapeFilter);stdcall;external LIBRARY_chipmunk;

  function cpCircleShapeAlloc:PcpCircleShape;stdcall;external LIBRARY_chipmunk;

  function cpCircleShapeInit(circle:PcpCircleShape; body:PcpBody; radius:cpFloat; offset:cpVect):PcpCircleShape;stdcall;external LIBRARY_chipmunk;

  function cpCircleShapeNew(body:PcpBody; radius:cpFloat; offset:cpVect):PcpShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpCircleShapeGetOffset(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpCircleShapeGetRadius(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpSegmentShapeAlloc:PcpSegmentShape;stdcall;external LIBRARY_chipmunk;

  function cpSegmentShapeInit(seg:PcpSegmentShape; body:PcpBody; a:cpVect; b:cpVect; radius:cpFloat):PcpSegmentShape;stdcall;external LIBRARY_chipmunk;

  function cpSegmentShapeNew(body:PcpBody; a:cpVect; b:cpVect; radius:cpFloat):PcpShape;stdcall;external LIBRARY_chipmunk;

  procedure cpSegmentShapeSetNeighbors(shape:PcpShape; prev:cpVect; next:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSegmentShapeGetA(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSegmentShapeGetB(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSegmentShapeGetNormal(shape:PcpShape):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSegmentShapeGetRadius(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpPolyShapeAlloc:PcpPolyShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeInit(poly:PcpPolyShape; body:PcpBody; count:longint; verts:PcpVect; transform:cpTransform; 
             radius:cpFloat):PcpPolyShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeInitRaw(poly:PcpPolyShape; body:PcpBody; count:longint; verts:PcpVect; radius:cpFloat):PcpPolyShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeNew(body:PcpBody; count:longint; verts:PcpVect; transform:cpTransform; radius:cpFloat):PcpShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeNewRaw(body:PcpBody; count:longint; verts:PcpVect; radius:cpFloat):PcpShape;stdcall;external LIBRARY_chipmunk;

  function cpBoxShapeInit(poly:PcpPolyShape; body:PcpBody; width:cpFloat; height:cpFloat; radius:cpFloat):PcpPolyShape;stdcall;external LIBRARY_chipmunk;

  function cpBoxShapeInit2(poly:PcpPolyShape; body:PcpBody; box:cpBB; radius:cpFloat):PcpPolyShape;stdcall;external LIBRARY_chipmunk;

  function cpBoxShapeNew(body:PcpBody; width:cpFloat; height:cpFloat; radius:cpFloat):PcpShape;stdcall;external LIBRARY_chipmunk;

  function cpBoxShapeNew2(body:PcpBody; box:cpBB; radius:cpFloat):PcpShape;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeGetCount(shape:PcpShape):longint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeGetVert(shape:PcpShape; index:longint):cpVect;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPolyShapeGetRadius(shape:PcpShape):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintDestroy(constraint:PcpConstraint);stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintFree(constraint:PcpConstraint);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetSpace(constraint:PcpConstraint):PcpSpace;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetBodyA(constraint:PcpConstraint):PcpBody;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetBodyB(constraint:PcpConstraint):PcpBody;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetMaxForce(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetMaxForce(constraint:PcpConstraint; maxForce:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetErrorBias(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetErrorBias(constraint:PcpConstraint; errorBias:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetMaxBias(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetMaxBias(constraint:PcpConstraint; maxBias:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetCollideBodies(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetCollideBodies(constraint:PcpConstraint; collideBodies:cpBool);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetPreSolveFunc(constraint:PcpConstraint):cpConstraintPreSolveFunc;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetPreSolveFunc(constraint:PcpConstraint; preSolveFunc:cpConstraintPreSolveFunc);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetPostSolveFunc(constraint:PcpConstraint):cpConstraintPostSolveFunc;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetPostSolveFunc(constraint:PcpConstraint; postSolveFunc:cpConstraintPostSolveFunc);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintGetUserData(constraint:PcpConstraint):cpDataPointer;stdcall;external LIBRARY_chipmunk;

  procedure cpConstraintSetUserData(constraint:PcpConstraint; userData:cpDataPointer);stdcall;external LIBRARY_chipmunk;

  function cpConstraintGetImpulse(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsPinJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpPinJointAlloc:PcpPinJoint;stdcall;external LIBRARY_chipmunk;

  function cpPinJointInit(joint:PcpPinJoint; a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect):PcpPinJoint;stdcall;external LIBRARY_chipmunk;

  function cpPinJointNew(a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPinJointGetAnchorA(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpPinJointSetAnchorA(constraint:PcpConstraint; anchorA:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPinJointGetAnchorB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpPinJointSetAnchorB(constraint:PcpConstraint; anchorB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPinJointGetDist(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpPinJointSetDist(constraint:PcpConstraint; dist:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsSlideJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSlideJointAlloc:PcpSlideJoint;stdcall;external LIBRARY_chipmunk;

  function cpSlideJointInit(joint:PcpSlideJoint; a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect; 
             min:cpFloat; max:cpFloat):PcpSlideJoint;stdcall;external LIBRARY_chipmunk;

  function cpSlideJointNew(a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect; min:cpFloat; 
             max:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSlideJointGetAnchorA(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpSlideJointSetAnchorA(constraint:PcpConstraint; anchorA:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSlideJointGetAnchorB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpSlideJointSetAnchorB(constraint:PcpConstraint; anchorB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSlideJointGetMin(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSlideJointSetMin(constraint:PcpConstraint; min:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSlideJointGetMax(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSlideJointSetMax(constraint:PcpConstraint; max:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsPivotJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpPivotJointAlloc:PcpPivotJoint;stdcall;external LIBRARY_chipmunk;

  function cpPivotJointInit(joint:PcpPivotJoint; a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect):PcpPivotJoint;stdcall;external LIBRARY_chipmunk;

  function cpPivotJointNew(a:PcpBody; b:PcpBody; pivot:cpVect):PcpConstraint;stdcall;external LIBRARY_chipmunk;

  function cpPivotJointNew2(a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPivotJointGetAnchorA(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpPivotJointSetAnchorA(constraint:PcpConstraint; anchorA:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpPivotJointGetAnchorB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpPivotJointSetAnchorB(constraint:PcpConstraint; anchorB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsGrooveJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpGrooveJointAlloc:PcpGrooveJoint;stdcall;external LIBRARY_chipmunk;

  function cpGrooveJointInit(joint:PcpGrooveJoint; a:PcpBody; b:PcpBody; groove_a:cpVect; groove_b:cpVect; 
             anchorB:cpVect):PcpGrooveJoint;stdcall;external LIBRARY_chipmunk;

  function cpGrooveJointNew(a:PcpBody; b:PcpBody; groove_a:cpVect; groove_b:cpVect; anchorB:cpVect):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpGrooveJointGetGrooveA(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpGrooveJointSetGrooveA(constraint:PcpConstraint; grooveA:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpGrooveJointGetGrooveB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpGrooveJointSetGrooveB(constraint:PcpConstraint; grooveB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpGrooveJointGetAnchorB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpGrooveJointSetAnchorB(constraint:PcpConstraint; anchorB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsDampedSpring(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpDampedSpringAlloc:PcpDampedSpring;stdcall;external LIBRARY_chipmunk;

  function cpDampedSpringInit(joint:PcpDampedSpring; a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect; 
             restLength:cpFloat; stiffness:cpFloat; damping:cpFloat):PcpDampedSpring;stdcall;external LIBRARY_chipmunk;

  function cpDampedSpringNew(a:PcpBody; b:PcpBody; anchorA:cpVect; anchorB:cpVect; restLength:cpFloat; 
             stiffness:cpFloat; damping:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetAnchorA(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetAnchorA(constraint:PcpConstraint; anchorA:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetAnchorB(constraint:PcpConstraint):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetAnchorB(constraint:PcpConstraint; anchorB:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetRestLength(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetRestLength(constraint:PcpConstraint; restLength:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetStiffness(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetStiffness(constraint:PcpConstraint; stiffness:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetDamping(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetDamping(constraint:PcpConstraint; damping:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedSpringGetSpringForceFunc(constraint:PcpConstraint):cpDampedSpringForceFunc;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedSpringSetSpringForceFunc(constraint:PcpConstraint; springForceFunc:cpDampedSpringForceFunc);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsDampedRotarySpring(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpDampedRotarySpringAlloc:PcpDampedRotarySpring;stdcall;external LIBRARY_chipmunk;

  function cpDampedRotarySpringInit(joint:PcpDampedRotarySpring; a:PcpBody; b:PcpBody; restAngle:cpFloat; stiffness:cpFloat; 
             damping:cpFloat):PcpDampedRotarySpring;stdcall;external LIBRARY_chipmunk;

  function cpDampedRotarySpringNew(a:PcpBody; b:PcpBody; restAngle:cpFloat; stiffness:cpFloat; damping:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedRotarySpringGetRestAngle(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedRotarySpringSetRestAngle(constraint:PcpConstraint; restAngle:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedRotarySpringGetStiffness(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedRotarySpringSetStiffness(constraint:PcpConstraint; stiffness:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedRotarySpringGetDamping(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedRotarySpringSetDamping(constraint:PcpConstraint; damping:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpDampedRotarySpringGetSpringTorqueFunc(constraint:PcpConstraint):cpDampedRotarySpringTorqueFunc;stdcall;external LIBRARY_chipmunk;

  procedure cpDampedRotarySpringSetSpringTorqueFunc(constraint:PcpConstraint; springTorqueFunc:cpDampedRotarySpringTorqueFunc);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsRotaryLimitJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpRotaryLimitJointAlloc:PcpRotaryLimitJoint;stdcall;external LIBRARY_chipmunk;

  function cpRotaryLimitJointInit(joint:PcpRotaryLimitJoint; a:PcpBody; b:PcpBody; min:cpFloat; max:cpFloat):PcpRotaryLimitJoint;stdcall;external LIBRARY_chipmunk;

  function cpRotaryLimitJointNew(a:PcpBody; b:PcpBody; min:cpFloat; max:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpRotaryLimitJointGetMin(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpRotaryLimitJointSetMin(constraint:PcpConstraint; min:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpRotaryLimitJointGetMax(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpRotaryLimitJointSetMax(constraint:PcpConstraint; max:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsRatchetJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpRatchetJointAlloc:PcpRatchetJoint;stdcall;external LIBRARY_chipmunk;

  function cpRatchetJointInit(joint:PcpRatchetJoint; a:PcpBody; b:PcpBody; phase:cpFloat; ratchet:cpFloat):PcpRatchetJoint;stdcall;external LIBRARY_chipmunk;

  function cpRatchetJointNew(a:PcpBody; b:PcpBody; phase:cpFloat; ratchet:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpRatchetJointGetAngle(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpRatchetJointSetAngle(constraint:PcpConstraint; angle:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpRatchetJointGetPhase(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpRatchetJointSetPhase(constraint:PcpConstraint; phase:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpRatchetJointGetRatchet(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpRatchetJointSetRatchet(constraint:PcpConstraint; ratchet:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConstraintIsGearJoint(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpGearJointAlloc:PcpGearJoint;stdcall;external LIBRARY_chipmunk;

  function cpGearJointInit(joint:PcpGearJoint; a:PcpBody; b:PcpBody; phase:cpFloat; ratio:cpFloat):PcpGearJoint;stdcall;external LIBRARY_chipmunk;

  function cpGearJointNew(a:PcpBody; b:PcpBody; phase:cpFloat; ratio:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpGearJointGetPhase(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpGearJointSetPhase(constraint:PcpConstraint; phase:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpGearJointGetRatio(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpGearJointSetRatio(constraint:PcpConstraint; ratio:cpFloat);stdcall;external LIBRARY_chipmunk;


(* Const before type ignored *)

  function cpConstraintIsSimpleMotor(constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSimpleMotorAlloc:PcpSimpleMotor;stdcall;external LIBRARY_chipmunk;

  function cpSimpleMotorInit(joint:PcpSimpleMotor; a:PcpBody; b:PcpBody; rate:cpFloat):PcpSimpleMotor;stdcall;external LIBRARY_chipmunk;

  function cpSimpleMotorNew(a:PcpBody; b:PcpBody; rate:cpFloat):PcpConstraint;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSimpleMotorGetRate(constraint:PcpConstraint):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSimpleMotorSetRate(constraint:PcpConstraint; rate:cpFloat);stdcall;external LIBRARY_chipmunk;

  function cpSpaceAlloc:PcpSpace;stdcall;external LIBRARY_chipmunk;

  function cpSpaceInit(space:PcpSpace):PcpSpace;stdcall;external LIBRARY_chipmunk;

  function cpSpaceNew:PcpSpace;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceDestroy(space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceFree(space:PcpSpace);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetIterations(space:PcpSpace):longint;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetIterations(space:PcpSpace; iterations:longint);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetGravity(space:PcpSpace):cpVect;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetGravity(space:PcpSpace; gravity:cpVect);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetDamping(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetDamping(space:PcpSpace; damping:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetIdleSpeedThreshold(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetIdleSpeedThreshold(space:PcpSpace; idleSpeedThreshold:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetSleepTimeThreshold(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetSleepTimeThreshold(space:PcpSpace; sleepTimeThreshold:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetCollisionSlop(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetCollisionSlop(space:PcpSpace; collisionSlop:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetCollisionBias(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetCollisionBias(space:PcpSpace; collisionBias:cpFloat);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetCollisionPersistence(space:PcpSpace):cpTimestamp;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetCollisionPersistence(space:PcpSpace; collisionPersistence:cpTimestamp);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetUserData(space:PcpSpace):cpDataPointer;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSetUserData(space:PcpSpace; userData:cpDataPointer);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetStaticBody(space:PcpSpace):PcpBody;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpSpaceGetCurrentTimeStep(space:PcpSpace):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpSpaceIsLocked(space:PcpSpace):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddDefaultCollisionHandler(space:PcpSpace):PcpCollisionHandler;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddCollisionHandler(space:PcpSpace; a:cpCollisionType; b:cpCollisionType):PcpCollisionHandler;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddWildcardHandler(space:PcpSpace; _type:cpCollisionType):PcpCollisionHandler;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddShape(space:PcpSpace; shape:PcpShape):PcpShape;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddBody(space:PcpSpace; body:PcpBody):PcpBody;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddConstraint(space:PcpSpace; constraint:PcpConstraint):PcpConstraint;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceRemoveShape(space:PcpSpace; shape:PcpShape);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceRemoveBody(space:PcpSpace; body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceRemoveConstraint(space:PcpSpace; constraint:PcpConstraint);stdcall;external LIBRARY_chipmunk;

  function cpSpaceContainsShape(space:PcpSpace; shape:PcpShape):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSpaceContainsBody(space:PcpSpace; body:PcpBody):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSpaceContainsConstraint(space:PcpSpace; constraint:PcpConstraint):cpBool;stdcall;external LIBRARY_chipmunk;

  function cpSpaceAddPostStepCallback(space:PcpSpace; func:cpPostStepFunc; key:pointer; data:pointer):cpBool;stdcall;external LIBRARY_chipmunk;


  procedure cpSpacePointQuery(space:PcpSpace; point:cpVect; maxDistance:cpFloat; filter:cpShapeFilter; func:cpSpacePointQueryFunc;
              data:pointer);stdcall;external LIBRARY_chipmunk;

  function cpSpacePointQueryNearest(space:PcpSpace; point:cpVect; maxDistance:cpFloat; filter:cpShapeFilter; aout:PcpPointQueryInfo):PcpShape;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceSegmentQuery(space:PcpSpace; start:cpVect; aend:cpVect; radius:cpFloat; filter:cpShapeFilter;
              func:cpSpaceSegmentQueryFunc; data:pointer);stdcall;external LIBRARY_chipmunk;

  function cpSpaceSegmentQueryFirst(space:PcpSpace; start:cpVect; aend:cpVect; radius:cpFloat; filter:cpShapeFilter; 
             aout:PcpSegmentQueryInfo):PcpShape;stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceBBQuery(space:PcpSpace; bb:cpBB; filter:cpShapeFilter; func:cpSpaceBBQueryFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  function cpSpaceShapeQuery(space:PcpSpace; shape:PcpShape; func:cpSpaceShapeQueryFunc; data:pointer):cpBool;stdcall;external LIBRARY_chipmunk;


  procedure cpSpaceEachBody(space:PcpSpace; func:cpSpaceBodyIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  procedure cpSpaceEachShape(space:PcpSpace; func:cpSpaceShapeIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;


  procedure cpSpaceEachConstraint(space:PcpSpace; func:cpSpaceConstraintIteratorFunc; data:pointer);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceReindexStatic(space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceReindexShape(space:PcpSpace; shape:PcpShape);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceReindexShapesForBody(space:PcpSpace; body:PcpBody);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceUseSpatialHash(space:PcpSpace; dim:cpFloat; count:longint);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceStep(space:PcpSpace; dt:cpFloat);stdcall;external LIBRARY_chipmunk;

  procedure cpSpaceDebugDraw(space:PcpSpace; options:PcpSpaceDebugDrawOptions);stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)

    var
      cpVersionString : ^char;cvar;external LIBRARY_chipmunk;

  function cpMomentForCircle(m:cpFloat; r1:cpFloat; r2:cpFloat; offset:cpVect):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpAreaForCircle(r1:cpFloat; r2:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpMomentForSegment(m:cpFloat; a:cpVect; b:cpVect; radius:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpAreaForSegment(a:cpVect; b:cpVect; radius:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpMomentForPoly(m:cpFloat; count:longint; verts:PcpVect; offset:cpVect; radius:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
(* Const before type ignored *)
  function cpAreaForPoly(count:longint; verts:PcpVect; radius:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
(* Const before type ignored *)
  function cpCentroidForPoly(count:longint; verts:PcpVect):cpVect;stdcall;external LIBRARY_chipmunk;

  function cpMomentForBox(m:cpFloat; width:cpFloat; height:cpFloat):cpFloat;stdcall;external LIBRARY_chipmunk;

  function cpMomentForBox2(m:cpFloat; box:cpBB):cpFloat;stdcall;external LIBRARY_chipmunk;

(* Const before type ignored *)
  function cpConvexHull(count:longint; verts:PcpVect; result:PcpVect; first:Plongint; tol:cpFloat):longint;stdcall;external LIBRARY_chipmunk;


  function cpHastySpaceNew:PcpSpace;stdcall;external LIBRARY_chipmunk;

  procedure cpHastySpaceFree(space:PcpSpace);stdcall;external LIBRARY_chipmunk;

  procedure cpHastySpaceSetThreads(space:PcpSpace; threads:dword);stdcall;external LIBRARY_chipmunk;

  function cpHastySpaceGetThreads(space:PcpSpace):dword;stdcall;external LIBRARY_chipmunk;

  procedure cpHastySpaceStep(space:PcpSpace; dt:cpFloat);stdcall;external LIBRARY_chipmunk;




  function cpv(x,y: cpFloat): cpVect;
const
  cpvzero: cpVect = (x: 0; y: 0);
  cpTransformIdentity: cpTransform = (a: 1.0; b: 0.0; c: 0.0; d: 1.0; tx: 0.0; ty: 0.0);
implementation

function cpv(x,y: cpFloat): cpVect;
begin
  Result.x := x;
  Result.y := y;
end;

end.
