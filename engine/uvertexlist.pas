unit uVertexList;

interface

uses
  SysUtils, GL, glext, uTextures, uEngineTypes, ushaderprocess,uMyGLEXT;

type

  { TVertexList }
  TVertexList = class
    NPoints: Integer;
    VAO, VBO:THandle;
    thetyp: GLEnum;
    BufferWasUpdated: Integer;
    VertexWasUpdated: array of boolean;
    //for easier initialization they are offset by 1, so 0 = nonexistant
    AttributeOffsets: array of Integer;
    CurFilledVertexSize: Integer;
    MaxVertexSize: Integer;
    Buffer: pointer;
    BufferChanged: Boolean;
    constructor Create(abuffer: Pointer; maxn: integer; aMaxVertexSize: Integer);
    procedure Draw(Mode: GLEnum; aPoints: Integer = -1);
    procedure AddAttribute(attrib: TShaderAttribute);
    procedure AddPadding(n: integer);
    procedure UpdateVertexFormat;
    procedure UpdateBuffer;
    procedure CheckVertexSize;
    function Clone: TVertexList;
    destructor Destroy; override;
  end;

  TSpriteVertex = packed record
    coords: tVec3Data;
    texture: tVec3Data;
    color: tVec4Data;
	end;

  { TSpriteList }

  TSpriteList = class(TVertexList)
    Data: array of TSpriteVertex;
    thetex: TGenericTexture;
    LineWidth: Single;
    constructor create(maxn: integer);
    procedure AddVertex(v: TSpriteVertex);overload;
    procedure AddLine(v1, v2: TSpriteVertex);
	end;

//const DUMMY_LOCATION = 0;

function Vertex(ax,ay,az,tx,ty,tz: Single; Color: Cardinal): TSpriteVertex;

implementation

uses ulog;

{ TModelList }

procedure TVertexList.UpdateBuffer;
begin
  if not BufferChanged then exit;
  BufferChanged := False;
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  if BufferWasUpdated < NPoints then
  begin
    glBufferData(GL_ARRAY_BUFFER, NPoints * MaxVertexSize, Buffer, GL_DYNAMIC_DRAW);
    BufferWasUpdated := NPoints;
  end
  else
    glBufferSubData(GL_ARRAY_BUFFER, 0, NPoints * MaxVertexSize, Buffer);
end;

constructor TSpriteList.create(maxn: integer);
begin
  inherited Create(nil, maxn, SizeOf(TSpriteVertex));
  SetLength(Data, maxn);
  Buffer := @Data[0];
  AddAttribute(ATTR_POS);
  AddAttribute(ATTR_TEXPOS);
  AddAttribute(ATTR_COLOR);
  NPoints := 0;
end;

procedure TSpriteList.AddVertex(v: TSpriteVertex);
begin
  if NPoints >= length(Data) then
  begin
    SetLength(Data, Length(Data)*2);
    Buffer := @Data[0];
  end;
  Data[NPoints] := v;
  inc(NPoints);
  BufferChanged := True;
end;

procedure TSpriteList.AddLine(v1, v2: TSpriteVertex);
var
  dvx, dvy, dl: single;
  v11, v12, v21, v22: TSpriteVertex;
begin
  if LineWidth <= 1.0 then
  begin
    AddVertex(v1);
    AddVertex(v2);
  end
  else
  begin
    dvx := -(v2.coords[1]-v1.coords[1]);
    dvy := (v2.coords[0]-v1.coords[0]);
    dl := LineWidth /2 / sqrt(dvx*dvx+dvy*dvy);
    dvx := dvx*dl;
    dvy := dvy*dl;

    v11 := v1;
    v11.coords[0] -= dvx;
    v11.coords[1] -= dvy;
    v12 := v1;
    v12.coords[0] += dvx;
    v12.coords[1] += dvy;
    v21 := v2;
    v21.coords[0] -= dvx;
    v21.coords[1] -= dvy;
    v22 := v2;
    v22.coords[0] += dvx;
    v22.coords[1] += dvy;

    AddVertex(v11);
    AddVertex(v21);
    AddVertex(v12);
    AddVertex(v21);
    AddVertex(v12);
    AddVertex(v22);
  end;
end;

constructor TVertexList.Create(abuffer: Pointer; maxn: integer;
  aMaxVertexSize: Integer);
begin
  inherited Create;
  Buffer := abuffer;
  MaxVertexSize := aMaxVertexSize;
  NPoints := maxn;
  glGenVertexArrays(1, @VAO);
  glGenBuffers(1, @VBO);
  BufferChanged := True;
end;

procedure TVertexList.Draw(Mode: GLEnum; aPoints: Integer = -1);
begin
  if NPoints = 0 then exit;
  if aPoints < 0 then aPoints := NPoints;
  UpdateBuffer;
  UpdateVertexFormat;
  if length(AttributeOffsets) > 0 then
    glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glDrawArrays(Mode, 0, aPoints);
end;

type
  TAttributeSizes = record
    gl_type: GLenum;
    n_components: integer;
    size: integer;
  end;

function attr_sizes(gl_type: GLenum; n_components: integer; single_size: integer): TAttributeSizes;
begin
  Result.gl_type := gl_type;
  Result.n_components := n_components;
  Result.size := n_components*single_size;
end;


function GetAttributeSizes(item: TGLSLItem): TAttributeSizes;
begin
  case item.typ of
    float:  Result := attr_sizes(GL_FLOAT, 1, 4);
    vec2:   Result := attr_sizes(GL_FLOAT, 2, 4);
    vec3:   Result := attr_sizes(GL_FLOAT, 3, 4);
    vec4:   Result := attr_sizes(GL_FLOAT, 4, 4);
    double: Result := attr_sizes(GL_DOUBLE, 1, 8);
    dvec2:  Result := attr_sizes(GL_DOUBLE, 2, 8);
    dvec3:  Result := attr_sizes(GL_DOUBLE, 3, 8);
    dvec4:  Result := attr_sizes(GL_DOUBLE, 4, 8);
    int:    Result := attr_sizes(GL_INT, 1, 4);
    ivec2:  Result := attr_sizes(GL_INT, 2, 4);
    ivec3:  Result := attr_sizes(GL_INT, 3, 4);
    ivec4:  Result := attr_sizes(GL_INT, 4, 4);
    unsigned_int: Result := attr_sizes(GL_UNSIGNED_INT, 1, 4);
    uvec2:        Result := attr_sizes(GL_UNSIGNED_INT, 2, 4);
    uvec3:        Result := attr_sizes(GL_UNSIGNED_INT, 3, 4);
    uvec4:        Result := attr_sizes(GL_UNSIGNED_INT, 4, 4);
    else
      begin
        Log('ATTRIBUTE TYPE IS NOT SUPPORTED: '+item.name+' is '+GLSL_TYPE_NAMES[item.typ]);
        Result := attr_sizes(GL_INT, 1, 1);
      end;
  end;
  if item.array_size > 1 then
  begin
    Log('ATTRIBUTE ARRAYS ARE NOT SUPPORTED: '+item.name);
    Result := attr_sizes(GL_INT, 1, 1);
  end;
    //Result := Result * item.array_size;
end;

procedure TVertexList.AddAttribute(attrib: TShaderAttribute);
var
  item: TGLSLItem;
begin
  if length(AttributeOffsets) <= attrib then
    SetLength(AttributeOffsets, attrib+1);
  item := AllAttributes[attrib];
  AttributeOffsets[attrib] := CurFilledVertexSize + 1;
  inc(CurFilledVertexSize, GetAttributeSizes(item).size);
  if CurFilledVertexSize > MaxVertexSize then
    Log('ERROR IN VERTEX FORMAT: after '+item.name+' size is '+IntToStr(CurFilledVertexSize)+' > maximum '+IntToStr(MaxVertexSize));
end;

procedure TVertexList.AddPadding(n: integer);
begin
  inc(CurFilledVertexSize, n);
  if CurFilledVertexSize > MaxVertexSize then
    Log('ERROR IN VERTEX FORMAT: after padding '+IntToStr(n)+' size is '+IntToStr(CurFilledVertexSize)+' > maximum '+IntToStr(MaxVertexSize));
end;

procedure TVertexList.UpdateVertexFormat;
var
  location: Integer;
  item: TGLSLItem;
  shader_id: Integer;
  sizes: TAttributeSizes;
begin
  CheckVertexSize;
  if CurShader = nil then exit;
  shader_id := CurShader.internal_index;
  if length(VertexWasUpdated) <= shader_id then
    SetLength(VertexWasUpdated, shader_id+1);
  if VertexWasUpdated[shader_id] then exit;
  VertexWasUpdated[shader_id] := true;
  if length(AttributeOffsets) > 0 then
    glBindVertexArray(VAO);
  for item in CurShader.attributes do
    if (item.index < Length(AttributeOffsets)) and (AttributeOffsets[item.index] > 0) then
    begin
      location := CurShader.GetAttributeLocation(item.index, typPointer);
      if location >= 0 then
      begin
        sizes := GetAttributeSizes(item);
        case item.typ of
          float,
          vec2,
          vec3,
          vec4:
            glVertexAttribPointer(location, sizes.n_components, sizes.gl_type, GLBoolean(False),
          	  MaxVertexSize, pointer(AttributeOffsets[item.index]-1));
          double,
          dvec2,
          dvec3,
          dvec4:
            glVertexAttribLPointer(location, sizes.n_components, sizes.gl_type,
          	  MaxVertexSize, pointer(AttributeOffsets[item.index]-1));
          int,
          ivec2,
          ivec3,
          ivec4,
          unsigned_int,
          uvec2,
          uvec3,
          uvec4:
            glVertexAttribIPointer(location, sizes.n_components, sizes.gl_type,
          	  MaxVertexSize, pointer(AttributeOffsets[item.index]-1));
        end;
      end;
    end;
  for item in CurShader.attributes do
    if (item.index < Length(AttributeOffsets)) and (AttributeOffsets[item.index] > 0) then
    begin
      location := CurShader.GetAttributeLocation(item.index, typPointer);
      if location >= 0 then
        glEnableVertexAttribArray(location);
    end;
end;

procedure TVertexList.CheckVertexSize;
begin
  assert(CurFilledVertexSize = MaxVertexSize)
end;

function TVertexList.Clone: TVertexList;
var
  i: integer;
begin
  Result := TVertexList.Create(Buffer, NPoints, MaxVertexSize);
  Result.thetyp := thetyp;
  Result.CurFilledVertexSize := CurFilledVertexSize;
  SetLength(Result.AttributeOffsets, length(AttributeOffsets));
  for i := 0 to Length(AttributeOffsets)-1 do
    Result.AttributeOffsets[i] := AttributeOffsets[i];
end;

destructor TVertexList.Destroy;
begin
  glDeleteVertexArrays(1, @VAO);
  glDeleteBuffers(1, @VBO);
  inherited Destroy;
end;

function Vertex(ax, ay, az, tx, ty, tz: Single; Color: Cardinal): TSpriteVertex;
begin
  Result.coords[0] := ax;
  Result.coords[1] := ay;
  Result.coords[2] := az;

  Result.texture[0] := tx;
  Result.texture[1] := ty;
  Result.texture[2] := tz;

  Result.color[0] := ((Color >> 24) and 255) / 255;
  Result.color[1] := ((Color >> 16) and 255) / 255;
  Result.color[2] := ((Color >> 8) and 255) / 255;
  Result.color[3] := ((Color >> 0) and 255) / 255;

end;

end.

