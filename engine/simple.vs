#version 130
uniform sampler2D tex;
uniform vec2 screen_size;
in vec3 pos;
in vec3 texpos; 
in vec4 color;

out vec4 pixel_color;
out vec3 texcoord;

void main(void)
{
  gl_Position = vec4(((pos.xy/screen_size)-0.5f)*vec2(2.0, -2.0), pos.z, 1.0f);
  texcoord = texpos;
  pixel_color = color;
}
