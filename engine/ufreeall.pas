unit uFreeAll;

{$mode objfpc}{$H+}

interface

uses
  uPoly;

procedure EngineFree; cdecl;

var
  WasShutDown: boolean;

implementation

uses uVertexWrap, uPathfind, uTextureWrap, uText, uLog, uSound, uPhysics,
  uRender, uGUI, uMainEngine, ushaderprocess, uLoader, uParticles, SDL2;

procedure EngineFree; cdecl;
begin
  if WasShutDown then
    exit;
  WasShutDown := True;
  LogEnter('Starting cleanup');
  try
    log('FreeParticles');
    FreeParticles;
    log('FreeCustomVertexLists');
    FreeCustomVertexLists;
    log('FreePathfind');
    FreePathfind;
    log('FreeUserTextures');
    FreeUserTextures;
    log('FreeFontPools');
    FreeFontPools;
    log('FreeSounds');
    FreeSounds;
    {$IFNDEF NO_PHYSICS}
    log('FreePhysics');
    FreePhysics;
    {$ENDIF}
    log('FreeRender');
    FreeRender;
    log('FreeGUI');
    FreeGUI;
    log('FreeShaders');
    FreeShaders;
    log('FreeResources');
    FreeResources;
    log('FreePolygons');
    FreePolygons;
    log('FreeMain');
    FreeMain;
    log('finally');
  finally
    LogExit('Cleanup complete, shutting down');
    FreeLog;
    SDL_Quit;
  end;
end;


end.
