unit uText;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uEngineTypes, sdl2_ttf,
  uPools, uLoader, uTextures;

procedure DrawText(font: TFontInstance; Text: PChar; x, y: TCoord); cdecl;
procedure DrawTextBoxed(font: TFontInstance; Text: PChar; x, y, w, h: TCoord;
  halign: THAlign; valign: TVAlign); cdecl;
function FontCreate(font: TFont; CharSize: integer; color: TColor;
  Styles: TFontStyles; kx, ky: single): TFontInstance; cdecl;
procedure FontConfig(font: TFontInstance; CharSize: integer; color: TColor;
  Styles: TFontStyles; kx, ky: single); cdecl;
procedure MeasureText(font: TFontInstance; Text: PChar; out x: TCoord; out y: TCoord);cdecl;

type

  //TODO - optimization: pool of messages rendered on last frame
  TMessagesPool = specialize TLinearPool<TSimpleTexture>;
  { TTTFFont }

  TTTFFont = class(TFontResource)
    filename: string;
    handles: array of PTTF_Font;
    sizes: array of integer;
    tex: TMessagesPool;
    procedure AddSize(size: integer);
    function GetBySize(size: integer): PTTF_Font;
    constructor Create(s: string);
    destructor Destroy; override;
    procedure SetStyle(astyle: integer);
  end;


  TBitmapParam = (bpXpos, bpYpos, bpWidth, bpHeight, bpXoffset,
    bpYoffset, bpOrigW, bpOrigH);
  TBitmapItem = array[TBitmapParam] of integer;

  { TBitmapFont }

  TBitmapFont = class(TFontResource)
    tex: TSimpleTexture;
    Data: array[AnsiChar] of TBitmapItem;
    constructor Create(atex: TSimpleTexture; afile: string);
    destructor Destroy; override;
  end;

  TFontConfig = record
    TheFont: TFont;
    kx: single;
    ky: single;
    Color: TColor;
    CharSize: integer;
    Style: cardinal;
  end;


var
  FontConfigs: array of TFontConfig;
  AllPools: TList;

procedure InitFontConfig(Count: integer);
procedure ResetFontPools;

procedure FreeFontPools;

implementation

uses uRender, uLog, uVertexList, gl, uMainEngine;

procedure InitFontConfig(Count: integer);
var
  i: integer;
  f: TTTFFont;
begin
  SetLength(FontConfigs, Count);
  for i := 0 to Count - 1 do
  begin
    FontConfigs[i].TheFont := i;
    FontConfigs[i].kx := 1;
    FontConfigs[i].ky := 1;
    FontConfigs[i].Color := WHITE;
    FontConfigs[i].Style := 0;
    FontConfigs[i].CharSize := 24;
    if Assigned(RESFont(FontConfigs[i].TheFont)) and
      (RESFont(FontConfigs[i].TheFont) is TTTFFont) then
    begin
      f := TTTFFont(RESFont(FontConfigs[i].TheFont));
      f.Addsize(FontConfigs[i].CharSize);
    end;
  end;
end;

procedure ResetFontPools;
var
  i: integer;
begin
  if not assigned(AllPools) then
    exit;
  for i := 0 to AllPools.Count - 1 do
    TMessagesPool(AllPools[I]).Reset;
end;

procedure FreeFontPools;
begin
  AllPools.Free;
  SetLength(FontConfigs, 0);
end;

function FontCreate(font: TFont; CharSize: integer; color: TColor;
  Styles: TFontStyles; kx, ky: single): TFontInstance; cdecl;
begin
  SetLength(FontConfigs, Length(FontConfigs) + 1);
  Result := High(FontConfigs);
  FontConfigs[Result].TheFont := font;
  FontConfig(Result, CharSize, color, Styles, kx, ky);
end;

procedure FontConfig(font: TFontInstance; CharSize: integer; color: TColor;
  Styles: TFontStyles; kx, ky: single); cdecl;
var
  f: TTTFFont;
begin
  if (font < 0) or (font >= length(FontConfigs)) then
  begin
    logf('FontConfig called for incorrect font: %d', [font]);
    exit;
  end;
  logf('FontConfig called for font %d: [%d, %f, %f]', [font, CharSize, kx, ky]);
  FontConfigs[font].kx := kx;
  FontConfigs[font].ky := ky;
  FontConfigs[font].Color := color;
  FontConfigs[font].CharSize := CharSize;
  FontConfigs[font].Style := TTF_STYLE_NORMAL;
  if (Bold in Styles) then
    Inc(FontConfigs[font].Style, TTF_STYLE_BOLD);
  if (Italic in Styles) then
    Inc(FontConfigs[font].Style, TTF_STYLE_ITALIC);
  if (Underlined in Styles) then
    Inc(FontConfigs[font].Style, TTF_STYLE_UNDERLINE);
  if Assigned(RESFont(FontConfigs[font].TheFont)) and
    (RESFont(FontConfigs[font].TheFont) is TTTFFont) then
  begin
    f := TTTFFont(RESFont(FontConfigs[font].TheFont));
    f.GetBySize(FontConfigs[font].CharSize);
    f.SetStyle(FontConfigs[font].Style);
  end;
end;

procedure MeasureTTFText(font: TFontInstance; Text: PChar; out x: TCoord; out y: TCoord);
var
  what: TTTFFont;
  tex: TSimpleTexture;
begin
  what := TTTFFont(RESFont(FontConfigs[font].TheFont));
  //if what.LastMessage <> Text then
  //begin
  tex := what.tex.Next;
  tex.UpdateFromMessage(what.GetBySize(FontConfigs[font].CharSize), Text);
  //what.LastMessage := Text;
  //end;
  x := FontConfigs[font].kx * tex.Width;
  y := FontConfigs[font].ky * tex.Height;
end;

procedure MeasureBitmapText(font: TFontInstance; Text: PChar;
  out x: TCoord; out y: TCoord);
var
  ax, ay: TCoord;
  what: TBitmapFont;
  i: integer;
  c: char;
  kxy: integer;
begin
  what := TBitmapFont(RESFont(FontConfigs[font].TheFont));
  ax := 0;
  ay := 0;
  x := 0;
  y := 0;
  kxy := FontConfigs[font].CharSize;
  for i := 0 to Length(Text) - 1 do
  begin
    c := Text[i];
    if c in ['|', #10] then
    begin
      if x < ax then
        x := ax;
      ax := 0;
      ay := ay + what.Data[c, bpOrigH] * kxy;
      y := ay;
    end
    else
    begin
      ax := ax + what.Data[c, bpOrigW] * kxy;
      if y < ay + what.Data[c, bpOrigH] * kxy then
        y := ay + what.Data[c, bpOrigH] * kxy;
    end;
  end;
end;

procedure MeasureText(font: TFontInstance; Text: PChar; out x: TCoord; out y: TCoord);cdecl;
begin
  if RESFont(FontConfigs[font].TheFont) is TTTFFont then
    MeasureTTFText(font, Text, x, y)
  else
    MeasureBitmapText(font, Text, x, y);
end;

procedure DrawTTFText(font: TFontInstance; Text: PChar; x, y: TCoord);
var
  what: TTTFFont;
  it: TSpriteList;
  v00, v01, v10, v11: TSpriteVertex;
  kx, ky: single;
  color: TColor;
  tex: TSimpleTexture;
begin
  what := TTTFFont(RESFont(FontConfigs[font].TheFont));
  tex := what.tex.Next;
  tex.UpdateFromMessage(what.GetBySize(FontConfigs[font].CharSize), Text);
  it := SplitSprites.Next;
  it.NPoints := 0;
  it.thetyp := GL_TRIANGLES;
  it.thetex := tex;

  kx := FontConfigs[font].kx;
  ky := FontConfigs[font].ky;
  color := FontConfigs[font].Color;
  v00 := CamVertex(x, y, 0, 0, Color);
  v01 := CamVertex(x, y + it.thetex.Height * ky, 0, 1, Color);
  v10 := CamVertex(x + it.thetex.Width * kx, y, 1, 0, Color);
  v11 := CamVertex(x + it.thetex.Width * kx, y + it.thetex.Height * ky, 1, 1, Color);

  it.AddVertex(v00);
  it.AddVertex(v10);
  it.AddVertex(v01);

  it.AddVertex(v10);
  it.AddVertex(v01);
  it.AddVertex(v11);
end;

procedure DrawBitmapText(font: TFontInstance; Text: PChar; x, y: TCoord);
var
  ax, ay: TCoord;
  it: TSpriteList;
  p00, p01, p10, p11: TSpriteVertex;
  acol: TColor;
  what: TBitmapFont;
  i: integer;
  c: char;
  item: TBitmapItem;
  sx, sy, kxy: integer;
begin
  what := TBitmapFont(RESFont(FontConfigs[font].TheFont));
  ax := x;
  ay := y;
  it := NextOrSame(GL_TRIANGLES, what.tex);
  acol := FontConfigs[font].Color;
  kxy := FontConfigs[font].CharSize;
  sx := what.tex.Width;
  sy := what.tex.Height;
  for i := 0 to Length(Text) - 1 do
  begin
    c := Text[i];
    item := what.Data[c];
    p00 := CamVertex(ax + item[bpXoffset] * kxy, ay + item[bpYoffset] *
      kxy, item[bpXpos] / sx, item[bpYpos] / sy, acol);
    p01 := CamVertex(ax + item[bpXoffset] * kxy, ay +
      (item[bpYoffset] + item[bpHeight]) * kxy, item[bpXpos] / sx,
      (item[bpYpos] + item[bpHeight]) / sy, acol);
    p10 := CamVertex(ax + (item[bpXoffset] + item[bpWidth]) * kxy,
      ay + item[bpYoffset] * kxy, (item[bpXpos] + item[bpWidth]) /
      sx, item[bpYpos] / sy, acol);
    p11 := CamVertex(ax + (item[bpXoffset] + item[bpWidth]) * kxy,
      ay + (item[bpYoffset] + item[bpHeight]) * kxy, (item[bpXpos] + item[bpWidth]) /
      sx, (item[bpYpos] + item[bpHeight]) / sy, acol);
    if c in ['|', #10] then
    begin
      ax := x;
      ay := ay + what.Data[c, bpOrigH] * kxy;
    end
    else
      ax := ax + what.Data[c, bpOrigW] * kxy;
    it.AddVertex(p00);
    it.AddVertex(p10);
    it.AddVertex(p01);

    it.AddVertex(p10);
    it.AddVertex(p01);
    it.AddVertex(p11);
  end;
end;

procedure DrawText(font: TFontInstance; Text: PChar; x, y: TCoord); cdecl;
begin
  if PARAMS[Antialias] = 0 then
  begin
    x := Round(x);
    y := Round(y);
  end;
  if RESFont(FontConfigs[font].TheFont) is TTTFFont then
    DrawTTFText(font, Text, x, y)
  else
    DrawBitmapText(font, Text, x, y);
end;

//TODO - actually limit text
procedure DrawTextBoxed(font: TFontInstance; Text: PChar; x, y, w, h: TCoord;
  halign: THAlign; valign: TVAlign); cdecl;
var
  sx, sy: TCoord;
begin
  MeasureText(font, Text, sx, sy);

  case halign of
    haCenter: x := x + w / 2 - sx / 2;
    haRight: x := x + w - sx;
  end;
  case valign of
    vaCenter: y := y + h / 2 - sy / 2;
    vaBottom: y := y + h - sy;
  end;
  DrawText(font, Text, x, y);
end;

{ TBitmapFont }


function TokenStr(var s: string): string;
var
  n, i, j: integer;

begin
  n := length(s);
  i := pos(' ', s);
  if i = 0 then
    i := n + 1;
  j := pos(#9, s);
  if (j > 0) and (j < i) then
    i := j;

  if i = 0 then
  begin
    Result := s;
    s := '';
  end
  else
  begin
    Result := copy(s, 1, i - 1);
    repeat
      Inc(i)
    until (i > n) or ((s[i] <> ' ') and (s[i] <> #9));
    Delete(s, 1, i - 1);
  end;
end;

constructor TBitmapFont.Create(atex: TSimpleTexture; afile: string);
var
  ff: TextFile;
  s: string;
  n: integer;
  c, ac: AnsiChar;
  First: boolean;
  par: TBitmapParam;
  Count: integer;
begin
  tex := atex;
  AssignFile(ff, afile);
  Reset(ff);
  ReadLn(ff, s);
  Logf('Bitmap font info: %s', [copy(s, 1, pos('--', s) - 1)]);
  ReadLn(ff);
  First := True;
  Count := 0;
  repeat
    readln(ff, s);
    Inc(Count);
    n := StrToInt(TokenStr(s));
    c := chr(n);
    for par in TBitmapParam do
      if par = High(TBitmapParam) then
        Data[c, par] := StrToInt(s)
      else
        Data[c, par] := StrToInt(TokenStr(s));
    if First then
    begin
      First := False;
      for ac in AnsiChar do
        Data[ac] := Data[c];
    end;
  until EOF(ff);
  Logf('Bitmap font loaded, total chars: %d', [Count]);
  CloseFile(ff);
end;

destructor TBitmapFont.Destroy;
begin
  tex.Free;
  inherited Destroy;
end;

{ TTTFFont }

function CrunchCreator: TSimpleTexture;
begin
  Result := TSimpleTexture.CreateEmpty;
end;

procedure TTTFFont.AddSize(size: integer);
begin
  SetLength(sizes, Length(sizes) + 1);
  SetLength(handles, Length(handles) + 1);
  sizes[Length(sizes) - 1] := size;
  handles[Length(handles) - 1] := TTF_OpenFont(PChar(filename), size);
  if Length(handles) = 1 then
    Logf('TTF font %s loaded for size %d', [TTF_FontFaceFamilyName(handles[0]), size]);
  TTF_SetFontHinting(handles[Length(handles) - 1], TTF_HINTING_MONO);
end;

function TTTFFont.GetBySize(size: integer): PTTF_Font;
var
  i: integer;
begin
  for i := Length(sizes) - 1 downto 0 do
    if sizes[i] = size then
    begin
      Result := handles[i];
      exit;
    end;
  AddSize(size);
  Result := handles[Length(handles) - 1];
end;

constructor TTTFFont.Create(s: string);
begin
  filename := s;
  SetLength(handles, 0);
  SetLength(sizes, 0);
  tex := TMessagesPool.Create(@CrunchCreator);
  if not assigned(AllPools) then
    AllPools := TList.Create;
  AllPools.Add(tex);
end;

destructor TTTFFont.Destroy;
begin
  tex.Free;
  inherited Destroy;
end;

procedure TTTFFont.SetStyle(astyle: integer);
var
  i: integer;
begin
  if Length(handles) = 0 then
    exit;
  if TTF_GetFontStyle(handles[0]) = astyle then
    exit;
  for i := 0 to Length(handles) - 1 do
    TTF_SetFontStyle(handles[i], astyle);
end;


end.
