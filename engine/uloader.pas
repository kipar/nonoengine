unit uLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uGUI, uRender, SDL2, sdl2_mixer, uTextures, uShaderProcess, uParticles;

procedure LoadResources(dir: string);

type
  TResourceType = (Raw, Sprite, Sound, Button, TileMap, Font, Shader, Particle, Emitter);

  TFontResource = class
  end;

  { TRawResourceData }

  TRawResourceData = class
    Size: integer;
    Data: pointer;
    destructor Destroy; override;
    constructor Create(aSize: integer);
  end;


const
  GRAPH_EXTS: array[1..6] of string = ('bmp', 'dds', 'jpg', 'png', 'tga', 'psd');
  SOUND_EXTS: array[1..3] of string = ('wav', 'ogg', 'flac');
  FONT_EXTS: array[1..3] of string = ('ttf', 'otf', 'fnt');
  SHADER_EXT = 'shader';
  SHADER_PARTS_EXTS: array[1..6] of string = ('vs', 'fs', 'gs', 'tcs', 'tes', 'hs');

  PARTICLE_EXT = 'particle';
  EMITTER_EXT = 'emitter';
  //DEFLECTOR_EXT = 'deflector';
  //SPACE_EXT = 'space';


var
  RES: array[TResourceType] of TList;


function RESRaw(index: integer): TRawResourceData;
function RESSprite(index: integer): TGenericTexture;
function RESSound(index: integer): PMix_Chunk;
function RESFont(index: integer): TFontResource;
function RESButton(index: integer): TButtonResource;
function RESTileMap(index: integer): TTileMapResource;
function RESShader(index: integer): TShader;
function RESParticle(index: integer): TParticle;
function RESEmitter(index: integer): TEmitter;

procedure PreloadProgress(dir: string);
procedure ShowProgress(progress: single);

var
  totalresources, curres: integer;

procedure FreeResources;

implementation

uses uText, uLog, uEngineTypes, uMainEngine, uVertexList, gl, uTextureWrap, uownshaders;

function RESRaw(index: integer): TRawResourceData;
begin
  Result := TRawResourceData(RES[Raw][Index]);
end;

function RESSprite(index: integer): TGenericTexture;
begin
  if index < USER_TEXTURES_START then
    Result := TSimpleTexture(RES[Sprite][Index])
  else
    Result := UserTextures[index - USER_TEXTURES_START];
end;

function RESSound(index: integer): PMix_Chunk;
begin
  Result := PMix_Chunk(RES[Sound][Index]);
end;

function RESFont(index: integer): TFontResource;
begin
  Result := TFontResource(RES[Font][Index]);
end;

function RESButton(index: integer): TButtonResource;
begin
  Result := TButtonResource(RES[Button][Index]);
end;

function RESTileMap(index: integer): TTileMapResource;
begin
  Result := TTileMapResource(RES[TileMap][Index]);
end;

function RESShader(index: integer): TShader;
begin
  Result := TShader(RES[Shader][Index]);
end;

function RESParticle(index: integer): TParticle;
begin
  Result := TParticle(RES[Particle][Index]);
end;

function RESEmitter(index: integer): TEmitter;
begin
  Result := TEmitter(RES[Emitter][Index]);
end;

function LoadRaw(s: string): TRawResourceData;
var
  ff: file;
begin
  AssignFile(ff, s);
  Reset(ff, 1);
  Result := TRawResourceData.Create(FileSize(ff));
  BlockRead(ff, Result.Data^, Result.Size);
  CloseFile(ff);
end;

function MatchTiled(s: string; out x, y: integer): boolean;
var
  bases: string;
  ax, st: integer;
  aend: string;
  First, second: string;
begin
  Result := False;
  bases := '';
  repeat
    st := pos('_tiled_', s);
    if st <= 0 then
      break;//do we need empty names?
    aend := RightStr(s, Length(s) - st - length('_tiled_') + 1);
    ax := Pos('x', aend);
    if (ax > 0) then
    begin
      First := Copy(aend, 1, ax - 1);
      second := Copy(aend, ax + 1, MAXINT);
      if TryStrToInt(First, x) and TryStrToInt(second, y) then
      begin
        Result := True;
        Delete(s, st, MAXINT);
        break;
      end;
    end;
    bases := bases + LeftStr(s, st - 1 + length('_tiled_') - 1);
    Delete(s, 1, st - 1 + length('_tiled_') - 1);
  until False;
  s := bases + s;
end;



function LoadResource(typ: TResourceType; s: string): Pointer;
var
  ax, ay: integer;
  tex: TSimpleTexture;
  bt: TButtonResource;
  flag: boolean;
begin
  Result := nil;
  Inc(curres);
  ShowProgress(curres / totalresources);
  case typ of
    Raw: Result := Pointer(LoadRaw(s));
    Sprite:
    begin
      Result := TSimpleTexture.Create(s, True);
    end;
    TileMap:
    begin
      tex := TSimpleTexture.Create(s, True);
      MatchTiled(ChangeFileExt(s, ''), ax, ay);
      Result := TTileMapResource.Create(tex, ax, ay);
    end;
    Sound:
      Result := Mix_LoadWAV(PChar(s));
    Button:
    begin
      bt := TButtonResource.Create;
      logf('loading %s', [s + '/normal.png']);
      bt.tex[bsNormal] := TSimpleTexture.Create(s + '/normal.png', True);
      bt.tex[bsHover] := bt.tex[bsNormal];
      if FileExists(s + '/hover.png') then
      begin
        logf('loading %s', [s + '/hover.png']);
        bt.tex[bsHover] := TSimpleTexture.Create(s + '/hover.png', True);
      end;
      bt.tex[bsPressed] := bt.tex[bsHover];
      if FileExists(s + '/pressed.png') then
      begin
        logf('loading %s', [s + '/pressed.png']);
        bt.tex[bsPressed] := TSimpleTexture.Create(s + '/pressed.png', True);
      end;
      Result := bt;
    end;
    Font:
      if FileExists(s) then
      begin
        Result := TTTFFont.Create(PChar(s));
      end
      else
      begin
        Result := TBitmapFont.Create(TSimpleTexture.Create(s + '/font.png', True),
          s + '/font.fnt');
      end;
    Shader:
    begin
      Result := TShader.Create;
      LogEnter('Shader program: ' + s);
      TShader(Result).Load(s);
      TShader(Result).Compile;
      flag := False;
      TShader(Result).Introspect(flag);
      if flag then
        logf('WARNING: shader %s introspection doesn''t match declaration!', [s]);
      LogExit('OK');
    end;
    Particle:
    begin
      Result := TParticle.Create(s);
    end;
    Emitter:
    begin
      Result := TEmitter.Create(s);
    end;
  end;
end;

procedure Process(s: string); forward;

procedure ProcessDir(s: string); forward;

procedure ProcessFile(s: string);
var
  typ: TResourceType;
  ext, test: string;
  ax, ay: integer;
begin
  ext := ExtractFileExt(s);
  if ext[1] = '.' then
    ext := Copy(ext, 2, MaxInt);
  ext := LowerCase(ext);
  typ := Raw;
  for test in SHADER_PARTS_EXTS do
    if ext = test then
    begin
      //Logf('Skipping: %s (extension ''%s'' is a part of shader)', [s, ext]);
      Inc(curres);
      ShowProgress(curres / totalresources);
      exit;
    end;
  for test in GRAPH_EXTS do
    if ext = test then
    begin
      typ := Sprite;
      if MatchTiled(ChangeFileExt(s, ''), ax, ay) then
        typ := TileMap;
      break;
    end;
  if typ = Raw then
    for test in SOUND_EXTS do
      if ext = test then
      begin
        typ := Sound;
        break;
      end;
  if typ = Raw then
    for test in FONT_EXTS do
      if ext = test then
      begin
        typ := Font;
        break;
      end;
  if typ = Raw then
    if ext = SHADER_EXT then
      typ := Shader;
  if typ = Raw then
    if ext = PARTICLE_EXT then
      typ := Particle;
  if typ = Raw then
    if ext = EMITTER_EXT then
      typ := Emitter;
  LogEnter(Format('Loading: %s (id:%d)', [s, RES[typ].Count]));
  RES[typ].Add(LoadResource(typ, s));
  LogExit('');
end;

{$IFDEF WINDOWS}

procedure ProcessTopDirs(s: string);
var
  ff: TSearchRec;
begin
  if FindFirst(s + '/*', faAnyFile, ff) <> 0 then
    exit;
  try
    if ff.Name = '.' then
      if FindNext(ff) <> 0 then
        exit;
    if ff.Name = '..' then
      if FindNext(ff) <> 0 then
        exit;
    repeat
      if DirectoryExists(s + '/' + ff.Name) then
      begin
        if (RightStr(ff.Name, 7) <> '_button') and
           (RightStr(ff.Name, 5) <> '_font') then
          ProcessDir(s + '/' + ff.Name);
      end;
    until FindNext(ff) <> 0;

  finally
    FindClose(ff);
  end;
end;

procedure ProcessDirResources(s: string);
var
  ff: TSearchRec;
begin
  if FindFirst(s + '/*', faAnyFile, ff) <> 0 then
    exit;
  try
    if ff.Name = '.' then
      if FindNext(ff) <> 0 then
        exit;
    if ff.Name = '..' then
      if FindNext(ff) <> 0 then
        exit;
    repeat
      if DirectoryExists(s + '/' + ff.Name) then
      begin
        if (RightStr(ff.Name, 7) = '_button') or (RightStr(ff.Name, 5) = '_font') then
          ProcessDir(s + '/' + ff.Name);
      end;
    until FindNext(ff) <> 0;

  finally
    FindClose(ff);
  end;

  FindFirst(s + '/*', faAnyFile - faDirectory, ff);
  repeat
    if FileExists(s + '/' + ff.Name) then
      ProcessFile(s + '/' + ff.Name);
  until FindNext(ff) <> 0;
  FindClose(ff);
end;

{$ELSE}
procedure ProcessTopDirs(s: string);
var
  list: TStringList;
  name: string;
  ff: TSearchRec;
begin
  if FindFirst(s + '/*', faAnyFile, ff) <> 0 then
    exit;
  list := TStringList.Create;
  try
    repeat
      if (ff.Name = '.') or (ff.Name = '..') then
        continue;
      if DirectoryExists(s + '/' + ff.Name) then
      begin
        if (RightStr(ff.Name, 7) <> '_button') and
           (RightStr(ff.Name, 5) <> '_font') then
          list.add(ff.name);
      end;
    until FindNext(ff) <> 0;
    list.sort;
    for name in list do
      ProcessDir(s + '/' + Name);
  finally
    FindClose(ff);
    list.free;
  end;
end;

procedure ProcessDirResources(s: string);
var
  ff: TSearchRec;
  list: TStringList;
  name: string;
begin
  if FindFirst(s + '/*', faAnyFile, ff) <> 0 then
    exit;
  list := TStringList.Create;
  try
    repeat
      if (ff.Name = '.') or (ff.Name = '..') then
        continue;
      if DirectoryExists(s + '/' + ff.Name) then
      begin
        if (RightStr(ff.Name, 7) = '_button') or (RightStr(ff.Name, 5) = '_font') then
          list.add(ff.name);
      end;
    until FindNext(ff) <> 0;
    list.sort;
    for name in list do
      ProcessDir(s + '/' + Name);
  finally
    FindClose(ff);
  end;

  list.clear;
  FindFirst(s + '/*', faAnyFile - faDirectory, ff);
  repeat
    if FileExists(s + '/' + ff.Name) then
      list.add(ff.name);
  until FindNext(ff) <> 0;
  FindClose(ff);
  list.sort;
  for name in list do
    ProcessFile(s + '/' + Name);
  list.free;
end;

{$ENDIF}

procedure ProcessDir(s: string);
begin
  if RightStr(s, 7) = '_button' then
  begin
    LogEnter(Format('Loading button: %s (id:%d)', [s, RES[Button].Count]));
    RES[Button].Add(LoadResource(Button, s));
    LogExit('');
    exit;
  end;
  if RightStr(s, 5) = '_font' then
  begin
    LogEnter(Format('Loading font: %s (id:%d)', [s, RES[Font].Count]));
    RES[Font].Add(LoadResource(Font, s));
    LogExit('');
    exit;
  end;
  ProcessTopDirs(s);
  ProcessDirResources(s);
end;

procedure Process(s: string);
begin
  if FileExists(s) then
    ProcessFile(s)
  else if DirectoryExists(s) then
    ProcessDir(s);
end;


function CountFiles(s: string): integer;
var
  ff: TSearchRec;
begin
  if FileExists(s) or (RightStr(s, 7) = '_button') or (RightStr(s, 5) = '_font') then
  begin
    Result := 1;
    exit;
  end;
  Result := 0;
  if FindFirst(s + '/*', faAnyFile, ff) <> 0 then
    exit;
  try
    repeat
      if (ff.Name = '.') or (ff.Name = '..') then
        if FindNext(ff) <> 0 then
          break
        else
          continue;
      Inc(Result, CountFiles(s + '/' + ff.Name));
    until FindNext(ff) <> 0;
  finally
    FindClose(ff);
  end;
end;


var
  LoadingBackTex, LoadingBetterRectTex: TSimpleTexture;
  LoadingBack, LoadingBetterRect: TSpriteList;

procedure PreloadProgress(dir: string);
var
  v00, v01, v10, v11: TSpriteVertex;
begin
  glClearColor((PARAMS[ClearColor] shr 24) / 255,
    ((PARAMS[ClearColor] shr 16) and 255) / 255,
    ((PARAMS[ClearColor] shr 8) and 255) / 255, 0);
  SetDefaulUniforms;
  DefaultShader.Activate;
  if FileExists(dir + '/loading_back.png') then
  begin
    LoadingBackTex := TSimpleTexture.Create(dir + '/loading_back.png', True);
    LoadingBack := TSpriteList.Create(6);
    v00 := Vertex(0, 0, 0, 0, 0, 0, WHITE);
    v01 := Vertex(0, LogicYSize, 0, 0, 1, 0, WHITE);
    v10 := Vertex(LogicXSize, 0, 0, 1, 0, 0, WHITE);
    v11 := Vertex(LogicXSize, LogicYSize, 0, 1, 1, 0, WHITE);
    LoadingBack.AddVertex(v00);
    LoadingBack.AddVertex(v10);
    LoadingBack.AddVertex(v01);
    LoadingBack.AddVertex(v10);
    LoadingBack.AddVertex(v01);
    LoadingBack.AddVertex(v11);
  end;

  if FileExists(dir + '/loading_bar.png') then
  begin
    LoadingBetterRectTex := TSimpleTexture.Create(dir + '/loading_bar.png', True);
  end;
  LoadingBetterRect := TSpriteList.Create(6);
end;

procedure ShowProgress(progress: single);
var
  x0, y0, x1, y1: TCoord;
  v00, v01, v10, v11: TSpriteVertex;
begin
  DefaultShader.Activate;
  if Assigned(LoadingBackTex) then
  begin
    LoadingBackTex.ActivateReading(0);
    LoadingBack.Draw(GL_TRIANGLES);
  end;
  x0 := PARAMS[ProgressX];
  x1 := progress * PARAMS[ProgressWidth] + PARAMS[ProgressX];
  y0 := PARAMS[ProgressY];
  y1 := PARAMS[ProgressHeight] + PARAMS[ProgressY];
  if Assigned(LoadingBetterRectTex) then
  begin
    LoadingBetterRectTex.ActivateReading(0);
    v00 := Vertex(x0, y0, 0, 0, 0, 0, WHITE);
    v01 := Vertex(x0, y1, 0, 0, 1, 0, WHITE);
    v10 := Vertex(x1, y0, 0, progress, 0, 0, WHITE);
    v11 := Vertex(x1, y1, 0, progress, 1, 0, WHITE);
  end
  else
  begin
    v00 := Vertex(x0, y0, 0, 0, 0, -1, $FFFF00FF);
    v01 := Vertex(x0, y1, 0, 0, 0, -1, $FFFF00FF);
    v10 := Vertex(x1, y0, 0, 0, 0, -1, $FFFF00FF);
    v11 := Vertex(x1, y1, 0, 0, 0, -1, $FFFF00FF);
  end;
  LoadingBetterRect.NPoints := 0;
  LoadingBetterRect.AddVertex(v00);
  LoadingBetterRect.AddVertex(v10);
  LoadingBetterRect.AddVertex(v01);
  LoadingBetterRect.AddVertex(v10);
  LoadingBetterRect.AddVertex(v01);
  LoadingBetterRect.AddVertex(v11);
  LoadingBetterRect.Draw(GL_TRIANGLES);

  SDL_GL_SwapWindow(Window);
  sleep(0);
  glClear(GL_COLOR_BUFFER_BIT);
end;

procedure FreeResources;
var
  i: integer;
  list: TList;
begin
  //  Shader
  LogEnter('freeing: raw');
  for i := 0 to RES[Raw].Count - 1 do
    TRawResourceData(RES[Raw][i]).Free;
  logf('sprite', []);
  for i := 0 to RES[Sprite].Count - 1 do
    TGenericTexture(RES[Sprite][i]).Free;
  logf('sound', []);
  for i := 0 to RES[Sound].Count - 1 do
    Mix_FreeChunk(PMix_Chunk(RES[Sound][i]));
  logf('font', []);
  for i := 0 to RES[Font].Count - 1 do
    TFontResource(RES[Font][i]).Free;
  Logf('button', []);
  for i := 0 to RES[Button].Count - 1 do
    TButtonResource(RES[Button][i]).Free;
  logf('tilemap', []);
  for i := 0 to RES[TileMap].Count - 1 do
    TTileMapResource(RES[TileMap][i]).Free;
  logf('shader', []);
  for i := 0 to RES[Shader].Count - 1 do
    TShader(RES[Shader][i]).Free;
  logf('lists', []);
  for list in res do
    list.Free;
  LogExit('');
end;

procedure InitDefaultShader;
var
  flag: boolean;
begin
  DefaultShader := TShader.Create;

  LogEnter('Shader program: *internal*');
  DefaultShader.component_names[uShaderProcess.vertex] := 'vertex';
  DefaultShader.Components[uShaderProcess.vertex] := SIMPLE_VS_SHADER;
  DefaultShader.component_names[uShaderProcess.fragment] := 'fragment';
  DefaultShader.Components[uShaderProcess.fragment] := SIMPLE_FS_SHADER;
  DefaultShader.Compile;
  flag := False;
  DefaultShader.Introspect(flag);
  RES[Shader].Add(DefaultShader);
  InitShadersCache(RES[Shader]);
  LogExit('');
end;

procedure LoadResources(dir: string);
var
  typ: TResourceType;
begin
  LogEnter('Loading resources...');
  for typ in TResourceType do
    RES[typ] := TList.Create;
  InitDefaultShader;
  InitShadersCache(RES[Shader]);
  totalresources := CountFiles(dir);
  curres := 0;
  PreloadProgress(dir);
  ProcessDir(dir);
  InitFontConfig(RES[Font].Count);
  InitShadersCache(RES[Shader]);
  LogExit('Loading complete');
end;

{ TRawResourceData }

destructor TRawResourceData.Destroy;
begin
  Freemem(Data);
  inherited Destroy;
end;

constructor TRawResourceData.Create(aSize: integer);
begin
  Size := aSize;
  Data := GetMem(aSize);
end;

end.
