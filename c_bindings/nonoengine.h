#ifndef _NONOENGINE_H_
#define _NONOENGINE_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

typedef float Coord;
typedef uint32_t Color;
typedef int32_t RawResource;
typedef int32_t TSprite;
typedef int32_t Sound;
typedef int32_t TButton;
typedef int32_t TileMap;
typedef int32_t Font;
typedef Font FontInstance;
typedef int32_t TPanel;
typedef int32_t Shader;
typedef int32_t ShaderUniform;
typedef int32_t ShaderAttribute;
typedef Sprite Texture;
typedef int32_t VertexList;
typedef double PhysicsCoord;
typedef int32_t Body;
typedef int64_t BodyID;
const BodyID NO_BODY_ID = -1;
typedef int32_t TMaterial;
typedef int32_t Polygon;

typedef enum {
  KEY_A,
  KEY_B,
  KEY_C,
  KEY_D,
  KEY_E,
  KEY_F,
  KEY_G,
  KEY_H,
  KEY_I,
  KEY_J,
  KEY_K,
  KEY_L,
  KEY_M,
  KEY_N,
  KEY_O,
  KEY_P,
  KEY_Q,
  KEY_R,
  KEY_S,
  KEY_T,
  KEY_U,
  KEY_V,
  KEY_W,
  KEY_X,
  KEY_Y,
  KEY_Z,
  KEY_Num0,
  KEY_Num1,
  KEY_Num2,
  KEY_Num3,
  KEY_Num4,
  KEY_Num5,
  KEY_Num6,
  KEY_Num7,
  KEY_Num8,
  KEY_Num9,
  KEY_Escape,
  KEY_LControl,
  KEY_LShift,
  KEY_LAlt,
  KEY_LSystem,
  KEY_RControl,
  KEY_RShift,
  KEY_RAlt,
  KEY_RSystem,
  KEY_Menu,
  KEY_LBracket,  // [
  KEY_RBracket,  // ]
  KEY_SemiColon, // ;
  KEY_Comma,     // ,
  KEY_Period,    // .
  KEY_Quote,     // '
  KEY_Slash,     // "/"
  KEY_BackSlash, // "\"
  KEY_Tilde,     // ~
  KEY_Equal,     // =
  KEY_Dash,      // -
  KEY_Space,
  KEY_Return,
  KEY_Backspace,
  KEY_Tab,
  KEY_PageUp,
  KEY_PageDown,
  KEY_End,
  KEY_Home,
  KEY_Insert,
  KEY_Delete,
  KEY_Add,      // +
  KEY_Subtract, // -
  KEY_Multiply, // *
  KEY_Divide,   // "/""
  KEY_Left,     // Left arrow
  KEY_Right,    // Right arrow
  KEY_Up,       // Up arrow
  KEY_Down,     // Down arrow
  KEY_Numpad0,
  KEY_Numpad1,
  KEY_Numpad2,
  KEY_Numpad3,
  KEY_Numpad4,
  KEY_Numpad5,
  KEY_Numpad6,
  KEY_Numpad7,
  KEY_Numpad8,
  KEY_Numpad9,
  KEY_F1,
  KEY_F2,
  KEY_F3,
  KEY_F4,
  KEY_F5,
  KEY_F6,
  KEY_F7,
  KEY_F8,
  KEY_F9,
  KEY_F10,
  KEY_F11,
  KEY_F12,
  KEY_F13,
  KEY_F14,
  KEY_F15,
  KEY_Pause,

  // special keys

  KEY_Quit = -1,
  KEY_Any = -2,
} Key;

typedef enum {
  MB_Left,
  MB_Right,
  MB_Middle,
} MouseButton;

typedef enum {
  MA_X,
  MA_Y,
  MA_Scroll,
  MA_ScaledX,
  MA_ScaledY,
} MouseAxis;

typedef enum {
  Up,
  Down,
  Pressed,
} TKeyState;

typedef enum {
  Up,
  Down,
  Clicked,
} MouseButtonState;

typedef enum {
  None,
  Top,
  Center,
  Bottom,
  Flow,
} VAlign;
typedef enum {
  None,
  Left,
  Center,
  Right,
  Flow,
} HAlign;

typedef enum {
  Fullscreen,
  Width,
  Height,
  VSync,
  Antialias,
  UseLog,
  Autoscale,
  Volume,
  ClearColor,
  PhysicsSpeed,
  GravityX,
  GravityY,
  Damping,
  ProgressWidth,
  ProgressHeight,
  ProgressX,
  ProgressY,
  RealWidth = 100,
  RealHeight = 101,
  FPS = 102,
  DeltaTime = 103,
} EngineValue;
// TEngineConfig = Fullscreen..ClearColor;

typedef enum {
  Bold,
  Italic,
  Underlined,
} FontStyle;

typedef enum {
  Normal,
  Hover,
  Pressed,
  Clicked,
} ButtonState;

typedef enum {
  X,
  Y,
  Width,
  Height,
  MouseX,
  MouseY,
} GUICoord;

typedef enum {
  AStarNew,
  AStarReuse,
  DijkstraNew,
  DijkstraReuse,
} PathfindAlgorithm;
// type PathfindCallback = (Float32, Float32, Float32, Float32, Void *->Float32)

typedef enum {
  AsByte,
  AsFloat,
} PixelFormat;

typedef enum {
  Points,
  Lines,
  Triangles,
} VertexListPrimitive;

typedef enum {
  Read,
  Write,
  ReadWrite,
  Increment,
} PhysicCoordinatesMode;

typedef enum {
  Dynamic,
  Static,
  Kinematic,
  NonRotating,
} BodyType;

typedef enum {
  Pass,
  Hit,
  PassDetect,
  HitDetect,
  Processable,
} CollisionType;

// Общие функции движка

void EngineInit(char *resources_dir);
void EngineSet(EngineValue param, int32_t value);
int32_t EngineGet(EngineValue param);
void EngineProcess(void);
void *RawResource(RawResource resource, int32_t *size);
uint32_t RawTexture(Sprite resource); // returns opengl handle
void EngineLog(char *s);
void EngineFree(void);

// Обработка ввода

TKeyState KeyState(Key key);
Coord MouseGet(MouseAxis axis);
MouseButtonState MouseState(MouseButton key);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _NONOENGINE_H_ */