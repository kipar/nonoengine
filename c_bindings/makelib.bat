echo LIBRARY %~n1 >> %~n1.def
echo EXPORTS >> %~n1.def
for /f "skip=19 tokens=4" %%A in ('dumpbin /exports %~n1.dll') do echo %%A >> %~n1.def
lib /def:%~n1.def /machine:x64
del %~n1.def
del %~n1.exp
