unit uGame;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fgl, Math, uEngine, uCommon;

type

  { TUnit }

  TUnit = class
    our: boolean;
    x, y: integer;
    tox, toy: integer;
    target: TUnit;
    hp: integer;
    selected: boolean;
    anim_offset: integer;
    played: boolean;
    constructor Create(is_our: boolean);
    procedure Process;
    function Distance(au: TUnit): integer;
    function FindTarget: TUnit;
    procedure Draw;
  end;

  TUnits = specialize TFPGList<TUnit>;

  THUDMode = (hudSelecting, hudCommanding);

  { THUD }

  THUD = class
    Mode: THUDMode;
    CornerX, CornerY: TCoord;
    constructor Create;
    procedure Process;
    procedure Draw;

    procedure ResetSelection;
    procedure DoSelect(x1, y1, x2, y2: TCoord);
    procedure DoCommand(x, y: TCoord);
  end;


var
  AllUnits: TUnits;
  HUD: THUD;

procedure DoGame;
procedure DrawGame;
procedure NewGame;

const
  UNIT_SCALE = 0.5;
  SNAKE_SCALE = 1.5;
  YARD = 200;
  TARGET_R = 50;

implementation

uses Resources;

function AnimationFrame(Count: integer; offset: integer): integer;
begin
  Result := (GetTickCount64 div 100 + offset) mod Count;
end;


procedure DoGame;
var
  u: TUnit;
  i: integer;
begin
  if random < 0.5 then
    AllUnits.Add(TUnit.Create(False));
  for u in AllUnits do
    u.Process;
  for i := AllUnits.Count - 1 downto 0 do
  begin
    u := AllUnits[i];
    if u.hp < 0 then
    begin
      if u.our then
        Play(RES.Game.Dead);
      AllUnits.Delete(i);
    end;
  end;
  HUD.Process;
end;

procedure DrawGame;
var
  u: TUnit;
begin
  for u in AllUnits do
    u.Draw;
  HUD.Draw;
end;

procedure NewGame;
var
  i: integer;
begin
  AllUnits := TUnits.Create;
  for i := 1 to 10 do
    AllUnits.Add(TUnit.Create(True));
  for i := 1 to 40 do
    AllUnits.Add(TUnit.Create(False));
end;

{ TUnit }

procedure GenSide(out x, y: integer);
var
  d: integer;
begin
  d := Random(2 * (EngineGet(Width) + EngineGet(Height)));
  if d < EngineGet(Width) then
  begin
    x := d;
    y := 50;
    exit;
  end;
  Dec(d, EngineGet(Width));
  if d < EngineGet(Width) then
  begin
    x := d;
    y := EngineGet(Height) - 50;
    exit;
  end;
  Dec(d, EngineGet(Width));
  if d < EngineGet(Height) then
  begin
    x := 50;
    y := d;
    exit;
  end;
  Dec(d, EngineGet(Height));
  x := EngineGet(Width) - 50;
  y := d;
end;

{ THUD }

constructor THUD.Create;
begin
  Mode := hudCommanding;
end;

procedure THUD.Process;
begin
  case Mode of
    hudCommanding:
    begin
      if MouseState(LeftButton) = mbsDown then
      begin
        ResetSelection;
        Mode := hudSelecting;
        CornerX := MouseGet(CursorX);
        CornerY := MouseGet(CursorY);
      end
      else if MouseState(RightButton) = mbsDown then
      begin
        DoCommand(MouseGet(CursorX), MouseGet(CursorY));
      end;
    end;
    hudSelecting:
    begin
      if MouseState(LeftButton) <> mbsDown then
      begin
        DoSelect(CornerX, CornerY, MouseGet(CursorX), MouseGet(CursorY));
        Mode := hudCommanding;
      end;
    end;
  end;
end;

procedure THUD.Draw;
begin
  if Mode = hudSelecting then
    Rect(CornerX, CornerY, MouseGet(CursorX) - CornerX, MouseGet(CursorY) - CornerY,
      False, LIME);
end;

procedure THUD.ResetSelection;
var
  u: TUnit;
begin
  for u in AllUnits do
    u.selected := False;
end;

procedure THUD.DoSelect(x1, y1, x2, y2: TCoord);
var
  u: TUnit;
  t: TCoord;
  any: boolean;
begin
  if x1 > x2 then
  begin
    t := x1;
    x1 := x2;
    x2 := t;
  end;
  if y1 > y2 then
  begin
    t := y1;
    y1 := y2;
    y2 := t;
  end;
  any := False;
  for u in AllUnits do
    if u.our and InRange(u.x, x1, x2) and InRange(u.y, y1, y2) then
    begin
      u.selected := True;
      any := True;
    end;
  if any then
    Play(RES.Game.Select);
end;

procedure THUD.DoCommand(x, y: TCoord);
var
  u: TUnit;
begin
  for u in AllUnits do
    if u.our and u.selected then
    begin
      u.tox := Trunc(x) + random(2 * TARGET_R) - TARGET_R;
      u.toy := Trunc(y) + random(2 * TARGET_R) - TARGET_R;
      if not SoundPlaying(RES.Game.Go) then
        Play(RES.Game.Go);
    end;
end;

constructor TUnit.Create(is_our: boolean);
begin
  our := is_our;
  hp := 100;
  anim_offset := random(500);
  if our then
  begin
    x := Random(2 * YARD) - YARD + EngineGet(Width) div 2;
    y := Random(2 * YARD) - YARD + EngineGet(Height) div 2;
  end
  else
  begin
    GenSide(x, y);
  end;
  tox := x;
  toy := y;
end;

function Distance(x1, y1, x2, y2: integer): integer;
begin
  Result := Sqr(x1 - x2) + Sqr(y1 - y2);
end;


procedure TUnit.Process;
var
  u: TUnit;
begin
  //  if random < 0.9 then exit;
  if our then
  begin
    if uGame.Distance(x, y, tox, toy) < 10 * 10 then
    begin
      u := FindTarget;
      if (u <> nil) and (Distance(u) < 200 * 200) then
      begin
        target := u;
        Dec(target.hp, 5);
      end
      else
        target := nil;
    end
    else
      target := nil;
  end
  else
  begin
    target := FindTarget;
    if target = nil then
    begin
      tox := x;
      toy := y;
    end
    else
    begin
      tox := target.x;
      toy := target.y;
      if Distance(target) < 10 * 10 then
        Dec(target.hp);
    end;
  end;

  if tox < x then
    Dec(x)
  else if tox > x then
    Inc(x);
  if toy < y then
    Dec(y)
  else if toy > y then
    Inc(y);

  if our then
  begin
    if tox < x then
      Dec(x)
    else if tox > x then
      Inc(x);
    if toy < y then
      Dec(y)
    else if toy > y then
      Inc(y);
  end;

end;

function TUnit.Distance(au: TUnit): integer;
begin
  Result := uGame.Distance(au.x, au.y, x, y);
end;

function TUnit.FindTarget: TUnit;
var
  u: TUnit;
  d, mind: integer;
begin
  mind := 100000000;
  Result := nil;
  for u in AllUnits do
    if u.our <> our then
    begin
      d := Distance(u);
      if d < mind then
      begin
        Result := u;
        mind := d;
      end;
    end;
end;

procedure TUnit.Draw;
var
  revert: integer;
begin
  if tox < x then
    revert := -1
  else
    revert := 1;
  if our then
  begin
    if target <> nil then
    begin
      DrawTiled(Res.Game.Robot, 7 + AnimationFrame(7, anim_offset), x,
        y, revert * UNIT_SCALE, UNIT_SCALE);
      if (AnimationFrame(7, anim_offset) = 0) then
        played := False;
      if (AnimationFrame(7, anim_offset) = 5) and not played and GameActive then
      begin
        Play(RES.Game.Shot, 50, self);
        played := True;
      end;
      if played then
      begin
        LineSettings(5);
        Line(x, y, target.x, target.y, RED);
        LineSettings(1);
      end;
    end
    else if (tox <> x) or (toy <> y) then
      DrawTiled(Res.Game.Robot, AnimationFrame(7, anim_offset), x,
        y, revert * UNIT_SCALE, UNIT_SCALE)
    else
      DrawTiled(Res.Game.Robot, 7, x, y, UNIT_SCALE, UNIT_SCALE);
    if selected then
    begin
      LineSettings(2);
      Rect(x - 10, y - 10, 20, 20, False, YELLOW);
      LineSettings(1);
    end;
  end
  else
  begin
    if (target <> nil) and (Distance(target) < 10 * 10) then
      DrawTiled(Res.Game.Snake, 30 + AnimationFrame(10, anim_offset),
        x, y, -revert * SNAKE_SCALE, SNAKE_SCALE, 0, RED)
    else
      DrawTiled(Res.Game.Snake, 20 + AnimationFrame(10, anim_offset),
        x, y, -revert * SNAKE_SCALE, SNAKE_SCALE);
  end;
  if hp < 100 then
  begin
    Rect(x - 5, y + 20, 10, 2, True, Red);
    Rect(x - 5, y + 20, hp div 10, 2, True, MONEYGREEN);
  end;
end;



begin
  HUD := THUD.Create;
end.
