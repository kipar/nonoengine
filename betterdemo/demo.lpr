program demo;

uses
  SysUtils,
  uEngine,
  Resources, uMainMenu, uCommon, uGame;


var
  NvOptimusEnablement :uint32 = 1;
  exports NvOptimusEnablement;


begin
//  EngineSet(Antialias, 4);
  EngineSet(VSync, 1);
  EngineSet(Autoscale, 0);
  EngineSet(ClearColor, GRAY);
  EngineInit('./resources');
  FontConfig(RES.Ui_black, 24, BLACK, [], 1, 1);
  repeat
    if GameStarted then
      Music(RES.Music.Game)
    else
      Music(RES.Music.Menu);

    if GameStarted then
      DrawGame;

    if not GameActive then
      DoMainMenu
    else
    begin
      DoGame;
      if (KeyState(KeyEscape) <> ksUp) then
        GameActive := False;
    end;

    EngineProcess;
  until (KeyState(Quit) <> ksUp) or Quitting;
end.
