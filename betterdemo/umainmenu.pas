unit uMainMenu;

{$mode objfpc}{$H+}

interface

uses
  SysUtils,
  uEngine,
  Resources;

var
  bt: TButton;
  Quitting: boolean;

procedure DoMainMenu;

implementation

uses uCommon, uGame;

procedure DoMainMenu;
var
  Caption: string;
begin
  if not GameStarted then
    Background(RES.Ui.Back, 1, 3);
  Panel(1, 0, 0, 0, 240, 400, haCenter, vaCenter);
  Button(Res.Ui.Frame, 1, 0, 0, 240, 400);
  if Button(RES.Ui.Stdbutton, 1, 0, 20, 200, 50, haCenter, vaFlow,
    'Новая игра', RES.Ui_font) = bsClicked then
  begin
    GameStarted := True;
    GameActive := True;
    Music(NO_MUSIC);
    NewGame;
    Play(RES.Ui.Select);
  end;

  if GameStarted then
  begin
    if Button(RES.Ui.Stdbutton, 1, 0, 20, 200, 50, haCenter, vaFlow,
      'Продолжить', RES.Ui_font) = bsClicked then
    begin
      GameActive := True;
      Play(RES.Ui.Select);
    end;
  end
  else
    Button(RES.Ui.Disabledbutton, 1, 0, 20, 200, 50, haCenter, vaFlow,
      'Продолжить', RES.Ui_black);
  if Button(RES.Ui.Stdbutton, 1, 0, 20, 200, 50, haCenter, vaBottom,
    'Выход', RES.Ui_font) = bsClicked then
    Quitting := True;
  if Button(RES.Ui.Slider, 1, 0, 20, 200, 45, haCenter, vaFlow,
    PChar(IntToStr(EngineGet(Volume)) + '%'), RES.Ui_font, Pointer(Volume)) = bsPressed then
  begin
    EngineSet(Volume, Trunc(100 * GetGUICoord(gcMouseX)));
  end;
  //TODO: draw slider

  if MouseGet(ScrollPos) <> 0 then
    EngineSet(Volume, EngineGet(Volume) + Trunc(MouseGet(ScrollPos)));
  if (KeyState(KeyEscape) = ksPressed) then
    Quitting := True;

  if Hardcore then
    Caption := 'Сложность - высокая'
  else
    Caption := 'Сложность - низкая';
  if Button(RES.Ui.Slider, 1, 0, 20, 200, 45, haCenter, vaFlow,
    PChar(Caption), RES.Ui_font, @Hardcore) = bsClicked then
    Hardcore := not Hardcore;
end;

end.
