unit Resources;

interface
{$J-}
type
TRawResource = (TRawResource_NOT_USED);
TSprite = (THE_SCREEN = -1, res_Back);
TSound = (NO_MUSIC = -1, res_Dead, res_Go, res_Select, res_Shot, res_Game_1, res_Menu, res_Select_1);
TButton = (res_Disabledbutton, res_Frame, res_Slider, res_Stdbutton);
TTileMap = (res_Robot, res_Snake);
TFont = (res_Ui_black, res_Ui_font);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, TShader_NOT_USED);

TShaderAttribute = (attr_color, attr_pos, attr_texpos);
TShaderUniform = (uni_screen_size, uni_tex);


TGame = record
  Dead: TSound;
  Go: TSound;
  Robot: TTileMap;
  Select: TSound;
  Shot: TSound;
  Snake: TTileMap;
end;

TMusic = record
  Game: TSound;
  Menu: TSound;
end;

TUi = record
  Back: TSprite;
  Disabledbutton: TButton;
  Frame: TButton;
  Select: TSound;
  Slider: TButton;
  Stdbutton: TButton;
end;

TRES = record
  Game: TGame;
  Music: TMusic;
  Ui: TUi;
  Ui_black: TFont;
  Ui_font: TFont;
end;

const RES: TRES = (
  Game: (
    Dead: res_Dead;
    Go: res_Go;
    Robot: res_Robot;
    Select: res_Select;
    Shot: res_Shot;
    Snake: res_Snake;
  );
  Music: (
    Game: res_Game_1;
    Menu: res_Menu;
  );
  Ui: (
    Back: res_Back;
    Disabledbutton: res_Disabledbutton;
    Frame: res_Frame;
    Select: res_Select_1;
    Slider: res_Slider;
    Stdbutton: res_Stdbutton;
  );
  Ui_black: res_Ui_black;
  Ui_font: res_Ui_font;
);

implementation
end.
