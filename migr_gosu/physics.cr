require "../crystal/engine.cr"
require "./resources.cr"

def anim_frame(count)
  (Time.monotonic.total_milliseconds / 500) % count
end

class Player < Engine::Body
  property score = 0

  def turn_left
    apply_force(v2(0, 0), v2(0, 0), -200000)
  end

  def turn_right
    apply_force(v2(0, 0), v2(0, 0), 200000)
  end

  def stop_turn
    return if omega.abs < 1e-6
    apply_force(v2(0, 0), v2(0, 0), -200000 * omega.clamp(-1, 1))
  end

  def accelerate
    apply_force(v2(20000, 0), v2(0, 0), 0)
  end

  def reverse
    apply_force(v2(-20000, 0), v2(0, 0), 0)
  end

  def go_target(pos)
    apply_control(pos, 1000, 10000)
  end

  def reset_target
    apply_control(pos, 0, 0)
  end

  def draw
    RES::Starfighter.draw(pos, angle: -90 + angle)
  end

  material 0.01, 0.5, 0.5, 25
  collide Walls
  can_collide Star do |coll|
    return false unless coll.first?
    star = coll.data
    if star.hp > 1
      star.hp -= 1
      true
    else
      @score += 10
      RES::Beep.play
      star.free
      STARS.delete star
      false
    end
  end
end

class Star < Engine::Body
  material 1, 0.5, 0.5, 10
  collide Walls

  property hp : Int32
  @color : Engine::Color

  def initialize
    super(rand * 640, rand * 480)
    @color = color(rand(256 - 40) + 40, rand(256 - 40) + 40, rand(256 - 40) + 40)
    @hp = rand(3 + 1)
  end

  def draw
    case hp
    when 3
      RES::Star.draw_frame(0, pos, v2(1, 1), 0, Color::WHITE)
    when 2
      RES::Star.draw_frame(anim_frame(10), pos, v2(1, 1), 0, Color::WHITE)
    when 1
      RES::Star.draw_frame(anim_frame(10), pos, v2(1, 1), 0, @color)
    end
  end
end

class Walls < Engine::StaticBody
  material 1, 0.5, 0.5
end

class Asteroid < Engine::Body
  material 1, 0.5, 0.5
  collide Walls
  collide Player

  def initialize
    super(200f64, 200f64, angle: 10f64)
    @poly = Polygon.new
    @poly.add_point v2(50, 0)
    @poly.add_point v2(0, 50)
    @poly.add_point v2(-50, 0)
    @poly.add_point v2(0, -50)
    add_poly(@poly)
  end

  def draw
    @poly.draw(pos, angle)
  end
end

include Engine

STARS = [] of Star

Engine[Params::Antialias] = 4
Engine[Params::VSync] = 1
Engine[Params::Width] = 640
Engine[Params::Height] = 480
Engine[Params::Damping] = 800

Engine.init "resources"

font = Font.new(RES::Vera)
font.color = Color::YELLOW

level = Walls.new
level.add_box aabb(v2(-100, 0), v2(0, 480))
level.add_box aabb(v2(600, 0), v2(700, 480))
level.add_line v2(0, 0), v2(600, 0)
level.add_line v2(0, 480), v2(600, 480)

asteroid = Asteroid.new

player = Player.new
player.warp_to(x: 320, y: 200, angle: 45)
loop do
  RES::Space.background
  asteroid.draw
  player.draw
  STARS.each { |star| star.draw }
  font.draw_text("Score: #{player.score}", v2(10, 10))

  if Keys[Key::Left].down?
    player.turn_left
  elsif Keys[Key::Right].down?
    player.turn_right
  else
    player.stop_turn
  end
  if Keys[Key::Up].down?
    player.accelerate
  elsif Keys[Key::Down].down?
    player.reverse
  end

  if rand(100) < 4 && STARS.size < 25
    STARS.push(Star.new)
  end

  player.process
  asteroid.process
  STARS.each { |star| star.process }

  process
  break if !Keys[Key::Quit].up? || !Keys[Key::Escape].up?
end
