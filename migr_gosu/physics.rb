require_relative "../ruby/nonoengine"
require_relative "./resources"

def anim_frame(count)
  (Time.now.to_f * 10).to_i % count
end

MATERIAL_PLAYER = 1
MATERIAL_STARS = 2
MATERIAL_WALLS = 3
MATERIAL_ASTEROID = 4

STARS = []

class Player
  attr_reader :score
  attr_reader :body

  def initialize
    @body = Engine::Body.new(MATERIAL_PLAYER, self, 0, 0, 0, 0, 90, 0)
    @score = 0
  end

  def warp(ax, ay)
    @body.write_coords(x: ax, y: ay)
  end

  def turn_left
    @body.apply_force(0, 0, 0, 0, -200000)
  end

  def turn_right
    @body.apply_force(0, 0, 0, 0, 200000)
  end

  def stop_turn
    return if @body.omega.abs < 1e-6
    @body.apply_force(0, 0, 0, 0, -200000 * @body.omega.clamp(-1, 1))
  end

  def accelerate
    @body.apply_force(20000, 0, 0, 0, 0)
  end

  def reverse
    @body.apply_force(-20000, 0, 0, 0, 0)
  end

  def go_target(x, y)
    @body.apply_control(x, y, 1000, 10000)
  end

  def reset_target
    @body.apply_control(x, y, 0, 0)
  end

  def draw
    RES::Starfighter.draw(@body.x, @body.y, 1, 1, -90 + @body.angle)
  end

  def process
    @body.collisions(MATERIAL_STARS) do |coll|
      next unless coll.first?
      star = coll.data
      if star.hp > 1
        star.hp -= 1
        true
      else
        @score += 10
        RES::Beep.play
        star.free
        STARS.delete star
        false
      end
    end
  end
end

class Star
  attr_reader :body
  attr_accessor :hp

  def initialize
    @color = Engine::color(rand(256 - 40) + 40, rand(256 - 40) + 40, rand(256 - 40) + 40)
    @body = Engine::Body.new(MATERIAL_STARS, self, rand * 640, rand * 480, 0, 0, 0, 0)
    @hp = rand(3 + 1)
  end

  def draw
    case hp
    when 3
      RES::Star.draw_frame(0, @body.x, @body.y, 1, 1, 0, Colors::WHITE)
    when 2
      RES::Star.draw_frame(anim_frame(10), @body.x, @body.y, 1, 1, 0, Colors::WHITE)
    when 1
      RES::Star.draw_frame(anim_frame(10), @body.x, @body.y, 1, 1, 0, @color)
    end
  end

  def free
    @body.free
  end
end

class NoTutorial
  def initialize
    Engine[Params::Width] = 640
    Engine[Params::Height] = 480
    Engine[Params::Damping] = 800

    Engine::init "resources"

    Engine::material(MATERIAL_WALLS, 1, 0.5, 0.5, :static, 0)
    @wall_body = Engine::Body.new(MATERIAL_WALLS)
    @wall_body.add_box(-100, 0, 0, 480)
    @wall_body.add_box(600, 0, 700, 480)
    @wall_body.add_line(0, 0, 600, 0)
    @wall_body.add_line(0, 480, 600, 480)

    Engine::material(MATERIAL_ASTEROID, 1, 0.5, 0.5, :dynamic, 1)
    @asteroid = Engine::Body.new(MATERIAL_ASTEROID)
    @poly = Polygon.new
    @poly.add_point 50, 0
    @poly.add_point 0, 50
    @poly.add_point -50, 0
    @poly.add_point 0, -50
    @asteroid.add_poly @poly
    @asteroid.write_coords(x: 200, y: 200, angle: 10)

    Engine::material(MATERIAL_PLAYER, 0.01, 0.5, 0.5, :dynamic, 25)
    Engine::material(MATERIAL_STARS, 1, 0.5, 0.5, :dynamic, 10)
    Engine::material_collisions(MATERIAL_PLAYER, MATERIAL_STARS, :processable)
    Engine::material_collisions(MATERIAL_PLAYER, MATERIAL_WALLS, :hit)
    Engine::material_collisions(MATERIAL_STARS, MATERIAL_WALLS, :hit)
    Engine::material_collisions(MATERIAL_ASTEROID, MATERIAL_WALLS, :hit)
    Engine::material_collisions(MATERIAL_ASTEROID, MATERIAL_PLAYER, :hit)

    @player = Player.new
    @player.warp(320, 240)

    @font = RES::Vera
    @font.config(color: Colors::YELLOW)
  end

  def update
    if Engine::Key[KEY_LEFT] == :down
      @player.turn_left
    elsif Engine::Key[KEY_RIGHT] == :down
      @player.turn_right
    else
      @player.stop_turn
    end
    if Engine::Key[KEY_UP] == :down
      @player.accelerate
    elsif Engine::Key[KEY_DOWN] == :down
      @player.reverse
    end
    @player.process

    if rand(100) < 4 and STARS.size < 25
      STARS.push(Star.new)
    end
  end

  def draw
    RES::Space.background
    @player.draw

    @poly.draw(@asteroid.x, @asteroid.y, @asteroid.angle)

    STARS.each { |star| star.draw }
    @font.draw_text("Score: #{@player.score}", 10, 10)
  end
end

app = NoTutorial.new
loop do
  app.update
  app.draw
  Engine::process
  break if Engine::Key[QUIT] != :up
end
