program Physics;

uses
  SysUtils,
  uEngine,
  Math,
  fgl,
  Resources;

const
  MATERIAL_PLAYER = 1;
  MATERIAL_STARS = 2;
  MATERIAL_WALLS = 3;
  MATERIAL_ASTEROID = 4;

  function AnimationFrame(Count: integer): integer;
  begin
    Result := (GetTickCount div 100) mod Count;
  end;

type
  { TStar }

  TStar = class
    body: TBody;
    x, y: double;
    color: TColor;
    hp: integer;
    constructor Create;
    procedure Draw;
    destructor Destroy; override;
  end;
  TStars = specialize TFPGList<TStar>;
var
  Stars: TStars;


type
  { TPlayer }

  TPlayer = class
    body: TBody;
    x, y: double;
    vx, vy: double;
    angle, omega: double;
    score: integer;
    constructor Create;
    procedure Warp(ax, ay: integer);
    procedure TurnLeft;
    procedure TurnRight;
    procedure StopTurn;
    procedure Accelerate;
    procedure Reverse;
    procedure Draw;
    procedure GoTarget(tx, ty: TCoord);
    procedure ResetTarget;
    procedure Process;
    destructor Destroy; override;
  end;


  { TPlayer }

  constructor TPlayer.Create;
  begin
    body := BodyCreate(MATERIAL_PLAYER, self);
  end;

  procedure TPlayer.Warp(ax, ay: integer);
  begin
    x := ax;
    y := ay;
    BodyCoords(body, Write, @x, @y, nil, nil,nil,nil);
  end;

  procedure TPlayer.TurnLeft;
  begin
    BodyApplyForce(body, 0, 0, 0, 0, -200000);
  end;

  procedure TPlayer.TurnRight;
  begin
    BodyApplyForce(body, 0, 0, 0, 0, 200000);
  end;

  procedure TPlayer.StopTurn;
  begin
    if abs(omega) > 1e-6 then
      BodyApplyForce(body, 0, 0, 0, 0, -200000 * EnsureRange(omega, -1, 1));
  end;

  procedure TPlayer.Accelerate;
  begin
    BodyApplyForce(body, 20000, 0, 0, 0, 0);
  end;

  procedure TPlayer.Reverse;
  begin
    BodyApplyForce(body, -20000, 0, 0, 0, 0);
  end;

  procedure TPlayer.Draw;
  begin
    Sprite(RES.Starfighter, Trunc(x), Trunc(y), 1, 1, - 90 + angle);
  end;

  procedure TPlayer.GoTarget(tx, ty: TCoord);
  begin
    BodyApplyControl(body, tx, ty, 1000, 10000);
  end;

  procedure TPlayer.ResetTarget;
  begin
    BodyApplyControl(body, 0, 0, 0, 0);
  end;

  procedure TPlayer.Process;
  var
    st: TStar;
    First: boolean;
    b1, b2: Pointer;
  begin
    //if x < 0 then x := x+640;
    //if x > 640 then x := x-640;
    //if y < 0 then y := y+480;
    //if y > 480 then y := y-480;
    BodyCoords(body, Read, @x, @y, @vx, @vy, @angle, @omega);

    repeat
      if not GetMaterialCollisions(MATERIAL_STARS, MATERIAL_PLAYER, @b1, @b2, @First, nil, nil, nil, nil, nil, nil, nil) then break;
      st := TStar(b1);
      //st := TStar(GetCollisions(body, MATERIAL_STARS, @First, nil, nil,
      //  nil, nil, nil, nil, nil));
      if not assigned(st) then
        break;
      if not First then
        continue;
      if st.hp > 1 then
      begin
        Dec(st.hp);
        SetCurrentCollisionResult(True);
      end
      else
      begin
        Inc(score, 10);
        SetCurrentCollisionResult(False);
        Play(RES.Beep);
        st.Free;
        Stars.Remove(st);
      end;
    until False;
  end;

  destructor TPlayer.Destroy;
  begin
    BodyFree(body);
    inherited Destroy;
  end;

  { TStar }

  constructor TStar.Create;
  begin
    color := uEngine.color(random(256 - 40) + 40, random(256 - 40) +
      40, random(256 - 40) + 40, 255);
    x := random * 640;
    y := random * 480;
    hp := random(3)+1;
    body := BodyCreate(MATERIAL_STARS, self);
    BodyCoords(body, Write, @x, @y, nil, nil, nil, nil);
  end;

  procedure TStar.Draw;
  begin
    BodyCoords(body, Read, @x, @y, nil, nil, nil, nil);
    case hp of
      3: DrawTiled(RES.Star, 0, x, y, 1, 1, 0, WHITE);
      2: DrawTiled(RES.Star, AnimationFrame(10), x, y, 1, 1, 0, WHITE);
      1: DrawTiled(RES.Star, AnimationFrame(10), x, y, 1, 1, 0, color);
    end;
  end;

  destructor TStar.Destroy;
  begin
    BodyFree(body);
    inherited Destroy;
  end;

const
  XSPEEDS: array[0..3] of integer = (500, 1000, 2000, 8000);

var
  player: TPlayer;
  star: TStar;
  wall_body: TBody;
  asteroid: TBody;
  paused: boolean;
  xspeed: Integer;
  p: TPolygon;
  ax, ay, angle: TPhysicsCoord;
begin
  xspeed := 1;
  paused := False;
  EngineSet(Antialias, 4);
  EngineSet(Width, 600);
  EngineSet(Height, 480);
  EngineSet(Damping, 800);
  EngineSet(GravityX, 0);
  EngineInit('./resources');
  Material(MATERIAL_WALLS, 1, 0.5, 0.5, bodyStatic, 0);
  wall_body := BodyCreate(MATERIAL_WALLS, nil);
  BodyAddShapeBox(wall_body, -100,0,0,480);
  BodyAddShapeBox(wall_body, 600,0,700,480);
  BodyAddShapeLine(wall_body, 0,0,600,0);
  BodyAddShapeLine(wall_body, 0,480,600,480);

  Material(MATERIAL_ASTEROID, 1, 0.5, 0.5, bodyDynamic, 1);
  asteroid := BodyCreate(MATERIAL_ASTEROID, nil);
  p := PolygonCreate(7);
  PolygonAddPoint(p, 50,0);
  PolygonAddPoint(p, 0,50);
  PolygonAddPoint(p, -50,0);
  PolygonAddPoint(p, 0,-50);
  BodyAddShapePoly(asteroid, p);
  ax := 200;
  ay := 200;
  angle := 10;
  BodyCoords(asteroid, Write, @ax, @ay, nil, nil, @angle, nil);

  Material(MATERIAL_PLAYER, 0.01, 0.5, 0.5, bodyDynamic, 25);
  Material(MATERIAL_STARS, 1, 0.5, 0.5, bodyDynamic, 10);
  MaterialCollisions(MATERIAL_PLAYER, MATERIAL_STARS, Processable);
  MaterialCollisions(MATERIAL_PLAYER, MATERIAL_WALLS, Hit);
  MaterialCollisions(MATERIAL_STARS, MATERIAL_WALLS, Hit);
  MaterialCollisions(MATERIAL_ASTEROID, MATERIAL_WALLS, Hit);
  MaterialCollisions(MATERIAL_ASTEROID, MATERIAL_PLAYER, Hit);

  player := TPlayer.Create;
  player.Warp(320, 200);
  FontConfig(RES.Vera, 24, YELLOW);
  Stars := TStars.Create;
  repeat
    Background(RES.Space);



    BodyCoords(asteroid, Read, @ax, @ay, nil, nil, @angle, nil);
    DrawText(RES.Vera, PChar(Format('%f, %f, %f', [ax, ay, angle])), 10, 40);
    PolygonDraw(p, ax, ay, angle, WHITE);



    player.Draw;
    for Star in Stars do
      star.Draw;
    DrawText(RES.Vera, PChar('Score: ' + IntToStr(Player.score)), 10, 10);
    //DrawText(RES.Vera, PChar('omega: ' + FloatToStr(Player.omega)), 200, 10);


    if not paused then
    begin
      if KeyState(KeyLeft) <> ksUp then
        player.TurnLeft
      else if KeyState(KeyRight) <> ksUp then
        player.TurnRight
      else
        player.StopTurn;

      if KeyState(KeyUp) <> ksUp then
        player.Accelerate;
      if KeyState(KeyDown) <> ksUp then
        player.Reverse;

      if MouseState(LeftButton) = mbsDown then
        player.GoTarget(MouseGet(CursorX), MouseGet(CursorY))
      else
        player.ResetTarget;
    end;


    if KeyState(KeyR) = ksPressed then
    begin
      for star in Stars do
        star.Free;
      Stars.Clear;
      player.Free;
      PhysicsReset;
      player := TPlayer.Create;
      player.Warp(320, 200);
    end;

    player.Process;
    //@player.collect_stars(@stars)

    if (random(100) < 4) and (stars.Count < 25) then
      Stars.Add(TStar.Create);

    if KeyState(KeyPause) = ksPressed then
    begin
      paused := not paused;
      if paused then
        EngineSet(PhysicsSpeed, 0)
      else
        EngineSet(PhysicsSpeed, XSPEEDS[xspeed])
    end;

    if (KeyState(KeyAdd) = ksPressed) and (xspeed < High(XSPEEDS)) then
    begin
      Inc(xspeed);
      if not paused then
        EngineSet(PhysicsSpeed, XSPEEDS[xspeed])
    end;
    if (KeyState(KeySubtract) = ksPressed) and (xspeed > Low(XSPEEDS)) then
    begin
      Dec(xspeed);
      if not paused then
        EngineSet(PhysicsSpeed, XSPEEDS[xspeed])
    end;
    DrawText(RES.Vera, PChar('Speed: x' + FloatToStr(XSPEEDS[xspeed]/1000)), 10, 30);
    if paused then
      DrawTextBoxed(RES.Vera, 'Paused', 0, 0, 640, 480, haCenter, vaCenter);

    EngineProcess;
  until KeyState(KeyEscape) <> ksUp;
  for star in Stars do
    star.Free;
  Stars.Free;
  player.Free;
end.
