require_relative "../ruby/nonoengine"
require_relative "./resources"

def distance(x1, y1, x2, y2)
  Math::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
end

def anim_frame(count)
  (Time.now.to_f * 10).to_i % count
end

def offset_x(angle, radius)
  -Math::sin(angle / 180 * Math::PI) * radius
end

def offset_y(angle, radius)
  -Math::cos(angle / 180 * Math::PI) * radius
end

class Player
  attr_reader :score

  def initialize
    @x = @y = @vel_x = @vel_y = @angle = 0.0
    @score = 0
  end

  def warp(x, y)
    @x, @y = x, y
  end

  def turn_left
    @angle += 4.5
  end

  def turn_right
    @angle -= 4.5
  end

  def accelerate
    @vel_x += offset_x(@angle, 0.5)
    @vel_y += offset_y(@angle, 0.5)
  end

  def move
    @x += @vel_x
    @y += @vel_y
    @x %= 640
    @y %= 480

    @vel_x *= 0.95
    @vel_y *= 0.95
  end

  def draw
    RES::Starfighter.draw(@x, @y, 1, 1, @angle)
  end

  def collect_stars(stars)
    stars.reject! do |star|
      if distance(@x, @y, star.x, star.y) < 35
        @score += 10
        RES::Beep.play
        true
      else
        false
      end
    end
  end
end

class Star
  attr_reader :x, :y

  def initialize
    @color = Engine::color(rand(256 - 40) + 40, rand(256 - 40) + 40, rand(256 - 40) + 40)
    @x = rand * 640
    @y = rand * 480
  end

  def draw
    RES::Star.draw_frame(anim_frame(10), @x, @y, 1, 1, 0, @color)
  end
end

class NoTutorial
  def initialize
    Engine[Params::Width] = 640
    Engine[Params::Height] = 480

    Engine::init "resources"

    @player = Player.new
    @player.warp(320, 240)

    @stars = Array.new

    @font = RES::Vera
    @font.config(color: Colors::YELLOW)
  end

  def update
    if Engine::Key[KEY_LEFT] == :down
      @player.turn_left
    end
    if Engine::Key[KEY_RIGHT] == :down
      @player.turn_right
    end
    if Engine::Key[KEY_UP] == :down
      @player.accelerate
    end
    @player.move
    @player.collect_stars(@stars)

    if rand(100) < 4 and @stars.size < 25
      @stars.push(Star.new)
    end
  end

  def draw
    RES::Space.background
    @player.draw
    @stars.each { |star| star.draw }
    @font.draw_text("Score: #{@player.score}", 10, 10)
  end
end

app = NoTutorial.new
loop do
  app.update
  app.draw
  Engine::process
  break if Engine::Key[QUIT] != :up
end
