require_relative "../ruby/nonoengine"
include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)

module RES
  Beep = Sound.new(0)
  Space = Sprite.new(0)
  Starfighter = Sprite.new(1)
  Star = TileMap.new(0)
  Vera = Font.new(0)
end
