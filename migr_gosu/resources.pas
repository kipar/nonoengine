unit Resources;

interface
{$J-}
type
TRawResource = (TRawResource_NOT_USED);
TSprite = (THE_SCREEN = -1, res_Space, res_Starfighter);
TSound = (NO_MUSIC = -1, res_Beep);
TButton = (TButton_NOT_USED);
TTileMap = (res_Star);
TFont = (res_Vera);
TShader = (ALL_SHADERS = -1, DEFAULT_SHADER = 0, TShader_NOT_USED);

TShaderAttribute = (attr_color, attr_pos, attr_texpos);
TShaderUniform = (uni_screen_size, uni_tex);


TRES = record
  Beep: TSound;
  Space: TSprite;
  Starfighter: TSprite;
  Star: TTileMap;
  Vera: TFont;
end;

const RES: TRES = (
  Beep: res_Beep;
  Space: res_Space;
  Starfighter: res_Starfighter;
  Star: res_Star;
  Vera: res_Vera;
);

implementation
end.
