﻿require "fiddle"

module Engine
  Libname = "nonoengine.dll"
  Lib = Fiddle.dlopen(Libname)
  Ptr = Fiddle::Pointer

  # Raw API entries
  module Raw
    BOOL = Fiddle::TYPE_CHAR
    EngineInit = Fiddle::Function.new(Lib["EngineInit"], [Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)
    EngineSet = Fiddle::Function.new(Lib["EngineSet"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    EngineGet = Fiddle::Function.new(Lib["EngineGet"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    EngineProcess = Fiddle::Function.new(Lib["EngineProcess"], [], Fiddle::TYPE_VOID)
    EngineFree = Fiddle::Function.new(Lib["EngineFree"], [], Fiddle::TYPE_VOID)
    RawResource = Fiddle::Function.new(Lib["RawResource"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOIDP)
    RawTexture = Fiddle::Function.new(Lib["RawTexture"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOIDP)
    EngineLog = Fiddle::Function.new(Lib["EngineLog"], [Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)
    KeyState = Fiddle::Function.new(Lib["KeyState"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    MouseGet = Fiddle::Function.new(Lib["MouseGet"], [Fiddle::TYPE_INT], Fiddle::TYPE_FLOAT)
    MouseState = Fiddle::Function.new(Lib["MouseState"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    Sprite = Fiddle::Function.new(Lib["Sprite"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    DrawTiled = Fiddle::Function.new(Lib["DrawTiled"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Background = Fiddle::Function.new(Lib["Background"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Line = Fiddle::Function.new(Lib["Line"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    LineSettings = Fiddle::Function.new(Lib["LineSettings"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    Ellipse = Fiddle::Function.new(Lib["Ellipse"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, BOOL, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    Rect = Fiddle::Function.new(Lib["Rect"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, BOOL, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    Point = Fiddle::Function.new(Lib["Point"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Triangle = Fiddle::Function.new(Lib["Triangle"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    TexturedTriangle = Fiddle::Function.new(Lib["TexturedTriangle"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    SetLayer = Fiddle::Function.new(Lib["SetLayer"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Camera = Fiddle::Function.new(Lib["Camera"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    FontCreate = Fiddle::Function.new(Lib["FontCreate"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_INT)
    FontConfig = Fiddle::Function.new(Lib["FontConfig"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    DrawText = Fiddle::Function.new(Lib["DrawText"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    DrawTextBoxed = Fiddle::Function.new(Lib["DrawTextBoxed"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Panel = Fiddle::Function.new(Lib["Panel"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    Button = Fiddle::Function.new(Lib["Button"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_INT)
    GetGUICoord = Fiddle::Function.new(Lib["GetGUICoord"], [Fiddle::TYPE_INT], Fiddle::TYPE_FLOAT)
    Play = Fiddle::Function.new(Lib["Play"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)
    Music = Fiddle::Function.new(Lib["Music"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    SoundPlaying = Fiddle::Function.new(Lib["SoundPlaying"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], BOOL)
    Pathfind = Fiddle::Function.new(Lib["Pathfind"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP], BOOL)

    PhysicsReset = Fiddle::Function.new(Lib["PhysicsReset"], [], Fiddle::TYPE_VOID)
    BodyCreate = Fiddle::Function.new(Lib["BodyCreate"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_INT)
    BodyFree = Fiddle::Function.new(Lib["BodyFree"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    BodyAddShapeCircle = Fiddle::Function.new(Lib["BodyAddShapeCircle"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    BodyAddShapeBox = Fiddle::Function.new(Lib["BodyAddShapeBox"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    BodyAddShapeLine = Fiddle::Function.new(Lib["BodyAddShapeLine"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    BodyApplyForce = Fiddle::Function.new(Lib["BodyApplyForce"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    BodyApplyControl = Fiddle::Function.new(Lib["BodyApplyControl"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    BodyCoords = Fiddle::Function.new(Lib["BodyCoords"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)
    Material = Fiddle::Function.new(Lib["Material"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    MaterialCollisions = Fiddle::Function.new(Lib["MaterialCollisions"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    GetCollisions = Fiddle::Function.new(Lib["GetCollisions"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOIDP)
    GetMaterialCollisions = Fiddle::Function.new(Lib["GetMaterialCollisions"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP], BOOL)
    SetCurrentCollisionResult = Fiddle::Function.new(Lib["SetCurrentCollisionResult"], [BOOL], Fiddle::TYPE_VOID)
    DebugPhysicsRender = Fiddle::Function.new(Lib["DebugPhysicsRender"], [], Fiddle::TYPE_VOID)
    RaycastLine - Fiddle::Function.new(Lib["DebugPhysicsRender"], [Fiddle::TYPE_DOUBLE,Fiddle::TYPE_DOUBLE,Fiddle::TYPE_DOUBLE,Fiddle::TYPE_DOUBLE,Fiddle::TYPE_INT,BOOL,Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP, Fiddle::TYPE_VOIDP], BOOL)

    PolygonCreate = Fiddle::Function.new(Lib["PolygonCreate"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    PolygonFree = Fiddle::Function.new(Lib["PolygonFree"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    PolygonAddPoint = Fiddle::Function.new(Lib["PolygonAddPoint"], [Fiddle::TYPE_INT, Fiddle::TYPE_DOUBLE, Fiddle::TYPE_DOUBLE], Fiddle::TYPE_VOID)
    PolygonDraw = Fiddle::Function.new(Lib["PolygonDraw"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    BodyAddShapePoly = Fiddle::Function.new(Lib["BodyAddShapePoly"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)

    GetPixel = Fiddle::Function.new(Lib["GetPixel"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    RenderTo = Fiddle::Function.new(Lib["RenderTo"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    TextureCreate = Fiddle::Function.new(Lib["TextureCreate"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_INT)
    TextureClone = Fiddle::Function.new(Lib["TextureClone"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    TextureDelete = Fiddle::Function.new(Lib["TextureDelete"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)

    TextureSave = Fiddle::Function.new(Lib["TextureSave"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)
    CaptureScreen = Fiddle::Function.new(Lib["CaptureScreen"], [Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    TextureGetPixels = Fiddle::Function.new(Lib["TextureGetPixels"], [Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_FLOAT, Fiddle::TYPE_INT], Fiddle::TYPE_VOIDP)
    TextureLoadPixels = Fiddle::Function.new(Lib["TextureLoadPixels"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)

    ShaderActivate = Fiddle::Function.new(Lib["ShaderActivate"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    ShaderHandle = Fiddle::Function.new(Lib["ShaderHandle"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    UniformSetInt = Fiddle::Function.new(Lib["UniformSetInt"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    UniformSetFloat = Fiddle::Function.new(Lib["UniformSetFloat"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_FLOAT], Fiddle::TYPE_VOID)
    UniformSetTexture = Fiddle::Function.new(Lib["UniformSetTexture"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    UniformSetPtr = Fiddle::Function.new(Lib["UniformSetPtr"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP], Fiddle::TYPE_VOID)

    VertexListCreate = Fiddle::Function.new(Lib["VertexListCreate"], [Fiddle::TYPE_VOIDP, Fiddle::TYPE_INT, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    VertexListAddField = Fiddle::Function.new(Lib["VertexListAddField"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    VertexListAddPadding = Fiddle::Function.new(Lib["VertexListAddField"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    VertexListDraw = Fiddle::Function.new(Lib["VertexListDraw"], [Fiddle::TYPE_INT, Fiddle::TYPE_INT, BOOL], Fiddle::TYPE_VOID)
    VertexListCopy = Fiddle::Function.new(Lib["VertexListCopy"], [Fiddle::TYPE_INT], Fiddle::TYPE_INT)
    VertexListDelete = Fiddle::Function.new(Lib["VertexListDelete"], [Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
    VertexListChange = Fiddle::Function.new(Lib["VertexListChange"], [Fiddle::TYPE_INT, Fiddle::TYPE_VOIDP, Fiddle::TYPE_INT, Fiddle::TYPE_INT], Fiddle::TYPE_VOID)
  end

  # Wrappers

  # Общие функции движка
  def init(path)
    Raw::EngineInit.call(Ptr[path])
  end

  module Params
    Fullscreen = 0
    Width = 1
    Height = 2
    VSync = 3
    Antialias = 4
    UseLog = 5
    Autoscale = 6
    Volume = 7
    ClearColor = 8
    PhysicsSpeed = 9
    GravityX = 10
    GravityY = 11
    Damping = 12
    ProgressWidth = 13
    ProgressHeight = 14
    ProgressX = 15
    ProgressY = 16
    RealWidth = 100
    RealHeight = 101
    FPS = 102
    DeltaTime = 103
  end

  def []=(param, value)
    Raw::EngineSet.call(param, value)
  end

  def [](param)
    return Raw::EngineGet.call(param)
  end

  @global_tick = 0

  def global_tick
    @global_tick
  end

  def process
    @global_tick += 1
    Raw::EngineProcess.call
  end

  def free
    Raw::EngineFree.call
  end

  # TODO
  #   function RawResource(res: TRawResource; out size: Integer): pointer;external nonoengine;
  #   function RawTexture(res: TSprite): Cardinal;external nonoengine;

  def log(s)
    Raw::EngineLog.call Ptr[s.to_s]
  end

  def color(r, g, b, a = 0xFF)
    if r > 127
      r = 255 ^ r
      g = 255 ^ g
      b = 255 ^ b
      a = 255 ^ a
      -((r << 24) | (g << 16) | (b << 8) | a) - 1
    else
      (r.to_i << 24) | (g.to_i << 16) | (b.to_i << 8) | a.to_i
    end
  end

  # Обработка ввода

  module Key
    def self.[](key)
      case Raw::KeyState.call(key)
      when 0 then :up
      when 1 then :down
      when 2 then :pressed
      end
    end
    extend self
  end

  module Mouse
    LEFT_BUTTON = 0
    RIGHT_BUTTON = 1
    MIDDLE_BUTTON = 2
    X = 3
    Y = 4
    SCROLL = 5

    def self.[](param)
      if param < X
        case Raw::MouseState.call(param)
        when 0 then :up
        when 1 then :down
        when 2 then :clicked
        end
      else
        Raw::MouseGet.call(param - X)
      end
    end
  end

  class RawResource
    attr_reader :id

    def initialize(id)
      @id = id
    end
  end

  # 2д-рендер - спрайты
  class Sprite
    attr_reader :id

    def initialize(id)
      @id = id
    end

    def draw(x, y, kx = 1, ky = 1, angle = 0, color = Colors::WHITE)
      Raw::Sprite.call(@id, x, y, kx, ky, angle, color)
    end

    def background(kx = 1, ky = 1, dx = 0, dy = 0, color = Colors::WHITE)
      Raw::Background.call(@id, kx, ky, dx, dy, color)
    end
  end

  class TileMap
    def initialize(id)
      @id = id
    end

    def draw_frame(frame, x, y, kx = 1, ky = 1, angle = 0, color = Colors::WHITE)
      Raw::DrawTiled.call(@id, frame, x, y, kx, ky, angle, color)
    end
  end

  # 2д-рендер - примитивы

  def line(x1, y1, x2, y2, color1, color2 = color1)
    Raw::Line.call(x1, y1, x2, y2, color1, color2)
  end

  def line_settings(width, stipple, stipple_scale)
    Raw::LineSettings.call(width, stipple, stipple_scale)
  end

  def ellipse(x, y, rx, ry, filled, color1, color2 = color1, angle = 0)
    Raw::Ellipse.call(x, y, rx, ry, filled ? 1 : 0, color1, color2, angle)
  end

  def circle(x, y, r, filled, color1, color2 = color1)
    ellipse(x, y, r, r, filled ? 1 : 0, color1, color2)
  end

  def rect(x0, y0, w, h, filled, color1, color2, color3, color4, angle = 0)
    Raw::Rect.call x0, y0, w, h, filled ? 1 : 0, color1, color2, color3, color4, angle
  end

  #TODO better params handling
  def rect2(x0, y0, w, h, filled, color, angle = 0)
    rect x0, y0, w, h, filled ? 1 : 0, color, color, color, color, angle
  end

  def point(x, y, color)
    Raw::Point.call(x, y, color)
  end

  def triangle(x1, y1, color1, x2, y2, color2, x3, y3, color3)
    Raw::Triangle.call x1, y1, color1, x2, y2, color2, x3, y3, color3
  end

  #TODO better params handling
  def triangle2(x1, y1, x2, y2, x3, y3, color)
    Raw::Triangle.call x1, y1, color, x2, y2, color, x3, y3, color
  end

  class Sprite
    def tex_triangle(x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
      Raw::TexturedTriangle.call(@id, x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
    end

    #TODO better params handling
    def tex_triangle2(x1, y1, x2, y2, x3, y3, dx = 0, dy = 0, kx = 1, ky = 1)
      Raw::TexturedTriangle.call(@id,
                                 x1, y1, dx, dy,
                                 x2, y2, dx + (x2 - x1) * kx, dy + (y2 - y1) * ky,
                                 x3, y3, dx + (x3 - x1) * kx, dy + (y3 - y1) * ky)
    end
  end

  # 2д-рендер - дополнительные функции
  def camera(dx, dy, kx = 1, ky = 1, angle = 0)
    Raw::Camera.call(dx, dy, kx, ky, angle)
  end

  class Sprite
    def render_to
      Raw::RenderTo.call(@id)
    end
  end

  def pixel_at(x, y, sprite = nil)
    id = sprite ? sprite.id : -1
    Raw::GetPixel.call(x, y, id)
  end

  def layer=(z)
    Raw::SetLayer.call(z)
  end

  # 2д-рендер - вывод текста

  module Align
    NONE = 0
    TOP = 1
    LEFT = 1
    CENTER = 2
    BOTTOM = 3
    RIGHT = 3
    FLOW = 4
  end

  module Colors
    def self.new(r, g, b, a = 0xFF)
      if r > 127
        r = 255 ^ r
        g = 255 ^ g
        b = 255 ^ b
        a = 255 ^ a
        -((r << 24) | (g << 16) | (b << 8) | a) - 1
      else
        (r.to_i << 24) | (g.to_i << 16) | (b.to_i << 8) | a.to_i
      end
    end

    BLACK = 0x000000FF
    MAROON = 0x800000FF
    GREEN = 0x008000FF
    OLIVE = 0x808000FF
    NAVY = 0x000080FF
    PURPLE = 0x800080FF
    TEAL = 0x008080FF
    GRAY = 0x808080FF
    SILVER = 0xC0C0C0FF
    RED = 0xFF0000FF
    LIME = 0x00FF00FF
    YELLOW = new(255, 255, 0)
    BLUE = 0x0000FFFF
    FUCHSIA = 0xFF00FFFF
    AQUA = 0x00FFFFFF
    WHITE = -1
    MONEYGREEN = 0xC0DCC0FF
    SKYBLUE = 0xA6CAF0FF
    CREAM = 0xFFFBF0FF
    MEDGRAY = 0xA0A0A4FF
  end

  class Font
    BOLD = 1
    ITALIC = 2
    UNDERLINED = 4
    attr_reader :id

    def initialize(id)
      @id = id
    end

    DEFAULT_CONFIG = { char_size: 24, color: Engine::Colors::WHITE, styles: 0, kx: 1, ky: 1 }

    def config(hash)
      hash = DEFAULT_CONFIG.merge(hash)
      Raw::FontConfig.call(@id, hash[:char_size], hash[:color], hash[:styles], hash[:kx], hash[:ky])
    end

    def draw_text(text, x, y)
      Raw::DrawText.call(@id, Ptr[text.to_s], x, y)
    end

    def draw_text_boxed(text, x, y, w, h, halign = Align::LEFT, valign = Align::CENTER)
      Raw::DrawTextBoxed.call(@id, Ptr[text.to_s], x, y, w, h, halign, valign)
    end
  end

  # ГУИ

  def panel(id, parent, x, y, w, h, halign = Align::NONE, valign = Align::NONE)
    Raw::Panel.call(id, parent, x, y, w, h, halign, valign)
  end

  class Button
    @@default_font = Font.new(0)
    #todo attr_accessor parent x y...
    def initialize(id)
      @id = id
    end

    def clone
      Button.new(@id)
    end

    def show(parent, x, y, w, h, halign = Align::NONE, valign = Align::NONE, text = nil, font = @@default_font)
      case Raw::Button.call(@id, parent, x, y, w, h, halign, valign, text ? Ptr[text.to_s] : nil, font.id, Ptr[self.object_id])
      when 0
        :normal
      when 1
        :hover
      when 2
        :pressed
      when 3
        :clicked
      end
    end
  end

  module GUI
    X = 0
    Y = 1
    WIDTH = 2
    HEIGHT = 3
    MOUSE_X = 4
    MOUSE_Y = 5

    def self.[](param)
      Raw::GetGUICoord.call(param)
    end
  end

  # Звук
  class Sound
    def initialize(id)
      @id = id
    end

    def clone
      Raw::Sound.new(@id)
    end

    def play(volume = 100)
      Raw::Play.call(@id, volume, Ptr[self.object_id])
    end

    def playing?
      Raw::SoundPlaying.call(@id, Ptr[self.object_id]) != 0
    end

    def music(volume = 100)
      Raw::Music.call(@id, volume)
    end
  end

=begin
  # TODO
  
  //Поиск пути
  function Pathfind(SizeX, SizeY: integer; algorithm: TPathfindAlgorithm; diagonal_cost: single;
    fromx, fromy, tox, toy: integer; out x: integer; out y: integer; callback: TPathfindCallback;
    opaque: pointer = nil): boolean; external nonoengine;
=end

  module Keys
    # Keyboard scancodes for events/states.
    KEY_A = 0
    KEY_B = 1
    KEY_C = 2
    KEY_D = 3
    KEY_E = 4
    KEY_F = 5
    KEY_G = 6
    KEY_H = 7
    KEY_I = 8
    KEY_J = 9
    KEY_K = 10
    KEY_L = 11
    KEY_M = 12
    KEY_N = 13
    KEY_O = 14
    KEY_P = 15
    KEY_Q = 16
    KEY_R = 17
    KEY_S = 18
    KEY_T = 19
    KEY_U = 20
    KEY_V = 21
    KEY_W = 22
    KEY_X = 23
    KEY_Y = 24
    KEY_Z = 25
    KEY_NUM0 = 26
    KEY_NUM1 = 27
    KEY_NUM2 = 28
    KEY_NUM3 = 29
    KEY_NUM4 = 30
    KEY_NUM5 = 31
    KEY_NUM6 = 32
    KEY_NUM7 = 33
    KEY_NUM8 = 34
    KEY_NUM9 = 35
    KEY_ESCAPE = 36
    KEY_ESC = KEY_ESCAPE
    KEY_LCONTROL = 37
    KEY_LSHIFT = 38
    KEY_LALT = 39
    KEY_LSYSTEM = 40
    KEY_RCONTROL = 41
    KEY_RSHIFT = 42
    KEY_RALT = 43
    KEY_RSYSTEM = 44
    KEY_MENU = 45
    KEY_LBRACKET = 46
    KEY_RBRACKET = 47
    KEY_SEMICOLON = 48
    KEY_COMMA = 49
    KEY_PERIOD = 50
    KEY_QUOTE = 51
    KEY_SLASH = 52
    KEY_BACKSLASH = 53
    KEY_TILDE = 54
    KEY_EQUAL = 55
    KEY_DASH = 56
    KEY_SPACE = 57
    KEY_RETURN = 58
    KEY_BACK = 59
    KEY_TAB = 60
    KEY_PAGEUP = 61
    KEY_PAGEDOWN = 62
    KEY_END = 63
    KEY_HOME = 64
    KEY_INSERT = 65
    KEY_DELETE = 66
    KEY_ADD = 67
    KEY_SUBTRACT = 68
    KEY_MULTIPLY = 69
    KEY_DIVIDE = 70
    KEY_LEFT = 71
    KEY_RIGHT = 72
    KEY_UP = 73
    KEY_DOWN = 74
    KEY_NUMPAD0 = 75
    KEY_NUMPAD1 = 75
    KEY_NUMPAD2 = 77
    KEY_NUMPAD3 = 78
    KEY_NUMPAD4 = 79
    KEY_NUMPAD5 = 80
    KEY_NUMPAD6 = 81
    KEY_NUMPAD7 = 82
    KEY_NUMPAD8 = 83
    KEY_NUMPAD9 = 84
    KEY_F1 = 85
    KEY_F2 = 86
    KEY_F3 = 87
    KEY_F4 = 88
    KEY_F5 = 89
    KEY_F6 = 90
    KEY_F7 = 91
    KEY_F8 = 92
    KEY_F9 = 93
    KEY_F10 = 94
    KEY_F11 = 95
    KEY_F12 = 96
    KEY_F13 = 97
    KEY_F14 = 98
    KEY_F15 = 99
    KEY_PAUSE = 100
    QUIT = -1
    ANY_KEY = -2
  end

  include Keys

  class Collision
    attr_reader :data, :x, :y, :nx, :ny, :energy, :impulsex, :impulsey, :first

    def initialize(data, x, y, nx, ny, energy, impulsex, impulsey, first)
      @data = data
      @x = x
      @y = y
      @nx = nx
      @ny = ny
      @energy = energy
      @impulsex = impulsex
      @impulsey = impulsey
      @first = first
    end

    def first?
      @first
    end
  end

  class Polygon
    attr_reader :id

    def initialize(arr = nil)
      @id = Raw::PolygonCreate.call(arr ? arr.count : 0)
      if arr
        arr.each do |p|
          add_point(p[0], p[1])
        end
      end
    end

    def free
      Raw::PolygonFree.call(@id)
    end

    def add_point(x, y)
      Raw::PolygonAddPoint.call(@id, x, y)
    end

    def draw(x, y, angle, color = Colors::WHITE)
      # procedure PolygonDraw(p: TPolygon; X, Y, Angle: TCoord; c: TColor; sprite: TSprite = TSprite(-1); dx: TCoord = 0;
      # dy: TCoord = 0; kx: TCoord = 1; ky: TCoord = 1); cdecl; external nonoengine;
      Raw::PolygonDraw.call(@id, x, y, angle, color, -1, 0, 0, 1, 1)
    end

    def draw_textured(x, y, angle, tex, color = Colors::WHITE, dx = 0, dy = 0, kx = 1, ky = 1)
      # procedure PolygonDraw(p: TPolygon; X, Y, Angle: TCoord; c: TColor; sprite: TSprite = TSprite(-1); dx: TCoord = 0;
      # dy: TCoord = 0; kx: TCoord = 1; ky: TCoord = 1); cdecl; external nonoengine;
      Raw::PolygonDraw.call(@id, x, y, angle, color, tex.id, dx, dy, kx, ky)
    end
  end

  class Body
    def initialize(material, owner = nil, x = 0, y = 0, vx = 0, vy = 0, angle = 0, omega = 0)
      @owner = owner
      @id = Raw::BodyCreate.call(material, Ptr.to_ptr(owner.object_id << 1))
      # @x, @y, @vx, @vy, @angle, @omega = x, y, vx, vy, angle, omega
      write_coords({ x: x, y: y, vx: vx, vy: vy, angle: angle, omega: omega })
    end

    def free
      Raw::BodyFree.call(@id)
    end

    def apply_force(fx, fy, dx, dy, torque)
      Raw::BodyApplyForce.call(@id, fx, fy, dx, dy, torque)
    end

    def apply_control(tx, ty, max_speed, max_force)
      Raw::BodyApplyControl.call(@id, tx, ty, max_speed, max_force)
    end

    def add_circle(dx, dy, r)
      Raw::BodyAddShapeCircle.call(@id, dx, dy, r)
    end

    def add_box(x1, y1, x2, y2)
      Raw::BodyAddShapeBox.call(@id, x1, y1, x2, y2)
    end

    def add_line(x1, y1, x2, y2)
      Raw::BodyAddShapeLine.call(@id, x1, y1, x2, y2)
    end

    def add_poly(poly)
      Raw::BodyAddShapePoly.call(@id, poly.id)
    end

    # Temporary buffer for by-pointer integer arguments.
    FSD = Fiddle::SIZEOF_DOUBLE
    Out = Fiddle::Pointer.malloc(FSD * 8)

    def collisions(material)
      loop do
        ptr = Raw::GetCollisions.call(@id, material,
                                      Out + 7 * FSD, Out + 0 * FSD, #to make boolean value a last
                                      Out + 1 * FSD, Out + 2 * FSD,
                                      Out + 3 * FSD, Out + 4 * FSD,
                                      Out + 5 * FSD, Out + 6 * FSD)
        break if ptr.null?
        x, y, nx, ny, energy, impulsex, impulsey, first = Out[0, 7 * FSD + 1].unpack("DDDDDDDC")
        coll = Collision.new(ptr.to_value, x, y, nx, ny, energy, impulsex, impulsey, first != 0)
        result = yield(coll)
        Raw::SetCurrentCollisionResult.call(result ? 1 : 0)
      end
    end

    def write_coords(hash)
      read_coords
      @x = hash[:x] if hash.has_key? :x
      @y = hash[:y] if hash.has_key? :y
      @vx = hash[:vx] if hash.has_key? :vx
      @vy = hash[:vy] if hash.has_key? :vy
      @angle = hash[:angle] if hash.has_key? :angle
      @omega = hash[:omega] if hash.has_key? :omega
      Out[0, 7 * FSD + 1] = [hash[:x] || 0, hash[:y] || 0, hash[:vx] || 0, hash[:vy] || 0, hash[:angle] || 0, hash[:omega] || 0].pack("DDDDDD")
      Raw::BodyCoords.call(@id, 1,
                           hash[:x] ? Out + 0 * FSD : nil, hash[:y] ? Out + 1 * FSD : nil,
                           hash[:vx] ? Out + 2 * FSD : nil, hash[:vy] ? Out + 3 * FSD : nil,
                           hash[:angle] ? Out + 4 * FSD : nil, hash[:omega] ? Out + 5 * FSD : nil)
    end

    @was_read = -1

    def read_coords
      return if Engine.global_tick == @was_read
      Raw::BodyCoords.call(@id, 0,
                           Out + 0 * FSD, Out + 1 * FSD,
                           Out + 2 * FSD, Out + 3 * FSD,
                           Out + 4 * FSD, Out + 5 * FSD)
      @x, @y, @vx, @vy, @angle, @omega = Out[0, 6 * FSD].unpack("DDDDDD")
      @was_read = Engine.global_tick
    end

    def self.physics_accessor(method_name)
      inst_variable_name = "@#{method_name}".to_sym
      define_method method_name do
        read_coords
        instance_variable_get inst_variable_name
      end
      define_method "#{method_name}=" do |new_value|
        # instance_variable_set inst_variable_name, new_value
        write_coords({ method_name => new_value })
      end
    end

    physics_accessor :x
    physics_accessor :y
    physics_accessor :vx
    physics_accessor :vy
    physics_accessor :angle
    physics_accessor :omega
  end

  SPECIAL_DECODES = { dynamic: 0, static: 1, kinematic: 2 }

  def material(mat, density, friction, elasticity, special_type, def_radius)
    Raw::Material.call(mat, density, friction, elasticity, SPECIAL_DECODES[special_type], def_radius)
  end

  COLLISION_DECODES = { pass: 0, hit: 1, pass_detect: 2, hit_detect: 3, processable: 4 }

  def material_collisions(mat1, mat2, typ)
    Raw::MaterialCollisions.call(mat1, mat2, COLLISION_DECODES[typ])
  end

  def reset_physics
    Raw::PhysicsReset.call()
  end

  # TODO - GetMaterialCollisions

  extend self
end
