-1. important: scaling of window
0. Maintenance
   1. C bindings?
   2. Reorganize folders
   3. Docs on how to install
   4. English docs?
1. Small features
   1. pack resources to bundle
   2. stipple
   3. texture array, atlas?
   4. pause all sounds
   5. Focus loss?
2. Particles
3. Complex GUI
4. Android
5. Physics: constraints