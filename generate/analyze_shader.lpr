program analyze_shader;
{$APPTYPE CONSOLE}

uses
  Windows,
  SysUtils,
  Messages,
  Classes,
  ushaderprocess,
  gl,glext;


var
  // http://stackoverflow.com/questions/10535950/forcing-nvidia-gpu-programmatically-in-optimus-laptops
  // http://developer.download.nvidia.com/devzone/devcenter/gamegraphics/files/OptimusRenderingPolicies.pdf
  NvOptimusEnablement: uint32 = 1; export name 'NvOptimusEnablement';

exports
  NvOptimusEnablement;

var
  dc: HDC;
  hrc: HGLRC;

  function LOpenGLCreateContext(DC: HDC): HWND;
  var
    pfd: PIXELFORMATDESCRIPTOR;
    PixelFormat: Integer;
  begin
    // get pixelformat
    FillChar(pfd,SizeOf(pfd),0);
    with pfd do begin
      nSize:=sizeOf(pfd);
      nVersion:=1;
      dwFlags:=PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL;
      dwFlags:=dwFlags or PFD_DOUBLEBUFFER;
      iPixelType:=PFD_TYPE_RGBA;
      cColorBits:=32; // color depth
      cRedBits:=8;
      cGreenBits:=8;
      cBlueBits:=8;
      cAlphaBits:=8;
      cDepthBits:=0; // Z-Buffer
      cStencilBits:=0;
      cAuxBuffers:=0;
      iLayerType:=PFD_MAIN_PLANE;
    end;
    PixelFormat:=ChoosePixelFormat(DC,@pfd);
    SetPixelFormat(DC,PixelFormat,@pfd);
    Result := wglCreateContext(DC);
  end;


procedure ProcessFile(filename: string);
var
  sh: TShader;
  i: integer;
  changes: boolean;
begin
  if ExtractFileExt(filename) <> '.shader' then exit;
  sh := TShader.Create;
  sh.Load(filename);
  sh.Compile;
  sh.Introspect(changes);
  //writeln('attributes (' + IntToStr(sh.attributes.Count) + '): ');
  //for i := 0 to sh.attributes.Count-1 do
  //  writeln('    '+sh.attributes[i].ToString);
  //writeln('uniforms (' + IntToStr(sh.uniforms.Count) + '): ');
  //for i := 0 to sh.uniforms.Count-1 do
  //  writeln('    '+sh.uniforms[i].ToString);
  if changes then
  begin
    writeln(filename, ': something changed');
    sh.Save(filename);
  end
  else
    writeln(filename, ': OK');
end;



procedure Process(s: string); forward;

procedure ProcessDir(s: string);
var
  ff: TSearchRec;
begin
  FindFirst(s + '\*', faAnyFile, ff);
  if ff.Name = '.' then
    FindNext(ff);
  if ff.Name = '..' then
    FindNext(ff);
  repeat
    Process(s + '\' + ff.Name);
  until FindNext(ff) <> 0;
  FindClose(ff);
end;

procedure Process(s: string);
begin
  if FileExists(s) then
    ProcessFile(s)
  else if DirectoryExists(s) then
    ProcessDir(s);
end;



begin

  DC := GetDC(CreateWindowEx(0, 'static', nil, Cardinal($90000000), 0, 0, 100, 100, 0, 0, 0, nil));
  hRC := LOpenGLCreateContext(DC);
  wglMakeCurrent(DC, hRC);
  if not Load_GL_VERSION_3_3() then
  begin
    writeln('Failed to initialize OpenGL');
    exit;
    //log('OpenGL version: ' + glGetString(GL_VERSION));
    //log('OpenGL renderer: ' + glGetString(GL_RENDERER));
    //log('OpenGL vendor: ' + glGetString(GL_VENDOR));
  end;

  try
    try
    Process(ParamStr(1));

  except
    on E: Exception do writeln(E.ToString);
  end;
  finally
    ExitProcess(0);
  end;


end.

