program generate;

uses
  SysUtils,
  Classes, ushaderprocess;

type
  TResourceType = (TRawResource, TSprite, TSound, TButton, TTileMap, TFont, TShader, TParticle, TEmitter, TDeflector, TParticleSpace);

const
  RES_NAMES: array[TResourceType] of string =
    ('TRawResource', 'TSprite', 'TSound', 'TButton', 'TTileMap', 'TFont', 'TShader', 'TParticle', 'TEmitter', 'TDeflector', 'TParticleSpace');
  GRAPH_EXTS: array[1..6] of string = ('bmp', 'dds', 'jpg', 'png', 'tga', 'psd');
  SOUND_EXTS: array[1..3] of string = ('wav', 'ogg', 'flac');
  FONT_EXTS: array[1..3] of string = ('ttf', 'otf', 'fnt');
  SHADER_EXT = 'shader';
  SHADER_PARTS_EXTS: array[1..6] of string = ('vs', 'fs', 'gs', 'tcs', 'tes', 'hs');

  PARTICLE_EXT = 'particle';
  EMITTER_EXT = 'emitter';
  DEFLECTOR_EXT = 'deflector';
  SPACE_EXT = 'space';


  PREFIX_TYPES: array[TResourceType] of string =
    ('', 'THE_SCREEN = -1, ', 'NO_MUSIC = -1, ', '', '', '','ALL_SHADERS = -1, DEFAULT_SHADER = 0, ', '', '', '','');

  UNIFORM_PREFIX = 'uni_';
  ATTRIBUTE_PREFIX = 'attr_';

  DEFAULT_SHADER_ATTRIBUTES: array[1..3] of string = (
    'color: vec4',
    'pos: vec3',
    'texpos: vec3'
  );
  DEFAULT_SHADER_UNIFORMS: array[1..2] of string = (
    'screen_size: vec2',
    'tex: sampler2D'
  );


type

  { TFile }

  TFile = class
    Name: string;
    UniqName: string;
    FullName: string;
    typ: TResourceType;
    ignored: boolean;
    constructor Create(s: string; afullname: string);
    constructor Create;
    procedure ProcessShaders(Attributes, Uniforms: TStringList);
  end;

  { TDirectory }

  TDirectory = class
    Dirs: TList;
    Items: TList;
    Name: string;
    constructor Create(s: string);
    procedure Dump(var ff: TextFile);
    procedure DumpValues(indent: string; var ff: TextFile);
    function DumpItems(typ: TResourceType): string;
    procedure ProcessShaders(Attributes, Uniforms: TStringList);
  end;

var
  UsedNames: TStringList;

  function FineName(s: string): string;
  var
    c: char;
    n: integer;
  begin
    Result := '';
    for c in s do
      if lowerCase(c) in ['0'..'9', 'a'..'z', '_'] then
        Result := Result + lowerCase(c);
    if (Result = '') or (Result[1] in ['0'..'9']) then
      Result := 'id_' + Result;
    Result[1] := upCase(Result[1]);
  end;

  function UniqueName(s: string): string;
  var
    c: char;
    n: integer;
  begin
    Result := s;
    if UsedNames.IndexOf(Result) >= 0 then
    begin
      n := 1;
      while UsedNames.IndexOf(Result + '_' + IntToStr(n)) >= 0 do
        Inc(n);
      Result := Result + '_' + IntToStr(n);
    end;
    UsedNames.Add(Result);
  end;

  procedure Process(s: string; root: TDirectory); forward;

  procedure ProcessFile(s: string; root: TDirectory);
  var
    afile: TFile;
  begin
    afile := TFile.Create(ExtractFileName(s), s);
    if not afile.ignored then
      root.Items.Add(afile);
  end;

  procedure ProcessDir(s: string; root: TDirectory);
  var
    ff: TSearchRec;
    adir: TDirectory;
    afile: TFile;
    res: integer;
  begin
    if RightStr(s, 7) = '_button' then
    begin
      afile := TFile.Create;
      Delete(s, Length(s) - 6, 7);
      afile.Name := FineName(ExtractFileName(s));
      afile.UniqName := UniqueName(afile.Name);
      afile.typ := TButton;
      root.Items.Add(afile);
      exit;
    end;

    if RightStr(s, 5) = '_font' then
    begin
      afile := TFile.Create;
      Delete(s, Length(s) - 4, 5);
      afile.Name := FineName(ExtractFileName(s));
      afile.UniqName := UniqueName(afile.Name);
      afile.typ := TFont;
      root.Items.Add(afile);
      exit;
    end;

    adir := TDirectory.Create(s);
    root.Dirs.Add(adir);

    res := FindFirst(s + '\*', faAnyFile, ff);
    if res = 0 then
    begin
      if ff.Name = '.' then
        res := FindNext(ff);
      if ff.Name = '..' then
        res := FindNext(ff);
      while res = 0 do
      begin
        Process(s + '\' + ff.Name, adir);
        res := FindNext(ff);
      end;
    end;
    FindClose(ff);
  end;

  procedure Process(s: string; root: TDirectory);
  begin
    if FileExists(s) then
      ProcessFile(s, root)
    else if DirectoryExists(s) then
      ProcessDir(s, root);
  end;

  { TFile }

  function MatchTiled(var s: string; out x,y: integer): boolean;
  var
    bases: string;
    ax, st: integer;
    aend: string;
    first, second: string;
  begin
    Result := False;
    bases := '';
    repeat
      st :=  pos('_tiled_', s);
      if st <= 0 then break;//do we need empty names?
      aend := RightStr(s, Length(s) - st - length('_tiled_') + 1);
      ax := Pos('x', aend);
      if (ax > 0) then
      begin
        first := Copy(aend, 1, ax-1);
        second := Copy(aend, ax+1, MAXINT);
        if TryStrToInt(first, x) and TryStrToInt(second, y) then
        begin
          Result := True;
          delete(s, st, MAXINT);
          break;
        end;
      end;
      bases := bases + LeftStr(s, st-1 + length('_tiled_') - 1);
      Delete(s, 1, st-1 + length('_tiled_') - 1);
    until false;
    s := bases + s;
  end;

  constructor TFile.Create(s: string; afullname: string);
  var
    ext, test: string;
    ax,ay: integer;
  begin
    FullName := afullname;
    Name := ChangeFileExt(s, '');
    ext := ExtractFileExt(s);
    if ext[1] = '.' then
      ext := Copy(ext, 2, MaxInt);
    ext := LowerCase(ext);
    typ := TRawResource;

    for test in SHADER_PARTS_EXTS do
      if ext = test then
      begin
        ignored := true;
        exit;
      end;

    for test in GRAPH_EXTS do
      if ext = test then
      begin
        typ := TSprite;
        if MatchTiled(Name, ax, ay) then
          typ := TTileMap;
        break;
      end;
    if typ = TRawResource then
      for test in SOUND_EXTS do
        if ext = test then
        begin
          typ := TSound;
          break;
        end;
    if typ = TRawResource then
      for test in FONT_EXTS do
        if ext = test then
        begin
          typ := TFont;
          break;
        end;
    if (typ = TRawResource) and (ext = SHADER_EXT) then
      typ := TShader;
    if (typ = TRawResource) and (ext = PARTICLE_EXT) then
      typ := TParticle;
    if (typ = TRawResource) and (ext = EMITTER_EXT) then
      typ := TEmitter;
    if (typ = TRawResource) and (ext = DEFLECTOR_EXT) then
      typ := TDeflector;
    if (typ = TRawResource) and (ext = SPACE_EXT) then
      typ := TParticleSpace;
    if typ = TRawResource then
      Name := Name +'_'+ ext;
    Name := FineName(Name);
    UniqName := UniqueName(Name);
  end;

  constructor TFile.Create;
  begin

  end;

  procedure TFile.ProcessShaders(Attributes, Uniforms: TStringList);
  var
    sh: ushaderprocess.TShader;
    s: string;
    it: TGLSLItem;
  begin
    sh := ushaderprocess.TShader.Create;
    sh.Load(FullName);
    for it in sh.attributes do
    begin
      s := it.name;
      if Attributes.IndexOf(s) < 0 then
        Attributes.Add(s);
    end;
    for it in sh.uniforms do
    begin
      s := it.name;
      if Uniforms.IndexOf(s) < 0 then
        Uniforms.Add(s);
    end;
    sh.Free;
  end;

  { TDirectory }

  constructor TDirectory.Create(s: string);
  begin
    Dirs := TList.Create;
    Items := TList.Create;
    Name := UniqueName(FineName(ExtractFileName(s)));
  end;

  procedure TDirectory.Dump(var ff: TextFile);
  var
    i: integer;
    adir: TDirectory;
    afile: TFile;
  begin
    for i := 0 to Dirs.Count - 1 do
      TDirectory(Dirs[i]).Dump(ff);
    writeln(ff);
    writeln(ff, 'T' + Name + ' = record');
    for i := 0 to Dirs.Count - 1 do
    begin
      adir := TDirectory(Dirs[i]);
      writeln(ff, '  ', adir.Name, ': T', adir.Name, ';');
    end;
    for i := 0 to Items.Count - 1 do
    begin
      afile := TFile(Items[i]);
      writeln(ff, '  ', afile.Name, ': ', afile.typ, ';');
    end;
    writeln(ff, 'end;');
  end;

  procedure TDirectory.DumpValues(indent: string; var ff: TextFile);
  var
    p: Pointer;
  begin
    if indent = '' then
      writeln(ff, 'const ', Name, ': T', Name, ' = (')
    else
      writeln(ff, indent, Name, ': (');
    for p in Dirs do
      TDirectory(p).DumpValues(indent + '  ', ff);
    for p in Items do
      writeln(ff, indent + '  ', TFile(p).Name, ': res_', TFile(p).UniqName, ';');
    writeln(ff, indent, ');');
  end;

  function TDirectory.DumpItems(typ: TResourceType): string;
  var
    p: Pointer;
    s: string;
  begin
    Result := '';
    for p in Dirs do
    begin
      s := TDirectory(p).DumpItems(typ);
      if s <> '' then
        Result := Result + ', ' + s;
    end;
    for p in Items do
      if TFile(p).typ = typ then
        Result := Result + ', res_' + TFile(p).UniqName;
    Delete(Result, 1, 2);
  end;

  procedure TDirectory.ProcessShaders(Attributes, Uniforms: TStringList);
  var
    p: Pointer;
  begin
    for p in Dirs do TDirectory(p).ProcessShaders(Attributes, Uniforms);
    for p in Items do
      if TFile(p).typ = TShader then
        TFile(p).ProcessShaders(Attributes, Uniforms);
  end;

function NiceDelimitedText(list: TStringList; prefix: string): string;
var
  i: integer;
begin
  if list.Count = 0 then
  begin
    Result := prefix+'NOT_USED';
    exit;
  end;
  Result := '';
  for i := 0 to list.Count-1 do
  begin
    Result := Result + prefix + list[i];
    if i < list.Count-1 then
      Result := Result + ', ';
  end;
end;

var
  base: TDirectory;
  afile: Text;
  atyp: TResourceType;
  s, s1: string;
  Attributes, Uniforms: TStringList;
  DefaultShader: ushaderprocess.TShader;
  i: integer;
begin
  Attributes := TStringList.Create;
  Uniforms := TStringList.Create;
  UsedNames := TStringList.Create;
  base := TDirectory.Create('RES');
  ProcessDir('.\resources', base);
  base := TDirectory(base.Dirs[0]);
  AssignFile(afile, './resources.pas');
  Rewrite(afile);
  writeln(afile, 'unit Resources;');
  writeln(afile);
  writeln(afile, 'interface');
  writeln(afile, '{$J-}');
  writeln(afile, 'type');
  for atyp in TResourceType do
  begin
    s := base.DumpItems(atyp);
    if s <> '' then
      writeln(afile, RES_NAMES[atyp], ' = (', PREFIX_TYPES[atyp], s, ');')
    else
      writeln(afile, RES_NAMES[atyp], ' = (', PREFIX_TYPES[atyp], RES_NAMES[atyp], '_NOT_USED);');
  end;
  writeln(afile);
  DefaultShader := ushaderprocess.TShader.Create;
  for s in DEFAULT_SHADER_ATTRIBUTES do
    DefaultShader.attributes.Add(TGLSLItem.FromString(s));
  for s in DEFAULT_SHADER_UNIFORMS do
    DefaultShader.uniforms.Add(TGLSLItem.FromString(s));
  DefaultShader.ProcessGeneration(Attributes, Uniforms);
  base.ProcessShaders(Attributes, Uniforms);
  writeln(afile, 'TShaderAttribute = (', NiceDelimitedText(Attributes, ATTRIBUTE_PREFIX), ');');
  writeln(afile, 'TShaderUniform = (', NiceDelimitedText(Uniforms, UNIFORM_PREFIX), ');');
  writeln(afile);

  with base do
  begin
    Name := 'RES';
    dump(afile);
  end;
  writeln(afile);
  base.DumpValues('', afile);
  writeln(afile);
  writeln(afile, 'implementation');
  writeln(afile, 'end.');
  CloseFile(afile);
end.
